.\" Norme de hi�rarchie du syst�me de fichiers 2.0	-*- nroff -*-
.ig

Time-stamp: <97/10/26 01:18:57 quinlan>

Copyright (C) 1994, 1995, 1996, 1997 Daniel Quinlan

Voir ci-dessous (sous "Partie l�gale") pour les conditions compl�tes de
copie.

Ce document est mis en page avec GNU groff 1.10 et les macros mm, et
pr�-trait� avec pic et tbl. Le site web de la FHS est situ� �
<URL:http://www.pathname.com/fhs/>.

Notes pour �crire ce document avec troff :

 * Utilisez toutes les cha�nes d�finies avec la commande ".ds".
 * Les noms de fichiers doivent �tre mis en page en police de taille
   constante, mais n'incluez pas la ponctuation, par exemple
   \f(CWfichier\fP.
 * Les noms de fichiers contenant des tirets doivent �tre pr�c�d�s de
   \%, par exemple \f(CW\%/pub/liste-fichiers\fP.
 * Utilisez la langue d�crite dans la section "Conformit�".

..
.\" -------------------------------------------------------------------
.\" Cha�nes pr�d�finies
.\" -------------------------------------------------------------------
.ds Date 1er f�vrier 1998
.ds Fs FHS
.ds Ux UNIX
.ie n .ds Tx TeX
.el .ds Tx T\h'-.2m'\v'+.3m'E\h'-.0m'\v'-.3m'X
.\" -------------------------------------------------------------------
.\" Mise en page du document
.\" -------------------------------------------------------------------
.nr Cl 2
.nr Hu 3
.nr Hy 0
.PGNH
.PH "'Norme de hi�rarchie du syst�me de fichiers''\*[Date]"
.SA 0
.ie t \{\
.PGFORM 6i 10.5i 1.25i
.ds HF B B B B B B B
.ds HP 14 12 12 10 10 10 10
\}
.el \{\
.PGFORM 7.2i 11i
\}
.\" -------------------------------------------------------------------
.\" Page de couverture
.\" -------------------------------------------------------------------
.COVER ms
.TL
Norme de hi�rarchie du syst�me de fichiers \(em Version 2.0
.AF "\fI�dit�e par Daniel Quinlan\fP"
.AU "\fRGroupe pour la norme de hi�rarchie du syst�me de fichiers\fP"
.AS 0 5
.nh
.P 1
Cette norme consiste en un ensemble d'exigences et de
suggestions concernant la disposition des fichiers et des r�pertoires
dans un syst�me d'exploitation de type \*(Ux. Les suggestions sont
faites pour faciliter l'interop�rabilit� des applications, des outils
d'administration syst�me, des outils de d�veloppement et des scripts,
ainsi qu'une documentation plus uniforme entre ces syst�mes.

.AE
.COVEND
.SK
.\" -------------------------------------------------------------------
.\" Partie l�gale
.\" -------------------------------------------------------------------
.nh
.nr % 1
.af P i
.PF "''- \\\\nP -''"
Toutes les marques d�pos�es et les copyrights appartiennent � leurs
propri�taires, sauf notification sp�cifique. L'utilisation d'un terme
dans ce document ne devrait pas �tre consid�r�e comme affectant la
validit� de toute marque d�pos�e ou marque de fabrique.

.BS
Copyright \(co 1994, 1995, 1996, 1997 Daniel Quinlan

Nous accordons la permission de faire et de distribuer des copies
exactes de cette norme � la condition que le copyright et cette note de
permission soient pr�serv�es sur toutes les copies.

.ig
Nous accordons la permission de faire traiter ce fichier par un outil de
mise en page (tel que troff) et d'imprimer le r�sultat, � la condition
que le document imprim� contienne une note de permission identique �
celle-ci mis � part l'omission de ce parapgraphe (ce paragraphe n'�tant
pas pertinent dans le document imprim�).

..
Nous accordons la permission de copier et distribuer des versions
modifi�es de cette norme sous les conditions de copie exacte, �
condition que la page de titre indique qu'elle a �t� modifi�e en
incluant une r�f�rence � la norme d'origine, � condition que soient
incluses les informations n�cessaires � la recherche de la norme
d'origine, et � condition que le travail d�riv� complet soit distribu�
sous les termes d'une note de permission identique � celle-ci.

Nous accordons la permission de copier et de distribuer des traductions
de cette norme dans une autre langue, avec les conditions ci-dessus pour
les versions modifi�es, � part le fait que cette note de permission soit
donn�e dans une traduction approuv�e par le tenant du copyright.

.BE
.SK
.\" -------------------------------------------------------------------
.\" Corps du document
.\" -------------------------------------------------------------------
.BS
.BE
.nr % 1
.af P 1
.nr Hu 4
.H 1 "Introduction"
.\" J'aimerais en finir avec ces sous-sections dans l'introduction :
.\" (en d�pla�ant certaines choses g�n�rales � cet endroit)
.\"
.\" D�claration d'int�r�t g�n�ral (ou est-ce le r�sum� ?)
.\"
.\" - Organisation
.\" - Documents de base, s'il y en a
.\" - Contexte (histoire)
.\" - Audience
.\" - But (objectifs)
.\"   - principes de base (possibilit� d'avoir /usr en lecture seule,
.\"     etc.) comprenant : impl�mentable � grande �chelle, changements
.\"     minimes par rapport aux impl�mentations historiques, changements
.\"     minimes par rapport aux impl�mentations existantes
.\" - Normes connexes

.H 2 "�tat de la norme"
.P
Voici la version 2.0 de la norme de hi�rarchie du syst�me de fichiers
(FHS 2.0).

Les commentaires sur cette norme sont les bienvenus de la part des
personnes int�ress�es. Les suggestions de changements devraient �tre
faites sous la forme d'une proposition de changement du texte,
accompagn�e des commentaires d'explication appropri�s.

Les suggestions de cette norme sont sujettes � modification. L'utilisation
des informations incluses dans ce document se fait � vos propres risques.

.ig
Cette norme propos�e est distribu�e pour l'instant � des fins de tests
et de commentaires.

Les commentaires sur cette norme sont les bienvenus de la part des
personnes int�ress�es. Les suggestions de changements devraient �tre de
la forme d'une proposition de changement du texte, accompagn�e des
commentaires d'explication appropri�s.

Les suggestions de cette norme sont sujets � modification. L'utilisation
des informations incluses dans ce document se fait � vos propres risques.
..

.H 2 "Organisation de la norme"
.P
Cette norme est divis�e entre ces sections :

.AL 1
.LI
Introduction
.LI
Le syst�me de fichiers : �tablissement de quelques principes cl�s.
.LI
Le r�pertoire racine.
.LI
La hi�rarchie \f(CW/usr\fP.
.LI
La hi�rarchie \f(CW/var\fP.
.LI
Annexe sp�cifique au syst�me d'exploitation.
.LE

.H 2 "Conventions"
.P
.ie t \{\
Une police de taille fixe est utilis�e pour l'affichage des noms de
fichiers et de r�pertoires.
\}
.el \{\
Nous recommandons que lisiez une version mise en pages de ce documents
plut�t que la version texte. Dans la version mise en pages, les noms de
fichiers et de r�pertoire sont affich�s dans une police � taille fixe.
\}

Les parties variables des noms de fichiers sont repr�sent�es par une
description de leur contenu � l'int�rieur des caract�res chevrons
"\f(CW<\fP" et "\f(CW>\fP", \f(CW<ainsi>\fP. Les adresses de courrier
�lectronique sont aussi entour�es de chevrons "<" et ">" mais sont
indiqu�es dans la police habituelle.

Les parties optionnelles des noms de fichiers sont entour�es des
caract�res crochet "\f(CW[\fP" et "\f(CW]\fP" et peuvent �tre combin�es
avec la convention "\f(CW<\fP" et "\f(CW>\fP". Par exemple, si on
pouvait trouver un fichier existant avec ou sans extension, on pourrait
le repr�senter par \f(CW<nom de fichier>[.<extension>]\fP.

Les parties de cha�nes variables des noms de r�pertoires et de fichiers
sont indiqu�es par une �toile : "\f(CW*\fP".

.SK
.H 2 "Historique de la \*(Fs"
.P
Le processus de d�veloppement d'une hi�rarchie de syst�me de fichiers
standard a d�but� en ao�t 1993 dans un effort de restructuration de la
structure de fichiers et de r�pertoires de Linux. La FSSTND, norme pour
une hi�rarchie du syst�me de fichiers sp�cifique au syst�me
d'exploitation Linux, est sortie le 14 f�vrier 1994. Des versions
successives sont sorties le 9 octobre 1994 et le 28 mars 1995.

Au d�but de 1995, avec l'aide de membres de la communaut� de
d�veloppement BSD, il a �t� d�cid� de d�velopper une version de FSSTND
plus compl�te pour englober non seulement Linux mais aussi les autres
syst�mes de type \*(Ux. En d�finitive, nous avons fait un effort
concert� pour nous concentrer sur des probl�mes g�n�raux aux syst�mes de
type \*(Ux. En reconnaissance de cette ouverture, le nom de la norme a
�t� modifi� pour devenir Norme de hi�rarchie du syst�me de fichiers ou
\*(Fs en abr�g�. (NDT : en anglais, FHS veut dire Filesystem Hierarchy
Standard.)

Les volontaires qui ont contribu� activement � cette norme se trouvent �
la fin de ce document. Cette norme repr�sente un consensus entre les
points de vue de ceux-ci et d'autres contributeurs.

.H 2 "�tendue"
.P
Ce document sp�cifie une hi�rarchie de syst�me de fichiers standard pour
les syst�mes de fichiers \*(Fs en sp�cifiant l'emplacement des fichiers
et r�pertoires, et le contenu de certains fichiers syst�me.

Cette norme a �t� faite pour �tre utilis�e par les int�grateurs de
syst�mes, les d�veloppeurs de paquetages et les administrateurs syst�me
dans la construction et la maintenance de syst�mes de fichiers
se conformant � \*(Fs. Elle est tout d'abord destin�e � servir de
r�f�rence et n'est pas un tutoriel sur la mani�re de g�rer une
hi�rarchie de syst�me de fichiers conforme.

La \*(Fs est bas�e sur des travaux pr�liminaires sur FSSTND, une norme
d'organisation du syst�me de fichiers pour le syst�me d'exploitation
Linux. Elle est bas�e sur la FSSTND pour pallier � des probl�mes
d'interop�rabilit� non seulement dans la communaut� Linux mais dans un
horizon plus vaste incluant les syst�mes d'exploitation bas�s sur
4.4BSD. Elle incorpore les le�ons concernant le support de
plusieurs architectures et les demandes en mati�re de r�seaux
h�t�rog�nes, le�ons apprises dans le monde BSD ou ailleurs.

Bien que cette norme soit plus compl�te que les tentatives pr�c�dentes
sur la normalisation de la hi�rarchie de syst�mes de fichiers, des mises
� jour p�riodiques peuvent s'av�rer n�cessaires � mesure que les
demandes changent par rapport � la technologie �mergeante. Il est aussi
possible que de meilleures solutions aux probl�mes �voqu�s ici soient
d�couvertes ou que nos solutions ne soient plus les meilleures
possibles. Des brouillons suppl�mentaires pourront �tre apport�s en plus
des mises � jour p�riodiques de ce document. Cependant, un des buts
suivis est la compatibilit� ascendante entre une version de ce document
et la suivante.

Les commentaires relatifs � cette norme sont les bienvenus. Tout
commentaire ou suggestion de changement devraient �tre adress�s �
l'�diteur de la \*(Fs (Daniel Quinlan <quinlan@pathname.com>), ou si
vous pr�f�rez, � la liste de distribution \*(Fs. Les commentaires de
nature typographique ou grammaticale doivent �tre adress�s directement �
l'�diteur de la \*(Fs.

Nous vous demandons de contacter en premier l'�diteur de la \*(Fs avant
d'envoyer un courrier � la liste de distribution afin d'�viter un
nouveau d�bat sur des sujets anciens. Les messages mal con�us ne seront
pas bien vus sur la liste de distribution.

Des questions concernant l'interpr�tation des objets de ce documents
peuvent se poser de temps en temps. Si vous avez besoin de pr�cisions,
veuillez contacter l'�diteur de la \*(Fs. Puisque cette norme repr�sente
le consensus de beaucoup de participants, il est important de s'assurer
que toute interpr�tation repr�sente aussi l'opinion collective. Pour
cette raison, il peut ne pas �tre possible de fournir une r�ponse
imm�diate sauf si la demande a d�j� fait l'objet d'une discussion.

.H 2 "Suggestions g�n�rales"
.P
Voici quelques unes des suggestions qui ont �t� utilis�es dans le
d�veloppement de cette norme :

.BL
.LI
R�soudre des probl�mes techniques en limitant les difficult�s li�es � la
transition.
.LI
Faire une sp�cification relativement stable.
.LI
Obtenir l'approbation des distributeurs, des d�veloppeurs, et autres
d�cideurs dans les groupes de d�veloppement ad�quats et encourager leur
participation.
.LI
Fournir une norme attractive pour les impl�menteurs des diff�rents
syst�mes de type \*(Ux.
.LE

.H 2 "Audience vis�e"
.P
L'audience vis�e par cette norme comprend, mais n'est pas limit�e aux
groupes de personnes suivants :

.BL
.LI
D�veloppeurs de syst�mes
.LI
Int�grateurs et distributeurs de syst�mes
.LI
D�veloppeurs d'applications
.LI
Auteurs de documentations
.LI
Administrateurs syst�me et autres personnes int�ress�es (� des fins
d'information)
.LE

.H 2 "Conformit� avec ce document"
.P
Cette section d�finit la signification des termes "conforme" et
"compatible" en ce qui concerne cette norme, et de conformit� et
compatibilit� "partielle".

Une "impl�mentation" fait ici r�f�rence � une distribution, un syst�me
install�, un programme, un paquetage (ou toute partie similaire d'un
logiciel ou de donn�es), ou tout composant de ceux-ci.

Une impl�mentation est totalement conforme � cette norme si chaque
exigence de cette norme est satisfaite. Chaque fichier ou r�pertoire
faisant partie de l'impl�mentation doit �tre situ� comme il est sp�cifi�
dans ce document. Si le contenu d'un fichier est d�crit ici, le contenu
v�ritable doit correspondre � sa description. L'impl�mentation doit
aussi tenter de trouver tout fichier ou r�pertoire (ext�rieur �
lui-m�me) au premier abord ou exclusivement � l'endroit sp�cifi� dans
cette norme.

.\" maladroit

Une impl�mentation est totalement compatible avec cette norme si chaque
fichier ou r�pertoire qu'elle contient peut �tre trouv� en regardant �
l'endroit sp�cifi� ici et sera trouv� avec un contenu identique � ce qui
est sp�cifi� ici, m�me si ce n'est pas l'emplacement de base ou physique
du fichier ou du r�pertoire en question. L'impl�mentation doit, quand
elle essaie de trouver un fichier ou un r�pertoire n'en faisant pas
partie, le faire � l'endroit sp�cifi� dans cette norme, bien qu'elle
puisse aussi tenter de le trouver � d'autres endroits (non standards).

Une impl�mentation est partiellement conforme ou compatible si elle est
conforme � ou compatible avec une partie significative de ce
document. La conformit� ou compatibilit� partielle n'est faite pour
s'appliquer qu'aux distributions et non � des programmes
s�par�s. L'expression "une partie significative" est effectivement
subjective, et dans les cas limites, la personne concern�e devrait
contacter l'�diteur de la \*(Fs. Nous avons anticip� le fait que des
variations soient tol�r�es dans les cas limites.

Afin de se d�finir comme partiellement conforme � la \*(Fs ou
partiellement compatible avec la \*(Fs, une impl�mentation doit fournir
une liste de tous les endroits auxquels elle et le document \*(Fs
diff�rent, en plus d'une explication br�ve de la raison de cette
diff�rence. Cette liste sera fournie avec l'impl�mentation en question,
et aussi mise � disposition de la liste de distribution \*(Fs ou de
l'�diteur de la \*(Fs.

Les termes "doit", "devrait", "contient", "est" et ainsi de suite
doivent �tre lus comme des exigences pour la conformit� ou la
compatibilit�.

Notez qu'une impl�mentation n'a pas besoin de contenir tous les fichiers
et r�pertoires sp�cifi�s dans cette norme pour �tre conforme ou
compatible. Il est simplement n�cessaire que les fichiers qu'elle
contient soient plac�s correctement. Par exemple, si le syst�me de
fichiers minix n'est pas support� par une distribution, les outils minix
n'ont pas besoin d'�tre inclus, m�me s'ils sont mentionn�s explicitement
dans la section sur \f(CW/sbin\fP.

De plus, certaines parties de ce document sont optionnelles. Dans ce
cas, ceci sera dit explicitement, ou indiqu� � l'aide d'un ou plusieurs
mots parmi "peut", "recommande" ou "sugg�re". Les objets indiqu�s comme
optionnels n'ont pas de port�e sur la conformit� ou la compatibilit�
d'une impl�mentation ; ce sont des suggestions faites pour encourager la
pratique courante, mais ils peuvent �tre situ�s n'importe o� au gr� de
l'impl�menteur.

.SK
.H 1 "Le syst�me de fichiers"
.P
Le syst�me de fichiers \*(Ux est caract�ris� par :

.BL
.LI
Une structure hi�rarchique
.LI
Le traitement uniforme des fichiers de donn�es
.LI
La protection des fichiers de donn�es
.LE
.P
Cette norme suppose que le syst�me d'exploitation sous-jacent au syst�me
de fichiers conforme � la \*(Fs supporte les m�mes possibilit�s de
s�curit� de base que l'on trouve dans la plupart des syst�mes de
fichiers \*(Ux. Notez que cette norme n'essaie pas d'�tre en accord au
mieux possible avec une impl�mentation particuli�re d'un syst�me
\*(Ux. Cependant, beaucoup d'aspects de cette norme sont bas�es sur des
id�es que l'on trouve dans \*(Ux et autres syst�mes de type \*(Ux.

Ceci apr�s une consid�ration attentive d'autres facteurs, comprenant :

.BL
.LI
Des pratiques courantes et saines dans les syst�mes de type \*(Ux.
.LI
L'impl�mentation d'autres structures de syst�mes de fichiers
.LI
Des normes applicables
.LE
.P
Il est possible de d�finir deux cat�gories orthogonales de
fichiers : partageables contre non partageables, et variables contre
statiques.
.\" cat�gories/cat�gorisations and fichiers/fichiers de donn�es

Les donn�es partageables sont ce qui peut �tre partag� entre plusieurs
machines diff�rentes ; non partageables est ce qui doit �tre sp�cifique � une
machine particuli�re. Par exemple, les r�pertoires personnels des
utilisateurs sont des donn�es partageables, mais pas les fichiers de
blocage de p�riph�riques (locks).

Les donn�es statiques comprennent les binaires, les biblioth�ques, la
documentation, et tout ce qui ne change pas sans l'intervention de
l'administrateur syst�me ; les donn�es variables sont tout le reste qui
change sans l'intervention de l'administrateur syst�me.

.\" Le texte donn� pr�c�demment se rapportait aux "principes" sans les
.\" sp�cifier. � la place, il d�crivait quatre cat�gories de telle sorte
.\" que �a *impliquait* un principe. Voici le principe.
Pour faciliter la sauvegarde, l'administration et le partage de fichiers
sur des r�seaux de syst�mes h�t�rog�nes, il est pr�f�rable d'�tablir une
correspondance simple et ais�ment compr�hensible entre les r�pertoires
(surtout les r�pertoires consid�r�s comment des points de montage
potentiels) et le type de donn�es qu'ils contiennent.

� travers ce document, et dans tout syst�me de fichiers bien organis�,
la compr�hension de ce principe de base aidera � diriger la structure
et lui apporter une coh�rence suppl�mentaire.

La distinction entre donn�es partageables et non partageables est
n�cessaire pour plusieurs raisons :

.BL
.LI
Dans un environnement en r�seau (par exemple, plus d'un h�te par site),
il y a une bonne partie des donn�es qui peuvent �tre partag�es entre les
diff�rentes machines pour sauver de la place et faciliter la t�che de
maintenance.
.LI
Dans un environnement en r�seau, certains fichiers contiennent des
informations sp�cifiques � une seule machine. Par cons�quent ces
syst�mes de fichiers ne peuvent �tre partag�s (sans prendre des mesures
sp�ciales).
.LI
Historiquement, certaines impl�mentations des syst�mes de fichiers de
type \*(Ux ont m�lang� des donn�es partageables et non partageables dans
la m�me hi�rarchie, rendant difficile le partage de grandes parties du
syst�me de fichiers.
.LE

La distinction "partageable" peut �tre utilis�e pour supporter, par
exemple :

.BL
.LI
Une partition \f(CW/usr\fP (ou des composants de \f(CW/usr\fP) mont�s
(en lecture seule) � travers le r�seau (en utilisant NFS).
.LI
Une partition \f(CW/usr\fP (ou des composants de \f(CW/usr\fP) mont�s
� partir d'un support en lecture seule. Un CD-ROM peut �tre consid�r�
comme un syst�me de fichiers en lecture seule partag� avec d'autres
syst�mes conformes � la \*(Fs, en utilisant le syst�me de courrier comme
un "r�seau".
.LE
.P

La distinction "statique" contre "variable" affecte le syst�me de
fichiers de deux mani�res principales :

.BL
.LI
Puisque \f(CW/\fP contient � la fois des donn�es statiques et variables,
il doit �tre mont� en lecture-�criture.
.LI
Puisque le traditionnel \f(CW/usr\fP contient � la fois des donn�es
variables et statiques, et puisque nous voudrions le monter en lecture
seule (voir ci-dessus), il est n�cessaire de fournir une m�thode pour
avoir \f(CW/usr\fP mont� en lecture seule. Ceci est obtenu par la
cr�ation d'une hi�rarchie \f(CW/var\fP qui est mont�e en
lecture-�criture (ou qui fait partie d'une autre partition en
lecture-ecriture, telle que \f(CW/)\fP, qui remplace bien des fonctions
traditionnelles de la partition \f(CW/usr\fP.
.LE

Voici un tableau pour r�sumer le tout. Puisque ce graphique contient des
exemples g�n�ralis�s, il peut ne pas s'appliquer � chaque impl�mentation
possible d'un syst�me conforme � la \*(Fs.

.TS
box,center;
l | l | l.
	partageable	non partageable
_
statique	/usr	/etc
	/opt	/boot
_
variable	/var/mail	/var/run
	/var/spool/news	/var/lock
.TE
.SK
.H 1 "Le r�pertoire racine"
.P
Cette section d�crit la structure du r�pertoire racine (root). Le
contenu du syst�me de fichiers root doit �tre ad�quat pour d�marrer,
reconstituer, r�tablir et/ou r�parer le syst�me :

.BL
.LI
Pour d�marrer un syst�me, il doit y avoir suffisamment de choses sur la
partition racine pour monter d'autres syst�mes de fichiers. Ceci
comprend les utilitaires, la configuration, les informations du chargeur
de d�marrage, et d'autres donn�es de d�marrage
essentielles. \f(CW/usr\fP, \f(CW/opt\fP et \f(CW/var\fP sont faits pour
pouvoir �tre situ�s sur d'autres syst�mes de fichiers.
.LI
Pour permettre le r�tablissement et/ou la r�paration d'un syst�me, les
utilitaires n�cessaires au mainteneur exp�riment� pour diagnostiquer et
reconstruire un syst�me endommag� doivent �tre pr�sents sur le syst�me
de fichiers racine.
.LI
Pour reconstituer un syst�me, les utilitaires n�cessaires � la
reconstitution � partir des sauvegardes syst�me (sur disque, bande,
etc.) doivent �tre pr�sents sur le syst�me de fichiers racine.
.LE
.P
Le principal argument utilis� pour contrer ces consid�rations, qui
penchent pour mettre beaucoup de choses sur le syst�me de fichiers racine,
est le but de garder la racine aussi petite que possible dans les
limites du raisonnable. Pour plusieurs raisons, il est souhaitable de
limiter la taille du syst�me de fichiers racine :

.BL
.LI
Il est mont� de temps en temps � partir d'un moyen de stockage tr�s
petit.
.LI
Le syst�me de fichiers racine contient beaucoup de fichiers de
configuration sp�cifiques au syst�me. Les exemples possibles comprennent
un noyau sp�cifique au syst�me, un nom d'h�te diff�rent, etc. Ceci veut
dire que le syst�me de fichiers racine n'est pas toujours partageable
entre des syst�mes en r�seau. Limiter sa taille sur des syst�mes en r�seau
minimise l'espace perdu en fichiers non-partageables sur les
serveurs. Cela permet aussi d'avoir des stations de travail avec des
disques durs locaux plus petits.
.LI
Bien que vous puissiez avoir le syst�me de fichiers racine sur une grande
partition, et pouvez le remplir � votre aise, il y aura des gens avec
des partition plus petites. Si vous avez plus de fichiers install�s,
vous pourrez trouver des incompatibilit�s avec d'autres syst�mes qui
utilisent des syst�mes de fichiers racine sur des partitions plus
petites. Si vous �tes d�veloppeur vous pouvez changer votre hypoth�se en
un probl�me pour un grand nombre d'utilisateurs.
.LI
Les erreurs de disque qui corrompent les donn�es sur le syst�me de
fichiers racine sont un probl�me plus important que les erreurs sur tout
autre partition. Un syst�me de fichiers racine petit est moins sujet �
la corruption � la suite d'un plantage syst�me.
.LE
.P
Les logiciels ne doivent jamais cr�er ou demander des fichiers sp�ciaux
ou des sous-r�pertoires dans le r�pertoire racine. D'autres emplacements
dans la hi�rarchie \*(Fs fournissent plus de flexibilit� qu'il n'en faut
pour tout paquetage.

.HU "Raison d'�tre"
.br
.P
Il y a plusieurs raisons pour lesquelles l'introduction d'un nouveau
r�pertoire dans le syst�me de fichiers racine est interdit :
.BL
.LI
Ceci demande de la place sur une partition racine que l'administrateur
syst�me veut garder petite et simple pour des raisons de performance ou
de s�curit�.
.LI
Ceci contredit toute logique que l'administrateur syst�me a pu mettre en
place pour distribuer des hi�rarchies de fichiers standards sur des
volumes montables.
.LE

.PS
copy "draft.pic"
dir(/,le r�pertoire racine)
sub("bin","Binaires des commandes essentielles")
sub("boot","Fichiers statiques du chargeur de d�marrage")
sub("dev","Fichiers de p�riph�riques")
sub("etc","Configuration syst�me sp�cifique � la machine")
sub("home","R�pertoires personnels des utilisateurs")
sub("lib","Biblioth�ques partag�es essentielles et modules du noyau")
sub("mnt","Point de montage des partitions temporaires")
sub("opt","Paquetages d'applications logicielles suppl�mentaires")
sub("root","R�pertoire personnel de l'utilisateur root")
sub("sbin","Binaires syst�mes essentiels")
sub("tmp","Fichiers temporaires")
sub("usr","Hi�rarchie secondaire")
sub("var","Donn�es variables")
.PE

Chaque r�pertoire list� ci-dessus est sp�cifi� en d�tail dans des
sous-sections s�par�es ci-dessous. \f(CW/usr\fP et \f(CW/var\fP ont
chacun une section compl�te dans ce document � cause de la complexit� de
ces r�pertoires.

L'image du noyau du syst�me d'exploitation doit �tre situ� soit dans
\f(CW/\fP soit dans \f(CW/boot\fP. Les informations suppl�mentaires sur
l'emplacement du noyau se trouvent dans la section concernant
\f(CW/boot\fP ci-dessous.

.H 2 "/bin : binaires de commandes utilisateurs essentielles (pour tous les utilisateurs)"
.P
\f(CW/bin\fP contient des commandes qui peuvent �tre utilis�es � la fois par
l'administrateur syst�me et les utilisateurs, mais qui sont obligatoires
en mode utilisateur simple. Il peut aussi contenir des commandes
utilis�es indirectement par des scripts.

Il ne devrait pas y avoir de sous-r�pertoires � l'int�rieur de
\f(CW/bin\fP.

Les binaires des commandes qui ne sont pas suffisamment essentielles
pour rester dans \f(CW/bin\fP doivent �tre mises dans \f(CW/usr/bin\fP,
� la place. Les objets qui ne sont utilis�s que par des utilisateurs non
root (\f(CWmail\fP, \f(CWchsh\fP, etc.) ne sont pas assez essentiels
pour �tre plac�s dans la partition racine.


.HU "Fichiers obligatoires pour /bin :"
.BL
.LI
Commandes g�n�rales :
.sp
Les commandes suivantes ont �t� incluses parce qu'elles sont
essentielles. Certaines sont pr�sentes � cause de leur emplacement
traditionnel dans \f(CW/bin\fP.

.VL 2
.LI "\f(CW{"
cat, chgrp, chmod, chown, cp, date, dd, df, dmesg, echo, ed,
false, kill, ln, login, ls, mkdir, mknod, more, mount, mv, ps, pwd, rm,
rmdir, sed, setserial, sh, stty, su, sync, true, umount, uname }\fP
.LE
.P
Si \f(CW/bin/sh\fP est le Bash, alors \f(CW/bin/sh\fP devrait �tre un
lien symbolique ou dur vers \f(CW/bin/bash\fP puisque Bash se comporte
de mani�re diff�rente quand il est appel� en tant que \f(CWsh\fP ou
\f(CWbash\fP. \f(CWpdksh\fP, qui peut �tre \f(CW/bin/sh\fP sur certains
disques d'installation, devrait �tre arrang� de la sorte avec
\f(CW/bin/sh\fP pointant vers \f(CW/bin/ksh\fP. L'utilisation d'un lien
symbolique dans ces cas permet aux utilisateurs de voir ais�ment que
\f(CW/bin/sh\fP n'est pas un vrai shell de Bourne.

Puisque l'emplacement standard de-facto du shell C est \f(CW/bin/csh\fP,
si et seulement si un shell C ou �quivalent (comme \f(CWtcsh\fP) est
disponible sur le syst�me, il devrait �tre disponible par le nom
\f(CW/bin/csh\fP. \f(CW/bin/csh\fP peut �tre un lien symbolique vers
\f(CW/bin/tcsh\fP ou \f(CW/usr/bin/tcsh\fP.

.ft I
Note : les commandes \f(CW[\fP et \f(CWtest\fP sont int�gr�es dans les
remplacements du shell de Bourne (\f(CW/bin/sh\fP) les plus couramment
utilis�s. Ces deux commandes ne doivent pas �tre plac�es dans
\f(CW/bin\fP ; elles peuvent �tre plac�es dans \f(CW/usr/bin\fP. Elles
doivent �tre incluses comme binaires s�par�s avec tout syst�me \*(Ux ou
de type \*(Ux essayant de se conformer � la norme POSIX.2.
.ft R

.LI
Commandes de remise en �tat :
.sp
Ces commandes ont �t� ajout�es pour permettre la remise en �tat d'un
syst�me (en supposant que \f(CW/\fP soit intact).
.VL 2
.LI "\f(CW{"
tar, gzip, gunzip \fR(lien vers gzip)\fP, zcat \fR(lien vers gzip)\fP }\fP
.LE
.P
Si les sauvegardes syst�me sont faites en utilisant des programmes
autres que \f(CWgzip\fP et \f(CWtar\fP, alors la partition racine
devrait contenir les composants de remise en �tat minimaux. Par exemple,
beaucoup de syst�mes devraient inclure \f(CWcpio\fP puisque c'est
l'utilitaire de sauvegarde le plus couramment utilis� apr�s
\f(CWtar\fP. Inversement, si l'on est s�r de ne faire aucune remise en
�tat de la partition racine, ces binaires peuvent alors �tre omis (par
exemple, une partition racine en ROM, montant \f(CW/usr\fP en NFS). Si
la remise en �tat d'un syst�me est pr�vue � travers le r�seau, alors
\f(CWftp\fP ou \f(CWtftp\fP (avec tout ce qui est n�cessaire �
l'�tablissement d'une connexion ftp) doit �tre disponible sur la
partition racine.

Les commandes de remise en �tat peuvent appara�tre soit dans
\f(CW/bin\fP soit dans \f(CW/usr/bin\fP sur des syst�mes diff�rents.

.LI
Commandes r�seau :
.sp
Voici les seuls binaires n�cessaires pour le r�seau, autres que ceux de
\f(CW/usr/bin\fP ou \f(CW/usr/local/bin\fP, et que � la fois root et les
utilisateurs voudront ou auront besoin d'ex�cuter.
.VL 2
.LI "\f(CW{"
domainname, hostname, netstat, ping }\fP
.LE
.LE

.H 2 "/boot : fichiers statiques du chargeur de d�marrage"
.P
Ce r�pertoire contient tout ce qu'il faut pour le processus de d�marrage
� part les fichiers de configuration et l'installeur de carte. Ainsi,
\f(CW/boot\fP stocke les donn�es utilis�es avant que le noyau ne
commence � ex�cuter des programmes en mode utilisateur. Ceci peut
comprendre les secteurs de d�marrage principaux sauvegard�s, les
fichiers de cartes des secteurs, et tout autre donn�e qui n'est
pas directement �dit�e � la main. Les programmes n�cessaires � ce que le
chargeur de d�marrage soit capable de d�marrer sur un fichier doivent
�tre plac�s dans \f(CW/sbin\fP. Les fichiers de configuration pour les
chargeurs de d�marrage doivent �tre plac�s dans \f(CW/etc\fP.

Le noyau du syst�me d'exploitation doit �tre situ� soit dans \f(CW/\fP
soit dans  \f(CW/boot\fP

.ft I
Note : sur certaines machines i386, il peut �tre n�cessaire que
\f(CW/boot\fP soit situ� sur une partition s�par�e situ�e compl�tement
en-dessous du cylindre 1024 du p�riph�rique de d�marrage � cause de
contraintes mat�rielles.

.ft P

.H 2 "/dev : fichiers de p�riph�riques"
.P
Le r�pertoire \f(CW/dev\fP est l'emplacement des fichiers sp�ciaux ou de
p�riph�riques.

S'il est possible que des p�riph�riques dans \f(CW/dev\fP aient besoin
d'�tre cr��s � la main, \f(CW/dev\fP contiendra une commande nomm�e
\f(CWMAKEDEV\fP, qui pourra cr�er les p�riph�riques au besoin. Il peut
aussi disposer d'une commande \f(CWMAKEDEV.local\fP pour tout
p�riph�rique local.

Au besoin, \f(CWMAKEDEV\fP doit avoir de quoi cr�er n'importe quel
p�riph�rique qu'on pourrait trouver dans le syst�me, et non pas
simplement ceux qu'une impl�mentation particuli�re installe.

.H 2 "/etc : configuration syst�me sp�cifique � la machine"
.P
\f(CW/etc\fP contient les fichiers et les r�pertoires de configuration
sp�cifiques au syst�me en cours.

Aucun binaire ne devrait �tre situ� dans \f(CW/etc\fP.

.PS
copy "draft.pic"
dir(/etc,configuration syst�me sp�cifique � la machine)
sub("X11","Configuration pour le syst�me X Window")
sub("opt","Configuration pour /opt")
.PE

La section suivante sert surtout � illustrer la description du contenu
de \f(CW/etc\fP avec un certain nombre d'exemples ; ce n'est surtout pas
une liste exhaustive.

.HU "Fichiers obligatoires pour /etc :"
.BL
.LI
Fichiers g�n�raux :
.VL 2
.LI "\f(CW{"
adjtime, csh.login, disktab, fdprm, fstab, gettydefs, group, inittab,
confissue, ld.so.conf, lilo.conf, motd, mtab, mtools, passwd,
profile, securetty, shells, syslog.conf, ttytype }\fP
.LE

.LI
Fichiers de r�seau :
.VL 2
.LI "\f(CW{"
exports, ftpusers, gateways, host.conf, hosts, hosts.allow, hosts.deny,
hosts.equiv, hosts.lpd, inetd.conf, networks, printcap, protocols,
resolv.conf, rpc, services }\fP
.LE
.LE
.P
.ft I
Notes :

La mise en place des scripts de commandes invoqu�s au d�marrage peut
ressembler aux mod�les System V ou BSD. Des sp�cifications
suppl�mentaires dans ce domaine pourront �tre ajout�es � une version
future de la norme.

Les syst�mes qui utilisent la suite shadow password auront des fichiers
de configuration suppl�mentaires dans \f(CW/etc\fP (\f(CW/etc/shadow\fP
et autres) et des programmes dans \f(CW/usr/sbin\fP (\f(CWuseradd\fP,
\f(CWusermod\fP, et autres).

.ft P

.H 3 "/etc/X11 : Configuration pour le syst�me X Window"
.sp 0
.P
\f(CW/etc/X11\fP est l'emplacement recommand� pour toute configuration
X11 sp�cifique � la machine. Ce r�pertoire est n�cessaire pour permettre
un contr�le local si \f(CW/usr\fP est mont� en lecture seule. Les
fichiers qui devraient �tre dans ce r�pertoire comprennent
\f(CWXconfig\fP (et/ou \f(CWXF86Config\fP) et \f(CWXmodmap\fP.

Les sous-r�pertoires de \f(CW/etc/X11\fP peuvent inclure ceux pour
\f(CWxdm\fP et pour tout autre programme (certains gestionnaires de
fen�tres, par exemple) qui en a besoin. Nous recommandons que les
gestionnaires de fen�tres qui n'ont qu'un fichier de configuration
par d�faut \f(CW.*wmrc\fP le nomment \f(CWsystem.*wmrc\fP
(sauf s'il existe un nom de rechange largement reconnu) et n'utilisent
pas de sous-r�pertoire. Tout sous-r�pertoire de gestionnaire de fen�tres
devrait �tre nomm� du m�me nom que le binaire r�el du gestionnaire de
fen�tres.

\f(CW/etc/X11/xdm\fP contient les fichiers de configuration pour
\f(CWxdm\fP. Ce sont la plupart des fichiers que l'on trouve
habituellement dans \f(CW/usr/lib/X11/xdm\fP. Certaines donn�es
variables et locales pour \f(CWxdm\fP sont stock�es dans
\f(CW/var/state/xdm\fP.

.H 3 "/etc/opt : fichiers de configuration pour /opt"
.sp 0
.P
Les fichiers de configuration sp�cifiques � la machine pour les
paquetages des logiciels d'application suppl�mentaires seront install�s
dans le r�pertoire \f(CW/etc/opt/<paquetage>\fP, o� \f(CW<paquetage>\fP
est le nom du sous-r�pertoire de \f(CW/opt\fP o� les donn�es statiques
de ce paquetage sont stock�es. Aucune structure n'est impos�e sur la
disposition interne de \f(CW/etc/opt/<paquetage>\fP.

Si un fichier de configuration doit r�sider dans un endroit diff�rent
pour que le paquetage ou le syst�me fonctionne correctement, on peut le
placer dans un endroit diff�rent de \f(CW/etc/opt/<paquetage>\fP.

.HU "Raison d'�tre :"
.br
.P
Voir la raison d'�tre pour \f(CW/opt\fP.

.H 2 "/home : r�pertoires personnels des utilisateurs (optionnel)"
.P
\f(CW/home\fP est un concept tr�s standard, mais c'est clairement un
syst�me de fichiers sp�cifique au site. Sa mise en place sera diff�rente
d'une machine � l'autre. Cette section ne d�crit qu'un ordonnancement
sugg�r� des r�pertoires personnels des utilisateurs ; n�anmoins nous
recommandons que toutes les distributions conformes � la \*(Fs l'utilisent
comme emplacement par d�faut des r�pertoires utilisateurs.

Sur les petits syst�mes, chaque r�pertoire utilisateur est en g�n�ral
l'un des nombreux sous-r�pertoires de \f(CW/home\fP comme
\f(CW/home/dupont\fP, \f(CW/home/torvalds\fP, \f(CW/home/admin\fP, etc.

Sur des grands syst�mes (surtout quand les r�pertoires \f(CW/home\fP
sont partag�s entre beaucoup d'h�tes par NFS) il est utile de subdiviser
les r�pertoires personnels des utilisateurs. Le partage peut se faire en
utilisant des sous-r�pertoires comme \f(CW/home/equipe\fP,
\f(CW/home/invites\fP, \f(CW/home/eleves\fP, etc.

D'autres personnes pr�f�rent placer les comptes utilisateurs � divers
autres endroits. Par cons�quent, aucun programme ne devrait se fier �
cet endroit. Si vous voulez trouver le r�pertoire personnel d'un
utilisateur, vous devriez utiliser la fonction de biblioth�que
\f(CWgetpwent(3)\fP plut�t que de compter sur \f(CW/etc/passwd\fP parce
que les informations sur les utilisateurs peuvent �tre stock�es �
distance en utilisant des syst�mes tels que NIS.

.H 2 "/lib : biblioth�ques partag�es importantes et modules du noyau"
.P

Le r�pertoire \f(CW/lib\fP contient les images des biblioth�ques
partag�es n�cessaires au d�marrage du syst�me et pour lancer les
commandes du syst�me de fichiers racine.

.PS
copy "draft.pic"
dir(/lib,biblioth�ques partag�es importantes et modules du noyau)
sub("modules","modules chargeables du noyau")
.PE

Ceci comprend \f(CW/lib/libc.so.*\fP, \f(CW/lib/libm.so.*\fP, l'�diteur
de liens dynamiques partag�s \f(CW/lib/ld.so\fP, et d'autres
biblioth�ques partag�es n�cessaires pour les binaires de \f(CW/bin\fP et
\f(CW/sbin\fP.

Les biblioth�ques partag�es n�cessaires uniquement aux binaires de
\f(CW/usr\fP (comme n'importe quel binaire pour X Window)
n'appartiennent pas � \f(CW/lib\fP. Seules les biblioth�ques partag�es
n�cessaires au fonctionnement des binaires de \f(CW/bin\fP et
\f(CW/sbin\fP devraient se trouver ici. La biblioth�que
\f(CWlibm.so.*\fP peut aussi se trouver dans \f(CW/usr/lib\fP si elle
n'est pas n�cessaire dans \f(CW/bin\fP ou \f(CW/sbin\fP.

Pour des raisons de compatibilit�, \f(CW/lib/cpp\fP doit exister et se
r�f�rer au pr�-processeur C install� sur le syst�me. L'emplacement
traditionnel de ce binaire est
\f(CW\%/usr/lib/gcc-lib/<cible>/<version>/cpp\fP. \f(CW/lib/cpp\fP peut
soit pointer vers ce binaire, soit vers toute r�f�rence � ce binaire qui
existe dans le syst�me de fichiers. (Par exemple, \f(CW/usr/bin/cpp\fP
est de m�me souvent utilis�.)

La sp�cification pour \f(CW/lib/modules\fP est en cours d'�laboration.

.H 2 "/mnt : point de montage pour les syst�mes de fichiers mont�s temporairement"
.P
Ce r�pertoire est install� pour que l'administrateur syst�me puisse
monter de mani�re temporaire et au besoin des syst�mes de fichiers. Le
contenu de ce r�pertoire est une affaire locale et ne devrait pas
affecter la mani�re dont n'importe quel programme est lanc�.

Nous sommes oppos�s � l'utilisation de ce r�pertoire par les programmes
d'installation, et nous sugg�rons qu'un r�pertoire temporaire convenable
non utilis� par le syst�me soit utilis� � la place.

.H 2 "/opt : paquetages de logiciels d'applications suppl�mentaires"
.SP
.PS
copy "draft.pic"
dir(/opt,paquetages de logiciels d'applications suppl�mentaires)
sub("<paquetage>","objets de paquetage statiques")
.PE
\f(CW/opt\fP est r�serv� � l'installation de paquetages de logiciels
d'application suppl�mentaires.

Un paquetage qui doit �tre install� dans \f(CW/opt\fP devra mettre ses
fichiers statiques dans une arborescence \f(CW/opt/<paquetage>\fP
s�par�e, o� \f(CW<paquetage>\fP est un nom d�crivant le paquetage
logiciel.

Les programmes devant �tre lanc�s par les utilisateurs seront situ�s
dans le r�pertoire \f(CW/opt/<paquetage>/bin\fP. Si le paquetage
comprend des pages de manuel \*(Ux, elle seront situ�es dans
\f(CW/opt/<paquetage>/man\fP et la m�me structure que
\f(CW/usr/share/man\fP sera utilis�e.

Les r�pertoires \f(CW/opt/bin\fP, \f(CW/opt/doc\fP,
\f(CW/opt/include\fP, \f(CW/opt/info\fP, \f(CW/opt/lib\fP, et
\f(CW/opt/man\fP sont r�serv�s � l'usage de l'administrateur syst�me
local. Les paquetages peuvent fournir des fichiers de "lancement"
(front-end) faits pour qu'un administrateur syst�me local les place (en
faisant un lien ou en les copiant) dans ces r�pertoires r�serv�s, mais
ils devront fonctionner normalement en l'absence de ces r�pertoires
r�serv�s.

Les fichiers de paquetage variables (qui changent avec un usage normal)
devraient �tre install�s dans \f(CW/var/opt\fP. Voyez la section sur
\f(CW/var/opt\fP pour plus d'informations.

Les fichiers de configuration sp�cifiques � la machine devraient �tre
install�s dans \f(CW/etc/opt\fP. Voyez la section sur \f(CW/etc/opt\fP
pour plus d'informations.

Aucun autre fichier de paquetage ne devrait exister en dehors des
hi�rarchies \f(CW/opt\fP, \f(CW/var/opt\fP et \f(CW/etc/opt\fP sauf pour
les fichiers de paquetage qui doivent r�sider dans des endroits
sp�cifiques � l'int�rieur de l'arborescence du syst�me de fichiers afin
de fonctionner correctement. Par exemple, les fichiers de bloquage des
p�riph�riques doivent �tre plac�s dans \f(CW/var/lock\fP et les
p�riph�riques dans \f(CW/dev\fP.

.HU "Raison d'�tre"
.br
.P
L'utilisation de \f(CW/opt\fP pour les logiciels suppl�mentaires est une
pratique bien �tablie dans la communaut� \*(Ux. L'interface Binaire
d'Applications (ABI) System V [AT&T 1990], bas�e sur la D�finition
d'Interface System V (troisi�me �dition) fournit une structure
\f(CW/opt\fP tr�s similaire � celle d�crite ici.

La Norme de Compatibilit� Binaire Intel version 2 (iBCS2) fournit aussi
une structure similaire pour \f(CW/opt\fP.

En g�n�ral, toutes les donn�es n�cessaires au support d'un paquetage sur
un syst�me doivent �tre pr�sentes dans \f(CW/opt/<paquetage>\fP, y
compris les fichiers destin�s � �tre copi�s dans
\f(CW/etc/opt/<paquetage>\fP et \f(CW/var/opt/<paquetage>\fP ainsi que
dans les r�pertoires r�serv�s de \f(CW/opt\fP.

.H 2 "/root : r�pertoire personnel pour l'utilisateur root (optionnel)"
.P
\f(CW/\fP est traditionnellement le r�pertoire personnel du compte root
sur les syst�mes \*(Ux. \f(CW/root\fP est utilis� sur de nombreux
syst�mes Linux et sur certains syst�mes \*(Ux (afin de r�duire
l'encombrement du r�pertoire \f(CW/\fP). Le r�pertoire
personnel du compte root peut �tre d�termin� par une pr�f�rence globale
au niveau du d�veloppeur ou locale. Les possibilit�s �videntes
comprennent \f(CW/\fP, \f(CW/root\fP et \f(CW/home/root\fP.

Si le r�pertoire personnel du compte root n'est pas stock� sur la
partition racine il sera n�cessaire de s'assurer qu'il prendra la
valeur \f(CW/\fP par d�faut si on ne peut le trouver.

.ft I
Note : nous nous opposons � l'utilisation du compte root pour des choses
simples comme le courrier �lectronique ou les nouvelles, et recommandons
qu'il ne soit utilis� qu'au titre de l'administration syst�me. Pour
cette raison, nous recommandons que les sous-r�pertoires tels que
\f(CWMail\fP et \f(CWNews\fP n'apparaissent pas dans le r�pertoire
personnel du compte root, et que le courrier � destination des r�les
administratifs comme root, postmaster ou webmaster soient redirig�s vers
un utilisateur appropri�.
.ft P

.H 2 "/sbin : binaires syst�mes (qui se trouvaient autrefois dans /etc)"
.P
Les utilitaires utilis�s pour l'administration syst�me (et autres
commandes faites uniquement pour root) sont stock�s dans \f(CW/sbin\fP,
\f(CW/usr/sbin\fP et \f(CW/usr/local/sbin\fP. \f(CW/sbin\fP contient
typiquement des binaires essentiels au d�marrage du syst�me en plus des
binaires de \f(CW/bin\fP. Tout ce qui est ex�cut� apr�s qu'il soit s�r
que \f(CW/usr\fP soit mont� (quand il n'y a pas de probl�mes) devrait
aller dans \f(CW/usr/sbin\fP. Les binaires d'administration syst�me
sp�cifiques au syst�me devraient �tre install�s dans
\f(CW/usr/local/sbin\fP.

D�cider de ce qui va dans les r�pertoires \f(CW"sbin"\fP est simple : si
un utilisateur normal (pas un administrateur syst�me) doit le lancer
directement, il devrait alors aller dans l'un des r�pertoires
\f(CW"bin"\fP. Les utilisateurs ordinaires ne devraient mettre aucun des
r�pertoires \f(CWsbin\fP dans leur chemin d'acc�s (path).

.ft I
Note : par exemple, les fichiers tels que \f(CWchfn\fP que les
utilisateurs n'utilisent que de temps en temps devraient quand m�me �tre
plac�s dans \f(CW/usr/bin\fP. \f(CWping\fP, bien qu'il ne soit
absolument n�cessaire que pour root (remise en �tat et diagnostic
r�seau) est souvent utilis� par les utilisateurs et pour cette raison
devrait exister dans \f(CW/bin\fP.
.ft R

Nous recommandons que les utilisateurs aient les permissions de lecture
et d'ex�cution pour tout ce qui se trouve dans \f(CW/sbin\fP mis � part,
peut-�tre, certains programmes setuid et setgid. La division entre
\f(CW/bin\fP et \f(CW/sbin\fP n'a pas �t� cr��e pour des raisons de
s�curit� ou pour emp�cher les utilisateurs de voir le syst�me
d'exploitation, mais pour fournir une bonne s�paration entre les
binaires que tout le monde utilise et ceux qui sont tout d'abord
utilis�s pour des t�ches d'administration. Il n'y a pas d'avantage
sp�cifique pour la s�curit� � rendre \f(CW/sbin\fP inaccessible aux
utilisateurs.

.HU "Fichiers obligatoires pour /sbin :"
.BL
.LI
Commandes g�n�rales :
.VL 2
.LI "\f(CW{"
clock, getty, init, update, mkswap, swapon, swapoff, telinit }\fP
.LE

.LI
Commandes d'extinction :
.VL 2
.LI "\f(CW{"
fastboot, fasthalt, halt, reboot, shutdown }\fP
.LE
.sp 0.5
(ou toute combinaison des fichiers ci-dessus, pourvu que
\f(CWshutdown\fP soit inclus.)

.LI
Commandes de gestion du syst�me de fichiers :
.VL 2
.LI "\f(CW{"
fdisk, fsck, fsck.*, mkfs, mkfs.* }\fP
.LE
.sp 0.5
\f(CW*\fP = un ou plus parmi \f(CWext, ext2, minix, msdos, xia\fP et
peut-�tre d'autres

.LI
Commandes r�seau :
.VL 2
.LI "\f(CW{"
ifconfig, route }\fP
.LE
.LE

.H 2 "/tmp : fichiers temporaires"
.P
Le r�pertoire \f(CW/tmp\fP devra �re mis � la disposition des programmes
qui ont besoin de cr�er des fichiers temporaires.

Bien que les donn�es stock�es dans \f(CW/tmp\fP puissent �tre effac�es
d'une mani�re sp�cifique � chaque site, il est recommand� que les
fichiers et r�pertoires situ�s dans \f(CW/tmp\fP soient effac�s � chaque
d�marrage du syst�me.

Les programmes ne devront pas supposer que tout fichier ou r�pertoire
de \f(CW/tmp\fP est pr�serv� entre deux lancements de ces programmes.

.HU "Raison d'�tre :"
.br
.P
La norme IEEE P1003.2 (POSIX, partie 2) donne des obligations similaires
� la section ci-dessus.

\*(Fs a ajout� la recommandation du nettoyage de \f(CW/tmp\fP au
d�marrage sur la base de pr�c�dents historiques et de pratique commune,
mais n'en a pas fait une obligation parce que l'administration syst�me
n'entre pas dans l'objectif de cette norme.
.SK

.H 1 "La hi�rarchie /usr"
.P
\f(CW/usr\fP est la deuxi�me grande partie du syst�me de
fichiers. \f(CW/usr\fP contient des donn�es partageables, en lecture
seule. Ceci veut dire qu'on devrait pouvoir partager \f(CW/usr\fP entre
plusieurs machines diverses se conformant � \*(Fs et on ne devrait pas y
�crire. Toute information sp�cifique � la machine ou qui varie avec le
temps est stock�e autre part.

Aucun grand paquetage logiciel ne devrait utiliser un sous-r�pertoire
direct sous la hi�rarchie \f(CW/usr\fP. Exception est faite du syst�me X
Window � cause d'un �norme pr�c�dent et d'une pratique largement
suivie. Cette section de la norme sp�cifie l'emplacement de la plupart
de ces paquetages.

.PS
copy "draft.pic"
dir(/usr,hi�rarchie secondaire)
sub("X11R6","Syst�me X Window, version 11 release 6")
sub("X386","Syst�me X Window, version 11 release 5 sur des plate-formes x86")
sub("bin","La plupart des commandes utilisateurs")
.\" old placement
.\" sub("g++-include","GNU C++ include files")
sub("games","Binaires de jeux et d'apprentissage")
sub("include","Fichiers d'en-t�tes inclus par les programmes C")
sub("lib","Biblioth�ques")
sub("local","Hi�rarchie locale (vide apr�s l'installation principale)")
sub("sbin","Binaires syst�mes non essentiels")
sub("share","Donn�es ind�pendantes de l'architecture")
sub("src","Code source")
.PE

Les liens symboliques vers les r�pertoires suivants peuvent �tre
pr�sents. Cette possibilit� est bas�e sur le besoin de pr�server la
compatibilit� avec les vieux syst�mes jusqu'� ce qu'on soit s�r que
toutes les impl�mentations utilisent la hi�rarchie \f(CW/var\fP.

.nf
.ft CW
    /usr/spool -> /var/spool
    /usr/tmp -> /var/tmp
    /usr/spool/locks -> /var/lock
.ft P
.fi

Une fois qu'un syst�me n'a plus besoin de l'un des liens symboliques
ci-dessus, le lien peut �tre enlev� si d�sir�.

.SK
.H 2 "/usr/X11R6 : Syst�me X Window, Version 11 Release 6"
.P
Cette hi�rarchie est r�serv�e au syst�me X Window, version 11 release 6,
et aux fichiers apparent�s.

Pour simplifier les choses et rendre XFree86 plus compatible avec le
syst�me X Window sur les autres syst�mes, les liens symboliques suivants
devraient �tre pr�sents :

.nf
.ft CW
    /usr/bin/X11 -> /usr/X11R6/bin
    /usr/lib/X11 -> /usr/X11R6/lib/X11
    /usr/include/X11 -> /usr/X11R6/include/X11
.ft P
.fi
.P
En g�n�ral, les logiciels ne devraient pas �tre install�s ou g�r�s par
l'interm�diaire de ces liens symboliques. Ils ne sont destin�s qu'� une
utilisation par les utilisateurs. La difficult� est li�e � la version de
sortie du syst�me X Window \(em dans les p�riodes de transition, il est
impossible de savoir quelle version de X11 est en cours d'utilisation.

Les donn�es sp�cifiques � la machine dans \f(CW/usr/X11R6/lib/X11\fP
devraient �tre prises comme des fichiers de d�monstration. Les
applications ayant besoin d'informations sur la machine courante (gr�ce
� des fichiers comme \f(CWXconfig\fP, \f(CWXF86Config\fP ou
\f(CWsystem.twmrc\fP) doivent se r�f�rer � un fichier de configuration
dans \f(CW/etc/X11\fP, qui peut �tre un lien vers un fichier de
\f(CW/usr/X11R6/lib\fP.

.H 2 "/usr/X386 : syst�me X Window, Version 11 Release 5, sur les plate-formes x86"
.P
Cette hi�rarchie est en g�n�ral identique � \f(CW/usr/X11R6\fP ; les
liens symboliques de \f(CW/usr\fP pour X11 devraient pointer vers la
version du syst�me X Window d�sir�e.

.H 2 "/usr/bin : la plupart des commandes utilisateurs"
.P
Voici le r�pertoire principal des commandes ex�cutables sur le syst�me.

.PS
copy "draft.pic"
dir(/usr/bin,Binaires non n�cessaires en mode utilisateur unique)
sub("mh","Commandes pour le syst�me de gestion de courrier MH")
sub("X11","Lien symbolique vers \f(CW/usr/X11R6/bin\fP")
.PE

Les interpr�teurs de scripts shell (qu'on lance avec \f(CW#!<chemin>\fP
sur la premi�re ligne d'un script shell) ne pouvant pas compter sur un
chemin, il est avantageux de normaliser leur emplacement. Les
interpr�teurs du shell de Bourne et du shell C sont d�j� fix�s dans
\f(CW/bin\fP, mais on trouve souvent Perl, Python et Tcl dans maints
endroits diff�rents. \f(CW/usr/bin/perl\fP, \f(CW/usr/bin/python\fP et
\f(CW/usr/bin/tcl\fP devraient faire r�f�rence respectivement aux
interpr�teurs de shell \f(CWperl\fP, \f(CWpython\fP et \f(CWtcl\fP. Il
peut y avoir des liens symboliques vers l'emplacement v�ritable des
interpr�teurs shell.

.SK
.H 2 "/usr/include : r�pertoire pour les fichiers d'en-t�tes standards."
.P
Voici l'endroit o� tous les fichiers d'en-t�tes � usage g�n�ral pour les
langages de programmation C et C++ devraient �tre plac�s.

.PS
copy "draft.pic"
dir(/usr/include,fichiers d'en-t�tes)
sub("X11","lien symbolique vers \f(CW/usr/X11R6/include/X11\fP")
sub("bsd","fichiers d'en-t�tes de compatibilit� BSD (si n�cessaire)")
sub("g++","fichiers d'en-t�tes pour le GNU C++")
.PE

.H 2 "/usr/lib : biblioth�ques pour la programmation et les paquetages"
.P
\f(CW/usr/lib\fP contient des fichiers objets, des biblioth�ques et des
binaires internes qui ne sont pas destin�s � �tre ex�cut�s par les
utilisateurs ou les scripts shell.

Les applications peuvent utiliser un sous-r�pertoire unique sous
\f(CW/usr/lib\fP. Si une application utilise un sous-r�pertoire, toutes
les donn�es d�pendantes de l'architecture utilis�es exclusivement par
cette application devraient �tre plac�es dans ce sous-r�pertoire. Par
exemple, le sous-r�pertoire \f(CWperl5\fP pour les modules et les
biblioth�ques de Perl 5.

Les fichiers et r�pertoires divers, statiques, ind�pendants de
l'architecure et sp�cifiques � une application devraient aller dans
\f(CW/usr/share\fP.

Certaines commandes ex�cutables telles que \f(CWmakewhatis\fP et
\f(CWsendmail\fP ont aussi �t� plac�es de mani�re traditionnelle dans
\f(CW/usr/lib\fP. \f(CWmakewhatis\fP est un binaire interne et devrait
�tre plac� dans un r�pertoire de binaires ; les utilisateurs acc�dent
uniquement � \f(CWcatman\fP. Les nouveaux binaires \f(CWsendmail\fP sont
maintenant plac�s par d�faut dans \f(CW/usr/sbin\fP ; un lien symbolique
devrait rester de \f(CW/usr/lib\fP. En plus, les syst�mes qui utilisent
Smail devraient placer Smail dans \f(CW/usr/sbin/smail\fP et
\f(CW/usr/sbin/sendmail\fP devrait �tre un lien symbolique vers
celui-ci.

Un lien symbolique \f(CW/usr/lib/X11\fP pointant vers le r�pertoire
\f(CWlib/X11\fP de la distribution X par d�faut est n�cessaire si X est
install�.

.\" XXX - Chris a effac� ceci. Peut-�tre le r�duire � une phrase ?
.\"
.ft I
Note : aucune donn�e sp�cifique � la machine pour le syst�me X Window ne
devrait �tre stock� dans \f(CW/usr/lib/X11\fP. Les fichiers de
configuration sp�cifiques � la machine tels que \f(CWXconfig\fP ou
\f(CWXF86Config\fP devraient exister dans \f(CW/etc/X11\fP. Ceci devrait
comprendre les donn�es de configuration tels que \f(CWsystem.twmrc\fP
m�me si l'on n'en fait qu'un lien symbolique vers un fichier de
configuration plus global (probablement dans \f(CW/usr/X11R6/lib/X11\fP).

.ft P

.H 2 "/usr/local : hi�rarchie locale"
.P
La hi�rarchie \f(CW/usr/local\fP est destin�e � l'utilisation de
l'administrateur syst�me quand il installe des logiciels en local. Il
doit �tre mis � l'abri de tout effacement lors de la mise � jour du
logiciel syst�me. On peut l'utiliser pour des programmes et des donn�es
qu'on peut partager parmi un groupe de machines, mais qu'on ne trouve
pas dans \f(CW/usr\fP.

.PS
copy "draft.pic"
dir(/usr/local,Hi�rarchie locale)
sub("bin","Binaires locaux")
sub("games","Binaires de jeux locaux")
sub("include","Fichiers d'en-t�tes C locaux")
sub("lib","Biblioth�ques locales")
sub("sbin","Binaires syst�me locaux")
sub("share","Hi�rarchie ind�pendante de la machine locale")
sub("src","Code source local")
.PE

Ce r�pertoire devrait toujours �tre vide apr�s la premi�re installation
d'un syst�me se conformant � la \*(Fs. Aucune exception � cette r�gle ne
devrait �tre faite � part les morceaux de r�pertoires list�s.

Les logiciels install�s localement devraient �tre plac�s dans
\f(CW/usr/local\fP plut�t que dans \f(CW/usr\fP sauf si on l'installe
pour remplacer ou mettre � jour un logiciel de \f(CW/usr\fP.

Notez que les logiciels plac�s dans \f(CW/\fP ou \f(CW/usr\fP peuvent
�tre �cras�s par les mises � jour syst�mes (bien que nous recommandons
que les distributions n'�crasent pas les donn�es de \f(CW/etc\fP dans
ces circonstances). Pour cette raison, les logiciels locaux ne devraient
pas aller en dehors de \f(CW/usr/local\fP sans bonne raison.

.H 2 "/usr/sbin : binaires syst�mes normaux non essentiels"
.P
Ce r�pertoire contient tous les binaires non essentiels utilis�s
exclusivement par l'administrateur syst�me. Les programmes
d'administration syst�me n�cessaires � la r�paration du syst�me, sa
remise en route, le montage de \f(CW/usr\fP ou d'autres fonctions
essentielles devraient plut�t �tre plac�s dans \f(CW/sbin\fP.

.\" XXX - Chris a barr� les deux paragraphes suivants. Effacer ?
Typiquement, \f(CW/usr/sbin\fP contient les daemons r�seau, tous les
outils d'administration non essentiels et les binaires pour les
programmes serveurs non critiques.

.\" Je ne suis pas s�r de devoir recommander ce paragraphe -
.\" il semble hors de notre port�e - iwj
Ces programmes serveurs sont utilis�s � l'entr�e dans l'�tat System V
connu sous le nom de "run level 2" (�tat multi-utilisateurs) et "run
level 3" (�tat en r�seau) ou dans l'�tat BSD connu sous le nom de "mode
multi-utilisateurs". � ce point le syst�me met certains services � la
disposition des utilisateurs (par exemple, les impressions) et d'autres
machines (par exemple, les exports NFS).

Les programmes d'administration syst�me install�s en local devraient
�tre plac�s dans \f(CW/usr/local/sbin\fP.

.H 2 "/usr/share : donn�es ind�pendantes de l'architecture"
.P

.PS
copy "draft.pic"
dir(/usr/share,Donn�es ind�pendantes de l'architecture)
sub("dict","Listes de mots")
sub("doc","Documentations diverses")
sub("games","Fichiers de donn�es statiques pour \f(CW/usr/games\fP")
sub("info","R�pertoire principal du syst�me Info de GNU")
sub("locale","Informations pour Locale")
sub("man","Pages de manuel en ligne")
sub("nls","Support pour la langue natale")
sub("misc","Donn�es diverses ind�pendantes de la machine")
sub("terminfo","R�pertoires pour la base de donn�es terminfo")
sub("tmac","Macros troff non distribu�es avec groff")
sub("zoneinfo","Information et configuration pour le fuseau horaire")
.PE

La hi�rarchie \f(CW/usr/share\fP contient tous les fichiers de donn�es
ind�pendantes de l'architecture en lecture seule. La plupart de ces
donn�es se trouvaient � l'origine dans \f(CW/usr\fP (\f(CWman\fP,
\f(CWdoc\fP) ou \f(CW/usr/lib\fP (\f(CWdict\fP, \f(CWterminfo\fP,
\f(CWzoneinfo\fP). Cette hi�rarchie est destin�e � �tre partag�e entre
toutes les plate-formes et architectures pour un syst�me d'exploitation
donn� ; ainsi, par exemple, un site avec des plate-formes i386, Alpha et
PPC peut maintenir un seul r�pertoire \f(CW/usr/share\fP qui est mont�
de mani�re centrale. Notez, cependant, que \f(CW/usr/share\fP n'est en
g�n�ral pas fait pour �tre partag� par des syst�mes d'exploitation
diff�rents ou par diff�rentes versions du m�me syst�me d'exploitation.

Tout programme ou paquetage qui contient ou n�cessite des donn�es qui
n'ont pas besoin d'�tre modifi�es devrait stocker ces donn�es dans
\f(CW/usr/share\fP (ou \f(CW/usr/local/share\fP en cas d'installation
locale). Il est recommand� d'utiliser un sous-r�pertoire de
\f(CW/usr/share\fP � cet effet.

Notez que Linux utilise pour le moment des fichiers de bases de donn�es
au format DBM. Bien que ceux-ci ne soient pas ind�pendants de
l'architecture, ils sont autoris�s dans \f(CW/usr/share\fP en
anticipation d'un passage au format DB 2.0 ind�pendant de
l'architecture.

Les donn�es de jeux stock�es dans \f(CW/usr/share/games\fP devraient
�tre des donn�es purement statiques. Tout fichier modifiable, comme les
fichiers de scores, les enregistrements de parties et ainsi de suite,
devraient �tre plac�s dans \f(CW/var/games\fP.

Il est recommand� que les r�pertoire sp�cifiques � une application,
ind�pendants de l'architecture soient plac�s ici. De tels r�pertoires
comprennent \f(CWgroff\fP, \f(CWperl\fP, \f(CWghostscript\fP,
\f(CWtexmf\fP et \f(CWkbd\fP (Linux) ou \f(CWsyscons\fP (BSD). Ils
peuvent, cependant, �tre plac�s dans \f(CW/usr/lib\fP pour des raisons
de compatibilit� ascendante, � la discr�tion du distributeur. De m�me,
une hi�rarchie \f(CW/usr/lib/games\fP peut �tre utilis�e en plus de la
hi�rarchie \f(CW/usr/share/games\fP si le distributeur d�sire placer
quelques donn�es de jeux � cet endroit.

.\"
.\" Note les fichiers de support de groff devraient �tre install�s dans
.\" /usr/share/groff pour simplifier la mise � jour de groff sur les
.\" syst�mes Linux, plut�t que la distribution des fichiers groff que
.\" l'on trouve sur les syst�mes BSD actuels.

.H 3 "/usr/share/dict : listes de mots"
.sp
.HU "Fichiers recommand�s pour /usr/share/dict :"
.VL 2
.LI "\f(CW{"
words }\fP
.LE
.P
Traditionnellement, ce r�pertoire ne contient que le fichier anglais
\f(CWwords\fP, utilis� par \f(CWlook(1)\fP et divers programmes de
correction orthographique. \f(CWwords\fP peut utiliser l'orthographe
am�ricaine ou britannique. Les sites qui veulent les deux peuvent faire
un lien \f(CWwords\fP vers \f(CW\%/usr/share/dict/american-english\fP ou
\f(CW\%/usr/share/dict/british-english\fP.

On peut ajouter des listes de mots pour d'autres langues en utilisant le
nom anglais de cette langue, par exemple \f(CW/usr/share/dict/french\fP,
\f(CW/usr/share/dict/danish\fP, etc. Ils devraient, si possible,
utiliser un jeu de caract�res ISO 8859 appropri� � la langue en question
; si possible en utilisant le jeu de caract�res Latin1 (ISO 8859-1) --
ce n'est souvent pas possible.

D'autres listes de mots, comme le "dictionnaire" \f(CWweb2\fP devraient
y �tre inclus, s'ils existent.

.HU "Raison d'�tre :"
.br
.P
La raison pour laquelle seules les listes de mots sont situ�es ici est
que ce sont les seuls fichiers communs � tous les correcteurs
d'orthographe.

.H 3 "/usr/share/man : pages de manuel"
.sp
Cette section parcourt en d�tails l'organisation des pages de manuel
pour le syst�me entier, en incluant
\f(CW/usr/share/man\fP. Reportez-vous aussi � la section sur
\f(CW/var/cache/man\fP.

Les pages de manuel sont stock�es dans
\f(CW<manrep>/<locale>/man<section>/<arch>\fP. \f(CW<manrep>\fP,
\f(CW<locale>\fP, \f(CW<section>\fP et \f(CW<arch>\fP sont expliqu�s
ci-dessous.

.PS
copy "draft.pic"
dir(<manrep>/<locale>,Hi�rarchie de pages de manuel)
sub("man1","Programmes utilisateurs")
sub("man2","Appels syst�me")
sub("man3","Appels de biblioth�que")
sub("man4","Fichiers sp�ciaux")
sub("man5","Formats de fichiers")
sub("man6","Jeux")
sub("man7","Divers")
sub("man8","Administration syst�me")
.PE
Le \f(CW<manrep>\fP principal du syst�me est
\f(CW/usr/share/man\fP. \f(CW/usr/share/man\fP contient des informations
du manuel pour les commandes et les donn�es des syst�mes de fichiers
\f(CW/\fP et \f(CW/usr\fP. �videmment, il n'y a pas de pages de manuel
dans \f(CW/\fP parce qu'elles ne sont pas utiles au d�marrage ni dans
les situations d'urgence.

La \f(CW<section>\fP d�crit la section du manuel.

Il faut s'assurer de faire de la place dans la structure de
\f(CW/usr/share/man\fP pour supporter les pages de manuel �crites en des
langues diff�rentes (ou multiples). Cette place doit prendre en compte
le stockage et la r�f�rence � ces pages de manuel. Les facteurs
pertinents comprennent la langue (avec les diff�rences g�ographiques),
et le codage des caract�res.

Le nommage des sous-r�pertoires sp�cifiques � la langue dans
\f(CW/usr/share/man\fP est bas� sur l'annexe E de la norme POSIX 1003.1
qui d�crit la cha�ne d'identification locale \(em la m�thode la mieux
accept�e pour d�crire un environnement culturel. La cha�ne
\f(CW<locale>\fP est :

.P 1
\f(CW<langue>[_<territoire>][.<code_caract�re>][,<version>]\fP

Le champ \f(CW<langue>\fP sera tir� d'ISO 639 (un code de repr�sentation
des noms de langues). Il sera constitu� de deux caract�res et sp�cifi�
en lettres minuscules uniquement.

Le champ \f(CW<territoire>\fP sera le code � deux lettres d'ISO 3166
(sp�cification des repr�sentations de pays) si possible. (La plupart des
personnes sont famili�res avec les codes � deux lettres utilis�es pour
les codes de pays des adresses �lectroniques.\*F) Il sera constitu� de
deux caract�res sp�cifi�s en lettres majuscules uniquement.
.FS
Une exception majeure � cette r�gle est le Royaume Uni, qui est `GB'
dans ISO 3166, mais `UK' pour la plupart des adresses �lectroniques.
.FE

Le champ \f(CW<code_caract�re>\fP devrait repr�senter la norme d�crivant
le code de caract�res. Si le champ \f(CW\%<code_caract�re>\fP est une
simple indication num�rique, le nombre repr�sente le num�ro de la norme
internationale d�crivant le code de caract�res. Il est recommand� que ce
soit une repr�sentation num�rique si possible (surtout pour les normes
ISO), qu'il n'inclue pas de symboles de ponctuation suppl�mentaires et
que toute lettre soit en minuscule.

Un param�tre sp�cifiant la \f(CW<version>\fP du profil peut �tre plac�
apr�s le champ \f(CW\%<code_caract�re>\fP, s�par� par une virgule. Ceci
peut servir � diff�rencier plusieurs besoins culturels ; par exemple,
l'ordre du dictionnaire compar� � un ordre d'assemblage plus orient�
syst�mes. Cette norme recommande de ne pas utiliser le champ
\f(CW<version>\fP, sauf si c'est n�cessaire.

Les syst�mes utilisant une langue et un code de caract�res uniques pour
toutes les pages de manuel peuvent omettre la sous-cha�ne
\f(CW<locale>\fP et stocker toutes les pages de manuel dans
\f(CW<manrep>\fP. Par exemple, les syst�mes qui n'ont que les pages de
manuel en anglais cod�es en ASCII peuvent stocker ces pages (les
r�pertoires \f(CWman<section>\fP) directement dans
\f(CW/usr/share/man\fP. (Ce qui repr�sente, en fait, la disposition et
les circonstances habituelles.)

Les pays pour lesquels un ensemble de codes de caract�res standard bien
accept� existe peuvent omettre le champ \f(CW\%<code_caract�re>\fP, mais
il est fortement recommand� de l'inclure, surtout pour les pays ayant
plusieurs normes en comp�tition.

Quelques exemples :

.TS
l l l l
l l l lfCW.
Langue	Territoire	Code de caract�re	R�pertoire
_
Anglais	\(em	ASCII	/usr/share/man/en
Anglais	Royaume Uni	ASCII	/usr/share/man/en_GB
Anglais	�tats-Unis	ASCII	/usr/share/man/en_US
Fran�ais	Canada	ISO 8859-1	/usr/share/man/fr_CA
Fran�ais	France	ISO 8859-1	/usr/share/man/fr_FR
Allemand	Allemagne	ISO 646	/usr/share/man/de_DE.646
Allemand	Allemagne	ISO 6937	/usr/share/man/de_DE.6937
Allemand	Allemagne	ISO 8859-1	/usr/share/man/de_DE.88591
Allemand	Suisse	ISO 646	/usr/share/man/de_CH.646
Japonais	Japon	JIS	/usr/share/man/ja_JP.jis
Japonais	Japon	SJIS	/usr/share/man/ja_JP.sjis
Japonais	Japon	UJIS (ou EUC-J)	/usr/share/man/ja_JP.ujis
.TE

De m�me, il faut faire de la place pour les pages de manuel d�pendant de
l'architecture, comme la documentation sur les pilotes de p�riph�riques
ou les commandes d'administration syst�me de bas niveau. Elles devraient
�tre plac�es dans un r�pertoire \f(CW<arch>\fP dans le r�pertoire
\f(CWman<section>\fP appropri� ; par exemple, une page de manuel pour la
commande i386 ctrlaltdel(8) peut �tre plac�e dans
\f(CW/usr/share/man/<locale>/man8/i386/ctrlaltdel.8\fP.

Les pages de manuel pour les commandes et les donn�es de
\f(CW/usr/local\fP sont stock�es dans \f(CW/usr/local/man\fP. Les pages
de manuel pour X11R6 sont stock�es dans \f(CW/usr/X11R6/man\fP. Il
s'ensuit que toutes les hi�rarchies de pages de manuel du syst�me
devraient avoir la m�me structure que \f(CW/usr/share/man\fP. Les
r�pertoires vides peuvent �tre oubli�s d'une hi�rarchie de pages de
manuel. Par exemple, si \f(CW/usr/local/man\fP n'a pas de pages de
manuel pour la section 4 (p�riph�riques), alors
\f(CW/usr/local/man/man4\fP peut �tre omis.

Les sections de pages cat (\f(CWcat<section>\fP) contenant des entr�es
de pages de manuel format�es se trouvent aussi dans les sous-r�pertoires
de \f(CW<manrep>/<locale>\fP, mais ne sont pas obligatoires ni ne
devraient �tre distribu�es � la place des pages de manuel en source nroff.

.\" d'autres sous-r�pertoires, ps<section>, dvi<section>, html<section>
.\" pourront y �tre ajout�s
.\" � revoir

Les sections num�rot�es "1" � "8" sont d�finies de mani�re
traditionnelle. En g�n�ral, le nom de fichier des pages de manuel
situ�es dans une section particuli�re se termine par \f(CW.<section>\fP.

De plus, certains grands ensembles de pages de manuel sp�cifiques � une
application poss�dent un suffixe suppl�mentaire accol� au nom de fichier
de la page de manuel. Par exemple, les pages de manuel du syst�me de
gestion de courrier MH devraient avoir \f(CWmh\fP accol� � toutes les
pages de manuel de MH. Toutes les pages de manuel du syst�me X Window
devraient avoir un \f(CWx\fP accol� au nom de fichier.

La pratique de placer les pages de manuel de diverses langues dans les
sous-r�pertoires appropri�s de \f(CW/usr/share/man\fP s'applique aussi
aux autres hi�rarchies de pages de manuel, comme \f(CW/usr/local/man\fP
et \f(CW/usr/X11R6/man\fP. (Cette partie de la norme s'appliquera aussi
plus loin dans la section sur la structure optionnelle
\f(CW/var/cache/man\fP.)

Voici une description de chaque section :

.BL
.LI
\f(CWman1\fP : programmes utilisateurs
.br
Les pages de manuel d�crivant les commandes accessibles � tous se
trouvent dans ce chapitre. La majeure partie de la documentation sur les
programmes dont aura jamais besoin un utilisateur se trouve ici.
.LI
\f(CWman2\fP : appels syst�me
.br
Cette section d�crit tous les appels syst�mes (requ�tes au noyau pour
effectuer des op�rations).
.\" enlever la remarque entre parenth�ses ? supposer les connaissances
.\" techniques ?
.LI
\f(CWman3\fP : fonctions et routines de la biblioth�que
.br
La section 3 d�crit les routines du programme de la biblioth�que qui ne
sont pas des appels directs aux services du noyau. Ceci ainsi que le
chapitre 2 ne sont vraiment int�ressants que pour les programmeurs.
.LI
\f(CWman4\fP : fichiers sp�ciaux
.br
La section 4 d�crit les fichiers sp�ciaux, les fonctions sp�cifiques aux
pilotes et le support r�seau disponible sur le syst�me. On y trouve
typiquement les fichiers de p�riph�riques de \f(CW/dev\fP et l'interface
noyau pour le support des protocoles r�seau.
.LI
\f(CWman5\fP : formats de fichiers
.br
Le format de nombreux fichiers de donn�es non intuitifs est document�
dans la section 5. Ceci comprend divers fichiers d'en-t�tes, les
fichiers de r�sultats de programmes et les fichiers syst�mes.
.LI
\f(CWman6\fP : jeux
.br
Ce chapitre documente les jeux, les d�monstrations et des programmes en
g�n�ral triviaux. Des personnes diff�rentes ont des notions vari�es sur
l'opportunit� de cette partie.
.LI
\f(CWman7\fP : divers
.br
Les pages de manuel difficiles � classer sont plac�es dans la section
7. Les paquetages de macros pour troff et autres processeurs de texte se
trouvent ici.
.LI
\f(CWman8\fP : administration syst�me
.br
La documentation pour les programmes utilis�s par les administrateurs
syst�me pour la bonne marche du syst�me et sa maintenance se trouve
ici. Certains de ces programmes sont aussi utiles de temps � autre pour
les utilisateurs normaux.
.LE

.H 3 "/usr/share/misc : diverses donn�es ind�pendantes de l'architecture"
.P
Ce r�pertoire contient divers fichiers ind�pendants de l'architecture
qui ne n�cessitent pas un sous-r�pertoire s�par� dans
\f(CW/usr/share\fP. C'est un r�pertoire obligatoire de
\f(CW/usr/share\fP.

Les fichiers suivants, s'ils sont pr�sents, devraient se trouver dans
\f(CW/usr/share/misc\fP :

.VL 2
.LI "\f(CW{"
ascii, magic, termcap, termcap.db }\fP
.LE

D'autres fichiers (sp�cifiques � une application) peuvent appara�tre
ici, mais un int�grateur peut les placer dans \f(CW/usr/lib\fP � sa
discr�tion. De tels fichiers comprennent :

.VL 2
.LI "\f(CW{"
airport, birthtoken, eqnchar, getopt, gprof.callg, gprof.flat,
inter.phone, ipfw.samp.filters, ipfw.samp.scripts, keycap.pcvt, mail.help,
mail.tildehelp, man.template, map3270, mdoc.template, more.help, na.phone,
nslookup.help, operator, scsi_modes, sendmail.hf, style, units.lib,
vgrindefs, vgrindefs.db, zipcodes }\fP
.LE

.H 2 "/usr/src : code source"
.P
Tout code source non local devrait �tre plac� dans ce sous-r�pertoire.

.SK
.H 1 "La hi�rarchie /var"
.PS
copy "draft.pic"
dir(/var,Donn�es variables)
sub("account","Historique de la comptabilit� des processus (si support�)")
sub("cache","Donn�es de cache des applications")
sub("crash","Donn�es brutes des plantages syst�me (si support�)")
sub("games","Donn�es variables des jeux")
sub("lock","Fichiers de verrous")
sub("log","Fichiers et r�pertoires d'historique")
sub("mail","Fichiers de bo�tes aux lettres utilisateurs")
sub("opt","Donn�es variables de /opt")
sub("run","Fichiers relatifs aux processus en cours")
sub("spool","Donn�es en attente pour les applications")
sub("state","Informations variables d'�tat")
sub("tmp","Fichiers temporaires pr�serv�s entre les red�marrages du syst�me")
sub("yp","fichiers de base de donn�es de NIS (Network Information Service)")
.PE
.P
\f(CW/var\fP contient des fichiers de donn�es variables. Ceci inclut les
r�pertoires et fichiers en attente (spool), les donn�es administratives
et d'historique, et les fichiers transitoires et temporaires.

Certaines parties de \f(CW/var\fP ne sont pas partageables entre des
syst�mes diff�rents. Par exemple, \f(CW/var/log\fP, \f(CW/var/lock\fP et
\f(CW/var/run\fP. D'autres parties peuvent �tre partag�es, comme
notamment \f(CW/var/mail\fP, \f(CW/var/cache/man\fP,
\f(CW/var/cache/fonts\fP et \f(CW/var/spool/news\fP.

\f(CW/var\fP est ici sp�cifi� afin de rendre possible le montage de
\f(CW/usr\fP en lecture seule. Tout ce qui allait auparavant dans
\f(CW/usr\fP sur lequel on �crivait pendant la marche du syst�me (au
contraire de l'installation et de la maintenance logicielle) doit aller
dans \f(CW/var\fP.

Si on ne peut pas faire de \f(CW/var\fP une partition s�par�e, il est
souvent pr�f�rable de d�placer \f(CW/var\fP hors de la partition racine
� l'int�rieur de la partition \f(CW/usr\fP. (Ceci est parfois effectu�
pour r�duire la taille de la partition racine ou quand l'espace se
r�duit dans la partition racine.) Cependant, \f(CW/var\fP ne devrait pas
�tre un lien vers \f(CW/usr\fP parce que cela rend la s�paration de
\f(CW/usr\fP et \f(CW/var\fP plus difficile et rend plus probable la
cr�ation d'un conflit de noms. � la place, faites un lien de
\f(CW/var\fP vers \f(CW/usr/var\fP.

Les applications ne devraient en g�n�ral pas ajouter de r�pertoire au
niveau sup�rieur de \f(CW/var\fP. De tels r�pertoires ne devraient �tre
ajout�s que s'ils concernent le syst�me en entier, et en consultant la
liste de distribution \*(Fs.

Les r�pertoires \f(CWcache\fP, \f(CWlock\fP, \f(CWlog\fP, \f(CWrun\fP,
\f(CWspool\fP, \f(CWstate\fP et \f(CWtmp\fP doivent �tre inclus et
utilis�s dans toutes les distributions ; les r�pertoires
\f(CWaccount\fP, \f(CWcrash\fP, \f(CWgames\fP, \f(CWmail\fP et
\f(CWyp\fP doivent �tre inclus et utilis�s si les applications ou
possibilit�s correspondantes sont fournies dans la distribution.

Les versions pr�c�dentes de la FSSTND incluaient une hi�rarchie
\f(CW/var/lib\fP. Pour plus d'informations, voir la section sur
\f(CW/var/state\fP.

Plusieurs r�pertoires sont `r�serv�s' dans le sens o� ils ne devraient
pas �tre utilis�s de fa�on arbitraire par une application nouvelle,
puisqu'ils entreraient en conflit avec une pratique historique et/ou
locale. Ce sont :

.nf
.ft CW
    /var/backups
    /var/cron
    /var/lib
    /var/local
    /var/msgs
    /var/preserve
.ft P
.fi

.H 2 "/var/account : historique de la comptabilit� des processus (si support�)"
.P
Ce r�pertoire contient l'historique en cours de la comptabilit� des
processus et les donn�es compos�es d'utilisation des processus (utilis�s
dans certains syst�me \*(Ux par \f(CWlastcomm\fP et \f(CWsa\fP).

.H 2 "/var/cache : donn�es de cache des applications"
.P
.PS
copy "draft.pic"
dir(/var/cache,r�pertoires de cache)
sub("fonts","fontes g�n�r�es en local")
sub("man","pages de manuel format�es en local")
sub("www","donn�es de cache ou de proxy WWW")
sub("<paquetage>","donn�es de cache sp�cifiques � paquetage")
.PE

\f(CW/var/cache\fP est fait pour les donn�es de cache des
applications. De telles donn�es sont g�n�r�es localement comme r�sultat
d'entr�es-sorties ou de calculs qui prennent du temps. L'application
doit �tre capable de reg�n�rer ou de retrouver les donn�es. � la
diff�rence de \f(CW/var/spool\fP, les fichiers cach�s peuvent �tre
effac�s sans perte de donn�es. Les donn�es doivent rester valides entre
deux lancements de l'application et apr�s le red�marrage du syst�me.

Les fichiers situ�s dans /var/cache peuvent expirer d'une mani�re
sp�cifique � l'application, par l'administrateur syst�me ou des deux
mani�res. L'application devrait toujours �tre capable de s'en remettre
suite � un effacement manuel des fichiers (g�n�ralement � cause d'un
manque d'espace disque). Aucune autre obligation n'est faite concernant
le format des donn�es dans les r�pertoires de cache.

.HU "Raison d'�tre :"
.br
.P
L'existence d'un r�pertoire s�par� pour les donn�es de cache permet aux
administrateurs syst�me d'attribuer une politique de disque et de
sauvegarde diff�rente des autres r�pertoires de \f(CW/var\fP.

.H 3 "/var/cache/fonts : fontes g�n�r�es en local"
.P
Le r�pertoire \f(CW/var/cache/fonts\fP devrait �re utilis� pour stocker
toute fonte cr��e de mani�re dynamique. En particulier,
\f(CW/var/cache/fonts/pk\fP stockera toutes les fontes automatiquement
g�n�r�es par \f(CWMakeTeXPK\fP.

Il devrait y avoir un lien de \f(CW/usr/lib/texmf/fonts/tmp\fP vers
\f(CW/var/cache/fonts\fP. Ce lien permet aux utilisateurs d'utiliser le
chemin unique \f(CW/usr/lib/texmf/fonts/tfm\fP quand ils effectuent des
changements � leur variable d'environnement TEXFONTS. (Ceci est le
chemin par d�faut pour les outils \*(Tx de Karl Berry, distribu�s sur
\f(CWftp.cs.umb.edu:/pub/tex\fP.\*F

.FS
La raison pour laquelle les outils de Karl Berry sont mentionn�s est
qu'ils sont le standard de-facto pour les installations \*(Ux de
\*(Tx. Ces outils sont largement utilis�s dans la communaut� \*(Ux
libre.

.FE
Si une autre distribution \*(Tx est utilis�e, un lien du r�pertoire de
fontes appropri� vers \f(CW/var/cache/fonts\fP devrait �tre fait.)

Le \f(CWMakeTeXPK\fP distribu� avec \f(CWdvipsk\fP placera les
fichiers \f(CW.pk\fP dans \f(CWfonts/pk/<p�riph�rique>/<nom_fonte>\fP (par
exemple, \f(CWfonts/pk/CanonCX/cmr10.300pk\fP). Les fichiers \f(CW.pk\fP
peuvent �tre p�riodiquement purg�s de l'arborescence
\f(CW/var/cache/fonts\fP ou peuvent �tre d�plac�s dans l'arborescence
\f(CW/usr/lib/texmf\fP. Si des g�n�rateurs automatiques de \f(CW.mf\fP
ou \f(CW.tfm\fP sont utilis�s, ils devraient placer leurs donn�es dans
les sous-r�pertoires \f(CWmf\fP ou \f(CWtfm\fP de
\f(CW/var/cache/fonts\fP.

D'autres fontes cr��es dynamiquement peuvent aussi �tre plac�es dans
cette arborescence, dans des sous-r�pertoires de
\f(CW/var/cache/fonts\fP nomm�s de mani�re ad�quate.

.H 3 "/var/cache/man : pages de manuel format�es en local (optionnel)"
.P
Ce r�pertoire fournit un emplacement standard pour les sites qui
fournissent une partition \f(CW/usr\fP en lecture seule, mais qui
veulent permettre le cache des pages de manuel format�es en local. Les
sites qui montent \f(CW/usr\fP en �criture (par exemple, les
installations en utilisateur unique) peuvent choisir de ne pas utiliser
\f(CW/var/cache/man\fP et peuvent �crire les pages de manuel format�es
dans les r�pertoires \f(CWcat<section>\fP directement dans
\f(CW/usr/share/man\fP. Nous recommandons que la plupart des sites
utilisent plut�t l'une des options suivantes :

.BL
.LI
Pr�formater toutes les pages de manuel � c�t� des versions non format�es.
.LI
Ne permettre aucun cache des pages de manuel format�es et obliger �
refaire le formatage � chaque utilisation d'une page de manuel.
.LI
Permettre le cache local des pages de manuel format�es dans
\f(CW/var/cache/man\fP.
.LE
.P
La structure de \f(CW/var/cache/man\fP n�cessite de refl�ter � la fois
les hi�rarchies multiples de pages de manuel et la possibilit� d'un
support multilingue.

�tant donn�e une page de manuel non format�e qui appara�t normalement
dans \f(CW<chemin>/man/<locale>/man<section>\fP, le r�pertoire pour
placer les pages de manuel format�es s'appelle
\f(CW/var/cache/man/<chemin_cat>/<locale>/cat<section>\fP, o�
\f(CW<chemin_cat>\fP s'inspire de \f(CW<chemin>\fP en enlevant toute
composante de nom de chemin \f(CWusr\fP au d�but et/ou \f(CWshare\fP � la
fin. (Notez que la composante \f(CW<locale>\fP peut ne pas �tre pr�sente.)

.\" Notez que /usr/local/man et /local/man entreront en conflit si un
.\" administrateur syst�me est assez timbr� pour utiliser les deux pour
.\" des choses diff�rentes.
Par exemple, \f(CW/usr/share/man/man1/ls.1\fP sera format�e en
\f(CW/var/cache/man/cat1/ls.1\fP et
\f(CW/usr/X11R6/man/<locale>/man3/XtClass.3x\fP en
\f(CW/var/cache/man/X11R6/<locale>/cat3/XtClass.3x\fP.

Les pages de manuel �crites dans \f(CW/var/cache/man\fP peuvent � la fin
�tre transf�r�es vers les r�pertoires pr�format�s appropri�s dans la
hi�rarchie source \f(CWman\fP ou bien expir�es ; De m�me, les pages
de manuel format�es dans la hi�rarchie source \f(CWman\fP peuvent �tre
expir�es si personne n'y a acc�d� pendant une certaine p�riode de temps.

Si les pages de manuel pr�format�es sont distribu�es avec un syst�me sur
des supports en lecture seule (un CD-ROM, par exemple), elles seront
install�es dans la hi�rarchie source \f(CWman\fP (par exemple
\f(CW/usr/share/man/cat<section>\fP). \f(CW/var/cache/man\fP est r�serv�
comme cache dans lequel on peut �crire les pages de manuel
format�es.

.HU "Raison d'�tre :"
.br
.P
La version 1.2 de la norme sp�cifiait \f(CW/var/catman\fP pour cette
hi�rarchie. Le chemin a �t� chang� en \f(CW/var/cache\fP pour mieux
refl�ter la nature dynamique des pages de manuel format�es. Le nom du
r�pertoire a �t� chang� en \f(CWman\fP pour permettre l'agrandissement
de la hi�rarchie et inclure des formats trait�s autres que "cat", comme
PostScript, HTML ou DVI.

.H 2 "/var/crash : donn�es brutes des plantages syst�me (si support�)"
.P
Ce r�pertoire contient des donn�es brutes (dump) des plantages
syst�me. � la date de la sortie de cette norme, les donn�es brutes de
plantage syst�me n'�taient pas support�s par Linux.

.H 2 "/var/games : donn�es variables des jeux"
.P
Toute donn�e variable concernant les jeux de \f(CW/usr\fP devrait �tre
plac�e ici. \f(CW/var/games\fP devrait contenir les donn�es variables
qu'on trouvait auparavant dans \f(CW/usr\fP ; Les donn�es statiques
telles que les textes d'aide, les descriptions de niveaux et ainsi de
suite devraient rester autre part, comme dans \f(CW/usr/share/games\fP.

Comme pour \f(CW/var/state\fP, les donn�es variables des jeux peuvent
�tre plac�es dans \f(CW/var/lib\fP en tant que mesure de transition
obsol�te. Cependant, cette permission sera lev�e dans une version future
de la norme.

.HU "Raison d'�tre :"
.br
.P
On a donn� une hi�rarchie \f(CW/var/games\fP � part enti�re, plut�t que
de le laisser m�lang� avec l'ancien \f(CW/var/lib\fP comme dans la
version 1.2. La s�paration permet un contr�le local des strat�gies de
sauvegarde, permissions et utilisation des disques, ainsi que de
permettre un partage inter-machines et de r�duire l'encombrement de
\f(CW/var/state\fP. De plus, \f(CW/var/games\fP est le chemin utilis�
traditionnellement par BSD.

.H 2 "/var/lock : fichiers de verrous"
.P
Les fichiers de verrous devraient �tre stock�s dans la structure de
r�pertoires \f(CW/var/lock\fP.

Les fichiers de verrous de p�riph�riques, tels les fichiers de verrous
du p�riph�rique s�rie qu'on trouvait d'habitude soit dans
\f(CW/usr/spool/locks\fP soit dans \f(CW/usr/spool/uucp\fP doivent
maintenant �tre stock�s dans \f(CW/var/lock\fP. La convention de nommage
� utiliser est
.ie t \{\
\f(CWLCK..\fP suivi du nom de base du p�riph�rique. Par exemple, pour
verrouiller \f(CW/dev/cua0\fP le fichier \f(CWLCK..cua0\fP serait cr��.
\}
.el \{\
"LCK.." suivi du nom de base du p�riph�rique. Par exemple, pour
verrouiller /dev/cua0 le fichier "LCK..cua0" serait cr��.
\}

Le format utilis� pour les fichiers de verrous de p�riph�rique doit �tre
le format de fichier de verrou HDB UUCP. Le format HDB permet de stocker
l'identificateur du processus (PID) comme un nombre d�cimal ASCII sur dix
octets, avec un signe nouvelle ligne � la fin. Par exemple, si le
processus 1230 tenait un fichier de verrou, il contiendrait les onze
caract�res : espace, espace, espace, espace, espace, espace, un, deux,
trois, z�ro et nouvelle ligne.

.\" Certaines versions d'UUCP ajoutent une deuxi�me ligne indiquant quel
programme a cr�� le verrou (uucp, cu ou getty).
Alors, tout ce qui voudrait utiliser \f(CW/dev/cua0\fP peut lire le
fichier de verrou et agir en cons�quence (tous les verrous de
\f(CW/var/lock\fP devraient �tre lisibles par tout le monde).

.H 2 "/var/log : fichiers et r�pertoires d'historique"
.P
Le r�pertoire contient divers fichiers d'historique (log). La plupart
des historiques devraient �tre �crits dans ce r�pertoire ou dans un
sous-r�pertoire appropri�.
.TS
tab(@);
lfCW l.
lastlog@enregistrement du dernier login de chaque utilisateur
messages@messages syst�me de \f(CWsyslogd\fP
wtmp@enregistrement de chaque login et logout
.TE

.H 2 "/var/mail : fichiers de bo�tes aux lettres utilisateurs"
.P
Ce r�pertoire contient les fichiers de bo�tes aux lettres utilisateurs
stock�s dans le format de bo�tes aux lettres \*(Ux standard.

.HU "Raison d'�tre :"
.br
.P
Ce r�pertoire a �t� relog� de \f(CW/var/spool/mail\fP pour amener \*(Fs
en accord avec la plupart des impl�mentations \*(Ux. Ce changement est
important pour l'interop�rabilit� puisqu'un \f(CW/var/mail\fP unique est
souvent partag� entre plusieurs machines et plusieurs impl�mentations
d'\*(Ux (malgr� les probl�mes de verrouillage NFS).

.H 2 "/var/opt : donn�es variables de /opt"
.P
Les donn�es variables devraient �tre install�es dans
\f(CW/var/opt/<paquetage>\fP, o� \f(CW<paquetage>\fP est le nom de la
sous-arborescence de \f(CW/opt\fP o� les donn�es statiques de tout
paquetage logiciel suppl�mentaire sont stock�es, sauf quand elles sont
supplant�es par un autre fichier dans \f(CW/etc\fP. Aucune structure
n'est impos�e sur la disposition interne de
\f(CW/var/opt/<paquetage>\fP.

.HU "Raison d'�tre :"
.br
.P
Voir la raison d'�tre pour \f(CW/opt\fP.

.H 2 "/var/run : fichiers variables d'ex�cution"
.P
Ce r�pertoire contient des fichiers d'information syst�me d�crivant le
syst�me depuis qu'il a d�marr�. Les fichiers de ce r�pertoire devraient
�tre nettoy�s (enlev�s ou tronqu�s selon le cas) au d�but du processus
de d�marrage.

Les fichiers d'identification de processus (PID), qui �taient plac�s �
l'origine dans \f(CW/etc\fP, devraient �tre plac�s dans
\f(CW/var/run\fP. La convention de nommage des fichiers PID est
\f(CW<nom_programme>.pid\fP. Par exemple, le fichier PID de
\f(CWcrond\fP est nomm� \f(CW/var/run/crond.pid\fP.

Le format interne des fichiers PID reste inchang�. Le fichier consiste
en un identificateur de processus en d�cimal cod� ASCII, suivi d'un
caract�re nouvelle ligne. Par exemple, si \f(CWcrond\fP �tait le
processus num�ro 25, \f(CW/var/run/crond.pid\fP contiendrait trois
caract�res : deux, cinq et nouvelle ligne.

Les programmes qui lisent les fichiers PID devraient �tre assez souples
dans ce qu'ils acceptent ; par exemple, ils devraient ignorer les
espaces blancs suppl�mentaires, les z�ros au d�but, l'absence d'une
nouvelle ligne � la fin ou les lignes suppl�mentaires dans le fichier
PID. Les programmes qui cr�ent des fichiers PID devraient utiliser la
sp�cification simple situ�e dans le paragraphe ci-dessus.

Le fichier \f(CWutmp\fP, qui stocke les informations sur qui est en
train d'utiliser le syst�me, est plac� dans ce r�pertoire.

Les programmes qui gardent des sockets du domaine \*(Ux temporaires
devraient les placer dans ce r�pertoire.

.H 2 "/var/spool : donn�es en attente pour les applications"
.PS
copy "draft.pic"
dir(/var/spool,R�pertoires d'attente)
sub("lpd","R�pertoire d'attente de l'imprimante")
sub("mqueue","File d'attente du courrier � l'exp�dition")
sub("news","R�pertoire d'attente des news")
sub("rwho","Fichiers rwhod")
sub("smail","R�pertoire d'attente pour smail")
sub("uucp","R�pertoire d'attente pour UUCP")
.PE
\f(CW/var/spool\fP contient des donn�es en attente d'un traitement
ult�rieur. Les donn�es de \f(CW/var/spool\fP repr�sentent un travail �
faire dans le futur (par un programme, un utilisateur ou un
administrateur) ; les donn�es sont souvent effac�es apr�s avoir �t�
trait�es.

.ig
\f(CW/var/spool\fP est fait pour les donn�es `en attente' des
applications. De telles donn�es restent valides m�me si l'application
qui les a cr��es s'arr�te et red�marre. Quelque temps apr�s avoir �t�
cr��es, les donn�es sont enlev�es automatiquement, d'une mani�re
sp�cifique � l'application ; cela arrive typiquement quand un �v�nement
quelconque se produit (par exemple, lpd imprime le fichier, ou sendmail
l'envoie) ou quand une limite de temps parvient � sa fin (par exemple,
un article de news). Les donn�es de \f(CW/var/spool\fP sont en g�n�ral
int�ressantes pour l'utilisateur en soi et pour soi, � la diff�rence des
donn�es de \f(CW/var/state\fP, qui n'ont g�n�ralement d'int�r�t
qu'indirectement.
..
Les fichiers de verrou UUCP doivent �tre plac�s dans
\f(CW/var/lock\fP. Voir la section ci-dessus � propos de
\f(CW/var/lock\fP.

.H 3 "/var/spool/lpd : file d'impression du daemon de l'imprimante ligne"
.PS
copy "draft.pic"
dir(/var/spool/lpd,R�pertoire d'attente de l'imprimante)
sub("<imprimante>","File d'attente d'une imprimante sp�cifique (optionnel)")
.PE

Le fichier de verrou de \f(CWlpd\fP, \f(CWlpd.lock\fP, devrait �tre
plac� dans \f(CW/var/spool/lpd\fP. Nous sugg�rons que le fichier de
verrou de chaque imprimante soit plac� dans le r�pertoire d'attente
sp�cifique � cette imprimante et soit nomm� \f(CWlock\fP.

.H 3 "/var/spool/rwho : fichier rwhod"
.sp
Ce r�pertoire contient les informations rwhod d'autres syst�mes sur le
r�seau local.

.HU "Raison d'�tre :"
.br
.P
Certaines versions de BSD utilisent \f(CW/var/rwho\fP pour ces donn�es ;
�tant donn� leur emplacement historique dans \f(CW/var/spool\fP sur
d'autres syst�mes et sa convenance approximative � la d�finition de
donn�es `en attente', cet emplacement a �t� estim� plus appropri�.

.H 2 "/var/state : informations variables d'�tat"

.PS
copy "draft.pic"
dir(/var/state,Informations variables d'�tat)
sub("<�diteur>","Fichiers de sauvegarde et �tat d'�diteurs")
.\" sub("elvis","Saved files after crash or hang-up from elvis")
.\" sub("emacs","State directory for Emacs")
sub("misc","Diverses donn�es d'�tat")
.\" sub("news","Fichiers variables pour Cnews/INN")
.\" sub("nvi","Fichiers sauvegard�s apre�s le plantage ou le bloquage de nvi")
sub("xdm","Donn�es variables du gestionnaire d'affichage X xdm")
sub("<pkgtool>","Fichiers d'aide au paquetage")
sub("<paquetage>","Donn�es d'�tat pour les paquetages et les sous-syst�mes")
.PE

Cette hi�rarchie contient des informations d'�tat se rapportant � une
application ou au syst�me. Les informations d'�tat sont des donn�es que
les programmes modifient au cours de leur ex�cution, relatives � une
machine sp�cifique. Les utilisateurs ne devraient jamais avoir besoin de
modifier des fichiers dans \f(CW/var/state\fP pour configurer la bonne
marche d'un paquetage.

Les informations d'�tat sont utilis�es en g�n�ral pour pr�server
l'environnement d'une application (ou d'un groupe d'applications li�es
entre elles) entre plusieurs lancements et entre plusieurs instances de
la m�me application. Les informations d'�tat devraient en g�n�ral rester
valides apr�s un red�marrage, ne devraient pas garder l'historique
de sortie d'un programme et ne devraient pas �tre des donn�es en attente.
.\" (mais notez que emacs/lock en est une exception),

Une application (ou un groupe d'applications li�es) devrait utiliser un
sous-r�pertoire de \f(CW/var/state\fP pour ses donn�es. Il y a un
sous-r�pertoire obligatoire, \f(CW/var/state/misc\fP, fait pour les
fichiers d'�tat qui n'ont pas besoin de sous-r�pertoire ; les autres
sous-r�pertoires ne devraient �tre pr�sents que si l'application en
question est incluse dans la distribution.

\f(CW/var/state/<nom>\fP est l'endroit � utiliser pour tout le support
de paquetage de la distribution. Des distributions diff�rentes peuvent
�videmment utiliser des noms diff�rents.

Les versions pr�c�dentes de cette norme utilisaient le nom
\f(CW/var/lib\fP pour cette hi�rarchie. \f(CW/var/lib\fP est obsol�te,
mais peut �tre utilis� en parall�le avec la hi�rarchie obligatoire
\f(CW/var/state\fP, comme mesure transitoire pour les donn�es
sp�cifiques aux applications. Notez cependant que cette permission sera
retir�e dans une version future de la norme. Par contre, vous pouvez
faire un lien symbolique de \f(CW/var/lib\fP vers \f(CW/var/state\fP.

.HU "Raison d'�tre :"
.br
.P
\f(CW/usr/lib\fP est de plus en plus utilis� uniquement pour les
fichiers objets ou leurs archives ; ceci est vrai pour les variantes BSD
\*(Ux actuelles ainsi que les paquetages GNU actuels. Dans le m�me
ordre d'id�es, le nom \f(CW/var/lib\fP semblait inappropri�.

BSD utilise le nom \f(CW/var/db\fP pour un r�pertoire similaire. Ce nom
semblait trop restrictif, puisqu'il impliquait une structure de
r�pertoires faite tout d'abord pour les fichiers de base de donn�es
(\f(CW.db\fP).

.H 3 "/var/state/<�diteur> : fichiers de sauvegarde et �tat d'un �diteur"
.sp
Ces r�pertoires contiennent des fichiers sauvegard�s par toute fin
inattendue d'un �diteur (par exemple elvis, jove, nvi).

D'autres �diteurs peuvent ne pas demander de r�pertoire pour les
fichiers de sauvegarde de plantage, mais peuvent n�cessiter un endroit
bien d�fini pour stocker d'autres informations pendant que l'�diteur
fonctionne. Ces informations devraient �tre stock�es dans un
sous-r�pertoire de \f(CW/var/state\fP (par exemple, GNU Emacs placerait
ses fichiers de verrou dans \f(CW/var/state/emacs/lock\fP).

Des �diteurs futurs pourront avoir besoin d'informations d'�tat
suppl�mentaires au-del� des fichiers de sauvegarde de plantage et des
fichiers de verrou -- ces informations devraient aussi �tre plac�es dans
\f(CW/var/state/<�diteur>\fP.

.HU "Raison d'�tre :"
.br
.P
Les versions pr�c�dentes de Linux, ainsi que tous les distributeurs du
commerce, utilisaient \f(CW/var/preserve\fP pour vi et ses
clones. Cependant, chaque �diteur utilise son propre format pour ces
fichiers de sauvegarde de plantage, c'est pourquoi un r�pertoire s�par�
est n�cessaire � chaque �diteur.

Les fichiers de verrous sp�cifiques � chaque �diteur sont en g�n�ral
assez diff�rents des fichiers de verrous de p�riph�rique ou de
ressources stock�s dans \f(CW/var/lock\fP et de ce fait sont stock�s
dans \f(CW/var/state\fP.

.H 3 "/var/state/misc : diverses donn�es variables"
.SP
Ce r�pertoire contient des donn�es variables qui ne sont pas plac�es
dans un sous-r�pertoire de \f(CW/var/state\fP. Il serait judicieux
d'utiliser dans la mesure du possible des noms uniques dans ce
r�pertoire pour �viter les conflits de noms.

Notez que cette hi�rarchie devrait contenir les fichiers de
\f(CW/var/db\fP des versions actuelles de BSD. Celles-ci comprennent
\f(CWlocate.database\fP et \f(CWmountdtab\fP, et la (les) base(s) de
donn�es des symboles du noyau.

.H 2 "/var/tmp : fichiers temporaires pr�serv�s entre les red�marrages du syst�me"
.P
Le r�pertoire \f(CW/var/tmp\fP est mis � la disposition des programmes
qui ont besoin de fichiers ou de r�pertoires temporaires pr�serv�s entre
les red�marrages du syst�me. Par cons�quent, les donn�es stock�es dans
\f(CW/var/tmp\fP restent plus longtemps que les donn�es de \f(CW/tmp\fP.

Les fichiers et r�pertoires situ�s dans \f(CW/var/tmp\fP ne doivent pas
�tre effac�es quand le syst�me d�marre. Bien que les donn�es stock�es
dans \f(CW/var/tmp\fP soient typiquement effac�es d'une mani�re
sp�cifique au site, il est recommand� que l'effacement se fasse dans un
intervalle plus long que pour \f(CW/tmp\fP.

.ig
Un lien symbolique \f(CW/var/tmp/vi.recover\fP vers
\f(CW/var/state/nvi\fP est permis pour supporter les versions de nvi
compil�es sans le nom de chemin sugg�r� dans la norme.

Les programmes ne doivent pas supposer que tout fichier ou r�pertoire
est pr�serv� entre deux appels du programme.
..
.\" XXX - Pourquoi le deuxi�me paragraphe a �t� mis en commentaire ?

.H 2 "/var/yp : fichiers de base de donn�es NIS (Network Information Service)"
.P
Les donn�es variables du Service d'Information R�seau (NIS ou Network
Information Service), qu'on appelait auparavant Pages Jaunes Sun (YP ou
Sun Yellow Pages), seront plac�es dans ce r�pertoire.

.HU "Raison d'�tre :"
.br
.P
\f(CW/var/yp\fP est le r�pertoire normal des donn�es NIS (YP) et est
utilis� presque exclusivement dans la documentation et les syst�mes NIS.

Il ne faut pas confondre NIS et Sun NIS+, qui utilise un r�pertoire
diff�rent, \f(CW/var/nis\fP.

.SK
.H 1 "Annexe sp�cifique aux syst�mes d'exploitation"
.P
Cette section contient des obligations et recommandations
suppl�mentaires qui ne s'appliquent qu'� un syst�me d'exploitation
sp�cifique. Les principes de cette section ne devraient jamais entrer en
conflit avec la norme de base.

.H 2 "Linux"
.P
Voici l'annexe pour le syst�me d'exploitation Linux.

.H 3 "/ : r�pertoire racine"
.sp
Sur les syst�mes Linux, si le noyau est situ� dans \f(CW/\fP, nous
recommandons d'utiliser les noms \f(CWvmlinux\fP ou \f(CWvmlinuz\fP, qui
sont utilis�s dans les paquetages r�cents de sources du noyau Linux.

.H 3 "/dev : fichiers de p�riph�riques et fichiers sp�ciaux"
.sp
Tous les fichiers de p�riph�riques et fichiers sp�ciaux de \f(CW/dev\fP
devraient se conformer au document \fILinux Allocated Devices\fP
(p�riph�riques allou�s dans Linux), que l'on trouve dans les sources du
noyau Linux. Il est maintenu par H. Peter Anvin <hpa@zytor.com>.

Les liens symboliques de \f(CW/dev\fP ne devraient pas �tre distribu�s
avec des syst�mes Linux sauf s'ils sont fournis dans le document
\fILinux Allocated Devices\fP.

.HU "Raison d'�tre :"
.br
.P
L'obligation de ne pas faire de liens symboliques au hasard est donn�e
parce que la configuration locale diff�re souvent de celle de la machine
de d�veloppement du distributeur. De plus, si un script d'installation
de distribution configure les liens symboliques au moment de
l'installation, ces liens symboliques ne seront souvent pas mis � jour
si des changements locaux ont lieu sur le mat�riel. Utilis�s de mani�re
responsable � un niveau local, cependant, on peut les utiliser � bon
escient.

.H 3 "/proc : syst�me de fichiers virtuel d'information sur le noyau et les processus"
.sp
Le syst�me de fichiers proc devient la m�thode standard de-facto sur
Linux pour manipuler les processus et les informations du syst�me,
plut�t que par \f(CW/dev/kmem\fP et autres m�thodes similaires. Nous
encourageons fortement ce principe pour le stockage et l'obtention
d'informations sur un processus ainsi que d'autres informations sur le
noyau et la m�moire.

.H 3 "/sbin : binaires syst�mes essentiels"
.sp
Les syst�mes Linux placent ces fichiers suppl�mentaires dans
\f(CW/sbin\fP.

.BL
.LI
Commandes du syst�me de fichiers �tendu deuxi�me version (ext2 --
optionnel) :
.VL 2
.LI "\f(CW{"
badblocks, dumpe2fs, e2fsck, mke2fs, mklost+found, tune2fs }\fP
.LE

.LI
Installateur de carte du chargeur de d�marrage :
.VL 2
.LI "\f(CW{"
lilo }\fP
.LE

.HU "Fichiers optionnels de /sbin :"
.BL
.LI
Binaires statiques :
.SP
.VL 2
.LI "\f(CW{"
ldconfig, sln, ssync }\fP
.LE
.P
Les binaires statiques \f(CWln\fP (\f(CWsln\fP) et \f(CWsync\fP
(\f(CWssync\fP) sont utiles quand quelque chose va mal. L'utilisation
principale de \f(CWsln\fP (pour r�parer les liens symboliques incorrects
dans \f(CW/lib\fP apr�s une mise � jour mal faite) n'est plus d'une
importance cruciale maintenant que le programme \f(CWldconfig\fP (situ�
g�n�ralement dans \f(CW/usr/sbin\fP) existe et peut agir comme guide
dans certaines situations d'urgence. Notez qu'ils ne doivent pas
forc�ment �tre des versions li�es en statique des commandes normales
\f(CWln\fP et \f(CWsync\fP, mais elle peuvent l'�tre.

Le binaire \f(CWldconfig\fP est optionnel dans \f(CW/sbin\fP puisqu'un
site peut choisir de lancer \f(CWldconfig\fP au d�marrage plut�t qu'� la
mise � jour des biblioth�ques partag�es. (Il n'est pas clair qu'il soit
avantageux de lancer \f(CWldconfig\fP � chaque d�marrage.) M�me ainsi,
certaines personnes aiment avoir \f(CWldconfig\fP � port�e de clavier
pour les situations suivantes (toutes trop fr�quentes) :

.LB 8 4 " " 3
.LI
Je viens d'enlever \f(CW/lib/<file>\fP.
.LI
Je ne peux pas trouver le nom de la biblioth�que parce que \f(CWls\fP
est li� en dynamique, j'utilise un shell qui n'a pas \f(CWls\fP int�gr�
et je ne sais pas utiliser "\f(CWecho *\fP" � la place.
.LI
J'ai un \f(CWsln\fP en statique, mais je ne sais pas comment appeler le
lien.
.LE
.LI
Divers :
.SP
.VL 2
.LI "\f(CW{"
ctrlaltdel, kbdrate }\fP
.LE
.P
Pour pallier au fait que certains claviers sont livr�s avec une
fr�quence de r�p�tition si grande qu'ils en sont inutilisables,
\f(CWkbdrate\fP peut �tre install� dans \f(CW/sbin\fP sur certains
syst�mes.
.\" devons-nous conseiller d'installer ceci ?

Puisque l'action par d�faut dans le noyau pour la combinaison de touches
Ctrl-Alt-Del est un red�marrage brutal instantan�, il est recommandable
de d�sactiver ce comportement avant de monter le syst�me de fichiers
racine en mode lecture/�criture. Certaines versions d'\f(CWinit\fP sont
capables de d�sactiver Ctrl-Alt-Del, mais d'autres n�cessitent le
programme \f(CWctrlaltdel\fP qui peut �tre install� dans \f(CW/sbin\fP
sur ces syst�mes.

.LE

.H 3 "/usr/include : fichiers d'en-t�te inclus par les programmes C"
.sp
Les liens symboliques suivants sont n�cessaires si un compilateur C ou
C++ est install�.

.nf
.ft CW
    /usr/include/asm -> /usr/src/linux/include/asm-<arch>
    /usr/include/linux -> /usr/src/linux/include/linux
.ft P
.fi

.H 3 "/usr/src : code source"
.sp
Le seul code source qui doit �tre plac� dans un endroit sp�cifique est
le code source du noyau Linux. Il est situ� dans \f(CW/usr/src/linux\fP.

Si un compilateur C ou C++ est install�, mais que le code source complet
du noyau Linux n'est pas install�, les fichiers d'en-t�te du code source
du noyau devront �tre situ�s dans ces r�pertoires :

.nf
.ft CW
    /usr/src/linux/include/asm-<arch>
    /usr/src/linux/include/linux
.ft P
.fi

\f(CW<arch>\fP est le nom de l'architecture du syst�me.

.ft I
Note : \f(CW/usr/src/linux\fP peut �tre un lien symbolique vers
l'arborescence r�elle du code source du noyau.

.ft P

.HU "Raison d'�tre :"
.br
.P
Il est important que les fichiers d'en-t�tes du noyau soient situ�s dans
\f(CW/usr/src/linux\fP et non dans \f(CW/usr/include\fP pour qu'il n'y
ait pas de problemes quand les administrateurs syst�me mettent � jour la
version du noyau pour la premi�re fois.

.H 3 "/var/spool/cron : travaux cron et at"
.P
Ce r�pertoire contient les donn�es variables pour les programmes cron et
at.

.SK
.\" -------------------------------------------------------------------
.\" Partie finale
.\" -------------------------------------------------------------------
.nr Hu 3
.HU "La liste de distribution \*(Fs"
.P
La liste de distribution \*(Fs est situ�e �
<fhs-discuss@ucsd.edu>. Pour vous abonner � la liste envoyez un courrier
� <listserv@ucsd.edu> avec dans le corps du message "\f(CWADD
fhs-discuss\fP".

Merci � Network Operations � l'universit� de Californie � San Diego qui
nous a autoris�s � utiliser leur super serveur de listes de
distribution.

Comme il est indiqu� dans l'introduction, veuillez ne pas envoyer de
courrier � la liste de distribution sans d'abord contacter l'�diteur de
la \*(Fs ou un contributeur list�.

.HU "Remerciements"
.P
Les d�veloppeurs de la \*(Fs souhaitent remercier les d�veloppeurs,
administrateurs syst�me et utilisateurs dont l'avis a �t� essentiel �
cette norme. Nous souhaitons remercier chacun des contributeurs qui ont
aid� � �crire, compiler et composer cette norme.

Le groupe \*(Fs souhaite aussi remercier les d�veloppeurs Linux qui ont
support� la FSSTND, pr�d�cesseur de cette norme. S'ils n'avaient pas
d�montr� le b�n�fice apport� par la FSSTND, la \*(Fs n'aurait jamais pu
�voluer.

.HU "Contributeurs"
.P
.TS
l l.
Brandon S. Allbery	<bsa@kf8nh.wariat.org>
Keith Bostic	<bostic@cs.berkeley.edu>
Drew Eckhardt	<drew@colorado.edu>
Rik Faith	<faith@cs.unc.edu>
Stephen Harris	<sweh@spuddy.mew.co.uk>
Ian Jackson	<ijackson@cus.cam.ac.uk>
John A. Martin	<jmartin@acm.org>
Ian McCloghrie	<ian@ucsd.edu>
Chris Metcalf	<metcalf@lcs.mit.edu>
Ian Murdock	<imurdock@debian.org>
David C. Niemi	<niemidc@clark.net>
Daniel Quinlan	<quinlan@pathname.com>
Eric S. Raymond	<esr@thyrsus.com>
Mike Sangrey	<mike@sojurn.lns.pa.us>
David H. Silber	<dhs@glowworm.firefly.com>
Theodore Ts'o	<tytso@athena.mit.edu>
Stephen Tweedie	<sct@dcs.ed.ac.uk>
Fred N. van Kempen	<waltje@infomagic.com>

.HU "Traducteurs :"
.P

La traduction fran�aise a �t� r�alis�e par Olivier Tharan,
<tharan@int-evry.fr>. Tous les commentaires sont accept�s.
.TE
.TC
.\" Local Variables:
.\" fill-column:72
.\" font-lock-maximum-size:0
.\" hilit-auto-highlight-maxout:100000
.\" End:
