<!doctype linuxdoc system>

<article>

<title>Mini Howto Linux pour syst�mes sans lecteur de disque(ttes)
<author>par Robert Nemkin <tt>buci@math.klte.hu</tt>&nl;
Traducteur: <url name="S�bastien Blondeel"
url="http://www.lifl.fr/~blondeel">
(<htmlurl url="mailto:sebastien.blondeel@lifl.fr"
name="sebastien.blondeel@lifl.fr">)&nl;
<date>v0.0.3 12 Sep 1996


<abstract>
Ce document d�crit la mani�re de configurer un syst�me sous 
Linux sans disque
dur ni lecteur de disquettes. Ses droits appartiennent � Robert Nemkin,
et il est plac� sous les termes de la Publique G�n�rale GNU. L'auteur
remercie Bela Kis &lt;bkis@cartan.math.klte.hu&gt; pour avoir traduit ce
document en anglais. S�bastien Blondeel,
&lt;Sebastien.Blondeel@lifl.fr&gt; a traduit ce document en fran�ais.
</abstract>

<toc>

<sect>Modifications<p>
<itemize>
<item>v0.0.3 12 Sep 1996: Quelques erreurs mineures ont �t� corrig�es
</itemize>

<sect>Comment configurer un syst�me sous Linux sans lecteur de disque(ttes)<p>
Ce document d�crit la mani�re de configurer un syst�me sous Linux sans
disque dur ni lecteur de disquette. Il peut parfois s'av�rer n�cessaire
de faire tourner Linux sur des ordinateurs personnels ("PC") qui ne
poss�dent ni disque dur ni lecteur de disquettes.
Si on dispose d'un r�seau, d'un autre syst�me sous Unix avec bootp et
tftp, d'un serveur NFS et d'un br�leur d'EPROM, alors il est
possible de configurer et de faire tourner un syst�me sous Linux sans
disque dur ni disquette.

<sect>Consulter �galement les documents suivants:<p>
<itemize>
<item>NFS-root Mini Howto
<item>Linux NET-2/3-HOWTO par
  Terry Dawson, <tt>94004531@postoffice.csu.edu.au</tt>
<item><tt>/usr/src/linux/README</tt> pour la configuration et la
compilation de nouveaux noyaux
</itemize>

<sect>Mat�riel<p>
Tout ce qui est d�crit ici a �t� test� avec la configuration suivante:
<itemize>
<item>Sun-OS 4.1.3 comme serveur d'amor�age
<item>Slackware 2.3 + Linux 1.2.8 + la carte ethernet wd 8013
<item>Un r�seau ethernet en �tat de fonctionnement
</itemize>

<sect>Id�es fondamentales<p>
L'id�e de base est la suivante: le PC va obtenir son adresse IP du
serveur d'amor�age par le protocole bootp, en utilisant 0.0.0.0 comme
adresse IP initiale et en obtenant son noyau par le protocole tftp.
<footnote>Un amor�age � travers des segments (via un routeur) n'est pas
un probl�me simple, aussi faut-il mettre � la fois le serveur et la
machine sans disque sur le m�me segment de r�seau, 
ou configurer une adresse UDP
d'aide dans votre routeur pointant vers l'adresse du serveur.
R�f�rez-vous au manuel de votre routeur pour de plus amples informations
sur le sujet.
</footnote>

Pour cela, suivez les �tapes ci-dessous.

<sect1>Configurer le PC<p>
Obtenez le paquetage nfsboot (ce paquetage est disponible sur votre site
miroir de Linux pr�f�r� dans le r�pertoire 
<tt>/pub/Linux/system/Linux-boot</tt>). Il contient une image d'amor�age
pour l'EPROM de la carte wd8013 qui peut �tre br�l�e telle quelle. <p>
Il y a d'autres mani�res de pr�parer le PC:
<itemize>
<item> si votre machine contient un petit disque ou un lecteur de
disquette, vous pouvez utiliser le petit programme sous DOS, ou
<item>l'image binaire pour disquette qui se trouve dans le m�me
paquetage.
</itemize> 
Si vous choisissez la deuxi�me option, il faut utiliser la commande dd
pour �crire l'image sur la disquette.<p>
Ces images contiennent un client bootp et un client tftp.
Vous devez �galement pr�parer un noyau pour linux, comportant l'option
NFS-root (amor�age par NFS).
<itemize>
<item>Si vous utilisez le dernier noyau stable, linux-1.2.13, alors il
faut corriger le noyau avec le fichier de correction contenu dans le
paquetage nfsboot
<footnote>Consulter patch(1)</footnote>
<item>Si vous utilisez le dernier noyau en date, instable, de la s�rie
linux-1.3.x, il vous faut configurer l'option NFS-root.
</itemize>
Vous pouvez ou non choisir de configurer le support pour p�riph�rique
en mode bloc (disque dur ou disquette), mais vous devez configurer le
support pour tcp/ip, pour la carte ethernet wd, et pour le syst�me de
fichiers NFS. Puis recompilez le noyau de mani�re habituelle.

<sect1>Configurer un bootpd sur le serveur<p>
On peut le trouver dans le paquetage 
<tt>bootpd-2.4.tar.gz</tt>
(qui se trouve sur votre site miroir de Linux pr�f�r� dans le r�pertoire 
<tt>/pub/Linux/system/Network/boot.net</tt>). Chargez le paquetage,
compilez-le et installez-le. Si votre autre syst�me sous Linux se trouve
�tre une distribution Slackware, vous pouvez passer � l'�tape suivante
puisque les distributions standard comportent un bootpd. On peut
d�marrer le d�mon, soit en tapant la commande 
<tscreen><code>
	bootpd -s

</code></tscreen>
soit en utilisant inetd. Dans ce dernier cas, il vous faut �diter: 
<itemize>
<item> /etc/inetd.conf pour �ter le signe di�se de mise en commentaire au 
d�but des lignes suivantes:

<tscreen><code>
# tftp   dgram   udp     wait    root    /usr/sbin/in.tftpd 	tftpd /export
# bootps dgram   udp     wait    root    /usr/sbin/in.bootpd	bootpd

</code></tscreen>

<item> ins�rer ou �tez le signe de commentaire pour les deux lignes 
suivantes dans /etc/services:

<tscreen><code>
bootps          67/tcp          # serveur BOOTP
tftp            69/udp		# serveur TFTP

</code></tscreen>

<item>red�marrez inetd en tapant

<tscreen><code>
	kill -HUP <num�ro d'identification du processus de inetd>.

</code></tscreen>
</itemize>

<sect1>Configurer le bootpd sur le serveur.<p>
Tout d'abord, bootpd poss�de un fichier de configuration qui s'appelle
bootptab et qui se trouve habituellement dans /etc. Vous devez le
modifier en indiquant les adresses IP de votre passerelle, de votre
serveur dns, et les adresses ethernet de votre ou vos machines sans
disques.
Voici un fichier /etc/bootptab d'exemple:
<tscreen><code>

global.prof:\
:sm=255.255.255.0:\
:ds=192.168.1.5:\
:gw=192.168.1.19:\
:ht=ethernet:\
:bf=linux:
machine1:hd=/export/root/machine1:tc=global.prof:ha=0000c0863d7a:ip=192.168.1.140:
machine2:hd=/export/root/machine2:tc=global.prof:ha=0800110244e1:ip=192.168.1.141:
machine3:hd=/export/root/machine3:tc=global.prof:ha=0800110244de:ip=192.168.1.142:
</code></tscreen>

global.prof est un patron g�n�ral pour les entr�es d'h�tes, o�
<itemize>
<item>le champ sm contient le masque pour le sous-r�seau
<item>le champ ds contient l'adresse du serveur de nom de domaine (DNS)
<item>le champ gw contient l'adresse de la passerelle par d�faut
<item>le champ ht contient le type de carte r�seau
<item>le champ bf contient le nom du fichier d'amor�age
</itemize>

Apr�s cela, chaque machine doit poss�der sa propre entr�e sur une ligne:
<itemize>
<item>le premier champ contient le nom de l'h�te
<item>le champ hd contient le r�pertoire du fichier d'amor�age
<item>on peut inclure le patron global avec le champ tc
<item>le champ ha contient l'adresse mat�rielle de la carte ethernet 
<item>le champ ip contient l'adresse IP qui a �t� attribu�e
</itemize>


<sect1>Comprendre tftp<p>
TFTP (<tt>Trivial File Transfer Protocol</tt>, ou protocole de transfert
de fichiers banal) est un protocole de transfert de fichiers, comme ftp,
mais il est beaucoup plus facile � programmer dans des m�moires de type
EPROM. On peut utiliser TFTP de deux mani�res:
<itemize>
<item> tftp simple: cela signifie que le client peut acc�der � la
totalit� de votre syst�me de fichiers. C'est plus simple, mais cela
constitue un gros trou de s�curit� (n'importe qui peut obtenir votre
fichier de mots de passe par tftp).
<item> tftp s�curis�: le serveur tftp utilise un appel syst�me chroot.2
pour modifier son propre r�pertoire racine. Tout ce qui n'est pas dans
cette racine sera absolument inaccessible.
Comme le r�pertoire chroot devient le nouveau r�pertoire racine, le
champ hd list� dans le fichier bootptab doit prendre cette situation en
compte. Par exemple: lorsqu'on utilise tftp non s�curis�, le champ hd
contient le chemin complet menant au r�pertoire d'amor�age: 
<tt>/export/root/machine1</tt>.
Lorsqu'on utilise tftp s�curis� avec /export comme r�pertoire racine,
alors /export devient /  et le champ hd doit �tre <tt>/root/machine1</tt>.
</itemize>
Comme pratiquement toute installation Unix comporte un serveur tftp,
vous n'aurez probablement pas besoin d'installer la votre.

<sect1>Configurer un Linux minimal sur le serveur distant.<p>
Il vous faut pour cela, par exemple, les paquetages a, ap, n et x
de la distribution Slackware. Il est possible d'installer plus de
choses; ce pendant les paquetages ci-dessus suffiront � l'installation
d'un terminal X sans disque. Pour l'installation, il vous faut un
syst�me sous Linux en �tat de marche. Trouvez un peu d'espace disque sur
la machine distante et exportez-le en lecture et en �criture. Montez le
r�pertoire export� quelque part (par exemple sur /mnt) sur le syst�me de
fichiers du syst�me sous Linux. D�marrez Linux et modifiez l'option de
racine dans la configuration; remplacez / par /mnt. Puis configurez les
paquetages ci-dessus de mani�re habituelle. Si vous ne souhaitez faire
tourner qu'un seul Linux sans disque, il ne vous faut rien modifier
d'autre. D'un autre c�t�, si vous pensez utiliser plus d'une machine
sans disque, la configuration d�crite ci-dessus ne fonctionnera pas
parce que certains fichiers et r�pertoires doivent �tre priv�s pour
chaque machine. 
On peut contourner ce probl�me on d�pla�ant le r�pertoire /usr (il ne
contient aucune donn�e personnelle) et ensuite de cr�er un
sous-r�pertoire pour chaque machine sans disque. Par exemple, si
/export/linux/machine1 est mont� sur /mnt alors la structure des
r�pertoires apr�s la configuration initiale ressemblera �:
<tscreen><code>
/export/linux/machine1/bin
/export/linux/machine1/sbin
/export/linux/machine1/lib
/export/linux/machine1/etc
/export/linux/machine1/var
/export/linux/machine1/usr

</code></tscreen>
<p>Apr�s les modifications vous obtiendrez
<tscreen><code>
/export/linux/machine1/bin
/export/linux/machine1/sbin
/export/linux/machine1/lib
/export/linux/machine1/etc
/export/linux/machine1/var
/export/linux/usr

</code></tscreen>

Maintenant cr�ez les sous-r�pertoires pour les autres machines.
Supposons pour l'instant que vos machines sans disque s'appellent 
machine1, machine2, machine3, etc.;
vous pouvez alors utiliser le script bash qui suit pour configurer les
autres r�pertoires:
<tscreen><code>	
	cd /export/linux
	for x in machine2 machine3; do
		mkdir $x; cd $x
		(cd ../machine1; tar cf - *) | tar xvf -
	done

</code></tscreen>	

Puis exportez les r�pertoires qui suivent:
<itemize>
<item>/export/linux/usr 			en lecture seule pour 
tout le monde.
<item>/export/linux/machine1		uniquement sur machine1, en
lecture/�criture et avec les droits de root.
<item>/export/linux/machine2		idem, sur machine2.
<item>/export/linux/machine3		idem, sur machine3.
</itemize>
comme suit<footnote>le format de cet exemple est conforme � la syntaxe
des exportations de fichiers pour SunOS 4.1.3</footnote>:
<tscreen><code>	
# Ce fichier est /etc/export
# pour des terminaux sous le syst�me Linux distants
# �crit par Buci
# N'�crivez cette ligne qu'une fois
/export/root/usr             -access=linuxnet
# N'�crivez ces lignes qu'une fois pour chaque h�te
/export/root/machine1       rw=machine1,root=machine1
/export/root/machine2       rw=machine2,root=machine2
/export/root/machine3       rw=machine3,root=machine3

</code></tscreen>	
N'oubliez pas de lancer exportfs -a.

<sect1>Configurer le serveur tftp<p>

C'est maintenant le moment de configurer le serveur tftp. Si vous n'avez
pas besoin de tftp s�curis� alors tout est tr�s simple puisque vos
clients peuvent �tre amorc�s depuis le r�pertoire /export.

Si vous utilisez un tftp s�curis� vous pouvez soit mettre en place une
structure de r�pertoire /export/linux compl�te sous 
/tftpboot (en n'utilisant qu'un seul v�ritable noyau et des liens
symboliques pour les autres machines),
ou laisser le r�pertoire /export
jouer le r�le du r�pertoire d'amor�age pour le tftpd s�curis�. 
Ou encore, si vous disposez d'un r�pertoire tftpboot s�par�, de fa�on
similaire, vous n'aurez besoin que d'un seul noyau dans la 
structure de r�pertoires d'origine, et de liens pour les autres. Vous
pouvez obtenir ce r�sultat en tapant ce qui suit:
<tscreen><code>
      mkdir -p /tftpboot/export/linux/machine1
      cd /tftpboot/export/linux/machine1
      cp /export/linux/machine1/<nom du noyau>.

</code></tscreen>

Puis tapez ce qui suit:

<tscreen><code>
      mkdir -p /tftpboot/export/linux/machine2
      cd ../machine2 
      ln -s ../machine2/<nom du noyau>

</code></tscreen>

<sect1>Derniers r�glages<p>

Enfin, il vous faut ins�rer
<tscreen><code>
   /sbin/mount nfs_server:/export/linux/usr /usr

</code></tscreen>

� la premi�re ligne de 

<tscreen><code>
   /export/linux/<machinex>/etc/rc.d/rc.S

</code></tscreen>
o� &lt;machinex&gt; signifie machine1, machine2, etc.
 
<sect>M�moire et espace disque requis; vitesse<p>. 
Je n'ai test� ceci que pour la distribution Slackware 2.3; pour d'autres
distributions ou versions les nombres qui suivent peuvent varier:
<itemize> Espace disque: 28Mo + 6.5Mo/machine
<item> RAM: J'utilise X avec 8Mo. Comme il ne faut que 8Mo 
de syst�me
de pagination sur m�moire de masse, on peut les mettre en place, je
pense -- de fa�on s�par�e pour chacune des machines -- dans /tmp.
N'oubliez pas de lancer mkswap.
<item> Vitesse: Je n'ai pas eu de probl�mes sur un 486 DX2/66 avec 8
M�gaoctets.
</itemize>

<sect>Erreurs possibles<p> 
<itemize>
<item>J'ai d�couvert une erreur �trange: dans le sous-r�pertoire /dev,
SunOS a corrompu les entr�es de p�riph�riques de telle sorte que j'ai d�
relancer MAKEDEV en montant le sous-r�pertoire sur un syst�me sous Linux
avec disque.
(La raison de cela provient des diff�rences entre le NFS de linux et le
NFS de SunOS: tous deux utilisent 32bits pour les num�ros de
p�riph�riques Mineur et Majeur, mais linux utilise des champs de
16bits
pour ces deux num�ros, alors que SunOS utilise un champ de 14bits pour
le num�ro de p�riph�rique Majeur, et un champ de 18bits pour le num�ro
de p�riph�rique Mineur.)
<item> Quand on amorce un syst�me sous Linux sans disque, la table de
routage au serveur tftp ne contient qu'un seul routage, et vous devez
configurer des tables de routage correctes. Vous avez pour cela deux
possibilit�s: 
<itemize>
<item> configurer le rc.S de chacune des machines � la main
<item> utiliser un paquetage de client bootp et r�diger un script de
configuration g�n�ralis�
</itemize>
</itemize>

<sect>Erreurs et d�veloppements possibles de ce document<p>
<itemize>
<item>Citer correctement les documents li�s � tout ceci.
<item>SunOS est fond� sur BSD. Il faut inclure une configuration de
serveur fond�e sur SVR4 (c'est-�-dire sur Solaris). 
<item>M�me si Linux ressemble beaucoup � SunOS en tant que serveur
bootp/tftp, il peut �tre utile de fournir un exemple de serveur fond�
sur Linux.
<item> Mettre � jour ce document pour le paquetage etherboot en cours.
<item> Montrer les diff�rences entre le noyau version 1.2.13 corrig�
pour la racine NFS et le dernier noyau 1.3.x, qui contient la correction
de racine par NFS.
<item> Besoin d'essayer d'autres cartes ethernet que la wd8013
<item> Inclure des renseignements de configuration pour bootpc, un
client bootp pour Linux qui sert � configurer des tables de routage
correctes.
<item>Fautes de frappe et autres: notifiez-les, s'il vous pla�t, � 
<tt>buci@math.klte.hu</tt> ou � <tt>Sebastien.Blondeel@lifl.fr</tt> pour
la traduction fran�aise.
Merci.
</itemize>

</article>
