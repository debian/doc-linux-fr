<!doctype linuxdoc system>

<article>

<!-- Title information -->

<title>Comment changer le titre d'un xterm</title>
<author>Ric Lister, <tt/ric&commat;giccs.georgetown.edu/ <newline>
traduction Jean-Albert Ferrez, <tt/Jean-Albert.Ferrez&commat;epfl.ch/ </author>
<date>Derni�re modification&nbsp;: 22.11.1999, v2.0</date>

<abstract>
Ce document explique comment utiliser des s�quences d'�chappement pour
modifier dynamiquement le titre de la fen�tre et de l'ic�ne d'un xterm. Des
exemples sont donn�s pour plusieurs shells, et l'annexe donne les s�quences
pour d'autres types de terminaux.
</abstract>

<toc>

<sect>O� trouver ce document
<p>
Ce document fait d�sormais partie des <url name="HOWTOs Linux"
url="http://sunsite.unc.edu/LDP/HOWTO/"> et peut �tre trouv� �&nbsp;: <url
url="http://sunsite.unc.edu/LDP/HOWTO/mini/Xterm-Title.html">.

<p>
La derni�re version en date se trouve �&nbsp;: <url
url="http://www.giccs.georgetown.edu/~ric/howto/Xterm-Title/">.

<p>
Ce document remplace le howto initial �crit par Winfried Tr&uuml;mper.

<p>
Ndt&nbsp;: La version fran�aise de ce document se trouve �&nbsp;:
<url url="http://www.freenix.fr/linux/HOWTO/mini/Xterm-Title.html">

<sect>Titres statiques
<p>
Les titres des �mulateurs de terminaux xterm, color-xterm ou rxvt peuvent
�tre donn�s sur la ligne de commande avec les options -T et -n&nbsp;:

<tscreen><verb>
xterm -T "Le titre de mon XTerm" -n "Le titre de son ic�ne"
</verb></tscreen>

<sect>Titres dynamiques
<p>
Bon nombre de personnes trouvent utile de faire appara�tre dans le titre de
leur terminal une information qui change dynamiquement, telle que le nom du
serveur sur lequel on est connect�, le r�pertoire courant, etc.

<sect1>Les s�quences d'�chappement xterm

<p>
On peut changer le titre de la fen�tre et de l'ic�ne dans un xterm lanc� en 
utilisant les s�quences d'�chappement XTerm. Les s�quences suivantes sont
utiles dans ce but&nbsp;:
<itemize>
  <item><tt>ESC]0;<bf>nom</bf>BEL</tt> -- Change le titre de la fen�tre et
  de l'ic�ne
  <item><tt>ESC]1;<bf>nom</bf>BEL</tt> -- Change le titre de l'ic�ne
  <item><tt>ESC]2;<bf>nom</bf>BEL</tt> -- Change le titre de la fen�tre
</itemize>

o� ESC est le caract�re escape (�chappement, &bsol;033), et BEL est le
caract�re bell (bip, &bsol;007).

<p>
Afficher l'une de ces s�quences dans un xterm causera le changement du
titre de la fen�tre ou de l'ic�ne.

<p>
<bf/Note/: Ces s�quences fonctionnent �galement avec la plupart des d�riv�s de
xterm, tels que nxterm, color-xterm et rxvt. D'autres �mulateurs de
terminaux utilisent d'autres s�quences&nbsp;; quelques exemples sont donn�s en
annexe. La liste compl�te des s�quences d'�chappement est donn�e dans le
fichier <url name="ctlseq2.txt"
url="http://www.giccs.georgetown.edu/~ric/howto/Xterm-Title/ctlseq2.txt">
de la distribution de xterm, ou dans le fichier <url name="xterm.seq"
url="http://www.giccs.georgetown.edu/~ric/howto/Xterm-Title/xterm.seq"> de
la distribution de <url name="rxvt" url="http://www.rxvt.org/">.

<sect1>Afficher les s�quences d'�chappement

<p>
Pour les informations qui ne changent pas au cours de l'�x�cution du shell,
telles que le serveur et le nom d'utilisateur, il suffit d'afficher les
s�quences depuis le fichier rc du shell&nbsp;:
<tscreen><verb>
echo -ne "\033]0;${USER}@${HOST}\007"
</verb></tscreen>
devrait donner un titre du genre <tt>nom&commat;serveur</tt>, pour
autant que les variables <tt>$USER</tt> et <tt>$HOST</tt> soient
correctes. Les options requises pour echo peuvent d�pendre du shell (cf
ci-dessous).

<p>
Pour les informations qui peuvent changer au cours de l'ex�cution du shell, 
telles que le r�pertoire courant, ces s�quences doivent vraiment �tre
donn�es lors de chaque changement de l'invite.
De cette fa�on, le titre est mis � jour lors de chaque commande
et peut ainsi refl�ter des informations telles que le r�pertoire en cours,
le nom d'utilisateur, le nom du serveur, etc. Certains shells offrent des
fonctions sp�ciales pour y parvenir, d'autres pas&nbsp;: il faut dans ce
cas ins�rer la cha�ne directement dans le texte de l'invite.

<sect>Exemples pour quelques shells

<p>
Nous donnons ci-dessous des exemples pour les shells les plus
courants. Nous commen�ons avec <tt>zsh</tt> car il offre des possibilit�s
qui facilitent grandement notre t�che. Nous progresserons ensuite vers des
exemples de plus en plus difficiles.

<p>
Dans tous les exemples ci-dessous, on teste la variable d'environnement
TERM pour �tre certain de n'appliquer ces s�quences que si l'on est dans un
xterm (ou d�riv�). Le test est fait sur TERM=xterm*, de mani�re � inclure
au passage les variantes telles que TERM=xterm-color (d�fini par rxvt).

<p>
Encore une remarque au sujet des d�riv�s du C shell tels que <tt>tcsh</tt>
et <tt>csh</tt>. Dans ces shells, les variables non-d�finies causent des
erreurs fatales. Il est d�s lors n�cessaire avant de tester la valeur de la 
variable <tt>$TERM</tt>, de tester si elle existe pour ne pas
interrompre un shell non-interactif. Pour y parvenir, il faut inclure les
exemples ci-dessous dans quelque chose du genre&nbsp;:
<tscreen><verb>
  if ($?TERM) then
      ...
  endif
</verb></tscreen>
(� notre avis, il s'agit d'une raison parmi beaucoup d'autres de ne
pas utiliser les C shells. Voir <it><url name="Csh Programming Considered
Harmful" url="http://language.perl.com/versus/csh.whynot"></it> pour une
discussion utile).

<p>
Pour utiliser les exemples suivants, placez-les dans le fichier
d'initialisation du shell apropri�, c'est-�-dire un fichier lu lors du
lancement d'un shell interactif. Le plus souvent il s'agit de 
<tt>.<it>shell</it>rc</tt> (ex&nbsp;: <tt>.zshrc</tt>, <tt>.tcshrc</tt>, etc.)


<sect1>zsh

<p>
On utilise quelques fonctions et codes offerts par <tt>zsh</tt>&nbsp;:
<tscreen><verb>
precmd ()   fonction ex�cut�e juste avant chaque invite
chpwd ()    fonction ex�cut�e lors de chaque changement de r�pertoire
\e          code du caract�re escape (ESC)
\a          code du caract�re bip (BEL)
%n          code remplac� par $USERNAME
%m          code remplac� par le hostname jusqu'au premier '.'
%~          code remplac� par le r�pertoire, avec '~' � la place de $HOME
</verb></tscreen>
De nombreux autres codes sont disponibles, voir 'man zshmisc'.

<p>
Ainsi, le code suivant, mis dans &tilde;/.zshrc, affiche
"nom&commat;serveur:r�pertoire" dans le titre de la fen�tre (et de l'ic�ne).
<tscreen><verb>
case $TERM in
   xterm*)
       precmd () {print -Pn "\e]0;%n@%m: %~\a"}
       ;;
esac
</verb></tscreen>
On arrive au m�me r�sultat en utilisant <tt>chpwd()</tt> au lieu de
<tt>precmd()</tt>. La commande interne <tt>print</tt> fonctionne comme
<tt>echo</tt>, mais donne acc�s aux s�quences <tt>&percnt;</tt>.


<sect1>tcsh

<p>
<tt>tcsh</tt> offre des possibilit�s similaires � celles de
<tt>zsh</tt>&nbsp;:
<tscreen><verb>
precmd ()   fonction ex�cut�e juste avant chaque invite
chpwd ()    fonction ex�cut�e lors de chaque changement de r�pertoire
%n          code remplac� par $USERNAME
%m          code remplac� par le hostname jusqu'au premier '.'
%~          code remplac� par le r�pertoire, avec '~' � la place de $HOME
</verb></tscreen>

<p>
Malheureusement, il n'y a pas d'�quivalent � la fonction print de
<tt>zsh</tt> qui permette d'utiliser les codes de l'invite dans la cha�ne
du titre&nbsp;; le mieux que l'on puisse faire est d'utiliser les variables du
shell (dans <tt>&tilde;/.tcshrc</tt>)&nbsp;:
<tscreen><verb>
switch ($TERM)
   case "xterm*":
       alias precmd 'echo -n "\033]0;${HOST}:$cwd\007"'
       breaksw
endsw
</verb></tscreen>
mais on obtient alors le chemin complet du r�pertoire, sans '&tilde;'. Par
contre, on peut mettre la cha�ne dans l'invite&nbsp;:
<tscreen><verb>
switch ($TERM)
   case "xterm*":
       set prompt="%{\033]0;%n@%m:%~\007%}tcsh%# "
       breaksw
   default:
       set prompt="tcsh%# "
       breaksw
endsw
</verb></tscreen>
ce qui donne "<tt>tcsh&percnt; </tt>" comme invite, et
"<tt><it>nom</it>&commat;<it>serveur</it>: <it>r�pertoire</it></tt>" dans
le titre (et l'ic�ne) de xterm. Les "&percnt;&lcub;...&percnt;&rcub;"
doivent �tre plac�s autour des s�quences d'�chappement (et ne peuvent pas
�tre le dernier �l�ment de l'invite, 'man tcsh' donne plus de d�tails).


<sect1>bash

<p>
<tt>bash</tt> offre la variable <tt>PROMPT_COMMAND</tt> qui contient une
commande � ex�cuter avant d'afficher l'invite. Ce code (ins�r� dans
<tt>&tilde;/.bashrc</tt>) affiche <tt>nom&commat;serveur: r�pertoire</tt>
dans le titre de la fen�tre (et de l'ic�ne).
<tscreen><verb>
PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD}\007"'
</verb></tscreen>
o� <tt>&bsol;033</tt> est le caract�re <tt>ESC</tt> et <tt>&bsol;007</tt>
<tt>BEL</tt>.

<p>
Il convient de noter que les guillemets jouent un r�le important&nbsp;: les
variables entre <tt>"..."</tt> sont �valu�es, alors que celles entre
<tt>&quot;...&quot;</tt> ne le sont pas. Ainsi, <tt>PROMPT_COMMAND</tt>
re�oit bien le nom des variables, ces derni�res �tant �valu�es lorsque
<tt>PROMPT_COMMAND</tt> est invoqu�e.

<p>
Cependant, <tt>&dollar;PWD</tt> donne le r�pertoire complet. Si l'on veut
utiliser le raccourci <tt>&tilde;</tt>, il faut placer toute la s�quence
d'�chappement dans l'invite pour avoir acc�s aux codes suivants&nbsp;:
<tscreen><verb>
\u          est remplac� par $USERNAME
\h          est remplac� par le hostname jusqu'au premier '.'
\w          est remplac� par le repertoire, avec '~' � la place de $HOME
\[...\]     signale une suite de caract�re non-imprimables
</verb></tscreen>

<p>
Ainsi le code suivant produit l'invite <tt>bash&dollar; </tt>, et place
<tt>nom&commat;serveur: r�pertoire</tt> dans le titre (et l'ic�ne) de
xterm.
<tscreen><verb>
case $TERM in
   xterm*)
       PS1="\[\033]0;\u@\h: \w\007\]bash\$ "
       ;;
   *)
       PS1="bash\$ "
       ;;
esac
</verb></tscreen>
L'utilisation de <tt>&bsol;&lsqb;...&bsol;&rsqb;</tt> signale �
<tt>bash</tt> la pr�sence de caract�res non-imprimables, information dont
il a besoin lorsqu'il calcule la longueur de l'invite. Sans cette
pr�caution, les commandes d'�dition de ligne ne savent plus tr�s bien o�
placer le curseur.


<sect1>ksh

<p>
<tt>ksh</tt> n'offre pas grand chose en terme de fonctions et codes, il
faut donc mettre notre cha�ne dans l'invite pour qu'elle soit mise � jour
dynamiquement. L'exemple suivant produit l'invite <tt>ksh&dollar; </tt>, et
place <tt>nom&commat;serveur: r�pertoire</tt> dans le titre (et l'ic�ne) de
xterm.
<tscreen><verb>
case $TERM in
   xterm*)
       HOST=`hostname`
       PS1='^[]0;${USER}@${HOST}: ${PWD}^Gksh$ '
       ;;
   *)
       PS1='ksh$ '
       ;;
esac
</verb></tscreen>
Cependant, <tt>&dollar;PWD</tt> donne le r�pertoire complet. On peut �ter
le pr�fixe <tt>&dollar;HOME/</tt> en utilisant la construction
<tt>&dollar;&lcub;...&num;&num;...&rcub;</tt>. De m�me, on peut tronquer le
nom du serveur � l'aide de
<tt>&dollar;&lcub;...&percnt;&percnt;...&rcub;</tt>.
<tscreen><verb>
HOST=`hostname`
HOST=${HOST%%.*}
PS1='^[]0;${USER}@${HOST}: ${PWD##${HOME}/}^Gksh$ '
</verb></tscreen>
Les caract�res <tt>&circ;&lsqb;</tt> et <tt>&circ;G</tt> d�signent
<tt>ESC</tt> et <tt>BEL</tt> (ils peuvent �tre saisis dans emacs � l'aide
de <tt>C-q ESC</tt> et <tt>C-q C-g</tt>).


<sect1>csh

<p>
C'est assez difficile � r�aliser avec <tt>csh</tt>. On finit par mettre ce
qui suit dans le &tilde;/.cshrc&nbsp;:
<tscreen><verb>
switch ($TERM)
   case "xterm*":
       set host=`hostname`
       alias cd 'cd \!*; echo -n "^[]0;${user}@${host}: ${cwd}^Gcsh% "'
       breaksw
   default:
       set prompt='csh% '
       breaksw
endsw
</verb></tscreen>
Il a fallu faire un alias de la commande <tt>cd</tt> pour mettre � jour
l'invite. Les caract�res <tt>&circ;&lsqb;</tt> et <tt>&circ;G</tt>
d�signent <tt>ESC</tt> et <tt>BEL</tt> (ils peuvent �tre saisis dans emacs
� l'aide de <tt>C-q ESC</tt> et <tt>C-q C-g</tt>).

<p>
Notes&nbsp;: sur certains syst�mes <tt>hostname -s</tt> peut �tre utilis�
pour obtenir le nom de la machine au lieu du nom qualifi�. Les utilisateurs
ayant des liens symboliques sur des r�pertoires trouveront <tt>`pwd`</tt>
plus pr�cis que <tt>$cwd</tt>.


<sect>Afficher le nom de la commande en cours d'ex�cution

<p>
Souvent un utilisateur lance une longue commande en avant plan telle que
<tt>top</tt>, un �diteur, un lecteur de courrier �lectronique, etc, et
voudrait que le nom de cette commande figure dans le titre de la fen�tre.
C'est un probl�me d�licat qui n'est facile � r�soudre qu'avec <tt>zsh</tt>.

<sect1>zsh

<p>
<tt>zsh</tt> offre une fonction id�ale pour cet objectif&nbsp;:
<tscreen><verb>
preexec()   fonction ex�cut�e juste avant qu'une commande soit ex�cut�e
$*,$1,...   arguments pass�s � preexec()
</verb></tscreen>
On peut donc ins�rer le nom de la commande de la mani�re suivante&nbsp;:
<tscreen><verb>
case $TERM in
    xterm*)
      preexec () {
        print -Pn "\e]0;$*\a"
      }
    ;;
esac
</verb></tscreen>
Note: la fonction <tt>preexec()</tt> est apparue vers la version 3.1.2 de 
<tt>zsh</tt>, vous devrez peut-�tre mettre � jour votre ancienne version.


<sect1>Autres shells

<p>
Ce n'est pas facile avec les autres shells qui n'ont pas l'�quivalent de la 
fonction <tt>preexec()</tt>. Si quelqu'un a des exemples, merci de les
communiquer par email � l'auteur.


<sect>Annexe&nbsp;: s�quences d'�chappement pour d'autres �mulateurs de
terminaux

<p>
De nombreux �mulateurs de terminaux modernes sont des d�riv�s de
<tt>xterm</tt> ou <tt>rxvt</tt> et acceptent les s�quences d'�chappement
que nous avons utilis�es jusqu'ici. Certains terminaux propri�taires
fournis avec les diverses variantes d'unix utilisent leur propres
s�quences.


<sect1><tt>aixterm</tt> d'IBM

<p>
<tt>aixterm</tt> reconna�t les s�quences d'�chappement de <tt>xterm</tt>.


<sect1><tt>wsh</tt>, <tt>xwsh</tt> et <tt>winterm</tt> de SGI
<p>
Ces terminaux d�finissent <tt>$TERM=iris-ansi</tt> et utilisent&nbsp;:
<itemize>
<item><tt>ESCP1.y<it>texte</it>ESC\    Pour le titre de la fen�tre</tt>
<item><tt>ESCP3.y<it>texte</it>ESC\    Pour le titre de l'ic�ne</tt>
</itemize>
La liste compl�te des s�quences est donn�e dans la page man
<tt>xwsh(1G)</tt>.

<p>
Les terminaux d'Irix supportent �galement les s�quences de <tt>xterm</tt>
pour d�finir individuellement le titre de la fen�tre et de l'ic�ne, mais
pas celle pour d�finir les deux en m�me temps.


<sect1><tt>cmdtool</tt> et <tt>shelltool</tt> de Sun
<p>
<tt>cmdtool</tt> et <tt>shelltool</tt> d�finissent <tt>$TERM=sun-cmd</tt> et
utilisent&nbsp;:
<itemize>
<item><tt>ESC]l<it>texte</it>ESC\    Pour le titre de la fen�tre</tt>
<item><tt>ESC]L<it>texte</it>ESC\    Pour le titre de l'ic�ne</tt>
</itemize>
Ce sont des programmes vraiment horribles, il vaut mieux utiliser autre chose.

<sect1>CDE dtterm
<p>
<tt>dtterm</tt> d�finit <tt>$TERM=dtterm</tt>. Il semble qu'il reconnaisse � la fois les
s�quences <tt>xterm</tt> standard ainsi que celles du <tt>cmdtool</tt> de
Sun (test� sur Solaris 2.5.1, Digital Unix 4.0, HP-UX 10.20).


<sect1>HPterm

<p>
<tt>hpterm</tt> d�finit <tt>$TERM=hpterm</tt> et utilise les s�quences
suivantes&nbsp;:
<itemize>
<item><tt>ESC&amp;f0k<it>longueur</it>D<it>texte</it>  Donne le texte
<it>texte</it> de longueur <it>longueur</it> comme titre de fen�tre</tt>
<item><tt>ESC&amp;f-1k<it>longueur</it>D<it>texte</it>  Donne le texte
<it>texte</it> de longueur <it>longueur</it> comme nom de l'ic�ne</tt>
</itemize>

<p>
Un programme C simple pour calculer la longueur et afficher la bonne
s�quence ressemble �&nbsp;:
<tscreen><verb>
#include <string.h>
int main(int argc, char *argv[])
{
    printf("\033&amp;f0k%dD%s", strlen(argv[1]), argv[1]);
    printf("\033&amp;f-1k%dD%s", strlen(argv[1]), argv[1]);
    return(0);
}
</verb></tscreen>

<p>
On peut �galement �crire un shell-script �quivalent, utilisant
<tt>${#string}</tt> (<tt>zsh</tt>, <tt>bash</tt>, <tt>ksh</tt>) ou
<tt>${%string}</tt> (<tt>tcsh)</tt> pour obtenir la longueur d'une
cha�ne. L'exemple suivant est pour <tt>zsh</tt>&nbsp;:
<tscreen><verb>
case $TERM in
    hpterm)
        str="\e]0;%n@%m: %~\a"
	precmd () {print -Pn "\e&amp;f0k${#str}D${str}"}
   	precmd () {print -Pn "\e&amp;f-1k${#str}D${str}"}
	;;
esac
</verb></tscreen>


<sect>Annexe&nbsp;: exemples dans d'autres langages

<p>
Il peut �tre utile d'�crire des bouts de codes pour changer le titre de la
fen�tre � l'aide des s�quences <tt>xterm</tt>. Voici quelques exemples&nbsp;:


<sect1>C

<p>
<tscreen><verb>
#include <stdio.h>

int main (int argc, char *argv[]) {
  printf("%c]0;%s%c", '\033', argv[1], '\007');
  return(0);
}
</verb></tscreen>


<sect1>Perl

<p>
<tscreen><verb>
#!/usr/bin/perl
print "\033]0;@ARGV\007";
</verb></tscreen>


<sect>Cr�dits
<p>
Merci aux personnes suivantes pour leur contribution � ce document.

<p>
Paul D. Smith <tt>&lt;psmith@BayNetworks.COM&gt;</tt> et Christophe Martin
<tt>&lt;cmartin@ipnl.in2p3.fr&gt;</tt> ont tous les deux remarqu� que j'avais
interverti les guillemets dans le <tt>PROMPT_COMMAND</tt> pour
<tt>bash</tt>. Les avoir dans le bon ordre garantit que les variables sont
�valu�es dynamiquement.

<p>
Paul D. Smith <tt>&lt;psmith@BayNetworks.COM&gt;</tt> a propos� de prot�ger
les caract�res non-imprimables dans l'invite de <tt>bash</tt>.

<p>
Christophe Martin <tt>&lt;cmartin@ipnl.in2p3.fr&gt;</tt> a donn� la
solution pour ksh.

<p>
Keith Turner <tt>&lt;keith@silvaco.com&gt;</tt> a donn� les s�quences
d'�chappement pour les <tt>cmdtool</tt> et <tt>shelltool</tt> de Sun.

<p>
Jean-Albert Ferrez <tt>&lt;ferrez@dma.epfl.ch&gt;</tt>
a signal� un manque de coh�rence dans l'utilisation de "<tt>PWD</tt>"
et "<tt>$PWD</tt>", ainsi que de "<tt>\</tt>" et "<tt>\\</tt>".

<p>
Bob Ellison <tt>&lt;papillo@hpellis.fc.hp.com&gt;</tt> et
Jim Searle <tt>&lt;jims@broadcom.com&gt;</tt> ont test� <tt>dtterm</tt>
sur HP-UX.

<p>
Teng-Fong Seak <tt>&lt;seak@drfc.cad.cea.fr&gt;</tt> a sugg�r� l'option
<tt>-s</tt> de <tt>hostname</tt>, l'utilisation de <tt>`pwd`</tt>, et de
<tt>echo</tt> sous <tt>csh</tt>.

<p>
Trilia <tt>&lt;trilia@nmia.com&gt;</tt> a sugg�r� les exemples dans
d'autres langages.

<p>
Brian Miller <tt>&lt;bmiller@telstra.com.au&gt;</tt> a fourni les s�quences 
d'�chappement et les exemples pour <tt>hpterm</tt>.

<p>
Lenny Mastrototaro <tt>&lt;lenny@click3x.com&gt;</tt> a expliqu�
l'utilisation des s�quences xterm dans les �mulateurs de terminaux Irix.

<p>
Paolo Supino <tt>&lt;paolo@init.co.il&gt;</tt> a sugg�r� l'utilisation de
<tt>\\$</tt> dans le prompt de <tt>bash</tt>.

</article>
