<!doctype linuxdoc system>

  <article>
    
      <title>TkRat mini-HOWTO
      <author>Dave Whitinger, <htmlurl url="mailto:dave@linuxtoday.com"
	    name="dave@linuxtoday.com">&nl;
	  Version fran�aise&nbsp;: Philippe Martin, <htmlurl url="mailto:feloy@wanadoo.fr"
	    name="feloy@wanadoo.fr">
      <date>v1.2, 15 Septembre 1999
      <abstract>
	Voici le TkRat mini-HOWTO.
	Ce document est destin� � quiconque voudrait utiliser Linux pour envoyer et
	recevoir des messages �lectroniques par l'Internet.
      </abstract>
      
    <toc>
    
    <sect>Introduction
      <p>
	Ce document est maintenu par Dave Whitinger (<htmlurl
	  url="mailto:dave@linuxtoday.com" name="dave@linuxtoday.com">). Merci de me
	contacter pour toutes questions ou id�es relatives � cet HOWTO.
	
	Ce document va tenter de d�crire comment&nbsp;:
	
	<enum>
	  <item>Installer le programme de messagerie TkRat,
	  <item>R�cup�rer ses messages chez un Fournisseur de Services Internet (<bf/FSI/),
	  <item>Lire, stocker et envoyer des messages,
	  <item>Trier ses messages dans diff�rents classeurs.
	</enum>
	
	Ce document suppose que vous utilisez Linux avec le syst�me X Window, avez
	d�j� �tabli une connexion avec votre Fournisseur de Services Internet, et
	avez un compte POP chez ce fournisseur.
	
	Responsabilit�s&nbsp;: Toutes responsabilit�s habituelles s'appliquent. L'auteur
	ne peut en aucun cas �tre tenu responsable d'une (mauvaise) utilisation des
	informations donn�es ici.  Ce document ne tente en aucun cas de faire
	autorit�. L'auteur a volontairement cr�� ce document s�par�ment de toute
	entit� avec laquelle il pourrait �tre impliqu�. En lisant ce document, vous
	acceptez de ne cr�er � l'auteur ni ennuis ni griefs.  :)
	
    <sect>Installer TkRat
      <P>
	Je voudrais souligner qu'il existe un grand nombre d'excellents programmes
	de messagerie disponibles sous Linux. C'est toujours une question de go�t
	personnel que de choisir lequel utiliser.
	
	Cela dit, je dois vous avouer que j'utilise un programme appel�
	<tt>TkRat</tt> depuis plus de deux ans maintenant --&nbsp;et je l'adore. C'est un
	programme avec une interface graphique qui a toutes les caract�ristiques que
	vous attendez d'un programme de messagerie de qualit�&nbsp;: il est simple
	d'apprentissage, il est puissant, et il est libre. Pour ces raisons, cet
	HOWTO ne traitera que de TkRat.
	
	La premi�re chose � faire est de r�cup�rer <tt>TkRat</tt>. Vous pouvez soit
	obtenir les sources que vous compilerez, soit obtenir les ex�cutables.
	
	Voici quelques URL pour le logiciel&nbsp;:
	
	<itemize>
	  <item><url url="ftp://ftp.md.chalmers.se/pub/tkrat/tkrat-2.0b3.tar.gz"
	      name="ftp://ftp.md.chalmers.se/pub/tkrat/tkrat-2.0b3.tar.gz">
	    (T�l�chargement via ftp)
	    
	  <item><url
	      url="http://www.dtek.chalmers.se/~maf/ratatosk/tkrat-2.0b3.tar.gz"
	      name="http://www.dtek.chalmers.se/~maf/ratatosk/tkrat-2.0b3.tar.gz">
	    (T�l�chargement via http) 
	    
	  <item><url
	      url="http://www.dtek.chalmers.se/~maf/ratatosk/download.html"
	      name="http://www.dtek.chalmers.se/~maf/ratatosk/download.html">
	    (Page de t�l�chargement - v�rifiez la derni�re version&nbsp;!) 
	    
	</itemize>

	Lorsque vous avez t�l�charg� l'archive, vous �tes pr�t � l'installer
	sur votre syst�me.

	Pour installer les sources <tt>tar.gz</tt>, utilisez les commandes&nbsp;:
	<code>
	  tar -zcvpf tkrat-2.0b3.tar.gz
	  cd tkrat-2.0b3
	  ./configure
	  make
	  make install
	</code>
	Si quelque chose �choue, consultez les fichiers <tt>INSTALL</tt> et
	<tt>README</tt> se trouvant dans le r�pertoire
	<tt>tkrat-2.0b3</tt>. Notez que vous aurez besoin d'une version de tcl
	sup�rieure � 8.1 pour compiler.  Vous pouvez obtenir la derni�re version
	de tcl depuis <url url="http://www.scriptics.com/" name="scriptics.com">.

	Bravo - <tt>TkRat</tt> devrait �tre install� sur votre syst�me maintenant.

    <sect>R�cup�rer vos messages
      <P>
	Afin de lire vos messages, le mieux est g�n�ralement d'utiliser le programme
	appel� <tt>fetchmail</tt>. Les instructions suivantes sont r�put�es marcher
	parfaitement pour la version 2.2 de <tt>fetchmail</tt> --&nbsp;bien qu'elles
	doivent �tre applicables � d'autres versions.

	(Note : <tt>fetchmail</tt> est tr�s certainement d�j� install� sur votre
	syst�me, celui-ci �tant inclus dans toutes les distributions Linux que je
	connaisse -&nbsp;si tel n'est pas le cas, pri�re de me le faire savoir !)

	Ex�cutez les �tapes suivantes&nbsp;:

	<code>
	  echo "server pop.fr protocol pop3 username dave password xxxxx" > ~/.fetchmailrc
	</code>
	
	(N'oubliez pas de remplacer <em>pop.fr</em> par le nom du serveur POP de
	votre FSI. Remplacez <em/dave/ par votre identifiant de messagerie et
	<em/xxxxx/ par votre mot de passe associ�.)
	
	� partir de maintenant, chaque fois que vous voulez voir si vous avez des
	messages, tapez juste <tt/fetchmail/ sur la ligne de
	commande. <tt/fetchmail/ va partir r�cup�rer tous vos messages et les
	stocker dans votre classeur de messages local.

    <sect>Configurer <tt/TkRat/
      <p>

	Depuis la ligne de commande, tapez <tt/tkrat/ suivi d'<em/Entr�e/.&nl;
	Cliquez sur <tt/Create/.&nl;
	Lisez le message qu'il vous pr�sente, puis cliquez sur <tt/Continue/.

	� ce point, vous �tes amen� au programme principal de <tt/TkRat/. La
	premi�re chose � faire est de l'adapter � votre syst�me local.

	<enum>
	  <item>Allez dans le menu <tt/Admin/, et choisissez <tt/Preferences/,
	  <item>Cliquez sur <tt/Sending/,
	  <item>Entrez votre adresse �lectronique dans le champ <tt/Default Reply-To/,
	  <item>Entrez votre adresse �lectronique, suivie de votre nom entre
	  parenth�ses dans le champ <tt/Use from address/, par exemple
	  dave@linuxtoday.com (Dave Whitinger)
	  <item>Entrez votre nom de domaine dans le champ <tt/Domain/. Si votre
	  adresse �lectronique est <tt/utilisateur@xyz.fr/, entrez <tt/xyz.fr/
	  <item>Cliquez sur <tt/Apply/,
	  <item>Cliquez sur <tt/Method/,
	  <item>Pour <tt/Send Via/, s�lectionnez <tt/SMTP/,
	  <item>Dans le champ <tt/SMTP host/, entrez l'adresse du serveur SMTP de
	    votre FSI (ils devraient �tre capable de vous fournir cette information),
	  <item>Cliquez sur <tt/Apply/, puis sur <tt/Dismiss/.
	</enum>
	
    <sect>Utiliser <tt/TkRat/
      <P>
	Au d�marrage de <tt/TkRat/, vous noterez que vous �tes dans un classeur
	appel� <tt/INBOX/. Utilisez <tt/fetchmail/ pour r�cup�rer vos messages. Si
	vous en avez, ils appara�tront dans le classeur <tt/INBOX/.

	<tt/TkRat/ a une interface intuitive. La moiti� sup�rieure de l'�cran montre
	la liste des messages que vous avez re�us (chaque ligne �tant un message
	diff�rent). La moiti� inf�rieure montre le contenu du message s�lectionn�.

	Vous pouvez lire un message en cliquant simplement sur ce message dans la liste.

	Lorsque vous avez fini de lire un message, vous pouvez aller voir un autre
	message, r�pondre � ce message, �crire un nouveau message, ou d�placer ce
	message vers un autre classeur.

	Les <tt>Folders</tt>
	sont un moyen de stocker vos messages �lectroniques dans
	diff�rents <em/classeurs/. Ainsi, vous pouvez conserver plusieurs classeurs,
	chacun contenant les messages sur un certain sujet.

	Ex�cutez les instructions suivantes pour cr�er des classeurs&nbsp;:
	<enum>
	  <item>Choisissez le menu <tt/Admin/, puis <tt>New/Edit Folders</tt>
	  <item>Vous voyez une bo�te avec un classeur <tt/INBOX/. Cliquez (avec le
	    bouton gauche) sur cette bo�te.
	  <item>Il vous est alors pr�sent� un menu, s�lectionnez-y <tt/New File Folder/.
	  <item>Il vous sera pr�sent� une bo�te de dialogue vous demandant des informations.
	  <item>Dans le premier champ, entrez un nom pour le classeur.
	  <item>Dans le second, entrez le nom de fichier qui sera utilis� pour
	    y stocker les messages.&nl;
	    Un petit conseil&nbsp;: utilisez quelque chose comme
	    <tt>/home/utilisateur/mail/nom_du_classeur</tt>. Par exemple, si le premier
	    champ est <em/Personnel/, le second serait quelque chose comme
	    <tt>home/dave/mail/personnel</tt> (en supposant que votre nom d'utilisateur
	    soit <em/dave/.)
	  <item>Cliquez sur <tt/OK/. Vous noterez que votre nouveau classeur appara�t
	    maintenant dans la liste des classeurs. Allez alors dans <tt/Window/, puis
	    <tt/Close/. 
	</enum>

	Vous pouvez maintenant d�placer vos messages vers le nouveau classeur - et
	basculer vers ce classeur en s�lectionnant son nom sous le menu <tt/Folders/.

	Envoyer des messages est aussi simple. Cliquez sur <tt/Compose/, tapez
	l'adresse �lectronique du destinataire, le sujet, puis le message.

	Lorsque vous avez termin�, cliquez sur <tt/Send/.

    <sect>Tri automatique des messages dans les classeurs
      <P>
	Si vous �tes comme moi, vous recevez pr�s de 3500 messages par jour. Chaque
	fois que vous allez chercher vos messages 
	(Note&nbsp: <em/fetch mail en anglais/),
	vous devez parcourir tous vos messages pour trouver ceux qui vous
	int�ressent.

	Ce n'est plus un probl�me, gr�ce � <tt/Procmail/.
	
	<tt/Procmail/ fonctionne ainsi&nbsp;:

	<tt/Procmail/ examine chaque message au moment o� il est r�cup�r�, et
	ex�cute une s�rie de t�ches fond�es sur certaines r�gles que vous lui avez
	sp�cifi�es. 

	Disons, par exemple, que je re�oive 200 messages par jour d'une certaine
	liste (prenons ici la liste <em/linux-machin/). Plut�t que de voir tous ces
	messages partir directement dans mon classeur <tt/INBOX/, je pr�f�re qu'ils
	soient automatiquement filtr�s et dirig�s dans un classeur appel�
	<em/liste-machin/.
	
	La premi�re chose � faire est de prendre quelques exemples de messages provenant de
	cette liste et d'en examiner les ent�tes.

	Je commence � discerner un motif r�p�titif. Chaque message provenant de
	cette liste contient une ligne disant&nbsp;:

	<verb>
	  Sender: owner-linux-machin@bidule.chouette.fr
	</verb>

	Je peux maintenant dire � <tt/procmail/ de placer chaque message contenant
	cette ligne dans un certain classeur.

	Je peux le pr�ciser � <tt/procmail/ � travers un fichier <tt/.procmailrc/.

	Je vais utiliser mon �diteur de texte pr�f�r� pour cr�er un fichier texte
	dans mon r�pertoire racine, appel� <tt/.procmailrc/.

	Ce fichier ressemblera �&nbsp;:

	<code>
	  LOGFILE=$HOME/.pmlog
	  MAILDIR=$HOME/mail
	  VERBOSE

	  # liste linux-machin
	  :0 Hw
	  * ^.*[Ss]ender: owner-linux-machin@bidule.chouette.fr
	  liste-machin

	  # S'il arrive ici, l'envoyer dans mon classeur 'nouveaux'
	  :0 Hw
	  nouveaux

	</code>
	
	Examinez la ligne <tt>[Ss]ender</tt>. Vous noterez que cette ligne est ce
	que tous les messages provenant de cette liste ont en commun. 

	Cette section dit � <tt/procmail/ de placer les messages arrivant avec une
	ent�te comme celle-ci dans le classeur <em/liste-machin/.

	La section suivante dit que si le message ne correspond � aucune r�gle
	pr�c�dente, alors il faut le placer dans le classeur <em/nouveaux/.

	Maintenant, disons que je veuille que les messages de la part de mon ami
	<em/EJ/ aillent dans un classeur nomm� <em/EJ/.

	Il suffit de rajouter une section � mon fichier <tt/.procmailrc/&nbsp;:

	<code>
	  LOGFILE=$HOME/.pmlog
	  MAILDIR=$HOME/mail
	  VERBOSE
	  
	  # liste linux-machin
	  :0 Hw
	  * ^.*[Ss]ender: owner-linux-machin@bidule.chouette.fr
	  liste-machin

	  # Message de E.J. !
	  :0 Hw
	  * ^.*[Ff]rom: ej@monpote.fr
	  ej

	  # S'il arrive ici, l'envoyer dans mon classeur 'nouveaux'
	  :0 Hw
	  nouveaux
	</code>

	Remarquez la nouvelle section pour <em/EJ/. Lorsqu'un message y arrive avec
	son adresse dans le champ <tt/From/, il sera automatiquement plac� dans mon
	classeur <tt/ej/.

	Maintenant, disons qu'il y ait un boiteux par-l� qui tienne �
	m'�crire. Je ne veux pas entendre parler de lui, mais il insiste. Encore une
	fois - <tt/procmail/ � la rescousse.

	Disons alors que ne ne veuille recevoir aucun message de <em/Bill Gates/. Je
	peux d�finir un exp�diteur dont tous les messages seront effac�s. Voyez alors
	mon nouveau <tt/.procmailrc/&nbsp;:

	<code>

	  LOGFILE=$HOME/.pmlog
	  MAILDIR=$HOME/mail
	  VERBOSE
	  
	  # Provient-il de Bill Gates?  Si oui, A LA POUBELLE !!!
	  :0 Hw
	  * ^.*[Ff]rom: bgates@microsoft.com
	  /dev/null

	  # liste linux-machin
	  :0 Hw
	  * ^.*[Ss]ender: owner-linux-machin@bidule.chouette.fr
	  liste-machin

	  # Message de EJ !
	  :0 Hw
	  * ^.*[Ff]rom: ej@monpote.fr
	  ej

	  # S'il arrive ici, l'envoyer dans mon classeur 'nouveaux'
	  :0 Hw
	  nouveaux
	</code>

	Maintenant, j'ai un fichier <tt/.procmailrc/ plut�t bien. Examinons 
	ce que <tt/procmail/ va faire de chaque message qu'il re�oit.

	Premi�rement, il regarde si ce message provient de
	<em/bgates@microsoft.com/. Si oui, il l'efface et c'est fait.
	
	Si le message est pass� � travers la premi�re v�rification, 
	<tt>procmail</tt> regarde s'il
	contient <em/owner-linux-machin@bidule.chouette.fr/ dans l'ent�te. Si
	oui, il le d�pose dans le classeur <tt/liste-machin/, et voil�.

	Si le message est aussi pass� � travers celle-l�, il regarde s'il provient
	d'<em/EJ/. Si oui, il le place dans le classeur <tt/ej/.

	Maintenant, si le message est pass� � travers tous ces tests, alors il le
	place dans le classeur <tt/nouveaux/.

	Une fois ces classeurs mis en place et <tt/procmail/ filtrant correctement
	vos messages, vous pouvez aller dans <tt/TkRat/, <tt>Admin - New/Edit
	  Folder</tt> et cr�er les classeurs pour chaque fichier. Ils seront plac�s
	dans <tt>/home/utilisateur/mail/nom_classeur</tt>

    <sect>Copyright
      <P>
	Ce HOWTO est Copyright 1998, 1999 par Dave Whitinger, 
	et c'est un document libre.
	Vous pouvez le redistribuer et/ou le modifier sous les termes de la GPL (GNU
	General Public License) publi�e par la FSF (Free Software Foundation)&nbsp;; soit
	la version 2 de la Licence, soit une version ult�rieure.


    <sect>Remerciements
      <P>
	Je remercie tout particuli�rement Martin Forsse'n
	(<em/maf@dtek.chalmers.se/) pour avoir cr�� l'<em/agent utilisateur de
	  messagerie/ <tt/TkRat/.

	Je remercie �galement le <em/North Texas Linux User's Group/ (Dallas, Texas),
	pour qui j'ai � l'origine �crit ce document d�but
	1998. Aujourd'hui aucun remerciement ne va plus � aucun groupe
	d'utilisateurs, car je vis dans les montagnes de l'Est du Tennessee, o�
	je pense �tre le seul utilisateur de Linux (ou m�me utilisateur d'un
	ordinateur) � 50 miles � la ronde.

	Notez aussi&nbsp;: <url url="mailto:nic@postilion.org" name="Nic Bernstein">
	a fait bifurquer le code de TkRat pour cr�er un agent de messagerie
	appel� <url url="http://www.postilion.org/" name="Postilion">.  Il
	semble �tre un excellent agent de messagerie avec un grand nombre de
	nouvelles fonctionnalit�s qui ne sont pas dans TkRat, mais, n�ammoins, 
	je continue � utiliser TkRat.

  </article>

