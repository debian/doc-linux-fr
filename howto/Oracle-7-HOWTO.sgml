<!doctype linuxdoc system>

<article>

<!-- Title Information -->

<title>Oracle Database HOWTO
<author>Paul Haigh, <tt><htmlurl url="mailto:paul@nailed.demon.co.uk" name="paul@nailed.demon.co.uk"></tt> &nl;
Adaptation fran�aise par St�phane Lee Chip Hing, 
<tt><htmlurl url="mailto:slee@ile-maurice.com" name="slee@ile-maurice.com"></tt>
<date>v1.2, 04 ao�t 1998
<abstract>
Un guide pour installer et configurer le Serveur de Base de Donn�es Oracle sur un syst�me Linux.
</abstract>

<!-- Table des mati�res -->
<toc>

<!-- Begin -->

<sect>Introduction
<sect1>Historique des Versions
<p>
<itemize>
<item><tt>v0.1 - 21 f�v. 1998 - Paul Haigh - Version Originale</tt>
<item><tt>v0.2 - 01 mars 1998 - Paul Haigh - Adjonction des Commentaires des Relecteurs</tt>
<item><tt>v1.0 - 10 mars 1998 - Paul Haigh - Publi� sous LDP </tt>
<item><tt>v1.1 - 20 juin 1998 - Paul Haigh - Section D�pannage ajout�e et rangement g�n�ral</tt>
<item><tt>v1.2 - 04 ao�t 1998 - Paul Haigh - Nouvelles d'Oracle Corp ajout�e et section sur les Am�loirations Futures supprim�e. </tt>
</itemize>

<sect1>Copyright
<p>
Le HOWTO Oracle Database est copyright (c) 1998, Paul Haigh.
<p>
Comme tous les documents HOWTO Linux, celui-ci peut �tre reproduit et distribu� en entier ou en extrait, sur n'importe quel support, physique ou �lectronique, tant que ce copyright est maintenu sur toutes les copies.
<p>

La redistribution commerciale est autoris�e et encourag�e. Cependant, l'auteur aimerait �tre avis� de telles distributions. Vous pouvez traduire ce HOWTO dans n'importe quelle langue tant que vous incluez une notice pr�cisant celui
 ou celle qui a traduit ce document.

<sect1>Mise en garde
<p>
Bien que j'ai essay� d'inclure les informations les plus correctes et les plus r�centes a ma disposition, je ne peux garantir que leur utilisation ne va pas occasionner des pertes de donn�es ou de mat�riel.
Je ne fournis AUCUNE GARANTIE sur les informations dans ce HOWTO et je ne suis responsable d'aucune cons�quence r�sultant de l'utilisation des informations de ce document.

<sect1>But de ce HOWTO

<p>
Dans ce HOWTO, je vais essayer de couvrir l'installation et l'administration basique d'une base de donn�es Oracle tournant sur une machine Linux. Je vais couvrir particuli�rement l'installation du serveur Oracle, la configuration de SQL*Net et du client.

<p>
Ce document n'est pas un tutoriel complet sur l'utilisation et l'administration d'une base de donn�es Oracle. Si c'est cela que vous recherchez, il y a de bons livres sur ces sujets publi�s par O'Reilly et autres.

<p>
Je ne vais pas non plus couvrir le d�veloppement des programmes Oracle sous UNIX. Si cela vous est absolument n�cessaire, je vous recommanderais d'acheter le syst�me de d�veloppement de SCO (avec OpenServer 5.x) , dont
 on m'a dit qu'il pouvait �tre obtenu pour un prix tr�s raisonnable US &dollar;19, � <url name="SCO" url="http://www.sco.com">
<sect1>Pr�-Requis

<p>
Je suppose acquises un certain nombre de notions pour comprendre la suite du HOWTO.
<itemize>
<item><bf>Le CD Serveur Oracle pour SCO OpenServer (Version 7.3.3.0.0.)</bf>
<descrip>
Ceci  <bf>doit</bf> �tre une copie l�gale.  N'oubliez pas qu'Oracle est une soci�t� commerciale et vend ses produits. Si vous voulez une base de donn�es compatible SQL gratuite, utilisez PostgreSQL ou quelque chose de
similaire.

Il est aussi possible d'installer Oracle, avec une licence d'�valuation de 60 jours, � partir d'un fichier <tt/tar/ t�l�chargeable sur le site web d'Oracle. Je ne l'ai pas personnellement essay� et ce n'est pas v�rifi�.

</descrip>

<item><bf>Un Serveur Linux</bf>

<descrip>
Vous n'auriez pas lu ceci sans un... N'est-ce pas?

</descrip>

<item><bf>Noyau 2.0.30+</bf>

<descrip>
Je ne peux garantir que les instructions seront fiables pour les autres noyaux. (ni pour le 2.0.30, d'ailleurs...)

</descrip>

<item><bf>iBCS</bf>

<descrip>
Il est tr�s important que ceci soit install� et tourne avec la version la plus r�cente possible pour votre plate-forme. (J'utilise iBCS-2.0-10.i386.rpm de RedHat Linux).

</descrip>

<item><bf>Beaucoup d'espace disque</bf>

<descrip>
600 Mb+ est une quantit� raisonnable. Il est possible d'installer avec moins de place mais vous devez faire des sacrifices, et je ne commence jamais avec �a. N�anmoins, je vais essayer de souligner les endroits o� l'on peut lib�rer de l'espace. 

</descrip>

<item><bf>32Mb+ Ram</bf>

<descrip>
Je sais que ceci peut sembler important, surtout en ce qui concerne Linux, mais n'oubliez pas que Oracle est un logiciel complexe. Vous n'auriez pas ces r�serves sur SCO!
<p>
Je ne dis pas que Oracle ne marchera pas avec moins, mais que c'est le
minimum recommand� par Oracle, ce que je n'aurais pas conseill�.
</descrip>

<item><bf>Licences de Oracle</bf>

<descrip>
Je sais que je l'ai deja mentionn� ceci mais je voudrais �tre clair
sur son importance. Utilisez les logiciels d'Oracle sans licence est ill�gal.
</descrip>

</itemize>


<sect1>Nouvelles d'Oracle Corporation
<p>
Oracle a c�d� sous la pression de la communaut� Linux. Oracle Corporation a d�cid� de supporter <bf>officiellement</bf> Oracle 8 sur la plate-forme Linux (i386). Elle doit �tre lanc� en d�cembre 1998, selon le site web d'Oracle.
<p>
Mieux encore, Oracle va aussi porter <tt>Oracle Applications</tt> sur Linux. Il doit �tre disponible dans le premier semestre 1999, selon le site web d'Oracle. 
<p>
R�f�rences:
<itemize>
<item><tt><htmlurl url="http://www.oracle.com/html/linux.html" name="http://www.oracle.com/html/linux.html"></tt>
<item><tt><htmlurl url="http://www.news.com/News/Item/0,4,24436,00.html" name="http://www.news.com/News/Item/0,4,24436,00.html"></tt>
<item><tt><htmlurl url="http://www.zdnet.com/pcweek/news/0720/20morac.html" name="http://www.zdnet.com/pcweek/news/0720/20morac.html"></tt>
</itemize>

<sect>Installation du logiciel Oracle

<sect1>Pr�paration du Serveur

<sect2>Cr�ation d'un Utilisateur Oracle

<p>
Nous avons �videmment besoin d'un utilisateur pour maintenir la base de donn�es Oracle. Comme nous n'avons l'intention de relier le noyau Oracle (plus sur ceci plus tard), nous devons accepter les noms d'utilisateur et de groupe par d�faut d'Oracle. Il inclut l'utilisateur ORACLE et le groupe DBA.

<enum>
<item>Se connecter comme root
<p>
<item>Cr�er l'utilisateur <tt/oracle/ et le groupe <tt/dba/.
<tscreen><code>
$ groupadd dba 
$ useradd  oracle
</code></tscreen>
<item> S'assurer que le r�pertoire personnel est cr�� pour l'utilisateur oracle.
<tscreen><code>
$ mkdir /home/oracle
$ mkdir /home/oracle/7.3.3.0.0 (Version of Oracle)
$ chown -R oracle.dba /home/oracle
</code></tscreen>
</enum>

<sect1>Installation depuis le CDROM

<p>
Malheureusement, l'Installateur Oracle sur le CD SCO ne marchera pas. Beaucoup de probl�mes peut �tre rencontr�s, des <tt/core dumps/ aux blocages. On doit donc copier les fichiers du CDROM manuellement et les d�compresser:
<p>
(S'assurer que le CDROM est mont� sur le syst�me).
<enum>
<item>Se connecter comme utilisateur Oracle
<item>Changer de r�pertoire pour <tt>/home/oracle/7.3.3.0.0.</tt>
<item>Copier les fichiers d'installation du CDROM
<tscreen><code>
$ cp -a /mnt/cdrom/* .
</code></tscreen>
<item>D�compresser les fichiers Oracle du CDROM.  
<tscreen><code>
$ find . -name *_ -exec ~/7.3.3.0.0/orainst/oiuncomp {} \;
</code></tscreen>
</enum>

<sect1>T�ches de Post Installation 

<sect2>T�ches pour Root
<p>
Ajouter les lignes suivantes dans <tt>/etc/profile</tt> ou  dans  <tt>.profile</tt> pour chaque utilisateur d'Oracle.
<tscreen><code>
# Oracle Specific
ORACLE_HOME=/home/oracle/7.3.3.0.0
ORACLE_SID=orcl
ORACLE_TERM=vt100
export ORACLE_HOME ORACLE_SID ORACLE_TERM

# Changer le chemin pour Oracle
PATH="$PATH:$ORACLE_HOME/bin"
</code></tscreen>

<p>
Nous devons aussi changer le propri�taire et les permissions de l'utilitaire Oracle d'augmentation de <tt/ulimit/.
<tscreen><code>
$ chown root.root $ORACLE_HOME/bin/osh
$ chmod u+s $ORACLE_HOME/bin/osh
</code></tscreen>

<sect2>T�ches pour Oracle
<p>
Changer les permissions pour les fichiers Oracle pour s'assurer de leur bonne ex�cution.
<tscreen><code>
$ chmod +x $ORACLE_HOME/bin/*
$ chmod u+s $ORACLE_HOME/bin/oracle
</code></tscreen>

<p>
Les outils Oracle demandent que les messages soient dans le r�pertoire <tt>&dollar;ORACLE_HOME/tool_name/mesg</tt>.
Donc, d�placez les fichiers  <tt/msb/  des r�pertoires  <tt/msg_ship/ aux r�pertoires <tt/mesg/.
<tscreen><code>
$ mv $ORACLE_HOME/plsql/mesg/mesg_ship/* $ORACLE_HOME/plsql/mesg/.
$ mv $ORACLE_HOME/rdbms/mesg/mesg_ship/* $ORACLE_HOME/rdbms/mesg/.
$ mv $ORACLE_HOME/svrmgr/mesg/mesg_ship/* $ORACLE_HOME/svrmgr/mesg/.
</code></tscreen>
Cr�er les r�pertoires suivants, s'ils n'existent pas:
<tscreen><code>
$ mkdir $ORACLE_HOME/rdbms/log
$ mkdir $ORACLE_HOME/rdbms/audit
$ mkdir $ORACLE_HOME/network/log
</code></tscreen>
<sect2>Ceux que vous pouvez enlever
<p>
Les r�pertoires suivants peuvent �tre enlev�s sans probl�me:
<itemize>
<item><tt>&dollar;ORACLE_HOME/guicommon2/</tt>
<item><tt>&dollar;ORACLE_HOME/ctx/</tt>
<item><tt>&dollar;ORACLE_HOME/md/</tt>
<item><tt>&dollar;ORACLE_HOME/mlx/</tt>
<item><tt>&dollar;RACLE_HOME/precomp/</tt>
<item><tt>&dollar;ORACLE_HOME/slax/</tt>
</itemize>
<sect>Cr�ation d'une base de donn�es
<p>
Maintenant que le serveur Oracle est install�, nous devons cr�er une base de donn�es pour tester l'installation.
<tt/Si vous utilisez Oracle 7.2.x ou une version anterieure, veuillez lire la section D�pannage ci-dessous./
<sect1>Cr�er le fichier d'Initialisation
<p>
Copiez <tt>&dollar;ORACLE_HOME/dbs/init.ora</tt> vers <tt>&dollar;ORACLE_HOME/dbs/initorcl.ora</tt>:
<tscreen><code>
$ cd $ORACLE_HOME/dbs
$ cp init.ora initorcl.ora
</code></tscreen>
Le modifier en ajoutant les lignes suivantes:
<tscreen><code>
db_name = orcl
COMPATIBLE=7.3.3.0.0
</code></tscreen>


<sect1>Cr�ation du script pour l'installation de la base de donn�es
<p>
Cr�er un fichier script nomm� <tt/makedb.sql/ dans le r�pertoire  <tt>&dollar;ORACLE_HOME/dbs</tt>:
<tscreen><code>
connect internal
startup nomount
set echo on
spool makedb.log
create database orcl
	maxinstances 1
	maxlogfiles  8
	datafile '$ORACLE_HOME/dbs/orcl_syst_01.dbf' size 40M reuse
	logfile
		'$ORACLE_HOME/dbs/orcl_redo_01.dbf' size 1M reuse,
		'$ORACLE_HOME/dbs/orcl_redo_02.dbf' size 1M reuse,
		'$ORACLE_HOME/dbs/orcl_redo_03.dbf' size 1M reuse;
@$ORACLE_HOME/rdbms/admin/catalog.sql
create tablespace rollback
	datafile '$ORACLE_HOME/dbs/orcl_roll_01.dbf' size 8.5M reuse;
create tablespace temp
	datafile '$ORACLE_HOME/dbs/orcl_temp_01.dbf' size 5M reuse 
	temporary;
create tablespace users
	datafile '$ORACLE_HOME/dbs/orcl_user_01.dbf' size 10M reuse;
create rollback segment r1 tablespace rollback
	storage ( optimal 5M );
alter rollback segment r1 online;
connect system/manager
@$ORACLE_HOME/rdbms/admin/catdbsyn.sql
connect internal
@$ORACLE_HOME/rdbms/admin/catproc.sql
connect system/manager
@$ORACLE_HOME/sqlplus/admin/pupbld.sql
spool off
exit
</code></tscreen>

<sect1>Ex�cuter le script pour l'installation de la base de donn�es
<p>
D�marrer <tt/svrmgrl/ et ex�cuter le script:
<tscreen><code>
$ cd $ORACLE_HOME/dbs
$ svrmgrl

Oracle Server Manager Release 2.3.3.0.0 - Production

Copyright (c) Oracle Corporation 1994, 1995. All rights reserved.

Oracle7 Server Release 7.3.3.0.0 - Production Release
PL/SQL Release 2.3.3.0.0 - Production

SVRMGR> connect internal
Connected.
SVRMGR> startup nomount
ORACLE instance started.
Total System Global Area       4313312 bytes
Fixed Size                       41876 bytes
Variable Size                  4140364 bytes
Database Buffers                122880 bytes
Redo Buffers                      8192 bytes
SVRMGR> @makedb
<beaucoup de messages>
SVRMGR> exit
Server Manager complete.
</code></tscreen>
<sect1>D�marrer la base de donn�es
<p>
Premi�rement, nous devons lancer la base de donn�es manuellement (nous l'automatiserons plus tard). Pour d�marrer une base de donn�es Oracle, nous devons �mettre la commande <tt>startup</tt> en �tant connect� localement:
<tscreen><code>
$ svrmgrl

Oracle Server Manager Release 2.3.3.0.0 - Production

Copyright (c) Oracle Corporation 1994, 1995. All rights reserved.

Oracle7 Server Release 7.3.3.0.0 - Production Release
PL/SQL Release 2.3.3.0.0 - Production

SVRMGR> connect internal
Connected.
SVRMGR> startup
ORACLE instance started.
Total System Global Area       4313316 bytes
Fixed Size                       41876 bytes
Variable Size                  4140368 bytes
Database Buffers                122880 bytes
Redo Buffers                      8192 bytes
Database mounted.
Database opened.
SVRMGR> exit
Server Manager complete.
</code></tscreen>
<sect1>Arr�ter la base de donn�es
<p>
Il est important de mentionner ici que red�marrer un serveur Linux sans fermer
auparavant la base de donn�es Oracle risque fort de corrompre la base de 
donn�es.
<p>
Donc, avant d'�mettre la commande Linux <tt>shutdown</tt>, il est sage de fermer le base de donn�es:
<tscreen><code>
$ svrmgrl

Oracle Server Manager Release 2.3.3.0.0 - Production

Copyright (c) Oracle Corporation 1994, 1995. All rights reserved.

Oracle7 Server Release 7.3.3.0.0 - Production Release
PL/SQL Release 2.3.3.0.0 - Production

SVRMGR> connect internal
Connected.
SVRMGR> shutdown
Database closed.
Database dismounted.
ORACLE instance shut down.
SVRMGR> exit
Server Manager complete.
</code></tscreen>
<sect1>Cr�er un Utilisateur par d�faut
<p>
La base de donn�es, telle qu'elle a �t� cr��e, a deux utilisateurs sp�ciaux qui sont cr��s automatiquement. Ce sont;
<tscreen><code>
Username		Password

SYSTEM			MANAGER
SYS			change_on_install
</code></tscreen>

<p>
Ces utilisateurs sont typiquement utilis�s pour maintenir les informations sur le dictionnaire standard de donn�es pour la base de donn�es. C'est une bonne id�e de changer les mots de passe par d�faut aussit�t que possible.

<p>
Ceci peut �tre fait par:
<tscreen><code>
sqlplus system/manager

SQL*Plus: Release 3.3.3.0.0 - Production on Sat Feb 21 12:43:33 1998

Copyright (c) Oracle Corporation 1979, 1996.  All rights reserved.


Connected to:
Oracle7 Server Release 7.3.3.0.0 - Production Release

SQL> alter user system identified by <newpassword>;

User altered.

SQL> alter user sys identified by <newpassword>;

User altered.

SQL> exit;
Disconnected from Oracle7 Server Release 7.3.3.0.0 - Production Release
PL/SQL Release 2.3.3.0.0 - Production
</code></tscreen>

<p>
Comme l'utilisateur <tt>system/manager</tt> est similaire � l'utilisateur <tt/root/ sur une machine UNIX, nous devons cr�er un utilisateur avec moins de pouvoir
 � causer des d�gats possibles (rappelez-vous de lancer la base de donn�es avant d'essayer de cr�er un utilisateur).
<p>
Se connecter � SQL*Plus et cr�er un utilisateur:
<tscreen><code>
$ sqlplus system/manager

SQL*Plus: Release 3.3.3.0.0 - Production on Sat Feb 21 12:43:33 1998

Copyright (c) Oracle Corporation 1979, 1996.  All rights reserved.


Connected to:
Oracle7 Server Release 7.3.3.0.0 - Production Release
PL/SQL Release 2.3.3.0.0 - Production

SQL> create user <user> identified by <psw> 
  2  default tablespace users 
  3  temporary tablespace temp;

User created.

SQL> grant connect, resource to <user>

Grant succeeded.

SQL> exit
Disconnected from Oracle7 Server Release 7.3.3.0.0 - Production Release
PL/SQL Release 2.3.3.0.0 - Production
</code></tscreen>
<p>
Maintenant que vous avez un nouvel utilisateur sur le syst�me, vous pouvez jouer avec. Pour se connecter sur la base de donn�es Oracle:
<tscreen><code>
$ sqlplus <user>/<password>
</code></tscreen>
<p>
Si ceci s'ex�cute sans messages d'erreur, vous avez donc une base de donn�es Oracle qui marche. Si vous ne voulez vous connecter qu'� partir de ce
serveur, votre travail est donc termin�. Amusez-vous!
<p>
Si, cependant, comme la plupart des gens, vous voulez configurer la partie r�seau du logiciel pour que vous puissiez vous connecter depuis d'autres machine, continuez votre lecture.


<sect>Configurer SQL*Net sur le Serveur
<p>
Tous ces fichiers configurent la partie r�seau d'Oracle (SQL*Net ou Net8 pour Oracle8). Ces fichiers doivent �tre cr��s sur le serveur dans le r�pertoire <tt>&dollar;ORACLE_HOME/network/admin</tt>.
<sect1><tt>tnsnames.ora</tt>
<p>
Le fichier <tt/TNSNAMES.ORA/ identifie les services disponibles depuis la machine. Dans notre cas, nous d�crirons toutes les bases de donn�es que le serveur a mont�. Pour chaque base de donn�es sur votre serveur, ajouter une section comme ci-dessous:

<tscreen><code>
orcl.world =
  (DESCRIPTION =
    (ADDRESS_LIST =
        (ADDRESS =
          (COMMUNITY = tcp.world)
          (PROTOCOL = TCP)
          (Host = <INSERER LE NOM LOGIQUE DU SERVEUR ICI> )
          (Port = 1521)
        )
        (ADDRESS =
          (COMMUNITY = tcp.world)
          (PROTOCOL = TCP)
          (Host = <INSERER LE NOM LOGIQUE DU SERVEUR ICI> )
          (Port = 1526)
        )
    )
    (CONNECT_DATA = (SID = ORCL)
    )
  )
</code></tscreen>
<sect1><tt>listener.ora</tt>
<p>
Le fichier <tt>listener.ora</tt> contient la description des services auxquels d'autres machines sont autoris�es � se connecter et toutes les configurations n�cessaires pour l'�couteur du serveur.

<p>
Il contient les sections pour le nom de l'�couteur, son
adresse, les bases de donn�es desservies par l'�couteur et
les param�tres de configuration.
 
<p>Voici un exemple:
<tscreen><code>
# Nom de l'�couteur et les adresses a �couter
LISTENER =
        ( ADDRESS_LIST =
                (ADDRESS =
                        (PROTOCOL=tcp)
                        (HOST=<INSERT HOST>)
                        (PORT=1521)
                        (COMMUNITY=UK_SUP_TCPIP)
                )
                (ADDRESS =
                        (PROTOCOL=ipc)
                        (KEY=700)
                        (COMMUNITY=UK_SUP_TCPIP)
                )
        )

# Liste des services d�sservis par l'�couteur
SID_LIST_LISTENER=
        (SID_LIST=
                (SID_DESC=
                        (SID_NAME=orcl)
                        (ORACLE_HOME=/home/oracle/7.3.3.0.0)
                )
        )

# D�but des param�tres de configuration
TRACE_LEVEL_LISTENER=OFF
TRACE_FILE_LISTENER = "listener"
LOG_FILE_LISTENER = "listener"
CONNECT_TIMEOUT_LISTENER = 10
STOP_LISTENER = YES
DBA_GROUP = dba
</code></tscreen>
<sect1><tt>sqlnet.ora</tt>
<p>
Le fichier <tt>sqlnet.ora</tt> contient la configuration pour le noeud du r�seau. Ceci est ind�pendant du nombre de bases de donn�es ou du nombre d'�couteurs. La chose la plus importante dans ce fichier est la variable de configuration
 <tt>Timeout des Connexions Mortes</tt>.
<p>

Le timeout des connexions mortes v�rifie chaque processus entrant � la base de donn�es et assure que le c�t� client r�pond
toujours. Si le client (de tout type) ne r�pond pas, le processus en t�che de fond du serveur Oracle sera tu�.
<p>
Ceci est tr�s utile si vous avez plusieurs clients qui acc�dent � la base de donn�es, surtout pendant la phase de d�veloppement o� ces clients ne r�ussiront certainement pas � sortir proprement de la base de donn�es Oracle.

<p>
Voici une copie de mon fichier <tt>sqlnet.ora</tt> pour vous servir d'exemple:
<tscreen><code>
TRACE_LEVEL_CLIENT = OFF
sqlnet.expire_time = 30 	# le nombre de secondes entre les v�rifications des clients.
names.default_domain = world
name.default_zone = world
</code></tscreen>

<sect1>Lancer et Arr�ter les Ecouteurs.
<p>
Maintenant que la configuration des �couteurs et de SQL*Net est finie, nous pouvons essayer de connecter la base de donn�es en utilisant la partie r�seau. (Avant nous utilisions l'acc�s direct � la base de donn�es, alors qu'ici nous simulons une connexion depuis une machine cliente distante).

<p>
Pour lancer l'�couteur en utilisant la configuration ci-dessus:
<tscreen><code>
$ lsnrctl

LSNRCTL for SCO System V/386: Version 2.3.3.0.0 - Production on 23-FEB-98 20:38:25

Copyright (c) Oracle Corporation 1994.  All rights reserved.

Welcome to LSNRCTL, type "help" for information.

LSNRCTL> start
Starting /home/oracle/7.3.3.0.0/bin/tnslsnr: please wait...

TNSLSNR for SCO System V/386: Version 2.3.3.0.0 - Production
System parameter file is /home/oracle/7.3.3.0.0/network/admin/listener.ora
Log messages written to /home/oracle/7.3.3.0.0/network/log/listener.log
Listening on: (ADDRESS=(PROTOCOL=tcp)(DEV=6)(HOST=192.168.1.1)(PORT=1521))
Listening on: (ADDRESS=(PROTOCOL=ipc)(DEV=10)(KEY=700))

Connecting to (ADDRESS=(PROTOCOL=tcp)(HOST=magic.com)(PORT=1521)(COMMUNITY=UK_SUP_TCPIP))
STATUS of the LISTENER
------------------------
Alias                     LISTENER
Version                   TNSLSNR for SCO System V/386: Version 2.3.3.0.0 - Production
Start Date                23-FEB-98 20:38:50
Uptime                    0 days 0 hr. 0 min. 0 sec
Trace Level               off
Security                  OFF
SNMP                      ON
Listener Parameter File   /home/oracle/7.3.3.0.0/network/admin/listener.ora
Listener Log File         /home/oracle/7.3.3.0.0/network/log/listener.log
Services Summary...
  orcl          has 1 service handler(s)
The command completed successfully
LSNRCTL> exit
</code></tscreen>
<p>
Pour arr�ter les �couteurs:
<tscreen><code>
$ lsnrctl

LSNRCTL for SCO System V/386: Version 2.3.3.0.0 - Production on 23-FEB-98 20:43:20

Copyright (c) Oracle Corporation 1994.  All rights reserved.

Welcome to LSNRCTL, type "help" for information.

LSNRCTL> stop
Connecting to (ADDRESS=(PROTOCOL=tcp)(HOST=magic.com)(PORT=1521)(COMMUNITY=UK_SUP_TCPIP))
The command completed successfully
LSNRCTL> exit
</code></tscreen>
<p>
Si vous avez une configuration DNS qui ne retourne pas l'adresse IP pour le nom logique specifi�, le lancement et l'arr�t de l'�couteur prendront donc in certain temps. (2-3 mins. d�pendant de la variable timeout du DNS). Si c'est le cas
, ne vous inqui�tez pas. Soyez patient.


<sect>Configuration du Client
<sect1>Clients Windows
<p>
La configuration SQL*Net sur un PC avec les versions r�centes du logiciel Client Oracle est tr�s facile. La meilleure fa�on (et la plus facile) pour r�ussir l'installation d'un client qui marche enti�rement, est d'utiliser l'outil <tt>SQL*Net Easy Configuration</tt> fourni par Oracle.
<p>
Cet outil a une interface qui vous guidera dans l'installation des fichiers <tt/tnsnames.ora/ et <tt/sqlnet.ora/.
<p>
S�lectionnez "Add Database Alias" et entrez le nom pour l'alias demand�.  Cet alias est le nom par lequel vous allez vous r�f�rer � l'instance de la base de donn�es, et doit donc �tre le m�me que celui de l'instance (orcl dans ce cas).
<p>
S�lectionnez TCP/IP comme protocole, le nom logique de la machine h�bergeant la base de donn�es et le nom de l'instance de la base de donn�es.  
<p>
C'est tout.
<p>
Cependant, si vous n'avez pas l'outil <tt>SQL*Net Easy Configuration Tool</tt>, ne vous inquietez pas. Vous pouvez simplement cr�er les fichiers <tt>tnsnames.ora</tt> et <tt>sqlnet.ora</tt> dans le r�pertoire <tt>&dollar;ORACLE_HOME/network/admin</tt> sur le client, exactement comme sur le serveur. 
Il fournira comme alias le m�me que sur le serveur (toujours une bonne id�e de toute fa�on).
<sect1>Clients Unix
<p>
Les clients UNIX ne sont pas tr�s diff�rents des clients Windows. Si vous avez <tt/Network Manager/ d'Oracle, alors utilisez-le
de la m�me fa�on que ci-dessus, sinon vous pouvez utiliser juste les m�mes fichiers de configuration
que sur le serveur dans le r�pertoire <tt>&dollar;ORACLE_HOME/network/admin</tt>.
<sect>Lancement et Arr�t Automatique
<sect1><tt/dbstart/ et <tt/dbstop/
<p>
Le d�marrage et l'arr�t automatique de la base de donn�es Oracle peuvent �tre effectu�s (avec la version 7.3.3.0.0) avec les 
fichiers <tt/dbstart/ et <tt/dbshut/, tous deux fournis par Oracle. Ces fichiers d�pendent sur l'existence du 
fichier <tt>/etc/oratab</tt> pour fonctionner (bien qu'en modifiant les fichiers <tt/dbshut/ et <tt/dbstart/ il puissent �tre d�plac�s).
<p>
Le format du fichier <tt>/etc/oratab</tt> est le suivant:
<tscreen><code>
SID:ORACLE_HOME:AUTO
</code></tscreen>
<p>
Un  exemple:
<tscreen><code>
orcl:/home/oracle/7.3.3.0.0:Y
leaveup:/home/oracle/7.3.2.1.0:N
</code></tscreen>
<sect1><tt/init.d/ et <tt/rc.d/
<p>
Pour d�marrer et arr�ter la base de donn�es quand la machine se lance ou s'�teint, il faut modifier les routines
de d�marrage pour la machine Linux. Ceci est tr�s facile, bien que je devrais
 souligner que cela peut changer suivant
la distribution Linux (Slackware, Debian, RedHat, etc).  Je vais montrer quelques exemples qui marchent avec RedHat Linux 5.0. Pour modifier ceux-ci pour votre propre distribution Linux, veuillez voir votre documentation Linux. (bien que
ceci doit �tre valable pour n'importe quel UNIX de type Sys V)
 
<p>
Premi�rement, nous devons cr�er le script qui ex�cutera  <tt/dbshut/ et <tt/dbstart/ dans le r�pertoire  <tt>/etc/rc.d/init.d</tt> . Cr�ez le fichier suivant comme <tt>/etc/rc.d/init.d/oracle</tt>:
<tscreen><code>
#!/bin/sh
#
# /etc/rc.d/init.d/oracle
# Description: Starts and stops the Oracle database and listeners
# See how we were called.
case "$1" in
  start)
        echo -n "Starting Oracle Databases: "
        echo "----------------------------------------------------" >> /var/log/oracle
        date +"! %T %a %D : Starting Oracle Databases as part of system up." >> /var/log/oracle
        echo "----------------------------------------------------" >> /var/log/oracle
        su - oracle -c dbstart >> /var/log/oracle
        echo "Done."
        echo -n "Starting Oracle Listeners: "
        su - oracle -c "lsnrctl start" >> /var/log/oracle
        echo "Done."
        echo ""
        echo "----------------------------------------------------" >> /var/log/oracle
        date +"! %T %a %D : Finished." >> /var/log/oracle
        echo "----------------------------------------------------" >> /var/log/oracle
        touch /var/lock/subsys/oracle
        ;;
  stop)
        echo -n "Shutting Down Oracle Listeners: "
        echo "----------------------------------------------------" >> /var/log/oracle
        date +"! %T %a %D : Shutting Down Oracle Databases as part of system down." >> /var/log/oracle
        echo "----------------------------------------------------" >> /var/log/oracle
        su - oracle -c "lsnrctl stop" >> /var/log/oracle
        echo "Done."
        rm -f /var/lock/subsys/oracle
        echo -n "Shutting Down Oracle Databases: "
        su - oracle -c dbshut >> /var/log/oracle
        echo "Done."
        echo ""
        echo "----------------------------------------------------" >> /var/log/oracle
        date +"! %T %a %D : Finished." >> /var/log/oracle
        echo "----------------------------------------------------" >> /var/log/oracle
        ;;
  restart)
        echo -n "Restarting Oracle Databases: "
        echo "----------------------------------------------------" >> /var/log/oracle
        date +"! %T %a %D : Restarting Oracle Databases as part of system up." >> /var/log/oracle
        echo "----------------------------------------------------" >> /var/log/oracle
        su - oracle -c dbstop >> /var/log/oracle
        su - oracle -c dbstart >> /var/log/oracle
        echo "Done."
        echo -n "Restarting Oracle Listeners: "
        su - oracle -c "lsnrctl stop" >> /var/log/oracle
        su - oracle -c "lsnrctl start" >> /var/log/oracle
        echo "Done."
        echo ""
        echo "----------------------------------------------------" >> /var/log/oracle
        date +"! %T %a %D : Finished." >> /var/log/oracle
        echo "----------------------------------------------------" >> /var/log/oracle
        touch /var/lock/subsys/oracle
        ;;
  *)
        echo "Usage: oracle {start|stop|restart}"
        exit 1
esac
</code></tscreen>
<p>
Il vaut la peine de v�rifier si le fichier arr�te et d�marre 
vraiment correctement les bases de donn�es pour le syst�me.
V�rifiez le fichier de log, <tt>/var/log/oracle</tt>, pour les messages d'erreur.
<p>
Une fois que le script marche, nous devons cr�er les liens symboliques de <tt/start/ et <tt/kill/ dans les r�pertoires appropri�s de niveau d'ex�cution (runlevel)
<tt>/etc/rc.d/rcX.d</tt>.
<p>
Les commandes suivantes assureront que les bases de donn�es vont d�marrer dans les niveaux d'ex�cution 2,3 et 4:
<tscreen><code>
$ ln -s ../init.d/oracle /etc/rc.d/rc2.d/S99oracle
$ ln -s ../init.d/oracle /etc/rc.d/rc3.d/S99oracle
$ ln -s ../init.d/oracle /etc/rc.d/rc4.d/S99oracle
</code></tscreen>
<p>
Pour arr�ter les bases de donn�es avant un r�amor�age ou arr�t, nous avons besoin des liens suivants: 
<tscreen><code>
$ ln -s ../init.d/oracle /etc/rc.d/rc0.d/K01oracle          # Halting
$ ln -s ../init.d/oracle /etc/rc.d/rc6.d/K01oracle          # Rebooting
</code></tscreen>
<sect>Autres trucs
<sect1>Agent Intelligent
<p>
Si vous avez besoin d'un <tt>Agent Intelligent Oracle</tt>, vous pouvez l'ex�cuter sans changement
de la configuration. 
<p>
Pour d�marrer le AI, essayez:
<tscreen><code>
$ lsnrctl dbsnmp_start
</code></tscreen>
<p>
Pour arr�ter l'AI, essayez:
<tscreen><code>
$ lsnrctl dbsnmp_stop
</code></tscreen>
<p>
Il ne semble pas y avoir de message indiquant le lancement avec succ�s ou non de l'agent intelligent. 
Cependant, l'AI r�pond au gestionnaire <em/Enterprise/ sur le
c�t� client. Donc, je ne peux que supposer qu'il marche.

<sect>D�pannage
<p>
Voir ci-dessous les diff�rents conseils de d�pannage.
<sect1>Je ne peux pas cr�er une base de donn�es en utilisant Oracle 7.2.x.
<p>
Les fichiers exp�di�s par Oracle dans les versions 7.2.x sont incorrects en supposant que vous voulez installer une configuration parall�le de serveur. Le fichier <tt/init.ora/ contient la ligne suivante:
<tscreen><code>
# define parallel server (multi-instance) parameters
ifile = ora_system:initps.ora
</code></tscreen>
  Pour fixer le probl�me, mettez la en commentaire:
<tscreen><code>
# define parallel server (multi-instance) parameters
#ifile = ora_system:initps.ora
</code></tscreen>
<sect1>J'obtiens les d�fauts de segmentation dans le  <tt/svrmgrl/ de la version 7.3.4.x.
<p>
Ce probl�me m'a �t� signal� par un certain nombre de personnes.  Gerald Weber <tt/gerald_weber@master.co.at/ l'a r�solu: 
<tscreen><code>
Bonjour Paul,

D'abord merci pour votre aide, mais aucun des probl�mes possubles que vous pensez n'�tait responsable de mon probl�me. 
Le probl�me esy l'�mulateur iBCS lui-m�me.
Il semble que Oracle ex�cute les appels sysconf qui ne sont pas support�s dans la version en cours d'iBCS. 
Regardez la trace:

<7>[22]615 sysconf(34)
<7>iBCS2 unsupported sysconf call 34
<7>[22]615 sysconf error return linux=-22 -> ibcs=22 <Invalid argument>
<7>[24]615 sysconf(34)
<7>iBCS2 unsupported sysconf call 34
<7>[24]615 sysconf error return linux=-22 -> ibcs=22 <Invalid argument>

Solution:  patcher iBCS-source.apply avec le diff-patch suivant:


--- sysconf.c   Sun Apr 19 19:19:15 1998
+++ sysconf.c.ori       Sun Apr 19 19:28:45 1998
@@ -60,7 +60,6 @@
 #define _SC_JOB_CONTROL        5
 #define _SC_SAVED_IDS  6
 #define _SC_VERSION    7
-#define _SC_HACK_FOR_ORACLE 34

 #define _SC_PAGESIZE   11

@@ -97,11 +96,6 @@
                case _SC_SAVED_IDS: {
                        return (1);
                }
-
-               case _SC_HACK_FOR_ORACLE: {
-                 return (1);
-               }
-

                case _SC_PAGESIZE: {
                  return PAGE_SIZE;

</code></tscreen>

<sect>Cr�dits
<p>
Ce document est bas� sur un document �crit par <htmlurl name="Bob Withers" url="mailto:bwit@pobox.com">. Des informations suppl�mentaires sont
extraites des documents �crits par <htmlurl name="Georg Rehfeld" url="mailto:rehfeld@wmd.de"> et <htmlurl name="David Mansfield" url="mailto:david@claremont.com">.
<p>
Relectures suppl�mentaires faites par Bob Withers, <htmlurl name="Mark Watling" url="mailto:mwatling@mjw-ltd.demon.co.uk">, <htmlurl name="Peter Sodhi" url="mailto:petersodhi@unn.unisys.com"> et <htmlurl name="Greg Hankins" url="mailto:greg.hankins@cc.gatech.edu">.
<p>
Mes remerciements vont au support immense de la part des gens 
impliqu�s dans ce document et aux recherches qu'ils ont effectu�es. Remerciements particuliers � Bob Withers et Mark Watling pour leurs commentaires additionnels et l'aide qu'ils m'ont apport�e.
</article>
