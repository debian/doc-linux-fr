<!DOCTYPE LINUXDOC SYSTEM>

  <article>
      <title>Le Linux Serial Programming HOWTO
      <author>par Peter H. Baumann, <TT><HTMLURL URL="mailto:Peter.Baumann@dlr.de" NAME="Peter.Baumann@dlr.de"></TT>
            &nl;
Adaptation fran�aise Etienne BERNARD <TT><HTMLURL URL="mailto:eb@via.ecp.fr" NAME="eb@via.ecp.fr"></TT>
      <date>v1.0, 22 janvier 1998
      <abstract>
Ce document d�crit comment programmer sous Linux la communication avec des p�riph�riques sur port s�rie.
</abstract>
    <TOC>
    
    <SECT>Introduction
      <P>
Voici le Linux Serial Programming HOWTO, qui explique comment programmer sous Linux la communication avec des p�riph�riques ou des ordinateurs via le port s�rie. Diff�rentes techniques sont abord�es&nbsp;: Entr�es/Sorties canoniques (envoi ou r�ception ligne par ligne), asynchrones, ou l'attente de donn�es depuis de multiples sources.
      <P>
Ce document ne d�crit pas comment configurer les ports s�ries, puisque c'est d�crit par Greg Hankins dans le Serial-HOWTO.
      <P>
Je tiens � insister sur le fait que je ne suis pas un expert dans ce domaine, mais j'ai eu � r�aliser un projet utilisant la communication par le port s�rie. Les exemples de code source pr�sent�s dans ce document sont d�riv�s du programme <tt/miniterm/, disponible dans le <EM/Linux programmer's guide/ (<TT>ftp://sunsite.unc.edu/pub/Linux/docs/LDP/programmers-guide/lpg-0.4.tar.gz</TT> et les miroirs, par exemple <TT>ftp://ftp.lip6.fr/pub/linux/docs/LDP/programmers-guide/lpg-0.4.tar.gz</TT>) dans le r�pertoire contenant les exemples.
      <P>
Depuis la derni�re version de ce document, en juin 1997, j'ai d� installer Windows NT pour satisfaire les besoins des client, et donc je n'ai pas pu investiguer plus en avant sujet. Si quelqu'un a des commentaires � me faire, je me ferai un plaisir de les inclure dans ce document (voyez la section sur les commentaires). Si vous d�sirez prendre en main l'�volution de ce document, et l'am�liorer, envoyez moi un courrier �lectronique.
      <P>
Tous les exemples ont �t� test�s avec un i386, utilisant un noyau Linux de version 2.0.29.

      <SECT1>Copyright
        <P>
Le Linux Serial-Programming-HOWTO est copyright (c) 1997 Peter Baumann. Les HOWTO de Linux peuvent �tre reproduits et distribu�s int�gralement ou seulement par partie, sur quelconque support physique ou �lectronique, aussi longtemps que ce message de copyright sera conserv� dans toutes les copies. Une redistribution commerciale est autoris�e, et encourag�e; cependant, l'auteur <EM/appr�cierait/ d'�tre pr�venu en cas de distribution de ce type.
        <P>
Toutes les traductions ou travaux d�riv�s incorporant un document HOWTO Linux doit �tre plac� sous ce copyright. C'est-�-dire que vous ne pouvez pas produire de travaux d�riv�s � partir d'un HOWTO et imposer des restrictions additionnelles sur sa distribution. Des exceptions � cette r�gle peuvent �tre accord�es sous certaines conditions&nbsp; ; contactez le coordinateur des HOWTO Linux � l'adresse donn�e ci-dessous.
        <P>
En r�sum�, nous d�sirons promouvoir la distribution de cette information par tous les moyens possibles. N�anmoins, nous d�sirons conserver le copyright sur les documents HOWTO, et nous <EM/aimerions/ �tre inform�s de tout projet de redistribution des HOWTO.
        <P>
Pour toute question, veuillez contacter Greg Hankins, le coordinateur des HOWTO Linux, � <TT><HTMLURL URL="mailto:gregh@sunsite.unc.edu" NAME="gregh@sunsite.unc.edu"></TT> par mail.

      <SECT1>Nouvelles versions de ce document.
        <P>
Les nouvelles version du Serial-Programming-HOWTO seront disponibles �<NEWLINE>
<TT><HTMLURL URL="ftp://sunsite.unc.edu:/pub/Linux/docs/HOWTO/Serial-Programming-HOWTO" NAME="ftp://sunsite.unc.edu:/pub/Linux/docs/HOWTO/Serial-Programming-HOWTO"></TT> et les sites miroir, comme par exemple <TT><HTMLURL URL="ftp://ftp.lip6.fr/pub/linux/docs/HOWTO/Serial-Programming-HOWTO" NAME="ftp://ftp.lip6.fr/pub/linux/docs/HOWTO/Serial-Programming-HOWTO"></TT>. Il existe sous d'autres formats, comme PostScript ou DVI dans le sous r�pertoire <TT/other-formats/. Le Serial-Programming-HOWTO est �galement disponible sur <TT><HTMLURL URL="http://sunsite.unc.edu/LDP/HOWTO/Serial-Programming-HOWTO.html" NAME="http://sunsite.unc.edu/LDP/HOWTO/Serial-Programming-HOWTO.html"></TT>, et sera post� dans <TT><HTMLURL URL="news:comp.os.linux.answers" NAME="comp.os.linux.answers"></TT> tous les mois (NdT&nbsp;: la version fran�aise de ce document est �galement post�e dans <TT><HTMLURL URL="news:fr.comp.os.linux.annonce" NAME="fr.comp.os.linux.annonce"></TT> tous les mois).

      <SECT1>Commentaires
        <P>
Envoyez moi, s'il vous pla�t toute correction, question, commentaire, suggestion ou compl�ment. Je d�sire am�liorer cet HOWTO&nbsp;! Dites moi exactement ce que vous ne comprenez pas, ou ce qui pourrait �tre plus clair. Vous pouvez me contacter � <TT><HTMLURL URL="mailto:Peter.Baumann@dlr.de" NAME="Peter.Baumann@dlr.de"></TT> par courrier �lectronique. Veuillez inclure le num�ro de version de ce document pour tout courrier. Ce document est la version 0.3.

    <SECT>D�marrage
      <P>
      <SECT1>D�buggage
        <P>
Le meilleur moyen de d�bugguer votre code est d'installer une autre machine sous Linux et de connecter les deux ordinateurs par un c�ble null-modem. Utilisez <TT/miniterm/ (disponible dans le Linux programmers guide -- <TT>ftp://sunsite.unc.edu/pub/Linux/docs/LDP/programmers-guide/lpg-0.4.tar.gz</TT> -- dans le r�pertoire des exemples) pour transmettre des caract�res � votre machine Linux. <TT/Miniterm/ est tr�s simple � compiler et transmettra toute entr�e clavier directement sur le port s�rie. Vous n'avez qu'� adapter la commande <TT>#define MODEMDEVICE "/dev/ttyS0"</TT> � vos besoins. Mettez <TT>ttyS0</TT> pour COM1, <TT>ttyS1</TT> for COM2, etc... Il est essentiel, pour les tests, que <EM/tous/ les caract�res soient transmis bruts (sans traitements) au travers de la ligne s�rie. Pour tester votre connexion, d�marrez <TT/miniterm/ sur les deux ordinateurs et taper au clavier. Les caract�res �crit sur un ordinateur devraient appara�tre sur l'autre ordinateur, et vice versa. L'entr�e clavier sera �galement recopi�e sur l'�cran de l'ordinateur local.
        <P>
Pour fabriquer un c�ble null-modem, pour devez croiser les lignes TxD (<EM/transmit/) et RxD (<EM/receive/). Pour une description du c�ble, r�f�rez vous � la section 7 du Serial-HOWTO.
        <P>
Il est �galement possible de faire cet essai avec uniquement un seul ordinateur, si vous disposez de deux ports s�rie. Vous pouvez lancez deux <TT/miniterm/s sur deux consoles virtuelles. Si vous lib�rez un port s�rie en d�connectant la souris, n'oubliez pas de rediriger <TT>/dev/mouse</TT> si ce fichier existe. Si vous utilisez une carte s�rie � ports multiples, soyez s�r de la configurer correctement. La mienne n'�tait pas correctement configur�e, et tout fonctionnait correctement lorsque je testais sur mon ordinateur. Lorsque je l'ai connect� � un autre, le port a commenc� � perdre des caract�res. L'ex�cution de deux programmes sur un seul ordinateur n'est pas totalement asynchrone.

      <SECT1>Configuration des ports
        <P>
Les p�riph�riques <TT>/dev/ttyS*</TT> sont destin�s � connecter les terminaux � votre machine Linux, et sont configur�s pour cet usage apr�s le d�marrage. Vous devez vous en souvenir lorsque vous programmez la communication avec un p�riph�rique autre. Par exemple, les ports sont configur�s pour afficher les caract�res envoy�s vers lui-m�me, ce qui normalement doit �tre chang� pour la transmission de donn�es.
        <P>
Tous les param�tres peuvent �tre facilement configur� depuis un programme. La configuration est stock�e dans une structure de type <TT>struct termios</TT>, qui est d�finie dans <TT>&lt;asm/termbits.h&gt;</TT>&nbsp;:
<TSCREEN><VERB>
#define NCCS 19
struct termios {
	tcflag_t c_iflag;		/* Modes d'entr�e */
	tcflag_t c_oflag;		/* Modes de sortie */
	tcflag_t c_cflag;		/* Modes de contr�le */
	tcflag_t c_lflag;		/* Modes locaux */
	cc_t c_line;			/* Discipline de ligne */
	cc_t c_cc[NCCS];		/* Caract�res de contr�le */
};
</VERB></TSCREEN>

Ce fichier inclus �galement la d�finition des constantes. Tous les modes d'entr�e dans <TT>c_iflag</TT> prennent en charge le traitement de l'entr�e, ce qui signifie que les caract�res envoy�s depuis le p�riph�rique peuvent �tre trait�s avant d'�tre lu par <TT>read</TT>. De la m�me fa�on, <TT>c_oflags</TT> se chargent du traitement en sortie. <TT>c_cflag</TT> contient les param�tres du port, comme la vitesse, le nombre de bits par caract�re, les bits d'arr�t, etc... Les modes locaux, stock�s dans <TT>c_lflag</TT> d�terminent si les caract�res sont imprim�s, si des signaux sont envoy�s � votre programme, etc... Enfin, le tableau <TT>c_cc</TT> d�finit les caract�res de contr�le pour la fin de fichier, le caract�re stop, etc... Les valeurs par d�faut pour les caract�res de contr�le sont d�finies dans <TT>&lt;asm/termios.h&gt;</TT>. Les modes possibles sont d�crits dans la page de manuel de <TT>termios(3)</TT>. La structure <TT>termios</TT> contient un champ <TT>c_line</TT> (discipline de ligne), qui n'est pas utilis� sur les syst�mes conformes � POSIX.

      <SECT1>Fa�ons de lire sur les p�riph�riques s�rie
        <P>
Voici trois fa�ons de lire sur les p�riph�riques s�rie. Le moyen appropri� doit �tre choisi pour chaque application. Lorsque cela est possible, ne lisez pas les cha�nes caract�re par caract�re. Lorsque j'utilisais ce moyen, je perdais des caract�res, alors qu'un <TT>read</TT> sur la cha�ne compl�te ne donnait aucune erreur.

        <SECT2>Entr�e canonique
          <P>
C'est le mode de fonctionnement normal pour les terminaux, mais peut �galement �tre utilis� pour communiquer avec d'autres p�riph�riques. Toutes les entr�es sont trait�es lignes par lignes, ce qui signifie qu'un <TT>read</TT> ne renverra qu'une ligne compl�te. Une ligne est termin�e par d�faut avec un caract�re <TT>NL</TT> (ACII <TT>LF</TT>), une fin de fichier, ou un caract�re de fin de ligne. Un <TT>CR</TT> (le caract�re de fin de ligne par d�faut de DOS et Windows) ne terminera pas une ligne, avec les param�tres par d�faut.
          <P>
L'entr�e canonique peut �galement prendre en charge le caract�re erase, d'effacement de mot, et de r�affichage, la traduction de <TT>CR</TT> vers <TT>NL</TT>, etc...

        <SECT2>Entr�e non canonique
          <P>
L'entr�e non canonique va prendre en charge un nombre fix� de caract�re par lecture, et autorise l'utilisation d'un compteur de temps pour les caract�res. Ce mode doit �tre utilis� si votre application lira toujours un nombre fix� de caract�res, ou si le p�riph�rique connect� envoit les caract�res par paquet.
          <P>

        <SECT2>Entr�e asynchrone
          <P>
Les deux modes ci-dessus peut �tre utilis� en mode synchrone ou asynchrone. Le mode synchrone est le mode par d�faut, pour lequel un appel � <TT>read</TT> sera bloquant, jusqu'� ce que la lecture soit satisfaite. En mode asynchrone, un appel � <TT>read</TT> retournera imm�diatement et lancera un signal au programme appelant en fin de transfert. Ce signal peut �tre re�u par un gestionnaire de signal.

        <SECT2>Attente d'entr�e depuis de multiples sources
          <P>
Cela ne constitue pas un mode d'entr�e diff�rent, mais peut s'av�rer �tre utile, si vous prenez en charge des p�riph�riques multiples. Dans mon application, je traitais l'entr�e depuis une socket TCP/IP et depuis une connexion s�rie sur un autre ordinateur quasiment en m�me temps. L'exemple de programme donn� plus loin attendra des caract�res en entr�e depuis deux sources. Si des donn�es sur l'une des sources deviennent disponibles, elles seront trait�es, et le programme attendra de nouvelles donn�es.
          <P>
L'approche pr�sent�e plus loin semble plut�t complexe, mais il est important que vous vous rappeliez que Linux est un syst�me multi-t�che. L'appel syst�me <TT>select</TT> ne charge pas le processeur lorsqu'il attend des donn�es, alors que le fait de faire une boucle jusqu'� ce que des caract�res deviennent disponibles ralentirait les autres processus.

    <SECT>Exemples de programmes
      <P>
Tous les exemples ont �t� extraits de <TT>miniterm.c</TT>. Le tampon d'entr�e est limit� � 255 caract�res, tout comme l'est la longueur maximale d'une ligne en mode canonique (<TT>&lt;linux/limits.h&gt;</TT> ou <TT>&lt;posix1_lim.h&gt;</TT>).
      <P>
R�f�rez-vous aux commentaires dans le code source pour l'explication des diff�rents modes d'entr�e. J'esp�re que le code est compr�hensible. L'exemple sur l'entr�e canonique est la plus comment�e, les autres exemples sont comment�s uniquement lorsqu'ils diff�rent, afin de signaler les diff�rences.
      <P>
Les descriptions ne sont pas compl�tes, mais je vous encourage � modifier les exemples pour obtenir la solution la plus int�ressante pour votre application.
      <P>
N'oubliez pas de donner les droits corrects aux ports s�rie (par exemple, <TT>chmod a+rw /dev/ttyS1</TT>)&nbsp;!

      <SECT1>Traitement canonique
        <P>
<TSCREEN><VERB>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>

/* les valeurs pour la vitesse, baudrate, sont d�finies dans <asm/termbits.h>, qui est inclus
dans <termios.h> */
#define BAUDRATE B38400            
/* changez cette d�finition pour utiliser le port correct */
#define MODEMDEVICE "/dev/ttyS1"
#define _POSIX_SOURCE 1 /* code source conforme � POSIX */

#define FALSE 0
#define TRUE 1

volatile int STOP=FALSE; 

main()
{
  int fd,c, res;
  struct termios oldtio,newtio;
  char buf[255];
/* 
  On ouvre le p�riph�rique du modem en lecture/�criture, et pas comme
  terminal de contr�le, puisque nous ne voulons pas �tre termin� si l'on
  re�oit un caract�re CTRL-C.
*/
 fd = open(MODEMDEVICE, O_RDWR | O_NOCTTY ); 
 if (fd <0) {perror(MODEMDEVICE); exit(-1); }

 tcgetattr(fd,&ero;oldtio); /* sauvegarde de la configuration courante */
 bzero(&ero;newtio, sizeof(newtio)); /* on initialise la structure � z�ro */

/* 
  BAUDRATE: Affecte la vitesse. vous pouvez �galement utiliser cfsetispeed
            et cfsetospeed.
  CRTSCTS : contr�le de flux mat�riel (uniquement utilis� si le c�ble a
            les lignes n�cessaires. Voir la section 7 du Serial-HOWTO).
  CS8     : 8n1 (8 bits,sans parit�, 1 bit d'arr�t)
  CLOCAL  : connexion locale, pas de contr�le par le modem
  CREAD   : permet la r�ception des caract�res
*/
 newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
 
/*
  IGNPAR  : ignore les octets ayant une erreur de parit�.
  ICRNL   : transforme CR en NL (sinon un CR de l'autre c�t� de la ligne
            ne terminera pas l'entr�e).
  sinon, utiliser l'entr�e sans traitement (device en mode raw).
*/
 newtio.c_iflag = IGNPAR | ICRNL;
 
/*
 Sortie sans traitement (raw).
*/
 newtio.c_oflag = 0;
 
/*
  ICANON  : active l'entr�e en mode canonique
  d�sactive toute fonctionnalit� d'echo, et n'envoit pas de signal au
  programme appelant.
*/
 newtio.c_lflag = ICANON;
 
/* 
  initialise les caract�res de contr�le.
  les valeurs par d�faut peuvent �tre trouv�es dans
  /usr/include/termios.h, et sont donn�es dans les commentaires.
  Elles sont inutiles ici.
*/
 newtio.c_cc[VINTR]    = 0;     /* Ctrl-c */ 
 newtio.c_cc[VQUIT]    = 0;     /* Ctrl-\ */
 newtio.c_cc[VERASE]   = 0;     /* del */
 newtio.c_cc[VKILL]    = 0;     /* @ */
 newtio.c_cc[VEOF]     = 4;     /* Ctrl-d */
 newtio.c_cc[VTIME]    = 0;     /* compteur inter-caract�re non utilis� */
 newtio.c_cc[VMIN]     = 1;     /* read bloquant jusqu'� l'arriv�e d'1 caract�re */
 newtio.c_cc[VSWTC]    = 0;     /* '\0' */
 newtio.c_cc[VSTART]   = 0;     /* Ctrl-q */ 
 newtio.c_cc[VSTOP]    = 0;     /* Ctrl-s */
 newtio.c_cc[VSUSP]    = 0;     /* Ctrl-z */
 newtio.c_cc[VEOL]     = 0;     /* '\0' */
 newtio.c_cc[VREPRINT] = 0;     /* Ctrl-r */
 newtio.c_cc[VDISCARD] = 0;     /* Ctrl-u */
 newtio.c_cc[VWERASE]  = 0;     /* Ctrl-w */
 newtio.c_cc[VLNEXT]   = 0;     /* Ctrl-v */
 newtio.c_cc[VEOL2]    = 0;     /* '\0' */

/* 
  � pr�sent, on vide la ligne du modem, et on active la configuration
  pour le port
*/
 tcflush(fd, TCIFLUSH);
 tcsetattr(fd,TCSANOW,&ero;newtio);

/*
  la configuration du terminal est faite, � pr�sent on traite
  les entr�es
  Dans cet exemple, la r�ception d'un 'z' en d�but de ligne mettra
  fin au programme.
*/
 while (STOP==FALSE) {     /* boucle jusqu'� condition de terminaison */
 /* read bloque l'ex�cution du programme jusqu'� ce qu'un caract�re de
    fin de ligne soit lu, m�me si plus de 255 caract�res sont saisis.
    Si le nombre de caract�res lus est inf�rieur au nombre de caract�res
    disponibles, des read suivant retourneront les caract�res restants.
    res sera positionn� au nombre de caract�res effectivement lus */
    res = read(fd,buf,255); 
    buf[res]=0;       /* on termine la ligne, pour pouvoir l'afficher */
    printf(":%s:%d\n", buf, res);
    if (buf[0]=='z') STOP=TRUE;
 }
 /* restaure les anciens param�tres du port */
 tcsetattr(fd,TCSANOW,&ero;oldtio);
}
</VERB></TSCREEN>

      <SECT1>Entr�e non canonique
        <P>
Dans le mode non canonique, les caract�res lus ne sont pas assembl�s ligne par ligne, et ils ne subissent pas de traitement (erase, kill, delete, etc...). Deux param�tres contr�lent ce mode&nbsp;: <TT>c_cc[VTIME]</TT> positionne le <EM/timer/ de caract�res, et <TT>c_cc[VMIN]</TT> indique le nombre minimum de caract�res � recevoir avant qu'une lecture soit satisfaite.
        <P>
Si MIN &gt; 0 et TIME = 0, MIN indique le nombre de caract�res � recevoir avant que la lecture soit satisfaite. TIME est �gal � z�ro, et le <EM/timer/ n'est pas utilis�.
        <P>
Si MIN = 0 et  TIME &gt; 0, TIME est utilis� comme une valeur de <EM/timeout/. Une lecture est satisfaite lorsqu'un caract�re est re�u, ou que la dur�e TIME est d�pass�e (t = TIME * 0.1s). Si TIME est d�pass�, aucun caract�re n'est retourn�.
        <P>
Si MIN &gt; 0 et TIME &gt; 0, TIME est employ� comme <EM/timer/ entre chaque caract�re. La lecture sera satisfaite si MIN caract�res sont re�us, ou que le <EM/timer/ entre deux caract�res d�passe TIME. Le <EM/timer/ est r�initialis� � chaque fois qu'un caract�re est re�u, et n'est activ� qu'apr�s la r�ception du premier caract�re.
        <P>
Si MIN = 0 et TIME = 0, le retour est imm�diat. Le nombre de caract�res disponibles, ou bien le nombre de caract�res demand� est retourn�. Selon Antonino (voir le paragraphe sur les participations), vous pouvez utiliser un <TT>fcntl(fd, F_SETFL, FNDELAY);</TT> avant la lecture pour obtenir le m�me r�sultat.
        <P>
Vous pouvez tester tous les modes d�crit ci-dessus en modifiant <TT>newtio.c_cc[VTIME]</TT> et <TT>newtio.c_cc[VMIN]</TT>.

<TSCREEN><VERB>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>

#define BAUDRATE B38400
#define MODEMDEVICE "/dev/ttyS1"
#define _POSIX_SOURCE 1 /* code source conforme � POSIX */
#define FALSE 0
#define TRUE 1

volatile int STOP=FALSE; 

main()
{
  int fd,c, res;
  struct termios oldtio,newtio;
  char buf[255];

 fd = open(MODEMDEVICE, O_RDWR | O_NOCTTY ); 
 if (fd <0) {perror(MODEMDEVICE); exit(-1); }

 tcgetattr(fd,&ero;oldtio); /* sauvegarde de la configuration courante */

 bzero(&ero;newtio, sizeof(newtio));
 newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
 newtio.c_iflag = IGNPAR;
 newtio.c_oflag = 0;

 /* positionne le mode de lecture (non canonique, sans echo, ...) */
 newtio.c_lflag = 0;
 
 newtio.c_cc[VTIME]    = 0;   /* timer inter-caract�res non utilis� */
 newtio.c_cc[VMIN]     = 5;   /* read bloquant jusqu'� ce que 5 */
                              /* caract�res soient lus */
 tcflush(fd, TCIFLUSH);
 tcsetattr(fd,TCSANOW,&ero;newtio);


 while (STOP==FALSE) {       /* boucle de lecture */
   res = read(fd,buf,255);   /* retourne apr�s lecture 5 caract�res */
   buf[res]=0;               /* pour pouvoir les imprimer... */
   printf(":%s:%d\n", buf, res);
   if (buf[0]=='z') STOP=TRUE;
 }
 tcsetattr(fd,TCSANOW,&ero;oldtio);
}
</VERB></TSCREEN>

      <SECT1>Lecture asynchrone
        <P>
<TSCREEN><VERB>
#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/signal.h>
#include <sys/types.h>

#define BAUDRATE B38400
#define MODEMDEVICE "/dev/ttyS1"
#define _POSIX_SOURCE 1 /* code source conforme � POSIX */
#define FALSE 0
#define TRUE 1

volatile int STOP=FALSE; 

void signal_handler_IO (int status);   /* le gestionnaire de signal */
int wait_flag=TRUE;              /* TRUE tant que re�u aucun signal */

main()
{
  int fd,c, res;
  struct termios oldtio,newtio;
  struct sigaction saio;        /* d�finition de l'action du signal */
  char buf[255];

  /* ouvre le port en mon non-bloquant (read retourne imm�diatement) */
  fd = open(MODEMDEVICE, O_RDWR | O_NOCTTY | O_NONBLOCK);
  if (fd <0) {perror(MODEMDEVICE); exit(-1); }

  /* installe le gestionnaire de signal avant de passer le port en
     mode asynchrone */
  saio.sa_handler = signal_handler_IO;
  saio.sa_mask = 0;
  saio.sa_flags = 0;
  saio.sa_restorer = NULL;
  sigaction(SIGIO,&ero;saio,NULL);
  
  /* permet au processus de recevoir un SIGIO */
  fcntl(fd, F_SETOWN, getpid());
  /* rend le descripteur de fichier asynchrone (la page de manuel
     indique que seuls O_APPEND et O_NONBLOCK fonctionnent avec
     F_SETFL...) */
  fcntl(fd, F_SETFL, FASYNC);

  tcgetattr(fd,&ero;oldtio); /* sauvegarde de la configuration courante */
  /* positionne les nouvelles valeurs pour lecture canonique */
  newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
  newtio.c_iflag = IGNPAR | ICRNL;
  newtio.c_oflag = 0;
  newtio.c_lflag = ICANON;
  newtio.c_cc[VMIN]=1;
  newtio.c_cc[VTIME]=0;
  tcflush(fd, TCIFLUSH);
  tcsetattr(fd,TCSANOW,&ero;newtio);
 
  /* on boucle en attente de lecture. g�n�ralement, on r�alise
     des traitements � l'int�rieur de la boucle */
  while (STOP==FALSE) {
    printf(".\n");usleep(100000);
    /* wait_flag = FALSE apr�s r�ception de SIGIO. Des donn�es sont
       disponibles et peuvent �tre lues */
    if (wait_flag==FALSE) { 
      res = read(fd,buf,255);
      buf[res]=0;
      printf(":%s:%d\n", buf, res);
      if (res==1) STOP=TRUE; /* on arr�te la boucle si on lit une
                                ligne seule */
      wait_flag = TRUE;      /* on attend de nouvelles donn�es */
    }
  }
  /* restaure les anciens param�tres du port */
  tcsetattr(fd,TCSANOW,&ero;oldtio);
}

/***************************************************************************
* gestionnaire de signal. Positionne wait_flag � FALSE, pour indiquer �    *
* la boucle ci-dessus que des caract�res ont �t� re�us.                    *
***************************************************************************/

void signal_handler_IO (int status)
{
  printf("r�ception du signal SIGIO.\n);
  wait_flag = FALSE;
}
</VERB></TSCREEN>

      <SECT1>Multiplexage en lecture
        <P>
Cette section est r�duite au minimum, et n'est l� que pour vous guider. Le code source d'exemple pr�sent� est donc r�duit au strict minimum. Il ne fonctionnera pas seulement avec des ports s�rie, mais avec n'importe quel ensemble de descripteurs de fichiers.
        <P>
L'appel syst�me select et les macros qui lui sont attach�es utilisent un <TT>fd_set</TT>. C'est un tableau de bits, qui dispose d'un bit pour chaque descripteur de fichier valide. <TT/select/ accepte un <TT/fd_set/ ayant les bits positionn�s pour les descripteurs de fichiers qui conviennent, et retourne un <TT/fd_set/, dans lequel les bits des descripteurs de fichier o� une lecture, une �criture ou une exception sont positionn�s. Toutes les manipulations de <TT>fd_set</TT> sont faites avec les macros fournies. Reportez vous �galement � la page de manuel de <TT>select(2)</TT>.

<TSCREEN><VERB>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

main()
{
   int    fd1, fd2;  /* entr�es 1 et 2 */
   fd_set readfs;    /* ensemble de descripteurs */
   int    maxfd;     /* nombre max des descripteurs utilis�s */
   int    loop=1;    /* boucle tant que TRUE */

   /* open_input_source ouvre un p�riph�rique, configure le port
      correctement, et retourne un descripteur de fichier. */
   fd1 = open_input_source("/dev/ttyS1");   /* COM2 */
   if (fd1<0) exit(0);
   fd2 = open_input_source("/dev/ttyS2");   /* COM3 */
   if (fd2<0) exit(0);
   maxfd = MAX (fd1, fd2)+1; /* num�ro maximum du bit � tester */

   /* boucle d'entr�e */
   while (loop) {
     FD_SET(fd1, &ero;readfs);  /* test pour la source 1 */
     FD_SET(fd2, &ero;readfs);  /* test pour la source 2 */
     /* on bloque jusqu'� ce que des caract�res soient
        disponibles en lecture */
     select(maxfd, &ero;readfs, NULL, NULL, NULL);
     if (FD_ISSET(fd1))         /* caract�res sur 1 */
       handle_input_from_source1();
     if (FD_ISSET(fd2))         /* caract�res sur 2 */
       handle_input_from_source2();
   }

}   
</VERB></TSCREEN>

        <P>
L'exemple ci-dessus bloque ind�finiment, jusqu'� ce que des caract�res venant d'une des sources soient disponibles. Si vous avez besoin d'un <EM/timeout/, remplacez juste l'appel � select par&nbsp;:
<TSCREEN><VERB>
int res;
struct timeval Timeout;

/* fixe la valeur du timeout */
Timeout.tv_usec = 0;  /* millisecondes */
Timeout.tv_sec  = 1;  /* secondes */
res = select(maxfd, &ero;readfs, NULL, NULL, &ero;Timeout);
if (res==0)
/* nombre de descripteurs de fichiers avec caract�res
   disponibles = 0, il y a eu timeout */
</VERB></TSCREEN>

Cet exemple verra l'expiration du delai de <EM/timeout/ apr�s une seconde. S'il y a <EM/timeout/, select retournera 0, mais fa�tes attention, <TT>Timeout</TT> est d�cr�ment� du temps r�ellement attendu par <TT/select/. Si la valeur de <EM/timeout/ est 0, select retournera imm�diatement.


    <SECT>Autres sources d'information
      <P>

<ITEMIZE>
          <ITEM>Le Linux Serial-HOWTO d�crit comment mettre en place les ports s�rie et contient des informations sur le mat�riel.
          <ITEM>Le <URL URL="http://www.easysw.com/~mike/serial" NAME="Serial Programming Guide for POSIX Compliant Operating Systems">, par Michael Sweet. Ce lien est p�rim� et je n'arrive pas � trouver la nouvelle adresse du document. Quelqu'un sait-il o� je peux le retrouver&nbsp;? C'�tait un tr�s bon document&nbsp;!
          <ITEM>La page de manuel de <TT>termios(3)</TT> d�crit toutes les constantes utilis�es pour la structure <TT>termios</TT>.
        </ITEMIZE>

    <SECT>Contributions
      <P>
Comme je l'ai dit dans l'introduction, je ne suis pas un expert dans le domaine, mais j'ai rencontr� des probl�mes, et j'ai trouv� les solutions avec l'aide d'autres personnes. Je tiens � remercier pour leur aide M. Strudthoff du European Transonic WindTunnel, Cologne, Michael Carter (<TT>mcarter@rocke.electro.swri.edu</TT>) et Peter Waltenberg (<TT>p.waltenberg@karaka.chch.cri.nz</TT>).

      <P>
Antonino Ianella (<TT>antonino@usa.net</TT> a �crit le Serial-Port-Programming Mini HOWTO, au m�me moment o� je pr�parais ce document. Greg Hankins m'a demand� d'inclure le Mini-HOWTO d'Antonino dans ce document.

      <P>
La structure de ce document et le formattage SGML ont �t� d�riv�s du Serial-HOWTO de Greg Hankins. Merci �galement pour diverses corrections faites par&nbsp;: Dave Pfaltzgraff (<TT>Dave_Pfaltzgraff@patapsco.com</TT>), Sean Lincolne (<TT>slincol@tpgi.com.au</TT>), Michael Wiedmann (<TT>mw@miwie.in-berlin.de</TT>), et Adrey Bonar (<TT>andy@tipas.lt</TT>).

</ARTICLE>


