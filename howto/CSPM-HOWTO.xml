<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.3//EN"
  "http://www.docbook.org/xml/4.3/docbookx.dtd" [
<!ENTITY howto      "http://www.traduc.org/docs/howto/lecture/">
<!ENTITY guide      "http://www.traduc.org/docs/guides/lecture/">
<!ENTITY cspm       "http://sourceforge.net/projects/cspm/">
<!ENTITY images     "images/CSPM-HOWTO/">
]>

<article id="spm" lang="fr">

  <articleinfo>
  
  <title>
    Guide pratique de CSPM, l'outil de suivi des performances du syst�me
  </title>
  
  <subtitle>
  
    Version fran�aise du <foreignphrase>Complete System Performance
    Monitor HOWTO</foreignphrase>
  
  </subtitle>
  
    <abstract><para>
    
      L'outil de suivi des performances du syst�me
      (<acronym>CSPM</acronym>, acronyme de <foreignphrase>Complete
      System Performance Monitor</foreignphrase>) est un outil graphique
      qui surveille l'utilisation du processeur, de la m�moire, du
      stockage, du r�seau et des <acronym>IRQ</acronym> d'un syst�me
      Linux. <acronym>CSPM</acronym> recueille ces donn�es
      automatiquement et g�n�re des histogrammes de l'utilisation du
      syst�me.
      
      </para>
      
      <para>
      
      Ce guide pratique fournit un aper�u de CSPM, une description du
      produit et des informations sur la fa�on de le configurer.
    
    </para></abstract>
    
    <releaseinfo>Version&nbsp;: 2.0.fr.1.0</releaseinfo>  
    <pubdate>3 d�cembre 2004</pubdate>

    <author>
      <firstname>Chris</firstname>
      <surname>Lorenz</surname>
      <email>lorenzc CHEZ us POINT ibm POINT com</email>
    </author>
    <othercredit role="traduction" class="translator">
      <firstname>Simon</firstname>
      <surname>Depiets</surname>
      <contrib>Adaptation fran�aise</contrib>
      <email>2df CHEZ tuxfamily POINT org</email>
    </othercredit>

    <othercredit role="relecture" class="translator">
      <firstname>Guillaume</firstname>
      <surname>Lelarge</surname>
      <contrib>Relecture de la version fran�aise</contrib>
      <email>gleu CHEZ wanadoo POINT fr</email>
    </othercredit>

    <othercredit role="relecture" class="translator">
      <firstname>Jean-Philippe</firstname>
      <surname>Gu�rard</surname>
      <contrib>Relecture de la version fran�aise</contrib>
      <email>fevrier CHEZ tigreraye POINT org</email>
    </othercredit>

    <othercredit role="publication" class="copyeditor">
      <firstname>Jean-Philippe</firstname>
      <surname>Gu�rard</surname>
      <contrib>Pr�paration de la publication de la v.f.</contrib>
      <email>fevrier CHEZ tigreraye POINT org</email>
    </othercredit>

    <revhistory>
      <revision>
        <revnumber>2.0.fr.1.0</revnumber>
        <date>2004-12-03</date>
        <authorinitials>SD, GL, JPG</authorinitials>
        <revremark>
            
          Premi�re adaptation fran�aise.
            
        </revremark>
      </revision>
      <revision>
        <revnumber>2.0</revnumber>
        <date>2003-06-10</date>
        <authorinitials>CL</authorinitials>
      </revision>
    </revhistory>
  </articleinfo>

<sect1 id="copyfr"><title>Droits d'utilisations et informations l�gales</title>

<important><para>

Le texte ci-dessous est la version fran�aise de la licence de ce 
document. Seule la version originale de cette licence, pr�sent�e dans la 
section suivante, fait foi.

</para></important>

<para>Copyright &copy; 2003 IBM Corporation. Tous droits r�serv�s.</para>

<para>

Ce document vous est fourni tel quel, sans garantie explicite ou
implicite. L'utilisation des informations contenues dans ce document est
� vos risques et p�rils.

</para>

<para>

Linux est une marque d�pos�e par Linus Torvalds. Les autres noms
d'entreprises, de produits et de services peuvent avoir �t� d�pos�s.

</para>

<para>

Vous est autoris� � copier, distribuer et modifier la version originale
de ce document selon les termes de la Licence de documentation libre GNU
(GFDL), version 1.1 ou ult�rieure, telle que publi�e par la Free
Software Foundation&nbsp;; sans section inalt�rable, ni texte de
premi�re de couverture, ni texte de quatri�me de couverture. Une copie
de cette licence est disponible sur&nbsp;: <ulink
url="http://www.gnu.org/licenses/fdl.txt"/>.

</para>

<para>

Copyright &copy; 2004 Simon Depiets, Guillaume Lelarge et Jean-Philippe
Gu�rard pour la version fran�aise

</para>

<para>

La version fran�aise de ce guide pratique est publi�e en accord avec les
termes de la licence de documentation libre GNU (GFDL) sans section
invariante, sans texte de premi�re de couverture ni texte de quatri�me
de couverture. Une copie (en anglais) de la licence est disponible sur
<ulink url="http://www.gnu.org/copyleft/fdl.html"/>.

</para>

</sect1>

<sect1 id="copy"><title>Copyright and legal notice</title>

<important><para>
Le texte ci-dessous est la licence de ce document. Ce texte fait foi. Il 
est compos� de la licence en anglais du document orignal, suivi de la 
licence en fran�ais de sa traduction.
</para></important>

<para>Copyright � 2003 IBM Corporation. All rights reserved.</para>

<para>

This document is provided "AS IS," with no express or implied
warranties. Use the information in this document at your own risk.

</para>

<para>

Linux is a registered trademark of Linus Torvalds. Other company,
product, and service names may be trademarks or service marks of others.

</para>

<para>

Permission is granted to copy, distribute, and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.1 or
any later version published by the Free Software Foundation with no
Invariant Sections, no Front-Cover text, and no Back-Cover text. A copy
of the license can be found at <ulink
url="http://www.gnu.org/licenses/fdl.txt"/>.

</para>

<para>

Copyright &copy; 2004 Simon Depiets, Guillaume Lelarge et Jean-Philippe
Gu�rard pour la version fran�aise

</para>

<para>

La version fran�aise de ce guide pratique est publi�e en accord avec les
termes de la licence de documentation libre GNU (GFDL) sans section
invariante, sans texte de premi�re de couverture ni texte de quatri�me
de couverture. Une copie (en anglais) de la licence est disponible sur
<ulink url="http://www.gnu.org/copyleft/fdl.html"/>.

</para>

</sect1>

<sect1>

<title>Qu'est-ce que CSPM&nbsp;?</title>

<para>

      L'outil de suivi des performances du syst�me
      (<acronym>CSPM</acronym>, acronyme de <foreignphrase>Complete
      System Performance Monitor</foreignphrase>), �crit par Don Dupuis
      de la soci�t� Compaq, est un outil graphique qui surveille
      l'utilisation du processeur, de la m�moire, du stockage, du r�seau
      et des <acronym>IRQ</acronym> d'un syst�me Linux.
      <acronym>CSPM</acronym> recueille ces donn�es automatiquement et
      g�n�re des histogrammes de l'utilisation du syst�me.

</para>

</sect1>

<sect1><title>Configuration minimale</title>

<para>

<acronym>CSPM</acronym>, version 1.0 ou ult�rieure, n�cessite&nbsp;:

</para>

<itemizedlist>

<listitem><para>

Une distribution Linux Red Hat&nbsp;7.2 ou ult�rieure, Mandrake&nbsp;8.2
ou ult�rieure, ou toute distribution utilisant un noyau Linux dans
lequel la modification de Stephen Tweedie pour <command>sar</command> ou
<command>sysstat</command> a �t� appliqu� (c'est le cas des noyaux Linux
2.4.20 et ult�rieurs).

</para></listitem>

<listitem><para>

L'environnement de d�veloppement C++ Trolltech Qt3.0 ou ult�rieure, qui
est disponible gratuitement sur le site de Trolltech.

</para></listitem>

</itemizedlist>

</sect1>

<sect1>

<title>Installer <acronym>CSPM</acronym> et ses d�pendances</title>

<para>

Cette section explique comment r�cup�rer l'environnement de
d�veloppement Qt et l'installer.

</para>

<sect2>

<title>Installer Qt, version 3.0 ou ult�rieure</title>

<para>

Qt, version 3.0 ou ult�rieure, doit �tre install� sur le syst�me avant
d'installer <foreignphrase>CSPM</foreignphrase>. Suivez ces �tapes 
pour r�cup�rer et installer Qt&nbsp;:

</para>

<orderedlist>

<listitem><para>

t�l�chargez la derni�re version de la version libre de Qt pour X11
(<quote>Qt/X11 Open Source Edition</quote>) sur <ulink
url="http://www.trolltech.com">www.trolltech.com</ulink>
(Attention&nbsp;! Ce site est en anglais uniquement)&nbsp;;

</para></listitem>

<listitem><para>

suivez les instructions (en anglais) de Trolltech pour installer
Qt&nbsp;;

</para></listitem>

<listitem><para>

lancez <command>./configure</command> avec l'option
<constant>-thread</constant> pour que Qt fonctionne en mode
multifils&nbsp;:

</para>

<programlisting>
# ./configure -thread
</programlisting>

</listitem>

<listitem><para>

assurez-vous que vous exportez les variables d'environnement
<envar>QTDIR</envar> et <envar>LD_LIBRARY_PATH</envar>, ainsi que le
d�crivent les instructions d'installation de Qt distribu�es avec le
logiciel.

</para></listitem>

</orderedlist>

</sect2>

<sect2>

<title>Installer <acronym>CSPM</acronym></title>

<para>

Les sections suivantes expliquent comment t�l�charger et installer
<acronym>CSPM</acronym>. Ces �tapes doivent �tre effectu�es sous le
compte utilisateur <literal>root</literal>. Le processus d'installation
cr�e un r�pertoire appel� <filename>spm</filename> et place tous les
fichiers dans ce r�pertoire.

</para>

<sect3><title>Installer depuis le rpm</title>

<para>

Suivez ces �tapes pour installer <acronym>CSPM</acronym> depuis le
paquet <acronym>RPM</acronym>&nbsp;:

</para>

<orderedlist>

<listitem><para>

T�l�chargez le paquet <acronym>RPM</acronym> de <acronym>CSPM</acronym>
depuis le site de SourceForge sur <ulink url="&cspm;"/>. Le nom du
programme pour <acronym>CSPM</acronym> est <filename>spm2</filename>.

</para></listitem>

<listitem><para>Installez le logiciel avec la commande</para>
<programlisting># rpm -ihv --nodeps spm2-1.0-1.586.rpm</programlisting>
<para>Le <acronym>RPM</acronym> cr�e le binaire appel� <command>spm2</command>
dans le r�pertoire courant.</para>
</listitem>

</orderedlist>

</sect3>

<sect3><title>Installer depuis l'archive tar</title>

<para>

Suivez ces �tapes pour installer <acronym>CSPM</acronym> depuis
l'archive tar&nbsp;:

</para>

<orderedlist>

<listitem><para>

T�l�chargez l'archive tar de <acronym>CSPM</acronym> depuis le site de
SourceForge sur <ulink url="&cspm;"/>. Le nom du programme pour
<acronym>CSPM</acronym> est <filename>spm2</filename>.

</para></listitem>

<listitem><para>

D�compressez le fichier <filename>spm2.tar.gz</filename> avec la
commande&nbsp;:

</para>

<programlisting>
# tar xvzf spm2.tar.gz
</programlisting>

</listitem>

<listitem><para>

Compilez <acronym>CSPM</acronym>&nbsp;:

</para>

<programlisting>
# make all
</programlisting>

<para>

La commande <command>make</command> cr�e le fichier binaire appel�
<command>spm2</command> dans le r�pertoire courant.

</para></listitem>
</orderedlist>

</sect3>

</sect2>

<sect2>

<title>Lancer <acronym>CSPM</acronym></title>

<para>

Le programme <command>spm2</command> devrait �tre lanc� sous un compte
utilisateur non privil�gi�, car autement les pr�f�rences utilisateurs ne
sont pas enregistr�es.

</para>

<para>

Saisissez la commande suivante dans le r�pertoire o�
<acronym>CSPM</acronym> est install�&nbsp;:

</para>

<programlisting># ./spm2</programlisting>

<para>

Si une erreur de segmentation intervient durant le d�marrage de
<command>spm2</command>, assurez-vous d'avoir d�fini les variables
<envar>QTDIR</envar> et <envar>LD_LIBRARY_PATH</envar>, comme d�crit
dans les instructions d'installation livr�es avec Qt.

</para>
</sect2>
</sect1>

<sect1>

<title>Modifier les options de <acronym>CSPM</acronym></title>

<para>

Par d�faut, il y a 5 lignes de quadrillage le long de l'axe des y de
chaque histogramme. Quand il n'y a pas d'activit� pour un p�riph�rique
particulier, <acronym>CSPM</acronym> utilise les graduations par d�faut
0&nbsp; 0,2&nbsp;; 0,4&nbsp;; 0,6 et 0,8. Lorsque l'activit� commence,
<acronym>CSPM</acronym> d�fini les cinq graduations � intervalle
r�gulier de 0 � la plus haute valeur collect�e. Parfois, le quadrillage
tombera pile sur les nombres de la barre, rendant ces nombres difficiles
� lire. Vous pourrez soit ajuster la couleur du quadrillage, soit
ajuster l'intervalle, soit essayer d'ignorer le quadrillage.

</para>

<para>

Vous pouvez modifier le quadrillage, la taille et les options du
contr�leur pour chaque vue depuis le menu <menuchoice><guimenu
lang="en">Preferences</guimenu></menuchoice> (pr�f�rences). Depuis ce
menu, vous pourrez s�lectionner les entr�es <guilabel
lang="en">System</guilabel> (syst�me), <guilabel
lang="en">Tests</guilabel>, <guilabel lang="en">CPU</guilabel>
(processeur), <guilabel lang="en">Memory</guilabel> (m�moire), <guilabel
lang="en">Network</guilabel> (r�seau), <guilabel
lang="en">Storage</guilabel> (stockage) et <guilabel
lang="en">IRQ</guilabel> (interruptions). Une fois que vous aurez
s�lectionn� une de ces entr�es, vous pourrez voir <guilabel
lang="en">Grid</guilabel> (quadrillage) et <guilabel
lang="en">Monitor</guilabel> (contr�leurs) ou <guilabel
lang="en">Sizing</guilabel> (taille) pour chaque �l�ment.

</para>

<para>

Depuis l'onglet <guilabel lang="en">Grid</guilabel> (quadrillage), vous
pourrez modifier des �l�ments comme&nbsp;:

</para>

<itemizedlist>

<listitem><para>

la couleur des lignes du quadrillage&nbsp;;

</para></listitem>

<listitem><para>

la distance (ou le temps) en secondes s�parant les collectes de donn�es
(des barres apparaissent � chaque collecte)&nbsp;;

</para></listitem>

<listitem><para>

la couleur des diff�rentes barres, comme <guilabel
lang="en">read</guilabel> (lectures), <guilabel
lang="en">write</guilabel> (�critures), <guilabel
lang="en">user</guilabel> (utilisateur), <guilabel
lang="en">nice</guilabel> (priorit� modifi�e) et <guilabel
lang="en">sys</guilabel> (syst�me)&nbsp;;

</para></listitem>

<listitem><para>

le nombre de barres horizontales (cinq par d�faut)&nbsp;;

</para></listitem>

<listitem><para>

la taille des titres d'histogrammes.

</para></listitem>

</itemizedlist>

<para>

Depuis l'onglet <guilabel lang="en">Monitor</guilabel> (contr�leur),
vous pourrez par exemple modifier&nbsp;:

</para>

<itemizedlist>

<listitem><para>

la largeur et la hauteur des bo�tes d'affichage (en points)&nbsp;;

</para></listitem>

<listitem><para>

la couleur de ces bo�tes&nbsp;;

</para></listitem>

<listitem><para>

le type de donn�es devant �tre contr�l� (entr�e/sortie, donn�es, lectures
et �critures, secteurs et blocs).

</para></listitem>

</itemizedlist>

<para>

Depuis l'onglet <guilabel lang="en">Taille</guilabel>, vous pourrez changer la
largeur et la hauteur minimale des bo�tes d'affichage (en points).

</para>

</sect1>

<sect1>

<title>Affichage des donn�es</title>

<para>

CSPM affiche des histogrammes fournissant des informations sur l'utilisation
du syst�me. Le programme a huit onglets d'affichage pour les diff�rents
types d'informations syst�me que <acronym>CSPM</acronym> collecte. Ces
onglets sont&nbsp;:

</para>

<itemizedlist>

<listitem><para>

<guilabel lang="en">System Overview</guilabel> (aper�u du 
syst�me)&nbsp;;

</para></listitem>

<listitem><para>

<guilabel lang="en">IRQ</guilabel> (interruptions)&nbsp;;

</para></listitem>

<listitem><para>

<guilabel lang="en">CPU Utilization</guilabel> (utilisation du 
processeur)&nbsp;;

</para></listitem>

<listitem><para>

<guilabel lang="en">Memory</guilabel> (m�moire)&nbsp;;

</para></listitem>

<listitem><para>

<guilabel lang="en">Network</guilabel> (r�seau)&nbsp;;

</para></listitem>

<listitem><para>

<guilabel lang="en">Storage</guilabel> (stockage)&nbsp;;

</para></listitem>

<listitem><para>

<guilabel lang="en">Tests</guilabel>&nbsp;;

</para></listitem>

<listitem><para>

<guilabel lang="en">Tests summary</guilabel> (synth�se des tests).

</para></listitem>

</itemizedlist>

<para>

Les l�gendes en bas de chaque table d'histogrammes vous expliquent comment
interpr�ter les diff�rentes couleurs repr�sentant les donn�es.

</para>

<para>

Utilisez les barres de d�filement verticales et horizontales pour voir
les histogrammes qui ne tiennent pas sur l'�cran initial.

</para>

<sect2>

<title>Onglet <guilabel lang="en">System Overview</guilabel> (aper�u 
du syst�me)</title>

<para>

Quand <acronym>CSPM</acronym> d�marre, l'onglet <guilabel
lang="en">System Overview</guilabel> est affich�. Les histogrammes de
cet l'onglet montrent les informations relatives au syst�me entier,
notamment l'utilisation du processeur, de la m�moire, du r�seau et du
stockage. La capture d'�cran suivante montre un exemple de ce qu'affiche
cet onglet.

</para>

<para>
<graphic fileref="&images;sysover.png"/>
</para>

<para>

Sous chaque histogramme se trouve une l�gende d�crivant les donn�es
repr�sent�es. Par exemple dans l'histogramme <guilabel lang="en">Cpu
Total</guilabel> (utilisation globale du processeur), les barres rouges
repr�sentent l'utilisation du processeur par l'utilisateur, les barres
vertes repr�sentent les commandes ayant une priorit� modifi�e et les
barres bleues repr�sentent l'utilisation du processeur par le syst�me.

</para>

</sect2>

<sect2>

<title>Onglet <guilabel lang="en">IRQ</guilabel> (interruptions)</title>

<para>

Quand vous cliquez sur l'onglet <guilabel lang="en">IRQ</guilabel>, un
histogramme s'ouvre pour chaque ligne d'interruption (IRQ) reli�e � un
connecteur ISA du syst�me. La capture d'�cran suivante montre un exemple
de cet onglet.

</para>

<note><para>

Avec Qt&nbsp;3.0, les barres de d�filement horizontales de l'onglet IRQ
ne fonctionnent pas. Ce probl�me dispara�t avec Qt&nbsp;3.1.

</para></note>

<para>
<graphic fileref="&images;irqs.png"/>
</para>
<para>

Le nombre de grande taille en bleu � gauche de l'histogramme est le
num�ro de l'interruption. Les barres rouges repr�sentent le nombre
d'interruptions utilis�es par secondes par le p�riph�rique connect� au 
connecteur ISA de cette ligne d'interruption.

</para>
</sect2>

<sect2>

<title>Onglet <guilabel lang="en">CPU Utilization</guilabel>
(utilisation du processeur)</title>

<para>

Quand vous cliquez sur l'onglet <guilabel lang="en">CPU
Utilization</guilabel> (utilisation du processeur), un histogramme
s'ouvre pour chaque processeur du syst�me, comme dans la capture d'�cran
suivante&nbsp;:

</para>

<para><graphic fileref="&images;cpu.png"/></para>

<para>

L'histogramme montre des informations sur les processus utilisateur
(rouge), les processus syst�me (bleu) et ceux dont la priorit� a �t�
modifi�e (vert).

</para>
</sect2>

<sect2>

<title>Onglet <guilabel lang="en">Memory</guilabel> (m�moire)</title>

<para>

Cet onglet est encore en d�veloppement. Dans une version future de
<acronym>CSPM</acronym>, il montrera graphiquement la quantit� de
m�moire utilis�e par les processus.

</para>

</sect2>

<sect2>

<title>Onglet <guilabel lang="en">Network</guilabel> (r�seau)</title>

<para>

Quand vous cliquez sur cet onglet, des histogrammes montrant la quantit�
de donn�es transitant sur la carte r�seau virtuelle
<literal>loopback</literal> (en boucle) du syst�me et sur chaque
p�riph�rique r�seau sont affich�s, comme on peut le voir dans la capture
d'�cran qui suit.

</para>

<para>
<graphic fileref="&images;network.png"/>
</para>

<para>

Les envois sont en rouge et les r�ceptions en bleu.

</para>

</sect2>

<sect2>

<title>Onglet <guilabel lang="en">Storage</guilabel> (stockage)</title>

<para>

Quand vous cliquez sur cet onglet, un groupe d'histogrammes s'ouvre,
montrant des donn�es sur les contr�leurs, les disques et les partitions.
La l�gende en bas de l'histogramme indique quelle couleur d'histogramme
correspond � quel type de p�riph�rique.

</para>

<para>
<graphic fileref="&images;storage.png"/>
</para>

<para>

Sur la capture d'�cran, les couleurs sont le mauve pour les contr�leurs,
le vert pour les disques et l'orange pour les partitions. Les barres
rouges repr�sentent les lectures depuis le p�riph�rique et les barres
bleues les �critures vers le p�riph�rique.

</para>

<para>

Pour avoir des informations sur une partition (comme son nom dans le
syst�me de fichier, l'espace utilis� et l'espace disponible), faites un
clic droit sur le histogramme de la partition puis un clic gauche sur
<guimenuitem lang="en">Properties</guimenuitem> (propri�t�s). Une
fen�tre d'�tat de la partition appara�tra montrant les informations
relatives � la partition s�lectionn�e.

</para>

<para>
<graphic fileref="&images;snapshot9.png"/>
</para>

</sect2>

<sect2>

<title>Onglet <guilabel lang="en">Tests</guilabel></title>

<para>

Cet onglet ouvre une liste de tests qui peuvent �tre lanc�s sur le
syst�me, ce qui est pratique, par exemple, pour les techniciens
r�alisant des contr�les d'assurance qualit� et qui ont besoin de charger
des syst�mes de test pour des essais mat�riels et logiciels.

</para>

</sect2>

<sect2>

<title>Onglet <guilabel lang="en">Tests summary</guilabel> 
(synth�se des tests)</title>

<para>

Cet onglet contient les sorties des tests et le nombre de fois o� ils
ont �t� lanc�s. Vous pouvez imprimer ces r�sultats et les garder.

</para>

</sect2>

</sect1>

</article>
