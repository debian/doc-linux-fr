
<!--Chapitre 21. Suppression des objets-->

<chapter id="deleteobjects">

<title>Suppression des objets</title>

<para>

  Ce chapitre explique comment supprimer des objets EVMS à l'aide des 
  opérations "delete" (supprimer) et "delete recursive" (supprimer 
  récursivement).

</para>

<!--21.1 Comment supprimer des objets : "delete" et "delete recursive".-->

<sect1 id="howtodelete">

<title>Comment supprimer des objets&nbsp;: "delete" et "delete recursive"</title>

<para>

  Avec EVMS, il y a deux manières de détruire les objets dont vous ne 
  voulez plus&nbsp;: "delete" et "delete recursive". L'option "delete" 
  détruit uniquement l'objet que vous spécifiez. L'option "delete 
  recursive" détruit l'objet que vous spécifiez et tous ses objets 
  sous-jacents, jusqu'au conteneur s'il y en a un, ou bien jusqu'au 
  disque. Pour qu'un volume soit supprimé, il ne doit pas être monté. 
  EVMS vérifie que le volume que vous essayez de détruire n'est pas 
  monté et n'effectue pas la suppression si le volume est monté.

</para>
</sect1>
<!--21.2. Exemple : réaliser une opération "delete recursive"-->

<sect1 id="exdelete">

<title>Exemple&nbsp;: réaliser une opération "delete recursive"</title>

<para>

  L'exemple suivant montre comment détruire un volume et les objets 
  qu'il contient avec les interfaces graphiques d'EVMS, de Ncurses et à 
  partir de la ligne de commande.

</para>

<blockquote>

  <example>
  
    <title>Détruire un volume et la région et le conteneur qu'il contient</title>

    <para>
    
      Cet exemple utilise l'opération "delete recursive"  pour détruire 
      le volume /dev/evms/Sample Volume et la région et le conteneur 
      qu'il contient. Le volume en question a été créé dans l'exemple 
      plus haut. Bien que l'on puisse utiliser l'option « delete » sur 
      chaque objet séparément, l'option « delete recursive » nécessite 
      moins d'étapes. Il faut remarquer que, comme nous voulons 
      supprimer le conteneur ainsi que le volume, l'opération doit être 
      effectuée en deux étapes&nbsp;: la première consiste à supprimer 
      le volume et son contenu, la deuxième à supprimer le conteneur et 
      son contenu.
    
    </para>

  </example>
</blockquote>

<!--21.2.1 Utilisation de l'interface d'EVMS.-->

<sect2 id="exdeleteevms">

<title>Utilisation de l'interface d'EVMS</title>

<para>

  Pour monter un volume à l'aide de l'interface d'EVMS&nbsp;:

</para>

<orderedlist>
<listitem><para>

  Sélectionnez Actions->Delete->Volume.

</para></listitem>
<listitem><para>

  Sélectionnez le volume /dev/evms/Sample Volume dans la 
  liste.
  
</para></listitem>
<listitem><para>

  Cliquez sur "Delete Recursive". Cette étape supprime le volume et la 
  région lvm/Sample Container/Sample Region. Si vous voulez conserver 
  les parties sous-jacentes, ou supprimer chaque partie séparément, vous 
  devrez cliquer sur « Delete » au lieu de « Delete Recursive ».

</para></listitem>
<listitem><para>

  Si vous avez choisi "Delete Recursive", sélectionnez 
  Actions->Delete->Container. Sinon, supprimez la région avant de 
  continuer.

</para></listitem>
<listitem><para>

  Sélectionnez le conteneur lvm/Sample Container dans la 
  liste.

</para></listitem>
<listitem><para>

  Cliquez sur "Delete Recursive" pour détruire le conteneur et tout ce 
  qu'il contient. Vous pouvez aussi cliquer sur « delete » pour ne 
  détruire que le conteneur. (Si vous avez construit le conteneur sur 
  les disques comme dans l'exemple, les deux commandes ont le même 
  effet.)
  
</para></listitem>
</orderedlist>

<para>

  On peut aussi utiliser le menu contextuel&nbsp;:
  
</para>

<orderedlist>
<listitem><para>

  À partir de l'onglet "Volumes", cliquez droit sur /dev/evms/Sample 
  Volume.

</para></listitem>
<listitem><para>

  Cliquez sur "Delete..."
  
</para></listitem>
<listitem><para>

  Reprenez les opérations à partir de l'étape 3 des instructions de 
  l'interface d'EVMS .

</para></listitem>
</orderedlist>
</sect2>

<!--21.2.2 Utilisation de Ncurses.-->
<sect2 id="exdeletencurses">

<title>Utilisation de Ncurses</title>

<para>

  Pour supprimer le volume et le conteneur à l'aide de 
  Ncurses&nbsp;:

</para>

<orderedlist>

<listitem><para>

  Sélectionnez Actions->Delete->Volume.

</para></listitem>
<listitem><para>

  Sélectionnez le volume /dev/evms/Sample Volume dans la 
  liste.

</para></listitem>
<listitem><para>

  Activez "Delete Volume Recursively". Cela permet de supprimer le 
  volume et la région lvm/Sample Container/Sample Region. Si vous voulez 
  conserver les parties sous-jacentes, ou supprimer chaque partie 
  séparément, vous devrez activer <quote>Delete</quote> au lieu de 
  <quote>Delete Recursive</quote>.

</para></listitem>
<listitem><para>

  Si vous avez choisi "Delete Recursive", sélectionnez 
  Actions->Delete->Container. Sinon, supprimez la région avant de 
  continuer.

</para></listitem>
<listitem><para>

  Sélectionnez le conteneur lvm/Sample Container dans la liste.

</para></listitem>
<listitem><para>

  Cliquez sur "Delete Recursive" pour détruire le conteneur et tout ce 
  qu'il contient. Vous pouvez aussi activer <quote>Delete</quote> pour 
  ne détruire que le conteneur (si vous avez construit le conteneur sur 
  les disques comme dans l'exemple, les deux commandes ont le même 
  effet).

</para></listitem>
<listitem><para>

  Appuyez sur Entrée
  
</para></listitem>

</orderedlist>

<para>

  On peut aussi utiliser le menu contextuel de Ncurses&nbsp;:
  
</para>

<orderedlist>
<listitem><para>

  À partir de la vue "Volumes", faites Entrée sur /dev/evms/Sample 
  Volume.

</para></listitem>
<listitem><para>

  Activez "Delete"
  
</para></listitem>
<listitem><para>

  Reprenez les opérations à partir de l'étape 3 des instructions de 
  l'interface Ncurses classique.

</para></listitem>
</orderedlist>
</sect2>

<!-- 21.2.3 Utilisation de la ligne de commande [CLI]-->
<sect2 id="exdeletecli">

<title>

  Utilisation de la ligne de commande [CLI]

</title>

<para>

  Utilisez les commandes delete et delete recursive pour détruire les 
  objets EVMS. Entrez le nom de la commande suivie d'un 
  <quote>:</quote>, puis entrez le nom du volume, objet ou conteneur. 
  Par exemple&nbsp;:
  
</para>

<orderedlist>

<listitem><para>

  Entrez cette commande pour réaliser l'opération "Delete 
  Recursive"&nbsp;:

</para>

<programlisting>
delete recursive: "/dev/evms/Sample Volume"
</programlisting>

<para>

  Cela permet de supprimer le volume et la région /lvm/Sample 
  Container/Sample Region. Si vous voulez conserver les parties 
  sous-jacentes, ou supprimer chaque partie séparément, vous devez 
  utiliser la commande delete comme suit&nbsp;:

</para>

<programlisting>
delete: "/dev/evms/Sample Volume"
</programlisting>

</listitem>

<listitem><para>

  Si vous avez choisi "Delete Recursive", (sinon, supprimez la région 
  avant de continuer) entrez la commande suivante pour détruire le 
  conteneur et tout ce qu'il contient&nbsp;:

</para>

<programlisting>
delete recursive: ''lvm/Sample Container''
</programlisting>

<para>

  Pour ne détruire que le container&nbsp;:

</para>

<programlisting>
delete: ''lvm/Sample Container''
</programlisting>

</listitem>
</orderedlist>

</sect2>

<sect2 id="mkfsgui">

<title>Utilisation de l'interface d'EVMS</title>

<para>Pour supprimer le volume et le conteneur avec l'interface d'EVMS, suivez ces étapes&nbsp;: </para>

<orderedlist>
<listitem><para> Sélectionnez <menuchoice><guimenu>Actions</guimenu><guimenuitem>Delete</guimenuitem><guimenuitem>Volume</guimenuitem></menuchoice>
. </para></listitem>
<listitem><para> Sélectionnez dans le liste le volume <filename>/dev/evms/Sample Volume</filename>   </para></listitem>
<listitem><para> Cliquez sur <guibutton>Recursive Delete</guibutton> (suppression récursive). Cette étape supprime le volume et la région <filename>lvm/Sample Container/Sample Region</filename>. Pour garder les éléments sous-jacents ou supprimer chaque élément séparément, on cliquera sur <guibutton>Delete</guibutton>(supprimer) au lieu de <guibutton>Delete Recursive</guibutton>. </para></listitem>
<listitem><para> Si vous avez choisi la suppression récursive (dans le cas contraire, supprimez la région avant de poursuivre cette procédure), sélectionnez <menuchoice><guimenu>Actions</guimenu><guimenuitem>Delete</guimenuitem><guimenuitem>Container</guimenuitem></menuchoice>. </para></listitem>
<listitem><para> Sélectionnez dans la liste le conteneur <guimenu>lvm/Sample Container</guimenu>.  </para></listitem>
<listitem><para> Cliquez sur <guibutton>Recursive Delete</guibutton> pour supprimer le conteneur et tout ce qu'il contient. On peut aussi supprimer uniquement le conteneur en cliquant sur <guibutton>Delete</guibutton> (Si le conteneur a été construit sur des disques, comme dans l'exemple, les deux commandes auront le même effet). </para></listitem>
</orderedlist>
<para>
</para>
<para>On peut également effectuer certaines étapes de suppression de volume à l'aide du menu contextuel de l'interface&nbsp;: </para>
<orderedlist>
<listitem><para> Dans l'onglet <guimenuitem>Volumes</guimenuitem>, faites un clic droit sur <filename>/dev/evms/Sample Volume</filename>. </para></listitem>
<listitem><para> Cliquez sur <guibutton>Delete</guibutton>... (supprimer) </para></listitem>
<listitem><para> Poursuivez en reprenant à partir de l'étape 3 des instructions données pour l'interface.</para></listitem>
</orderedlist>
</sect2>

<sect2 id="mkfsncurses"><title> Utilisation de Ncurses</title>

<para>Pour supprimer le volume et le conteneur avec Ncurses, suivez ces étapes&nbsp;: </para>
<orderedlist>
<listitem><para> Sélectionnez <menuchoice><guimenu>Actions</guimenu><guimenuitem>Delete</guimenuitem><guimenuitem>Volume</guimenuitem></menuchoice>. </para></listitem>
<listitem><para> Sélectionnez dans le liste le volume <filename>/dev/evms/Sample Volume</filename>. </para></listitem>
<listitem><para> Activez <guibutton>Delete Volume Recursively</guibutton>. Cette étape supprime le volume et la région <filename>lvm/Sample Container/Sample Region</filename>. Pour garder les éléments sélectionnés ou supprimer chaque élément séparément, activez <guibutton>Delete</guibutton>(supprimer) au lieu de <guibutton>Delete Recursive</guibutton>. </para></listitem>
<listitem><para> Si vous avez choisi la suppression récursive (<guibutton>Delete Volume Recursively</guibutton>) (dans le cas contraire, supprimez la région avant de poursuivre cette procédure), sélectionnez <menuchoice><guimenu>Actions</guimenu><guimenuitem>Delete</guimenuitem><guimenuitem>Container</guimenuitem></menuchoice>. </para></listitem>
<listitem><para> Sélectionnez dans la liste le conteneur <guimenu>lvm/Sample Container</guimenu>. </para></listitem>
<listitem><para> Cliquez sur <guibutton>Recursive Delete</guibutton> pour supprimer le conteneur et tout ce qu'il contient. Pour supprimer uniquement le conteneur, cliquez sur <guibutton>Delete</guibutton> (Si le conteneur a été construit sur des disques, comme dans l'exemple, les deux commandes auront le même effet). </para></listitem>
<listitem><para> Appuyez sur la touche <keycap>Enter</keycap>. </para></listitem>
</orderedlist>
<para>
</para>
<para>Vous pouvez également effectuer certaines étapes de suppression de volume à l'aide du menu contextuel de l'interface&nbsp;: </para>

<orderedlist>
<listitem><para>

  Dans la vue des volumes, appuyez sur <keycap>Enter</keycap> sur 
  <filename>/dev/evms/Sample Volume</filename>.

</para></listitem>
<listitem><para>

  Activez <guibutton>Delete</guibutton> (supprimer).
 
</para></listitem> 
<listitem><para>

  Continuez les opérations à partir de l'étape 3 des instructions de 
  Ncurses.
  
</para></listitem>
</orderedlist>

</sect2>

<sect2 id="deleterecurcli">

<title>Utilisation du CLI</title>

<para>

Pour supprimer des objets EVMS, utilisez les commandes 
<command>delete</command> et <command>delete recursive</command>. 
Spécifiez le nom de la commande suivi de deux points et entrez ensuite 
le nom du volume, de l'objet ou du conteneur. Par exemple&nbsp;:

</para>

<orderedlist>

<listitem><para>

  Entrez cette commande pour effectuer une suppression 
  récursive&nbsp;:
  
</para>

<programlisting>
delete recursive: '/dev/evms/Sample Volume'
</programlisting>

<para>

  Cette étape supprime le volume et la région <filename>/lvm/Sample 
  Container/Sample Region</filename>. Pour garder les élément 
  selectionnés, ou pour supprimer chaque élément séparément, utilisez la 
  commande <command>delete</command>, comme ceci&nbsp;:

</para>

<programlisting>
delete: '/dev/evms/Sample Volume'
</programlisting>

</listitem> 

<listitem><para>

  Si vous avez choisi <command>Delete Volume Recursively</command> (dans 
  le cas contraire, supprimez la région avant de poursuivre cette 
  procédure), entrez la commande suivante pour supprimer le conteneur et 
  ce qu'il contient&nbsp;:

</para>
<programlisting>
delete recursive: 'lvm/Sample Container'
</programlisting>
<para>

  Pour supprimer uniquement le conteneur, entrez la commande 
  suivante&nbsp;:

</para>
<programlisting>
delete: 'lvm/Sample Container'
</programlisting></listitem> 

</orderedlist>

</sect2>

</sect1>

</chapter>
