<appendix id="jfsfsim"><title>Module d'interface de système de fichiers JFS</title>
  
  <para>Le module d'interface de système de fichiers JFS (JFS FSIM) permet aux utilisateurs EVMS de créer et gérer les systèmes de fichiers JFS à partir des interfaces EVMS. Afin d'utiliser le JFS FSIM, la version 1.0.9 ou plus récente des utilitaires JFS doit être installée sur votre système. On trouvera la dernière version de JFS sur <ulink url="http://oss.software.ibm.com/jfs/">http://oss.software.ibm.com/jfs/</ulink>.</para>
  
  <sect1 id="createjfsfsim"><title>Création de systèmes de fichiers JFS</title>
    <para>
      On peut créer les systèmes de fichiers JFS avec <command>mkfs</command> sur n'importe quel volume EVMS ou de compatibilité (de taille supérieure ou égale à 16Mo) qui ne possède pas déjà de système de fichiers. Voici les options disponibles pour créer des systèmes de fichiers JFS:</para>
    
    <variablelist>
      
      <varlistentry><term>badblocks</term>
        <listitem>
          <para>Effectue une recherche en lecture seule des blocs défectueux sur le volume avant de créer le système de fichiers. La valeur par défaut est false.</para>
        </listitem>
      </varlistentry>
      
      <varlistentry><term>caseinsensitive</term>
        <listitem>
          <para>Marque le système de fichier comme insensible à la casse (pour la compatibilité OS/2). La valeur par défaut est false.</para>
        </listitem>
      </varlistentry>
      
      <varlistentry><term>vollabel</term>
        <listitem>
          <para>Spécifie une étiquette (d'en-tête) de volume pour le système de fichiers. La valeur par défaut est none.</para>
        </listitem>
      </varlistentry>
      
      <varlistentry><term>journalvol</term>
        <listitem>
          <para>Spécifie que le volume sera utilisé pour un journal externe. Cette option est seulement disponible avec la version 1.0.20 ou plus récente des utilitaires JFS. La valeur par défaut est none.</para>
        </listitem>
      </varlistentry>
      
      <varlistentry><term>logsize</term>
        <listitem>
          <para>Spécifie la taille du fichier log (en Mo). Cette option est seulement disponible si aucune valeur n'a été indiquée pour l'option journalvol. La valeur par défaut est 0,4% de la taille du volume, jusqu'à 32 Mo.</para>
        </listitem>
      </varlistentry>
      
    </variablelist>
    
  </sect1>
  
  <sect1 id="checkjfsfsim"><title>Vérification des systèmes de fichiers JFS</title>
    <para>Voici les options disponibles pour vérifier les systèmes de fichiers JFS avec <command>fsck</command>:</para>
    
    <variablelist>
      
      <varlistentry><term>force</term>
        <listitem>
          <para>Force la vérification complète du système de fichiers, même si le système de fichiers est déjà marqué comme étant bon. La valeur par défaut est false.</para>
        </listitem>
      </varlistentry>
      
      <varlistentry><term>readonly</term>
        <listitem>
          <para>Vérifie si le système de fichiers est en lecture seule. Signale mais ne corrige pas les erreurs. Si le système de fichiers est monté, cette option est automatiquement sélectionnée. La valeur par défaut est false.</para>
        </listitem>
      </varlistentry>
      
      
      <varlistentry><term>omitlog</term>
        <listitem>
          <para>Avec cette option, le journal de transactions ne sera pas affiché. Elle ne doit être spécifiée que si le fichier log est corrompu. La valeur par défaut est false.</para>
        </listitem>
      </varlistentry>
      
      <varlistentry><term>verbose</term>
        <listitem>
          <para>Affiche les détails et les informations de débogage pendant la vérification. La valeur par défaut est false.</para>
        </listitem>
      </varlistentry>
      
      <varlistentry><term>version</term>
        <listitem>
          <para>Affiche la version de <filename>fsck.jfs</filename> et quitte sans avoir vérifié le système de fichiers. La valeur par défaut est false.</para>
        </listitem>
      </varlistentry>
      
    </variablelist>
    
  </sect1>
  
  <sect1 id="removejfsfsim"><title>Suppresion des systèmes de fichier JFS</title>
    
    <para>On peut supprimer un système de fichiers JFS dans un volume si le système de fichiers est démonté. Cette opération implique la suppression du superbloc de ce volume de sorte que le système de fichiers ne pourra être reconnu après. Aucune option n'est disponible pour la suppression des systèmes de fichiers.
    </para>
    
  </sect1>
  
  <sect1 id="expandjfsfsim"><title>Agrandissement des systèmes de fichiers JFS</title>
    
    <para>Un système de fichiers JFS s'agrandit automatiquement quand on agrandit son volume. Cependant, JFS n'autorise l'agrandissement du volume que si il est monté, car JFS exécute tous ses agrandissements en ligne. De plus, JFS ne permet les expansions que si la version 1.0.21 ou plus des utilitaires JFS est installée</para>
    
  </sect1>
  
  <sect1 id="shrinkjfsfsim"><title>Réduction des systèmes de fichiers JFS</title>
    
    <para>A ce jour, JFS ne supporte pas la réduction de ses systèmes de fichiers. Par conséquent, les volumes avec des systèmes de fichiers JFS ne peuvent être réduits.</para>
    
  </sect1>
</appendix>
