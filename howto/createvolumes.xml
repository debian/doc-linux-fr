<chapter id="creatvol"><title>Création de volumes</title>

<para>Ce chapitre explique quand et comment créer des volumes.</para>

<sect1 id="whencreatvol"><title>Quand créer un volume ?</title>
<para>EVMS traite les objets de stockage et les volumes séparément. Un objet de stockage ne devient pas automatiquement un volume. Il faut le transformer en volume.

Les volumes se créent à partir des objets de stockage. Les volumes sont soit des volumes natifs d'EVMS, soit des volumes compatibles. Les volumes compatibles sont prévus pour être compatibles avec un gestionnaire de volumes autre qu'EVMS, tel que Linux LVM, MD, OS/2 ou AIX. Les volumes compatibles peuvent avoir des restrictions sur ce qu'EVMS peut faire avec eux. Les volumes natifs d'EVMS n'ont pas de telles restrictions, mais ils ne peuvent être utilisés que sur des systèmes équipés avec EVMS. Les volumes sont montables et peuvent contenir des systèmes de fichiers.

Les volumes natifs d'EVMS contiennent des informations spécifiques à EVMS afin d'identifier le nom du volume. Après que les informations du volume sont appliquées, le volume n'est plus complètement rétro-compatible avec les types de volumes existants.

Au lieu d'ajouter des métadonnées EVMS à un objet existant, on peut indiquer à EVMS de faire un objet directement disponible en tant que volume. C'est ce qu'on appelle un volume compatible. En utilisant cette méthode, le produit final est complètement rétro-compatible avec le système souhaité.
</para>
</sect1>

<sect1 id="createvolnatex"><title>Exemple : Création d'un volume natif d'EVMS</title>

  <para>Cette partie explique en détail comment créer un volume natif d'EVMS avec EVMS en fournissant les instructions pour vous aider à effectuer les tâches suivantes.</para>


<blockquote><example><title>Créer un volume natif d'EVMS</title>
<para>
Créer un volume natif d'EVMS appelé "Sample Volume" à partir de la région, <filename>/lvm/Sample Container/Region</filename>, que l'on a créée dans le chapitre 9.</para></example></blockquote>

<sect2 id="guicreatvolnat"><title>Utilisation de l'interface EVMS</title>

<para>Pour créer un volume EVMS :</para>
<orderedlist>
  <listitem><para>Sélectionnez<menuchoice>
    <guimenu>Actions</guimenu>
    <guimenuitem>Create</guimenuitem>
    <guimenuitem>EVMS Volume.</guimenuitem>
  </menuchoice></para></listitem>

  <listitem><para>Choississez <filename>lvm/Sample Container/Sample Region</filename>.</para></listitem>

  <listitem><para>Entrez <userinput>Sample Volume</userinput> dans le champ name.</para></listitem>

  <listitem><para>Cliquez sur <guibutton>Create</guibutton>.</para></listitem>
</orderedlist>

<para>On peut également effectuer certaines de ces étapes pour créer un volume EVMS à partir du menu contextuel de l'interface :</para>
<orderedlist>
  <listitem><para>A partir de l'onglet <guimenuitem>Available Opions</guimenuitem>, faites un clic droit sur <filename>lvm/Sample Container/Sample Region</filename>.</para></listitem>
  
  <listitem><para>Cliquez sur <guibutton>Create EVMS Volume</guibutton>...</para></listitem>

  <listitem><para>Poursuivez à partir de l'étape 3 des instructions pour l'interface.</para></listitem>
</orderedlist>
</sect2>

<sect2 id="ncursecreatvolnat"><title>Utilisation de Ncurses</title>
<para>
Pour créer un volume :</para>
<orderedlist>
  <listitem><para>Sélectionnez <menuchoice>
    <guimenu>Actions </guimenu>
    <guimenuitem>Create </guimenuitem>
    <guimenuitem>EVMS Volume</guimenuitem>
  </menuchoice>.
  
  </para></listitem>

<listitem><para>Entrez <userinput>Sample Volume</userinput> à l'invite "name". Appuyez sur <keycap>Entrée</keycap>.</para></listitem>

<listitem><para>Activez <guibutton>Create</guibutton>.</para></listitem>
</orderedlist>

<para>On peut aussi effectuer certaines des étapes pour créer un volume EVMS à partir du menu contextuel :</para>
<orderedlist>
  <listitem><para>A partir de la vue <guimenuitem>Available Objects</guimenuitem>, faites <keycap>Entrée</keycap> sur <filename>lvm/Sample Container/Sample Region</filename>.</para></listitem>

  <listitem><para>Activez l'élément <guimenuitem>Create EVMS Volume</guimenuitem> du menu.</para></listitem>

  <listitem><para>Poursuivez à partir de l'étape 3 des instructions Ncurses.</para></listitem>
</orderedlist>
</sect2>

<sect2 id="clicreatvolnat"><title>Utilisation du CLI</title>

  <para>Pour créer un volume, utilisez la commande <command>Create</command>. Les arguments que la commande <command>Create</command> accepte dépendent de ce que l'on est en train de créer. Dans notre exemple, le premier argument est le mot-clé <command>volume</command> qui précise ce que l'on est en train de créer. Le deuxième argument est l'objet à transformer en volume, dans notre cas<filename>lvm/Sample Container/Sample Region</filename>. Le troisième argument est spécifique au type pour un volume EVMS, <command>Name=</command>, suivi du nom que vous voulez donner au volume, dans notre cas <command>Sample Volume</command>. La commande suivante crée le volume de l'exemple :</para>

    <programlisting>Create: Volume, «lvm/Sample Container/Sample Region», Name=«Sample Volume»</programlisting>
</sect2>
</sect1>


<sect1 id="creatvolcomex"><title>Exemple : création d'un volume compatible</title>

<para>Cette partie explique en détail comment créer un volume compatible avec EVMS, en fournissant les instructions pour vous aider à accomplir les tâches suivantes.</para>


<blockquote><example><title>Création d'un volume compatible</title>

    <para>Créez un volume compatible appelé <filename>"Sample Volume"</filename> à partir de la région, <filename>/lvm/Sample   Container/Region</filename>, que vous avez créée au chapitre 9.</para></example></blockquote>


<sect2 id="guicreatvolcom"><title>Utilisation de l'interface</title>

<para>Pour créer un volume compatible :</para>
<orderedlist>
  <listitem><para>Sélectionnez <menuchoice>
    <guimenu>Actions </guimenu>
    <guimenuitem>Create </guimenuitem>
    <guimenuitem>Compatibility Volume</guimenuitem>
  </menuchoice>.</para></listitem>

  <listitem><para>Choisissez dans la liste la région <filename>lvm/Sample Container/Sample Region</filename>.</para></listitem>

  <listitem><para>Cliquez sur le bouton <guibutton>Create</guibutton>.</para></listitem>

  <listitem><para>Cliquez sur l'onglet <guimenuitem>Volume</guimenuitem> dans l'interface pour voir le volume nommé <filename>/dev/evms/lvm/Sample Container/Sample Region</filename>. Ce volume est notre volume compatible.</para></listitem>
</orderedlist>

<para>On peut aussi effectuer certaines des étapes pour créer un volume compatible à partir du menu contextuel de l'interface :</para>
<orderedlist>
  
  <listitem><para>A partir de l'onglet <guimenuitem>Available Objects</guimenuitem>, faites un clic droit sur <filename>lvm/Sample Container/Sample Region</filename>.</para></listitem>
  <listitem><para>Cliquez sur <guimenuitem>Create Compatibility Volume</guimenuitem>...</para></listitem>
  <listitem><para>Poursuivez à partir de l'étape 3 des instructions pour l'interface.</para></listitem>
</orderedlist>
</sect2>


<sect2 id="ncursecreatvolcom"><title>Utilisation de Ncurses</title>

<para>Pour créer un volume compatible :</para>
<orderedlist>
  <listitem><para>Sélectionnez <menuchoice>
    <guimenu>Actions </guimenu>
    <guimenuitem>Create </guimenuitem>
    <guimenuitem>Compatibility Volume.</guimenuitem>
  </menuchoice></para></listitem>
  
  <listitem><para>Choisissez dans la liste la région <filename>lvm/Sample Container/Storage Region</filename>.</para></listitem>

  <listitem><para>Activez <guibutton>Create</guibutton>.</para></listitem>

</orderedlist>

<para>On peut aussi effectuer certaines des étapes pour créer un volume compatible à partir du menu contextuel :</para>
<orderedlist>
  <listitem><para>A partir de la vue <guimenuitem>Available Objects</guimenuitem>, faites <keycap>Entrée</keycap> sur <filename>lvm/Sample Container/Sample Region</filename>.</para></listitem>

  <listitem><para>Activez dans le menu l'élément <guimenuitem>Create Compatibility Volume</guimenuitem>.</para></listitem>

  <listitem><para>Poursuivez à partir de l'étape 3 des instructions de Ncurses.</para></listitem>
</orderedlist>
</sect2>

<sect2 id="clicreatvolcom"><title>Utilisation du CLI</title>

  <para>Pour créer un volume, utilisez la commande <command>Create</command>. Les arguments que la commande <command>Create</command> accepte dépendent de ce que l'on est en train de créer. Dans notre exemple, le premier argument est le mot-clé <command>volume</command> qui précise ce que l'on est en train de créer. Le deuxième argument est l'objet à transformer en volume, dans notre cas <command>lvm/Sample Container/Sample Region</command>. Le troisième argument, <command>compatibility</command>, indique que c'est un volume compatible et qu'il devrait être nommé ainsi.</para>

  <programlisting>Create:Volume,«lvm/Sample Container/Sample Region»,compatibility</programlisting>
</sect2>
</sect1>
</chapter>
