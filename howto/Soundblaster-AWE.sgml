<!-- This is the Sound Blaster AWE 32/64 HOWTO by Marcus Brinkmann -->
<!doctype linuxdoc system>
<!-- $Date: 2003/01/03 02:40:54 $ -->

<article>

<title>Sound Blaster AWE 32/64 HOWTO
<author>Marcus Brinkmann <tt><htmlurl
url="mailto:Marcus.Brinkmann@ruhr-uni-bochum.de"
name="Marcus.Brinkmann@ruhr-uni-bochum.de"></tt>;<newline>
Version Fran�aise par Arnaud Launay, <tt><htmlurl
url="mailto:asl@launay.org" name="asl@launay.org"></tt>
<date>v1.2, 11 Janvier 1998

<abstract>
Ce document d�crit l'installation et la configuration d'une Sound
Blaster 32 (SB AWE 32, SB AWE 64) de Creative Labs dans un syst�me
Linux en utilisant les extensions du p�riph�rique sonore
�crites par Takashi Iwai. Il couvre �galement quelques outils
sp�ciaux et lecteurs pour la s�rie des SB AWE. Le
syst�me de r�f�rence est le Debian GNU/Linux, mais
toute autre distribution Linux devrait fonctionner.
</abstract>

<toc>

<sect>Introduction<p>

Ceci est le Sound Blaster AWE HOWTO. Il vous donne des informations
d�taill�es sur la mani�re de tirer le maximum de votre
carte Sound Blaster 32 ou meilleure, incluant le synth�tiseur Wave
Table. Ce document couvre toutes les cartes SB jusqu'� la SB 32.

La s�rie des Sound Blaster est bien connue des communaut�s DOS
et Windows, et beaucoup d'utilisateurs de Linux d�sirent
�galement utiliser cette carte son sous Linux.
Malheureusement, Creative Labs fournit uniquement un pilote pour Windows et
DOS, et il n'est donc pas trivial d'installer et d'utiliser une carte SB
sous Linux. Ce document essaye de d�crire comment utiliser les
sp�cificit�s de la s�rie des SB AWE dans un
environnement Linux.

<sect1> Remerciements<p>

Ce document contient des informations que j'ai tir� de la Foire Aux
Questions (FAQ) du pilote AWE et de celle de l'ISA PnP. Voyez la section
<ref id="sources" name="Sources"> pour les auteurs et l'emplacement de ces
documents. Merci aux auteurs, qui ont permis de rendre possible le support
des SB AWE.

<url
url="http://www.4front-tech.com/usslite" name="Hannu
Savolainen"> a d�velopp� la plus grande partie du pilote sonore
qui vient avec le noyau Linux. Merci !

Je voudrais remercier <url name="Nicola Bernardelli"
url="mailto:n.bern@mail.protos.it"> pour tester la partie sur l'AWE64. Sans
lui, beaucoup d'erreurs seraient rest�es ind�tect�es.

Gr�ce au paquetage <url
url="ftp://sunsite.unc.edu/pub/Linux/utils/text/sgml-tools-0.99.0.tar.gz"
name="SGML Tools">, ce HOWTO est disponible en divers formats, venant tous
d'un fichier source commun.

<sect1>Histoire des r�visions<p>

<descrip>

<tag>Version 1.0</tag>premi�re version.
<tag>Version 1.1</tag>orthographe corrig�e (merci Curt!), ajout des
versions n�cessaires pour <tt/isapnp/, disponible dor�navant sur
sunsite et ses miroirs.
<tag>Version 1.2</tag>version fran�aise disponible, corrections mineures, un
grand nombre de fautes d'orthographes fix�es (VO) (merci � ispell).

</descrip>

<sect1>Nouvelles versions de ce document<p>

La derni�re version peut �tre trouv�e sur ma page
principale, � <url
url="http://homepage.ruhr-uni-bochum.de/Marcus.Brinkmann/soundblaster.html"
name="Sound Blaster AWE HOWTO">.
La derni�re version fran�aise se trouve sur <url
url="http://www.freenix.org/unix/linux/HOWTO/mini/Soundblaster-AWE.html">.

Les nouvelles versions de ce document seront envoy�es � divers
sites ftp anonymes qui archivent de telles informations, incluant <url
url="ftp://sunsite.unc.edu/pub/Linux/docs/HOWTO/mini"> (NdT: et <url
url="ftp://ftp.traduc.org/pub/HOWTO/FR/mini"> pour la version
fran�aise).

Les versions hypertextes de ce HOWTO et de nombreux autres HOWTOs Linux sont
disponibles sur beaucoup de sites World-Wide-Web, incluant <url
url="http://sunsite.unc.edu/LDP/">. La plupart des distributions Linux sur
CD-ROM contiennent les HOWTOs, souvent dans le r�pertoire
<tt>/usr/doc</tt>.

Si vous traduisez ce document dans une autre langue, fa�tes le moi savoir et
je rajouterais une r�f�rence ici.

<sect1> Contacter l'auteur<p>

Je compte sur vous, le lecteur, pour rendre ce HOWTO utile. Si vous avez eds
suggestions, des corrections, ou des commentaires, veuillez me les envoyer,
<htmlurl url="mailto:Marcus.Brinkmann@ruhr-uni-bochum.de"
name="Marcus.Brinkmann@ruhr-uni-bochum.de">, et je tenterais de les inclure
dans la version suivante.

J'aimerais sp�cialement recevoir des informations sur la proc�dure de
lancement (scripts de lancement, etc..) d'autres fameuses distributions de
Linux, comme la Red Hat ou la SuSE Linux.

Si vous publiez ce document sur un CD-ROM ou sous forme papier, une copie
serait appr�ci�e. Ecrivez moi pour mon adresse postale.
Consid�rez �galement une donation au Linux Documentation
Project pour aider le support de la documentation gratuite pour Linux.
Contactez le coordinateur des HOWTOs Linux, Greg Hankins <htmlurl
url="mailto:gregh@sunsite.unc.edu" name="gregh@sunsite.unc.edu">,
pour plus d'informations.

<sect1>License de Distribution<p>

Copyright 1997 Marcus Brinkmann.

Cette documentation est libre, vous pouvez la redistribuer et/ou la
modifier selon les termes de la Licence Publique G�n�rale GNU publi�e
par la Free Software Foundation (version 2 ou bien toute autre
version ult�rieure choisie par vous).

Cette documentation est distribu�e car potentiellement utile, mais
<bF>SANS AUCUNE GARANTIE</bf>, ni explicite ni implicite, y compris
les garanties de <bf>commercialisation</bf> ou <bf>d'adaptation dans
un but sp�cifique</bf>. Reportez-vous � la Licence Publique G�n�rale
GNU pour plus de d�tails.

Vous pouvez obtenir une copie de la Licence Publique G�n�rale GNU en
�crivant � la <url url="http://www.fsf.org" name="Free Software
Foundation">,
Inc., 675 Mass Ave, Cambridge, MA 02139, �tats-Unis.

<sect> Avant de commencer<p>

<sect1>Introduction<p>

Ce document essaye de vous aider � installer et � utiliser une
Sound Blaster AWE 32 ou une Sound Blaster AWE 64 de chez Creative Labs pour
votre syst�me Linux. Le syst�me de r�f�rence est
le <url name="Debian GNU/Linux" url="http://www.debian.org"> sur un
plateforme Intel i586, mais il devrait fonctionner avec toute autre
distribution Linux ainsi que sur toute plateforme supportant le pilote son
Linux (less diff�rences sont mentionn�es lorsqu'elles
apparaissent).

Lisez attentivement le HOWTO Linux Sound (voir section <ref id="moreinfo"
name="Informations Compl�mentaires">). Je consid�re ce
document comme un suppl�ment au Sound HOWTO, et vous pourrez souvent
y trouver plus d'informations sur les choses que j'ai laiss�es de
c�t� ici.

<sect1> Quelques notes g�n�rales sur les cartes SB AWE<p>

La carte son SB AWE 32 propose un p�riph�rique audio brut, un
synth�tiseur standard OPL-2/OPL-3, un port MIDI MPU-401 et un
synth�tiseur 32 voix EMU 8000 Wave Table (pour une explication de ces
termes et d'autres voyez le Linux Sound HOWTO). Un des buts de ce document
est de vous aider � faire fonctionner proprement toutes ces
sp�cificit�s.

La SB AWE 64 a les capacit�s de la SB AWE 32 et un
synth�tiseur Wave Guide additionnel dont Creative Labs est tr�s
fier. Le probl�me pour les utilisateurs Linux est que les 32 voix
suppl�mentaires sont g�n�r�es par un logiciel et
sorties par les p�riph�riques wave bruts. Parce que Creative
Labs ne voit aucun march� dans les pilotes Linux, un pilote son pour
le synth�tiseur Wave Guide est disponible uniquement sous Windows 3.1
et Windows 95.

Cel� signifie que, du point de vue d'un utilisateur Linux, la SB AWE
32 et la SB AWE 64 sont quasiment identiques. A partir de maintenant
je me r�f�rerais seulement aux SB AWE en g�n�ral
et mentionnerait simplement les diff�rences lorsqu'elles
appara�tront.

<sect1> Quelques notes g�n�rales sur les cartes Plug and Play<p>

La plupart des cartes modernes pour les plateformes Intel sont les cartes
ISA PnP, qui est une abbr�viation pour `Plug and Play''. Cel�
signifie que la carte doit �tre configur�e par le
syst�me d'exploitation, et ceci est fait au travers d'un routine
d'initialisation au lancement. En g�n�ral, il y a au moins
trois moyens pour le faire:

<enum>
<item>
Vous avez un Bios PnP, ce qui signifie que votre Bios est au courant de la
pr�sence des cartes PnP et qu'il peut les configurer. Si vous pensez
pouvoir utiliser toutes les sp�cificit�s de votre SB AWE PnP
juste parce que vous avez un Bios PnP, ce n'est pas de chance. M�me si
le Bios d�clare supporter les cartes PnP il initialise seulement une
partie des ports et des adresses utilis�es par votre carte son. Vous
pourrez probablement jouer des donn�es brutes, mais vous serez
incapable de jouer de la musique MIDI, par exemple. Pour cette raison, un
Bios PnP n'est pas une option.

<item>
Vous disposez d'un syst�me d'exploitation qui supporte les cartes
PnP. Le noyau Linux stable actuel (2.0.x) n'en est pas un, vous devrez
donc attendre les prochaines versions du noyau, qui supporteront les
p�riph�riques PnP.

<item>
Vous avez un programme sp�cial, lanc� au d�marrage, qui
initialise vos cartes PnP. C'est ce que nous allons employer.
</enum>

Le logiciel le plus commun�menet utilis� pour initialiser les
cartes PnP sous Linux sont les `isapnptools'' (voir la section <ref
id="sources" name="Sources"> et la section <ref id="isapnp" name="Commencer">).
Ils fournissent un moyen simple pour configurer toutes les cartes ISA PnP de
votre machine, et pas seulement votre carte son.

<sect1> Quelques notes g�n�rales sur les modules chargeables
par le noyau <p>

Quelques pilotes de p�riph�riques peuvent �tre
construits en tant que modules au lieu de les compiler dans le noyau. Vous
pourrez trouver plus d'informations sur les modules dans le Kernel HOWTO et
le Module HOWTO (voir section <ref id="moreinfo"
name="Informations Compl�mentaires">).

Si vous avez une carte PnP, vous <em/devez/ installer le support sonore en
tant que module chargeable par le noyau. Cel� signifie que vous ne
pouvez construire le pilote sonore dans le noyau, mais que vous devez le
compiler en tant que module, afin qu'il puisse �tre charg� dans
le noyau au lancement. Ceci, parce que votre noyau sera install�
avant la configuration de votre carte ISA PnP, et votre pilote sonore doit
�tre charg� apr�s la configuration de votre carte ISA
PnP.

Le module du son peut �tre charg� manuellement via <tt/insmod
sound/ ou <tt/modprobe -a sound/ ou dans le script de lancement de votre
syst�me Linux (sous Debian, il suffit d'ajouter une simple ligne
contenant <tt/sound/ � <tt>/etc/modules</tt>). Une autre approche
consiste � lancer <tt/kerneld/, un d�mon qui installe et
supprime les modules suivant les n�cessit�s.

Notez que <tt>kerneld</tt> peut ne pas �tre la meilleure solution pour
le module de l'AWE, car il prend du temps � charger le module dans le
noyau, surtout si vous voulez utiliser le synth�tiseur Wave Table et
charger de grosses banques Sound Font, ce que vous aurez � faire
� chaque insertion du module. Parce que <tt/kerneld/ supprime les
modules inutilis�es apr�s une minute par d�faut, il
serait pr�f�rable de charger le module son manuellement ou au
lancement. Notez que l'insertion manuelle ou au lancement du module emp�che
<tt/kerneld/ de le supprimer lorsqu'il est inutilis�. Mais vous
pouvez ins�rer manuellement le module et utiliser <tt/kerneld/ en
m�me temps. Les deux m�thodes ne sont pas conflictoires, mais
<tt/kerneld/ ne s'occupera plus du module son.

C'est particuli�rement utile si vos options pour le mixer sont
chang�es apr�s la suppression puis la r�installation du
module. Une solution � ce probl�me sera donn�e dans la
section <ref id="mixing" name="Mixing"> (elle d�crit le lancement
automatique d'un mixer lorsque vous chargez le modules du son).
Cependant, <tt>kerneld</tt> peut prendre du temps pour charger le module,
pour charger la banque sound font et pour lancer le mixer, et pour cette raison
(et quelques autres) il vaut mieux installer le module son au lancement et ne
pas laisser <tt/kerneld/ le supprimer.

<sect1> Quelques notes g�n�rales sur le pilote sonore du noyau
<p>
Vous pouvez compiler le support sonore dans le noyau ou en tant que module
chargeable. Si vous avez une carte PnP, vous devez installez le support
sonore en tant que module, car les cartes PnP n�cessitent d'�tre initialis�s
via les isapnptools avant le chargement du module.

Comme vous pouvez l'imaginer, vous devrez recompiler le noyau. Je vous
donnerai quelques trucs � ce propos plus bas. Pour l'instant, parlons du
support sonore dans les sources du noyau. Le noyau vient avec la version
gratuite (all�g�e) du pilote sonore OSS (USS). La version actuelle (3.5.4)
ne supporte pas enti�rement la SB AWE, mais seulement sa partie SB 16. Vous
aurez ainsi le p�riph�rique brut et les synth�tiseurs OPL-2/OPL-3
si vous l'utilisez, mais vous ne pourrez pas jouer de musique midi avec le
synth�tiseur Wave Table.

Si vous d�sirez utiliser le p�riph�rique Wave Table, vous pouvez soit
acheter le pilote sonore commercial de <url name="4Front Technologies"
url="http://www.4front-tech.com"> (si quelqu'un pouvait me confirmer que ce
pilote peut le faire), ou corriger votre noyau avec les
extensions du pilote sonore AWE 32 par Takashi Iwai. Le premier est en
dehors des vues de ce document, je supposerais donc que vous utiliserez le
dernier.

Les extensions du pilote sonore AWE 32 (voir section <ref id="sources"
name="Sources">) est publi� sous la licence GNU et vient avec un certain
nombre d'outils pour utiliser le synth�tiseur wave EMU 8000 des cartes SB
AWE.

Le logiciel <tt/awedrv/ est d�j� inclut dans les sources des noyaus les
plus r�cents (� partir des noyaus 2.1.x, mais vous devrez peut-�tre mettre
� jour vos sources dans l'arborescence du noyau, elles peuvent �tre
anciennes).

<sect> Comment installer le support des SB AWE <p>

<sect1> Les choses dont vous aurez besoin <p>

Ce qui suit est requis pour le support SB AWE sous Linux:
<itemize>
<item>
 un syst�me Linux fontionnel (par ex. la distribution Debian GNU/Linux),
<item>
 une carte SB AWE 32 ou compatible (c�d SB32, SB32 PnP, SB AWE64, ...),
<item>
 les sources du noyau Linux, incluant le pilote sonore OSS/Free (normalemnt
 inclus, v�rifiez dans <tt>/usr/src/Linux/drivers/sound/Readme</tt>),
<item>
 Les extensions du pilote AWE 32.
</itemize>

Si vous avez une carte PnP, vous aurez �galement besoin de:

<itemize>
<item>
  le paquetage logiciel des isapnptools.
</itemize>

Regardez dans la section <ref id="sources" Name="Sources"> pour les
informations concernant l'obtention de ces programmes.

Si vous avez un syst�me Debian GNU/Linux, vous aurez besoin des paquetages
<tt>kernel-source-&lt;version&gt;</tt>, <tt>awe-drv</tt> et peut-�tre le
paquetage <tt>isapnptools</tt>.
Vous d�sirez peut-�tre quelques-uns des autre paquetages <tt>awe-*</tt>,
mais cependant ils ne sont pas essentiels. Je recommande fortement le
<tt>kernel-package</tt> pour compiler et installer facilement
le noyau.

<sect1> Avant de commencer<label id="isapnp"><p>

Assurez vous que vous avez proprement install� votre carde dans un
emplacement sur votre carte m�re et peut-�tre l'avoir d�j� test�e dans un
environnement DOS ou Windows.

La prochaine �tape est d'initialiser la carte au lancement si et seulement
si c'est une carte PnP. Suivez la documentation dans les sources pour
compiler et installer <tt/isapnptools/ (ou utilisez simplement le paquetage
binaire Debian), et lancez

<tscreen><verb>
# pnpdump > /etc/isapnp.conf
</verb></tscreen>
<p>

en tant que super-utilisateur. Cel� g�n�rera un fichier de configuration
propre � votre carte PnP, mais avec tous les p�riph�riques comment�s. Editez
attentivement ce fichier, et comparez les values des canaux DMA, des bases
IO et des interruptions avec la configuration des cartes sous les
environnements DOS ou Windows si possible (Avec Win95, regardez les
ressources utilis�es par la carte sous <tt/resources/ dans le <tt/device
manager/). Si vous avez des probl�mes, lisez attentivement la
documentation fournie avec les <tt/isapnptools/.

<em/AVERTISSEMENT:/ les <tt/isapnptools/ �chouent souvent dans la d�tection
des trois ports I/O du p�riph�rique SB AWE Wave Table. Veuillez v�rifier
attentivement l'entr�e <tt/Wave Table/ de votre <tt/isapnp.conf/ avec l'exemple
� la fin de ce HOWTO. Ajustez les adresses I/O de bases si n�cessaire.
<p>

<em/AVERTISSEMENT:/ D'apr�s l' <tt/isapnp-faq/, quelques fois les
<tt/isapnp/ �chouent � programmer le nombre de p�riph�riques logiques. Si
vous rencontrez un message d'erreur comme celui-ci:

<tscreen><verb>
Error occurred executing request 'LD 2' on or around line...
</verb></tscreen>

essayez alors d'ajouter <tt/(VERIFYLD N)/ au d�but de <tt/isapnp.conf/. Vous
devez utiliser au minimum la version 1.10 pour que ceci fonctionne. Si vous
ne pouvez utiliser la version 1.10 ou ult�rieure, vous pouvez �galement POKE
directement les nombres des p�riph�riques logiques. Veuillez vous r�f�rer �
l'<tt>isapnp-faq</tt> pour plus d'informations sur cette approche. Si �a ne
marche pas pour vous, contactez moi (et les personnes d'<tt/isapnp/ seront
int�ress�es �galement, je pense).

<em>AVERTISSEMENT:</em> V�rifiez que la derni�re ligne est
<tt>(WAITFORKEY)</tt>, cel� sera parfois omis par les anciennes versions de
<tt>pnpdump</tt>.

Un <tt>isapnp.conf</tt> d'exemple pour seulement une seule carte PnP (la
carte son) peut-�tre trouv�e � la fin de ce document (voir la section <ref
id="isapnpconf" name="Exemple d'<tt>isapnp.conf</tt>">).

Si vous avez un syst�me Debian, aucun ajustement suppl�mentaire n'est
n�cessaire. <tt/isapnp/ sera lanc� au lancement dans
<tt>/etc/init.d/boot</tt> avec ce petit bout de script, que vous devrez
peut-�tre inclure dans vos scripts d'initialisation:
<code>
# Configure les cartes isa plug and play avant de charger
# les modules. N�cessite de faire ceci avant le chargement
# des modules pour avoir une chance de configurer et de
# lancer les cartes PnP avant que les pilotes ne viennent
# foutre le bordel.
#
if [ -x /etc/init.d/isapnp ]
then
  /etc/init.d/isapnp start
fi
</code>
o� <tt>/etc/init.d/isapnp</tt> est  
<code>
#! /bin/sh
# /etc/init.d/isapnp: configure les cartes Plug and Play
test -x /sbin/isapnp || exit 0
/sbin/isapnp /etc/isapnp.conf
exit 0
</code>

Si vous avez une autre distribution Linux, vous �tes chez vous. Je ne sais
pas ce qu'il faut faire (personne ne peut envoyer des informations plus
sp�cifiques ?). Soyez certains qu'isapnp sera lanc� <em/avant/
que les modules ne soient charg�s (voir plus haut).

<sect1> Compiler le noyau<p>

Avant de recompiler le noyau, vous devez ajouter les extensions AWE au
pilote sonore. M�me si vos sources du noyau ont d�j� les extensions
<tt/awedrv/ (voyez <tt>/drivers/sound/lowlevel/</tt> pour �a), vous pouvez avoir �
mettre le logiciel � jour.
Suivez les instructions des sources de <tt/awedrv/ pour l'installation. En
bref, vous devez lancer un script d'installation qui applique les
corrections � vos sources du noyau.

Faites attention si vous avez mis � jour vos sources du noyau en lan�ant le
script. En effet le script v�rifie simplement qu'un certain fichier existe -
s'il existe, il n'applique pas les corrections n�cessaires. Vous seriez
avis� de retirer le fichier <tt>drivers/sound/lowlevel/awe_wav.c</tt> avant
de lancer le script apr�s avoir mis � jour les sources du noyau.

Ensuite vous avez � configurer le noyau pour le support du son. J'esp�re que
vous connaissez quelque chose sur la compilation du noyau; voyez le Sound
HOWTO et le Kernel HOWTO pour les d�tails. Allez dans le r�pertoire o� se
trouvent vos sources du noyau (<tt>/usr/src/linux</tt> par exemple), et
lancez:

<tscreen><verb>
# make config
</verb></tscreen>

ou <tt>make menuconfig</tt> ou <tt>make xconfig</tt>. Ensuite vous devez
configurer votre noyau normalement. Utilisez cette opportunit� pour cr�er un
noyau petit et puissant, sp�cialement constitu� pour votre syst�me. R�pondez
bien <tt/Y/ � la question <tt/Enable loadable module support/, si
vous d�sirez installer le pilote sonore en tant que module chargeable. (vous
<em/devez/ le faire si vous avez une carte PnP), mais je ne suis pas
certain que vous d�siriez le faire, cependant.

A un moment, on vous demandera si vous d�sirez le support des cartes son.
Vous �tes libres de r�pondre avec <tt/Y/ ou avec <tt/M/ si vous
<em/n'avez pas/ de carte PnP.
Vous <em/devez/ r�pondre avec <tt/M/, pour module, si vous avez une
carte PnP. Vous avez � compiler le support carte son en module si vous avez
une carte PnP car les cartes PnP doivent �tre initialis�es avant que les
modules ne soient charg�s.

Vous devez r�pondre <tt/Y/ aux questions suivantes, toutes les autres
avec <tt/N/:

<code>
Sound Blaster (SB, SBPro, SB16, clones) support (CONFIG_SB) [Y/n/?]
Generic OPL2/OPL3 FM synthesizer support (CONFIG_ADLIB) [Y/n/?]
/dev/dsp and /dev/audio support (CONFIG_AUDIO) [Y/n/?]
MIDI interface support (CONFIG_MIDI) [Y/n/?]
FM synthesizer (YM3812/OPL-3) support (CONFIG_YM3812) [Y/n/?]
lowlevel sound driver support [Y/n/?]
AWE32 support (CONFIG_AWE32_SYNTH) [Y/n/?]
</code>

En fait, seul le dernier est pour le synth�tiseur Wave Table. Les autres
sont les options SB 16 du pilote OSS/Free.

De plus, vous devrez configurer le port I/O de la carte son. Voyez le
fichier <tt/isapnp.conf/ pour de l'aide, si vous en avez un. Pour moi,
les valeurs par d�faut sont suffisantes. Notez que les valeurs par d�faut
des scripts de configuration du kernel peuvent �tre fausses (principalement
les valeurs <tt/SBC_IRQ/ et <tt/SB_MPU_BASE/ qui semblent incorrectes pour
la plus grande partie des cartes).

<code>
I/O base for SB Check from manual of the card (SBC_BASE) [220]
Sound Blaster IRQ Check from manual of the card (SBC_IRQ) [5]
Sound Blaster DMA 0, 1 or 3 (SBC_DMA) [1]
Sound Blaster 16 bit DMA (_REQUIRED_for SB16, Jazz16, SMW) 5, 6 or 7
(use 1 for 8 bit cards) (SB_DMA2) [5]
MPU401 I/O base of SB16, Jazz16 and ES1688 Check from manual of the card
(SB_MPU_BASE) [330]
SB MPU401 IRQ (Jazz16, SM Wave and ES1688) Use -1 with SB16 (SB_MPU_IRQ)
[-1]
</code>

Maintenant recompilez le noyau. Les utilisateurs Debian doivent utiliser le
<tt/kernel-package/. Ce paquetage rend les compilations de noyau aussi
facile que l'installation d'un paquetage debian. Voyez la documentation dans
<tt>/usr/doc/kernel-package/</tt>. Par exemple:

<tscreen><verb>
# make-kpkg clean
# make-kpkg -revision custom.1.0 kernel_image
</verb></tscreen>

puis <tt>dpkg -i /usr/src/kernel-image-2.0.29_custom.1.0_i386.deb</tt>.

Si vous avez une autre distribution Linux, suivez le moyen normal de
compilation d'un nouveau noyau. N'oubliez pas <tt/make modules/ et
<tt/make modules_install/. Voyez le Sound HOWTO et peut-�tre le Kernel
HOWTO pour plus d'informations.

<sect1> Red�marrage<p>

Apr�s l'installation d'un nouveau noyau, vous devez relancer votre machine
(v�rifiez que vous avez une disquette de lancement sous la main). Croisez
les doigts.

Si vous avez une carte PnP, v�rifiez que vous lancez isapnp soit dans un
script de lancement (comme d�crit plus haut) ou manuellement:

<tscreen><verb>
# /sbin/isapnp /etc/isapnp.conf
Board 1 has Identity 74 00 00 e3 10 48 00 8c 0e:  CTL0048 Serial No 58128
[checksum 74]
</verb></tscreen>

Maintenant vous pouvez installer le pilote sonore, apr�s l'avoir compil� en
tant que module:

<tscreen><verb>
# modprobe -a sound
AWE32 Sound Driver v0.3.3e (DRAM 2048k)
</verb></tscreen>

Si vous pensez que la d�tection de la m�moire n'�tait pas correcte (j'ai eu
des �chos de quelqu'un qui a une AWE64 avec 4096k, et `detected'' a �t� de
28672k), vous pouvez soit essayer de mettre � jour le logiciel <tt/awedrv/
ou sp�cifier la taille de la m�moire ddans le fichier
<tt>/usr/src/linux/drivers/sound/lowlevel/awe_config.h</tt>, par exemple:

<tscreen><verb>
#define AWE_DEFAULT_MEM_SIZE  4096   /* kbytes */
</verb></tscreen>

D�sol�, vous devez alors recompiler le noyau (compiler les modules pourrait
suffire, mais je n'en suis pas s�r).

Si �a marche, vous pouvez d�sirer que le module sonore se charge
automatiquement. Vous pouvez utiliser <tt/kerneld/ (pourquoi c'est une
mauvaise id�e est expliqu� dans la section 1.4) ou ajouter une simple ligne
contenant <tt/sound/ � votre <tt>/etc/modules/</tt> (pour Debian) ou
ajouter <tt>/sbin/modprobe -a sound</tt> � votre script de lancement.

<sect> Tester le pilote sonore<p>

<sect1><tt>/proc/devices, /dev/sndstat</tt><p>

Si vous avez int�gr� le support sonore, vous obtiendez quelques informations
utilies au lancement. Si vous avez le support sonore en tant que module
chargeable, vous pouvez obtenir la m�me information (peut-�tre en retirant
le module sound avec <tt>modprobe -r sound</tt> d'abord) avec:

<tscreen><verb>
# modprobe -a sound trace_init=1
Sound initialization started

<Sound Blaster 16 (4.13)> at 0x220 irq 5 dma 1,5
<Sound Blaster 16> at 0x330 irq 5 dma 0
<Yamaha OPL3 FM> at 0x388
Sound initialization complete
AWE32 Sound Driver v0.3.3e (DRAM 2048k)
</verb></tscreen>

Si vous avez un syst�me de fichiers virtuel <tt>/proc</tt>, vous pouvez
regarder pour le p�riph�rique sonore avec:

<tscreen><verb>
# cat /proc/devices
Character devices:
[...]
14 sound
[...]
</verb></tscreen>

V�rifier ensuite que vous avez les p�riph�riques corrects install�s sous
<tt>/dev/</tt>. Regardez le Sound HOWTO pour les d�tails. Demandez ensuite �
<tt>/dev/sndstat</tt> le status du module sound:

<tscreen><verb>
# cat /dev/sndstat
Sound Driver:3.5.4-960630 (Sat Oct 11 19:35:14 CEST 1997 root,
Linux flora 2.0.29 #1 Sat Oct 11 19:12:56 CEST 1997 i586 unknown)
Kernel: Linux flora 2.0.29 #1 Sat Oct 11 19:36:23 CEST 1997 i586
Config options: 0

Installed drivers:
Type 1: OPL-2/OPL-3 FM
Type 2: Sound Blaster
Type 7: SB MPU-401

Card config:
Sound Blaster at 0x220 irq 5 drq 1,5
SB MPU-401 at 0x330 irq 5 drq 0
OPL-2/OPL-3 FM at 0x388 drq 0

Audio devices:
0: Sound Blaster 16 (4.13)

Synth devices:
0: Yamaha OPL-3
1: AWE32 Driver v0.3.3e (DRAM 2048k)

Midi devices:
0: Sound Blaster 16

Timers:
0: System clock

Mixers:
0: Sound Blaster
1: AWE32 Equalizer
</verb></tscreen>

Si vous n'avez pas de sortie ressemblant � celle-ci, il y a peut-�tre une
erreur dans votre configuration. Repartez et cherchez ce qui ne va pas, puis
retournez � l'�tape <ref id="isapnp" name="Avant de commencer">, en v�rifiant
tout.

<sect1> Sortie - Le p�riph�rique audio brut<p>

T�chez d'obtenir un fichier <tt>.au</tt> (Sun) ou un fichier brut de test,
et fa�tes

<tscreen><verb>
# cat bell.au > /dev/audio
</verb></tscreen>

ou

<tscreen><verb>
# cat sample > /dev/dsp
</verb></tscreen>

Vous devriez entendre le contenu du fichier via le <tt>Audio Device 0: Sound
Blaster 16 (4.3)</tt>.

<sect1> Sortie - Le Synth�tiseur OPL-2/OPL-3
<p>

Si vous d�sirez utiliser le synth�tiseur OPL-2/OPL-3 FM pour jouer des
fichiers MIDI avec votre carte son, essayez le programme <tt>playmidi</tt>
(voir Appendice B). Commencez avec

<tscreen><verb>
# playmidi -f dance.mid
</verb></tscreen>
<p>

ou
<p>

<tscreen><verb>
# playmidi -4 dance.mid
</verb></tscreen>

Le premier vous donnera du OPL-2, le dernier de la musique MIDI OPL-3. Si
vous �tes embarass�s par le son, ne bl�mez pas playmidi: c'est le
synth�tiseur FM qui rend mal.

Imaginez si vous aviez seulement le pilote OSS/Free: ceci serait la
meilleure qualit� de musique MIDI que vous pourriez avoir (en dehors de la
synth�se logicielle). Heureusement, vous avez une SB AWE, et vous pouvez
utiliser les possibilit�s Wave Table avec les extensions AWE.

<sect1>Sortie - Le Synth�tiseur Wave Table<p>

Les extensions AWE viennent avec des outils sp�ciaux (<tt/awesfx/) pour
pouvoir utiliser le synth�tiseur EMU 8000 Wave Table. Tout d'abord, vous
devrez charger une banque Sound Font sur votre carte - m�me si vous d�sirez
utiliser les samples de la ROM! Vous pouvez utiliser les fichiers de votre
installation Windows - regardez pour des fichiers se terminant en
<tt>*.sfb</tt> ou <tt>*.sf2</tt>.

Les samples de la ROM peuvent �tre charg�s avec <tt>SYNTHGM.SBK</tt>, de
vrais samples sont dans <tt>SYNTHGS.SBK</tt> et <tt>SYNTHMT.SBK</tt>, ainsi
que dans <tt>SAMPLE.SBK</tt>. Vous pouvez obtenir d'autres banques Sound
Font via ftp ou www, essayer la EMU Homepage, le site web de Creative Labs,
et regarder pour les samples Chaos, ils sont vraiment bons (v�rifiez sur le
site web du pilote AWE).

Essayez de charger le standard GM (ROM) avec:

<tscreen><verb>
# sfxload -i synthgm.sbk
</verb></tscreen>

puis jouer un des fichiers midi qui viennent avec la SB AWE:

<tscreen><verb>
# drvmidi dance.mid
</verb></tscreen>

<sect1>Mixer<label id="mixing"><p>

Prenez votre mixer pr�f�r� et lancez le. Lancez un sample audio brut et deux
fichiers MIDI en m�me temps, et testez les options du mixer. Jouez un peu,
ce doit �tre tr�s facile. Voici une liste des p�riph�riques et de leur nom:

<descrip>
<tag>Yamaha OPL-3</tag>Synth ou FM
<tag>AWE32 Driver</tag>Synth ou FM (est ce que quelqu'un connait un mixer o�
ces deux sont s�par�s ?)
<tag>Sound Blaster 16 (4.13)</tag>PCM ou DSP
<tag>haut parleur du PC (toujours actif)</tag>Spkr
</descrip>

Les autres options se r�f�rent au CD ROM, probablement connect� � la carte
son, au volume principal, aux basses, � la balance et au niveau
d'enregistrement des diverses lignes d'entr�e. Vous pouvez sp�cifier quelles
lignes doivent �tre enregistr�es.

<sect1> Entr�e - sampler avec le p�riph�rique audio brut<p>

Vous pouvez enregistrer � partir de diff�rentes sources: un CD ROM, un
microphone connect� � <tt>mic</tt>, et tout ce que vous pouvez mettre dans
<tt>line in</tt>. Placez le mixer dans la position appropri�e. Jouez un son
et enregistrez le dans un fichier, en lisant le p�riph�rique audio brut, par
exmple:

<tscreen><verb>
# cdplay
# dd bs=8k count=5 &etago;dev/dsp >music.au
5+0 records in
5+0 records out
# cat music.au >/dev/dsp
</verb></tscreen>

enregistre et joue cinq secondes d'audio � partie du p�riph�rique d'entr�e.

<sect1> Le Port MIDI<p>

D�sol�, pas encore d'informations sur le port AMIDI !

<sect>Logiciels du pilote AWE<p>

<sect1><tt>sfxload</tt><p>

Vous pouvez charger des samples dans votre DRAM de la carte son avec l'outil
<tt/sfxload/. Notez que vous pouvez seulement charger un fichier de sample
par banque, avec la banque 0 par d�faut. Ainsi, apr�s <tt>sfxload
synthgs.sbk</tt>, les seuls samples de votre carte son sont les samples GS.
si vous d�sirez charger des banques Sound Font suppl�mentaires, vous devez
utiliser l'option <tt/-b/, par exemple:

<tscreen><verb>
# sfxload synthgs.sbk
# sfxload -b1 sample.sbk
# drvmidi sfx.mid
</verb></tscreen>

Vous pouvez charger une banque Sound Font par d�faut automatiquement en
installant le module. Ajoutez juste une ligne comme

<tscreen><verb>
post-install sound /usr/bin/sfxload synthgm.sbk
</verb></tscreen>

� votre fichier <tt>/etc/conf.modules</tt>.

Voyez la documentation pour plus de d�tails sur <tt/sfxload/.

<sect1><tt>drvmidi</tt><p>

Avec le programme <tt/drvmidi/, vous pouvez utiliser votre pilote AWE pour
jouer des fichiers MIDI. Sp�cifiez juste le nom de votre fichier midi apr�s
la commande:

<tscreen><verb>
# drvmidi waltz.mid
</verb></tscreen>

Voyez la documentation pour des d�tails suppl�mentaires sur <tt/drvmidi/.

<sect> Appendice<p>

<sect1> Informations Suppl�mentaires<label id="moreinfo"><p>

<descrip>
<tag>Le Linux Sound HOWTO</tag>

Auteur: Jeff Tranter, <tt>&lt;<htmlurl
url="mailto:jeff&lowbar;tranter@pobox.com"
name="jeff&lowbar;tranter@pobox.com">&gt;</tt>

Derni�re Version: v1.19, 23 Janvier 1998

Lisez bien le Sound HOWTO (disponible sur sunsite.unc.edu et ftp.lip6.fr
pour la france). Il contient un bon nombre de donn�es sur la compilation du
noyau avec le support sonore, et explique beaucoup de choses sur les
p�riph�riques audio, les applications, etc...

Je prends ce HOWTO comme une addition au Sound HOWTO.

<tag>Le Linux Sound Playing HOWTO</tag>

Auteur: Yoo C. Chung, <tt>&lt;<htmlurl url="mailto:wacko@laplace.snu.ac.kr"
name="wacko@laplace.snu.ac.kr">&gt;</tt>

Derni�re Version: v1.5b, 2 F�vrier 1998

Vous devriez �galement lire le Sound Playing HOWTO. Il vous dit tout sur les
diff�rents formats sonores et les applications pour les jouer.

<tag>La FAQ du pilote AWE</tag>

Auteur: Takashi Iwai <tt>&lt;<htmlurl
url="mailto:iwai@dragon.mm.t.u-tokyo.ac.jp"
name="iwai@dragon.mm.t.u-tokyo.ac.jp">&gt;</tt>

Source: <url
url="http://bahamut.mm.t.u-tokyo.ac.jp/~iwai/awedrv/awedrv-faq.html">

Si vous avez des probl�mes pour installer le pilote AWE ou utiliser le
synth�tiseur Wave Table ou votre carte Sound Blaster, regardez ici.

<tag>La FAQ ISA PnP</tag>

Auteur: Peter Fox <tt>&lt;<htmlurl url="mailto:fox@roestock.demon.co.uk"
name="fox@roestock.demon.co.uk">&gt;</tt>

Source: <url
url="http://www.roestock.demon.co.uk/isapnptools/isapnpfaq.html">

Si vous avez des probl�mes pour configurer votre carte ISA PnP, alors ceci
est votre livre de chevet. 

</descrip>

<sect1>Sources<label id="sources"><p>

<descrip>

<tag><tt>isapnptools</tt></tag>

Auteur: Peter Fox <tt>&lt;<htmlurl url="mailto:fox@roestock.demon.co.uk"
name="fox@roestock.demon.co.uk">&gt;</tt>

Derni�re Version: 1.15

Source: <url url="http://www.roestock.demon.co.uk/isapnptools/index.html">

Si vous envoyez des patchs, des reports d'erreurs ou des commentaires,
veuillez mettre 'isapnp' quelque part sur la ligne du sujet, et �crire �
isapnp@roestock.demon.co.uk.

<tag><tt>awedrv</tt></tag>

Auteur: Takashi Iwai <tt>&lt;<htmlurl
url="mailto:iwai@dragon.mm.t.u-tokyo.ac.jp"
name="iwai@dragon.mm.t.u-tokyo.ac.jp">&gt;</tt>

Derni�re Version: 0.4.2d

Source: <url url="http://bahamut.mm.t.u-tokyo.ac.jp/~iwai/awedrv/">

<tag>Noyau Linux (kernel)</tag>

Auteur: Linus Torvald et beaucoup d'autres

Derni�re Version: En ce moment, vous devriez utiliser la 2.0.35

Source: partout, l� o� vous pouvez obtenir Linux <tt>:)</tt>

<tag>OSS/Free</tag>
Auteur: Hannu Savolainen (Veuillez voir <url
url="http://www.4front-tech.com/usslite"> avant de m'�crire).

Derni�re Version: J'ai la 3.8s9

Source: avec les sources du noyau Linux ou <url
url="ftp://ftp.opensound.com/ossfree/">

Information:  <url url="http://www.4front-tech.com/usslite"> ou <url
url="http://personal.eunet.fi/pp/voxware" name="miroir europ�en">.

</descrip>

<sect1> Fichier d'exemple d' <tt>isapnp.conf</tt><label id="isapnpconf"><p>

Dans le fichier de configuration ISA PnP cr�� par pnpdump, les p�riph�riques
de vos cartes PnP apparaissent en sections. Dans le fichier suivant, une
carte a �t� d�tect�e (la carte son), avec quatre p�riph�riques logiques:

<itemize>
<item><tt>LD 0</tt>: P�riph�rique Audio
<item><tt>LD 1</tt>: Interface IDE
<item><tt>LD 2</tt>: Wave Table
<item><tt>LD 3</tt>: Port Joystick
</itemize>

J'ai laiss� <tt>LD 1</tt> non configur�, car je n'ai pas de CD ROM attach�
au port IDE de ma carte son. Si vous n'avez pas de port IDE sur votre SB,
alors <tt>LD 1</tt> sera le port joystick et <tt>LD 3</tt> n'appara�tra pas.

Veuillez vous r�f�rer � la section <ref id="isapnp" name="Avant de
Commencer"> pour plus d'informations (importantes!) sur ce fichier.

<code>
# $Id: Soundblaster-AWE.sgml,v 1.1.1.1 2003/01/03 02:40:54 traduc Exp $
# This is free software, see the sources for details.
# This software has NO WARRANTY, use at your OWN RISK
#
# For details of this file format, see isapnp.conf(5)
#
# For latest information on isapnp and pnpdump see:
# http://www.roestock.demon.co.uk/isapnptools/
#
# Compiler flags: -DREALTIME -DNEEDSETSCHEDULER

(READPORT 0x0203)
(ISOLATE)
(IDENTIFY *)

# Try the following if you get error messages like
# Error occurred executing request 'LD 2' on or around line...

#(VERIFYLD N)

# ANSI string -->Creative SB32 PnP<--

(CONFIGURE CTL0048/58128 (LD 0
#     ANSI string -->Audio<--

  (INT 0 (IRQ 5 (MODE +E)))
  (DMA 0 (CHANNEL 1))
  (DMA 1 (CHANNEL 5))
  (IO 0 (BASE 0x0220))
  (IO 1 (BASE 0x0330))
  (IO 2 (BASE 0x0388))

  (ACT Y)
))

(CONFIGURE CTL0048/58128 (LD 1
#     ANSI string -->IDE<--

# (INT 0 (IRQ 10 (MODE +E)))
# (IO 0 (BASE 0x0168))
# (IO 1 (BASE 0x036e))

# (ACT Y)
))

(CONFIGURE CTL0048/58128 (LD 2
#     ANSI string -->Wave Table<--

  (IO 0 (BASE 0x0620))
  (IO 1 (BASE 0x0A20))
  (IO 2 (BASE 0x0E20))

  (ACT Y)
))

(CONFIGURE CTL0048/58128 (LD 3
#     ANSI string -->Game<--

  (IO 0 (BASE 0x0200))
  (ACT Y)
))

# Returns all cards to the "Wait for Key" state
(WAITFORKEY)
</code>

</article>
