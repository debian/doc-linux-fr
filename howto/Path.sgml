<!doctype linuxdoc system>

<article>

<title>PATH Mini-HowTo

<author>Esa Turtiainen (<tt>etu@dna.fi</tt>)

<date>Version 0.4, 15 Novembre 1997

<abstract>
L'objectif de ce HOWTO est de traiter l'utilisation des variables
d'environnement sous Unix, et en particulier de la variable PATH.

Adaptation fran&ccedil;aise par Mathieu Lafon
(<tt>Mathieu.Lafon@insalien.org</tt>),
r&eacute;alis&eacute;e le 22 Octobre 1998.
</abstract>

<sect>Introduction<p>

Ce document aborde les astuces et les probl&egrave;mes relatifs aux
variables d'environnement
sous Unix/Linux, et plus sp&eacute;cialement &agrave; la variable PATH.
PATH est une liste de r&eacute;pertoires dans lesquels le
syst&egrave;me recherche les commandes &agrave; ex&eacute;cuter.
Ce document s'appuie sur la distribution Debian Linux 1.3.

Remarque: Ce document est en phase de d&eacute;veloppement (b&ecirc;ta).
Vous pouvez m'envoyer vos commentaires ou vos corrections.

Les commentaires sur la traduction sont &agrave; envoyer &agrave;
Mathieu Lafon (<tt>Mathieu.Lafon@insalien.org</tt>).

<sect>Droits d'auteur<p>

Cette documentation est libre, vous pouvez la redistribuer et/ou la
modifier selon les termes de la Licence Publique G&eacute;n&eacute;rale
GNU publi&eacute;e par la Free Software Foundation (version 2 ou bien
toute autre version ult&eacute;rieure choisie par vous).

Cette documentation est distribu&eacute;e car potentiellement utile, mais
SANS AUCUNE GARANTIE, ni explicite ni implicite, y compris
les garanties de commercialisation ou d'adaptation dans
un but sp&eacute;cifique. Reportez-vous &agrave; la Licence Publique
G&eacute;n&eacute;rale GNU pour plus de d&eacute;tails.

Vous pouvez obtenir une copie de la Licence Publique
G&eacute;n&eacute;rale GNU en
&eacute;crivant &agrave; la Free Software Foundation,
Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.

<sect>G&eacute;n&eacute;ralit&eacute;s<p>

Tous les processus sous Unix poss&egrave;dent un <em>environnement</em>.
C'est une liste de variables contenant un nom et une valeur,
les deux sous la forme de cha&icirc;nes
(pouvant contenir la majorit&eacute; des caract&egrave;res).
Tous les processus Unix poss&egrave;dent un processus parent,
celui qui les a cr&eacute;&eacute;s. Les processus
fils h&eacute;ritent de l'environnement de leurs parents.
Ils peuvent ensuite y faire quelques modifications avant de le passer
&agrave; leurs propres processus fils.

Une variable importante de l'environnement est la variable PATH qui
se pr&eacute;sente sous la forme d'une liste de r&eacute;pertoires
s&eacute;par&eacute;s par le caract&egrave;re deux-points (':').
Ces r&eacute;pertoires sont parcourus pour rechercher les commandes.
Si vous essayez de lancer la commande <tt>bidule</tt>, tous les
r&eacute;pertoires contenus dans PATH seront 
examin&eacute;s (dans l'ordre), &agrave; la recherche de
l'ex&eacute;cutable <tt>bidule</tt> 
(un fichier avec le bit ex&eacute;cutable positionn&eacute;).
Si un tel fichier est trouv&eacute;, il sera ex&eacute;cut&eacute;.

Dans ce document, j'utilise le terme de <em>commande</em> pour un
programme ex&eacute;cutable qui est appel&eacute; sans indication de
son chemin, utilisant donc le m&eacute;canisme de PATH.

Sous Linux, m&ecirc;me les appels de bas niveau pour lancer des processus
(la famille des <tt>exec</tt>) se basent sur la variable PATH pour trouver
les ex&eacute;cutables&nbsp;: vous pouvez donc
utiliser le m&eacute;canisme de PATH n'importe o&ugrave;, o&ugrave;
vous voulez ex&eacute;cuter une commande.
Si un appel de <tt>exec</tt> re&ccedil;oit le nom d'un fichier qui ne contient
pas de '/', il cherchera dans la variable d'environnement PATH.
M&ecirc;me si cette variable
n'existe pas, les r&eacute;pertoires <tt>/bin</tt> et <tt>/usr/bin</tt>
seront examin&eacute;s &agrave; la recherche de cette commande.

Pour cr&eacute;er ou modifier l'environnement, on utilisera <tt>export</tt>
avec <tt>sh</tt> ou <tt>setenv</tt> avec <tt>csh</tt>. Par exemple&nbsp;:
<verb>
 sh:
     export PATH=/usr/local/bin:/usr/bin:/bin:/usr/bin/X11:/usr/games:.

 csh:
     setenv PATH /usr/local/bin:/usr/bin:/bin:/usr/bin/X11:/usr/games:.
</verb>
Les programmes C peuvent utiliser la fonction <tt>setenv()</tt> pour
modifier l'environnement. Perl, quand &agrave; lui, conserve
l'environnement dans le tableau associatif &percnt;ENV, 
et vous pouvez donc modifier PATH avec&nbsp;:
<verb>
    $ENV{PATH}="/bin"
</verb>
La commande <tt>env</tt> est le moyen le plus facile pour conna&icirc;tre
les variables de l'environnement courant. Elle peut &eacute;galement
&ecirc;tre utilis&eacute;e pour les modifier.

Pour trouver plus d'information sur les commandes d'acc&egrave;s &agrave;
l'environnement, vous pouvez regarder les pages de manuel de
<tt>environ</tt>, <tt>execl</tt>,
<tt>setenv</tt>, le fichier info <tt>env</tt>, ainsi que la
documentation des shells.

Quand Linux d&eacute;marre, le premier processus a &ecirc;tre lanc&eacute;
est <tt>init</tt>. C'est un processus particulier car il n'a pas de parent.
De plus, il s'agit de l'anc&ecirc;tre de tous les autres processus.
Son environnement restera celui des autres processus
tant qu'ils ne le modifieront pas. La plupart le modifieront.

Le programme init lance un groupe de processus sp&eacute;cifi&eacute;s
dans le fichier <tt>/etc/inittab</tt>. Ces processus
travaillent dans un environnement directement h&eacute;rit&eacute;
de <tt>init</tt>. Ce sont d'habitude
des processus comme <tt>getty</tt>, le programme qui &eacute;crit 'login:'
&agrave; l'&eacute;cran. Si vous lancez une connexion PPP ici, vous devez
savoir que vous travaillez avec l'environnement
de init. L'initialisation du syst&egrave;me est souvent effectu&eacute;e
par un script lanc&eacute; &agrave; cet endroit.
Dans le cas de la Debian 1.3, il s'agit de <tt>/etc/init.d/rc</tt> qui est
charg&eacute; de lancer &agrave; son tour, les scripts d'initialisation.

Le syst&egrave;me comprend plusieurs d&eacute;mons qui peuvent ou non
utiliser l'environnement par d&eacute;faut. La plupart de ceux-ci sont
lanc&eacute; par les
scripts d'initialisation et poss&egrave;dent donc l'environnement de init.

Quand un utilisateur se connecte, l'environnement est modifi&eacute; par les
param&egrave;tres contenus dans les programmes, les scripts d'initialisation
communs &agrave; tous, et ceux sp&eacute;cifiques &agrave; l'utilisateur.
C'est assez compliqu&eacute; et la
situation n'est pas compl&egrave;tement satisfaisante. En effet,
le comportement est totalement diff&eacute;rent
suivant que l'utilisateur se connecte &agrave; partir
du terminal texte, de <tt>XDM</tt> ou du r&eacute;seau.

<sect>init<p>

<tt>init</tt> est le processus parent de tous les autres processus
du syst&egrave;me. Ceux-ci h&eacute;ritent de son
environnement et m&ecirc;me de sa variable PATH dans
le rare cas o&ugrave; aucun autre PATH n'est indiqu&eacute;.

Le PATH de init est fix&eacute; dans le code source du programme.
Il s'agit de&nbsp;:
<verb>
    /usr/local/sbin:/sbin:/bin:/usr/sbin:/usr/bin
</verb>
Notez qu'il ne contient pas le r&eacute;pertoire <tt>/usr/local/bin</tt>.

Tous les programmes qui sont lanc&eacute;s &agrave; partir de
<tt>/etc/inittab</tt> travaillent avec l'environnement de <tt>init</tt>,
et en particulier les scripts d'initialisation
contenus dans <tt>/etc/init.d</tt> (dans le cas de la Debian 1.3).

Tout ce qui est lanc&eacute; par les scripts d'initialisation
poss&egrave;de par d&eacute;faut l'environnement de <tt>init</tt>.
Par exemple, <tt>syslogd</tt>, <tt>kerneld</tt>, <tt>pppd</tt> (lorsqu'il
est lanc&eacute; au d&eacute;marrage), <tt>gpm</tt>, et ce qui est le
plus important, <tt>lpd</tt> et <tt>inetd</tt> poss&egrave;dent
l'environnement de <tt>init</tt> et ne le modifient pas.

Un certain nombre de programmes sont lanc&eacute;s par les scripts de
d&eacute;marrage mais avec une variable PATH explicitement fix&eacute;e
dans le script. Les exemples de tels programmes sont <tt>atd</tt>,
<tt>sendmail</tt>, <tt>apache</tt> et <tt>squid</tt>.

D'autre programmes, par exemple <tt>cron</tt>, sont lanc&eacute;s
par les scripts mais modifient totalement la variable PATH.

<sect>Connexion<p>

Sur un terminal texte, il y a le programme <tt>getty</tt> qui attend le
login de l'utilisateur. Il est charg&eacute; d'&eacute;crire 'login:'
et quelques autres messages.
Il travaille avec l'environnement de <tt>init</tt>. Lorsque l'utilisateur
commence &agrave; se connecter au moyen de <tt>getty</tt>, ce dernier
invoque le programme <tt>login</tt>.
Celui-ci installe alors l'environnement utilisateur et lance le shell.

Le programme login fixe le PATH comme d&eacute;fini dans le fichier
<tt>/usr/include/paths.h</tt>.

Il s'agit, pour les utilisateurs normaux (_PATH_DEFPATH) de&nbsp;:
<verb>
    /usr/local/bin:/usr/bin:/bin:.
</verb>
Et pour root (_PATH_DEFPATH_ROOT) de&nbsp;:
<verb>
    /sbin:/bin:/usr/sbin:/usr/bin
</verb>
Le PATH des utilisateurs normaux ne contient aucun r&eacute;pertoires
<tt>sbin</tt>. Cependant, il contient le r&eacute;pertoire
courant '.', qui est consid&eacute;r&eacute; comme dangereux pour
l'utilisateur root. M&ecirc;me <tt>/usr/local/bin</tt> n'est pas
disponible pour root.

Le PATH obtenu lors du login est souvent modifi&eacute; par
l'initialisation du shell.
Cependant, il est possible d'utiliser d'autres programmes que des shells 
dans <tt>/etc/passwd</tt>. Par exemple, j'utilise la ligne suivante
pour lancer PPP quand
je me connecte avec le nom d'utilisateur etu-ppp. Dans ce cas,
<tt>pppd</tt> poss&egrave;de exactement le PATH du login.
<verb>
    etu-ppp:viYabVlxPwzDl:1000:1000:Esa Turtiainen, PPP:/:/usr/sbin/pppd
</verb>

<sect>Shells<p>

Les processus utilisateurs sont souvent des processus fils du shell
indiqu&eacute; pour cet utilisateur dans le fichier <tt>/etc/passwd</tt>.
Les fichiers d'initialisation de ces shells modifient souvent la
variable PATH.

Lors de la connexion, le nom du shell est pr&eacute;c&eacute;d&eacute;
d'un '-'. Par exemple, dans le cas
de <tt>bash</tt>, on aura <tt>-bash</tt>. Cela indique au shell qu'il est
en pr&eacute;sence d'un login shell et qu'il doit dans
ce cas ex&eacute;cuter les fichiers d'initialisation
sp&eacute;cifiques &agrave; la connexion. Dans le cas contraire,
on aura une initialisation plus l&eacute;g&egrave;re.
De plus, le shell d&eacute;termine s'il est interactif ou non, c'est &agrave;
dire si les commandes viennent d'un terminal (tty) ou d'un fichier.
Cela modifie &eacute;galement l'importance de l'initialisation si bien
qu'un shell non interactif et qui n'est pas lanc&eacute; avec une
connexion effectue vraiment tr&egrave;s peu d'initialisation
(<tt>bash</tt> n'ex&eacute;cute aucune initialisation dans ce cas l&agrave;).

<sect1>bash<p>

Pour un login shell normal, <tt>bash</tt> parcourt le fichier
<tt>/etc/profile</tt>, commun &agrave; tous,
o&ugrave; les variables d'environnement, dont PATH, peuvent &ecirc;tre
fix&eacute;es pour les utilisateurs de <tt>bash</tt>. Cependant, ce fichier
n'est pas relu lorsque le syst&egrave;me
se trouve face &agrave; un shell non interactif.
Le cas le plus important est <tt>rsh</tt>, o&ugrave; la commande est
ex&eacute;cut&eacute;e sur la machine voisine&nbsp;: le fichier
<tt>/etc/profile</tt> n'est pas lanc&eacute; et le PATH provient du
d&eacute;mon de <tt>rsh</tt>.

<tt>bash</tt> accepte les arguments <tt>-login</tt> et <tt>-i</tt> qui
sont utilis&eacute;s pour obtenir
respectivement un login shell et/ou un shell interactif.

L'utilisateur peut red&eacute;finir les param&egrave;tres contenus dans
<tt>/etc/profile</tt> en cr&eacute;ant un fichier
<tt>&tilde;/.bash_profile</tt>, <tt>&tilde;/.bash_login</tt> ou
<tt>&tilde;/.profile</tt>. Il faut
noter que seul le premier fichier sera ex&eacute;cut&eacute; m&ecirc;me
si cela diff&egrave;re des habitudes de <tt>csh</tt>. En particulier,
<tt>&tilde;/.bash_login</tt> ne sera pas forcement
ex&eacute;cut&eacute; pour un login shell, car si
<tt>&tilde;/.bash_profile</tt> existe, ce dernier sera prioritaire.

Si <tt>bash</tt> est lanc&eacute; par <tt>sh</tt> (qui est un lien
symbolique sur <tt>bash</tt>),
il se comporte comme le Bourne shell original&nbsp;: il ne parcourt que les
fichiers <tt>/etc/profile</tt> et <tt>&tilde;/.profile</tt> et uniquement
dans le cas d'un login shell.

<sect1>tcsh<p>

Pour un login shell, <tt>tcsh</tt> ex&eacute;cute dans l'ordre
les fichiers suivants&nbsp;:

<itemize>
   <item><tt>/etc/csh.cshrc</tt>
   <item><tt>/etc/csh.login</tt>
   <item><tt>&tilde;/.tcshrc</tt>
   <item><tt>&tilde;/.cshrc</tt>    (si <tt>&tilde;/.tcshrc</tt> n'existe pas)
   <item><tt>&tilde;/.history</tt>
   <item><tt>&tilde;/.login</tt>
   <item><tt>&tilde;/.cshdirs</tt>
</itemize>

<bf>Attention.</bf> <tt>tcsh</tt> peut &ecirc;tre compil&eacute; pour
ex&eacute;cuter les scripts de
connexion (<tt>login</tt>) avant les scripts <tt>cshrc</tt>.

Les shells non interactifs n'ex&eacute;cutent que les scripts
<tt>*cshrc</tt>. Les scripts <tt>*login</tt> peuvent &ecirc;tre
utilis&eacute;s pour ne fixer le PATH que lors d'une connexion.

<sect>Modifier l'identit&eacute; de l'utilisateur<p>

<sect1>su<p>

La commande <tt>su</tt> sert &agrave; indiquer la nouvelle identit&eacute;
&agrave; utiliser (sous r&eacute;serve de conna&icirc;tre le mot de passe),
root &eacute;tant la valeur par d&eacute;faut.

Normalement, <tt>su</tt> lance un sous-shell avec la nouvelle
identit&eacute;. Avec l'argument '-' (plus r&eacute;cemment <tt>-l</tt> ou
<tt>--login</tt>), <tt>su</tt> lance le shell comme un login shell. Cependant,
il n'utilise pas le programme <tt>login</tt> pour cela mais encore un
autre PATH int&eacute;gr&eacute; au programme pour simuler le login
(termes employ&eacute;s dans le code source). Il s'agit de&nbsp;:

pour les utilisateurs normaux&nbsp;:
<verb>
    /usr/local/bin:/usr/bin:/bin:/usr/bin/X11:.
</verb>
pour l'utilisateur root&nbsp;:
<verb>
    /sbin:/bin:/usr/sbin:/usr/bin:/usr/bin/X11:/usr/local/sbin:/usr/local/bin
</verb>
<tt>su</tt> r&eacute;alise &eacute;galement quelques changements mineurs
dans l'environnement.

<sect1>sudo<p>

Il y a un groupe de commandes qui permettent une utilisation plus s&ucirc;r
des commandes du super utilisateur. Elles permettent un meilleur suivi (au
sens o&ugrave; l'on garde une trace de chaque ex&eacute;cution - NdT), des
restrictions sur les utilisateurs et utilisent des mots de passe individuels.
La plus utilis&eacute;e est s&ucirc;rement <tt>sudo</tt>.
<verb>
    $ sudo env
</verb>
Cette commande ex&eacute;cute <tt>env</tt> en tant que super utilisateur
(si <tt>sudo</tt> est configur&eacute; pour le permettre).

La commande <tt>sudo</tt> a encore une autre approche en ce qui concerne
la gestion du PATH. Elle modifie les r&eacute;pertoires o&ugrave; chercher
la commande &agrave; ex&eacute;cuter pour que le r&eacute;pertoire courant
soit toujours le dernier. Cependant, elle ne modifie 
pas la variable PATH, seulement quelques variables comme SUDO_USER.

<sect>Serveurs<p>

La majorit&eacute; des serveurs ne devrait pas lancer n'importe quelle
sorte de processus. Pour des raisons de s&eacute;curit&eacute;, leur
PATH doit donc &ecirc;tre minimal.

La plus grosse exception est l'ensemble des services qui autorisent
une connexion sur le syst&egrave;me &agrave; partir du r&eacute;seau.
Cette section d&eacute;crit comment se trouve l'environnement
dans ces cas pr&eacute;cis. En effet, une commande ex&eacute;cut&eacute;
&agrave; distance avec <tt>rsh</tt> aura
un PATH diff&eacute;rent d'une commande ex&eacute;cut&eacute; avec
<tt>ssh</tt>. De la m&ecirc;me fa&ccedil;on,
une connexion &agrave; l'aide de <tt>rlogin</tt>, <tt>telnet</tt>
ou <tt>ssh</tt> est diff&eacute;rente.

<sect1>inetd<p>

La plupart des serveurs ne poss&egrave;dent pas de processus charg&eacute;
d'attendre en permanence l'arriv&eacute;e d'une requ&ecirc;te. Ce travail
est laiss&eacute; &agrave; un super serveur (Internet super server),
appel&eacute; <tt>inetd</tt>. Le programme <tt>inetd</tt> est &agrave;
l'&eacute;coute permanente du r&eacute;seau et lance le serveur
appropri&eacute; en fonction du port sur lequel arrive la requ&ecirc;te.
Son comportement est d&eacute;fini dans le fichier
<tt>/etc/inetd.conf</tt>.

<tt>inetd</tt> est d&eacute;marr&eacute; par les scripts de d&eacute;marrage
du syst&egrave;me. Il h&eacute;rite donc du PATH de <tt>init</tt>.
Il ne le modifie pas et tous les serveurs lanc&eacute;s par <tt>inetd</tt>
poss&egrave;dent donc le PATH de <tt>init</tt>. Un exemple de tel serveur est
<tt>imapd</tt>, le serveur du protocole IMAP.

D'autre exemples de processus lanc&eacute;s par <tt>inetd</tt> sont
<tt>telnetd</tt>, <tt>rlogind</tt>, <tt>talkd</tt>, <tt>ftp</tt>,
<tt>popd</tt>, certains serveurs http, etc...

Souvent, l'utilisation de <tt>inetd</tt> est compliqu&eacute;e par
l'utilisation du programme tcpd, charg&eacute; de lancer le v&eacute;ritable
serveur. C'est un programme qui effectue quelques v&eacute;rifications du
point de vue s&eacute;curit&eacute; avant de lancer le v&eacute;ritable
serveur. Il ne touche pas au PATH (information non v&eacute;rifi&eacute;e).

<sect1>rsh<p>

Le d&eacute;mon de <tt>rsh</tt> utilise le PATH d&eacute;fini par
_PATH_DEFPATH (<tt>/usr/include/path.h</tt>),
c'est &agrave; dire, le m&ecirc;me que celui utilis&eacute; par le
programme <tt>login</tt> pour connecter les utilisateurs normaux.
L'utilisateur root obtiendra le m&ecirc;me PATH que les autres.

En r&eacute;alit&eacute;, <tt>rshd</tt> ex&eacute;cute la commande
d&eacute;sir&eacute;e en se servant de la commande suivante&nbsp;:
<verb>
    shell -c ligne_de_commande
</verb>
O&ugrave; <tt>shell</tt> n'est pas un login shell. Il est
pr&eacute;f&eacute;rable que tous les shells mentionn&eacute;s dans
<tt>/etc/passwd</tt> prennent en compte l'option <tt>-c</tt> pour pouvoir
leur envoyer ce genre de ligne de commande.

<sect1>rlogin<p>

<tt>rlogin</tt> invoque login pour effectuer la proc&eacute;dure de connexion.
Si vous vous connectez avec <tt>rlogin</tt>, vous aurez le m&ecirc;me PATH
qu'avec <tt>login</tt>. La plupart des autres fa&ccedil;ons de se connecter
&agrave; un ordinateur sous Linux n'utilisent
pas <tt>login</tt>. Notez la diff&eacute;rence avec <tt>rsh</tt>.

La commande de <tt>login</tt> utilis&eacute;e est de la forme&nbsp;:
<verb>
    login -p -h nom_de_l_hote nom_d_utilisateur
</verb>
L'option <tt>-p</tt> conserve l'environnement &agrave; l'exception des
variables HOME, PATH, SHELL, TERM, MAIL et LOGNAME. L'option <tt>-h</tt>
indique le nom de l'ordinateur sur lequel doit se faire la connexion.

<sect1>telnet<p>

Le programme <tt>telnet</tt> est similaire &agrave; <tt>rlogin</tt>&nbsp;:
il utilise le programme <tt>login</tt> et la ligne de commande
utilis&eacute;e est de la m&ecirc;me forme.

<sect1>ssh<p>

<tt>ssh</tt> poss&egrave;de sa propre variable PATH, &agrave; laquelle il
ajoute le r&eacute;pertoire o&ugrave; se trouve <tt>ssh</tt>. Cela implique
souvent que le r&eacute;pertoire <tt>/usr/bin</tt> se retrouve en double&nbsp;:
<verb>
    /usr/local/bin:/usr/bin:/bin:.:/usr/bin
</verb>
La variable PATH ne contient pas <tt>/usr/bin/X11</tt> et le shell
invoqu&eacute; par <tt>ssh</tt> n'est pas un login shell. Ainsi, la commande
<verb>
    ssh hote_distant xterm
</verb>
ne marchera pas et rien de ce qui est contenu dans <tt>/etc/profile</tt> ou
<tt>/etc/csh.cshrc</tt> ne pourra changer cela.
Vous devrez toujours utiliser des chemins absolus, par exemple
<tt>/usr/bin/X11/xterm</tt>.

<tt>ssh</tt> cherche des variables d'environnement de la forme
VARIABLE=VALEUR dans le fichier <tt>/etc/environment</tt>.
Malheureusement, cela provoque des probl&egrave;mes avec XFree86.

<sect>XFree86<p>

<sect1>XDM<p>

XDM est la mani&egrave;re la plus courante pour se connecter &agrave;
partir d'un terminal graphique. M&ecirc;me s'il ressemble &agrave;
<tt>login</tt>, il se comporte, en interne, d'une mani&egrave;re
totalement diff&eacute;rente.

Les fichiers de configuration se trouvent dans le r&eacute;pertoire
<tt>/etc/X11/xdm</tt> et sont ex&eacute;cut&eacute;s pendant les
diff&eacute;rentes &eacute;tapes de la connexion. Xstartup
(et Xstartup_0 pour l'&eacute;cran 0) contient les commandes &agrave;
ex&eacute;cuter juste apr&egrave;s la connexion. Ces commandes sont
lanc&eacute;s en tant que root.

Le PATH qui est utilis&eacute; pour les utilisateurs se trouve dans
<tt>/etc/X11/xdm/xdm-config</tt>.
Ce sont les lignes&nbsp;:
<verb>
DisplayManager*userPath: /usr/local/bin:/usr/bin:/bin:/usr/bin/X11:/usr/games
DisplayManager*systemPath: /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/bin/X11
</verb>
C'est le PATH par d&eacute;faut pour les utilisateurs normaux
(<tt>userPath</tt>), et pour l'utilisateur
root (<tt>systemPath</tt>) respectivement. Il est tr&egrave;s important
que le r&eacute;pertoire <tt>/usr/bin/X11</tt> soit accessible
pour les utilisateurs sous X. En effet, si un utilisateur se connecte
&agrave; une autre machine pour lancer une application X, il faut qu'il
aie <tt>/usr/bin/X11</tt> dans son PATH
car la machine h&ocirc;te ne saura pas qu'il dispose d'un terminal X.

Apr&egrave;s Xstartup, XDM lance <tt>/etc/X11/Xsession</tt> en tant
qu'utilisateur final. La configuration locale est contenue dans le fichier
<tt>/etc/environment</tt> qui est parcouru, s'il existe, par Xsession.
Xsession &eacute;tant ex&eacute;cut&eacute; par <tt>/bin/sh</tt>,
<tt>/etc/environment</tt> doit donc &ecirc;tre un script <tt>sh</tt>.
Cela interf&egrave;re avec <tt>ssh</tt> qui suppose que
<tt>/etc/environment</tt> est un fichier qui ne
contient que des lignes de la forme VARIABLE=VALEUR.

<sect1>xterm -ls<p>

Par d&eacute;faut, le PATH de toutes les commandes lanc&eacute;s &agrave;
partir des menus du gestionnaire de fen&ecirc;tre est celui
h&eacute;rit&eacute; de XDM. Pour en utiliser
un autre, il faut le d&eacute;finir explicitement. Pour lancer un terminal X
avec un PATH "normal", on doit utiliser des options sp&eacute;ciales.
Pour <tt>xterm</tt>, l'option <tt>-ls</tt> (login shell) doit &ecirc;tre
utilis&eacute; pour obtenir un login shell avec le PATH d&eacute;fini dans
les fichiers d'initialisation du shell en question.

<sect1>Menus et boutons du gestionnaire de fen&ecirc;tre<p>

Le gestionnaire de fen&ecirc;tre h&eacute;rite de l'environnement de XDM.
Tous les programmes lanc&eacute;s par lui h&eacute;ritent donc de cet
environnement.

L'environnement du shell de l'utilisateur n'affecte pas les programmes
qui sont lanc&eacute;s par les menus ou les boutons. Par exemple, si un
programme est lanc&eacute; par un <tt>xterm</tt> (<tt>xterm -ls</tt>),
il poss&egrave;de l'environnement par d&eacute;faut du login shell,
par contre s'il est lanc&eacute; par un menu, il aura l'environnement
du gestionnaire de fen&ecirc;tre.

<sect>Commandes "&agrave; retardement" cron et at<p>

<sect1>cron<p>

C'est le programme <tt>cron</tt> qui ex&eacute;cute p&eacute;riodiquement
les commandes sp&eacute;cifi&eacute;es
dans <tt>/etc/crontab</tt> et dans les crontabs des utilisateurs. La
Debian&nbsp;1.3 poss&egrave;de en plus un m&eacute;canisme pour
ex&eacute;cuter les commandes de <tt>/etc/cron.daily</tt>,
<tt>/etc/cron.weekly</tt> et <tt>/etc/cron.monthly</tt>, respectivement
tous les jours, toutes les semaines et tous les mois.

<tt>cron</tt> est lanc&eacute; par les scripts de d&eacute;marrage mais
il change son PATH en une valeur assez &eacute;trange&nbsp;:
<verb>
       /usr/bin:/binn:/sbin:/bin:/usr/sbin:/usr/bin
</verb>
<bf>IL S'AGIT SUREMENT D'UN BOGUE DANS CRON.</bf> Il s'agit en fait du
PATH de init (<tt>/usr/bin:/bin</tt>) qui est copi&eacute; ici, mais sans
le 0 terminal (cha&icirc;ne en convention C - NdT)! Ce bogue n'existe pas
sur tous les syst&egrave;mes.

Dans la crontab, on peut d&eacute;finir un PATH sp&eacute;cifique pour
l'ex&eacute;cution des commandes.
Pour la Debian&nbsp;1.3, il s'agit de&nbsp;:
<verb>
    PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
</verb>
De cette fa&ccedil;on, le PATH de <tt>crond</tt> n'est jamais
utilis&eacute; dans les programmes utilisateurs. Tous les scripts de
<tt>/etc/cron.*</tt> obtiennent par d&eacute;faut le PATH
de la crontab. Celui ci est utilis&eacute; m&ecirc;me si le programme n'est
pas ex&eacute;cut&eacute; en tant que root.

<sect1>at<p>

La commande <tt>at</tt> est utilis&eacute;e pour lancer un programme
&agrave; une heure fix&eacute;e.

Le programme <tt>atd</tt> est lanc&eacute; avec le PATH de <tt>init</tt>.
Cependant, les programmes sont toujours lanc&eacute;s avec l'environnement
utilisateur gr&acirc;ce &agrave; <tt>sh</tt>.
Les sp&eacute;cificit&eacute;s de <tt>sh</tt> s'appliquent donc ici.
Reportez vous au chapitre sur <tt>bash</tt>.

<sect>Quelques exemples<p>

<sect1>magicfilter<p>

<tt>magicfilter</tt> est un outil standard permettant de manipuler les
fichiers &agrave; destination de l'imprimante. Il analyse le type du
fichier &agrave; imprimer et lance un filtre appropri&eacute; pour l'imprimer
de la meilleure fa&ccedil;on. Les scripts utilis&eacute;s pour filtrer
sont lanc&eacute;s par <tt>lpd</tt>, lui m&ecirc;me lanc&eacute; par le
script <tt>/etc/init.d/lpd</tt> lanc&eacute; par <tt>init</tt>. Le PATH
est donc identique &agrave; celui de <tt>init</tt> et ne contient donc pas
<tt>/usr/bin/X11</tt>.

Si vous voulez envoyer des fichier PDF (Portable Data Format) &agrave;
<tt>magicfilter</tt>, vous pouvez utiliser <tt>/usr/bin/X11/xpdf</tt>.
Mais vous ne devez pas oublier d'indiquer le chemin absolu. Sinon,
<tt>magicfilter</tt> ne trouvera pas <tt>xpdf</tt>. La plupart
des programmes utilis&eacute;s avec <tt>magicfilter</tt>, ne
n&eacute;cessitent pas forcement un chemin explicite car ils se trouvent
souvent dans <tt>/bin</tt> ou <tt>/usr/bin</tt>.

<sect1>Impression &agrave; partir d'applications X<p>

Au cas o&ugrave; vous utilisez la variable d'environnement PRINTER pour
s&eacute;lectionner l'imprimante &agrave; utiliser, vous devez savoir que
dans certains cas, certaines applications X risquent de ne pas la
conna&icirc;tre.

Vous vous souvenez s&ucirc;rement que si la session X a &eacute;t&eacute;
lanc&eacute; par XDM, le gestionnaire de fen&ecirc;tre ne se sert pas de
vos scripts de login. Toutes les applications X que vous lancez &agrave;
partir d'un <tt>xterm</tt> poss&egrave;dent donc la variable PRINTER.
Par contre, la m&ecirc;me application lanc&eacute;e &agrave; partir
d'un menu ou d'un bouton ne poss&eacute;dera pas cette variable.

Parfois, la variable PRINTER peut &ecirc;tre h&eacute;rit&eacute;e &agrave;
un niveau encore plus bas. Par exemple, une application auxiliaire
de Netscape pourra conna&icirc;tre votre
variable PRINTER m&ecirc;me si Netscape ne la conna&icirc;t pas.

<sect>Questions de s&eacute;curit&eacute;<p>

Le m&eacute;canisme de PATH est souvent un gros probl&egrave;me du point
de vue s&eacute;curit&eacute;. Utiliser une erreur dans la d&eacute;finition
du PATH est une mani&egrave;re fr&eacute;quente
de pirater un syst&egrave;me. Il est facile pour un pirate de fabriquer
des chevaux de Troie, s'il arrive &agrave; forcer root ou un autre
utilisateur &agrave; ex&eacute;cuter ses propres programmes.

Une erreur fr&eacute;quente par le pass&eacute; (?) &eacute;tait de laisser
le r&eacute;pertoire courant '.' dans le PATH de l'utilisateur root.
Un pirate malveillant peut alors cr&eacute;er
son propre programme <tt>'ls'</tt> dans son r&eacute;pertoire.
Ensuite, si root fait&nbsp;:
<verb>
    # cd &tilde;pirate
    # ls
</verb>
il ex&eacute;cute le programme du pirate...

De la m&ecirc;me fa&ccedil;on, cela s'applique &agrave; tous les programmes
ex&eacute;cut&eacute;s par root. Aucun important d&eacute;mon ne devrait
ex&eacute;cuter quoi que ce soit qui puisse
&ecirc;tre modifi&eacute; par un utilisateur. Dans certains syst&egrave;mes, 
<tt>/usr/local/bin</tt> peut contenir des programmes jug&eacute;s moins
s&ucirc;r, mais le r&eacute;pertoire est retir&eacute; du PATH de root.
Cependant, si on sait qu'un d&eacute;mon ex&eacute;cute <tt>bidule</tt>
avec 'PATH=/usr/local/bin:...', il est possible
de tromper le d&eacute;mon en lui faisant ex&eacute;cuter
<tt>/usr/local/bin/bidule</tt> &agrave; la place
de <tt>/bin/bidule</tt>. Dans ce cas, n'importe qui pouvant &eacute;crire dans
<tt>/usr/local/bin</tt> peut s&ucirc;rement pirater le syst&egrave;me.

Il est donc tr&egrave;s important de faire attention &agrave; l'ordre dans
lequel les r&eacute;pertoires sont plac&eacute;s dans le PATH.
Si <tt>/usr/local/bin</tt> se trouve avant <tt>/bin</tt>,
il y a un risque. Alors que s'il se trouve apr&egrave;s, il est impossible
de lancer la commande modifi&eacute;e <tt>/usr/local/bin/bidule</tt>
&agrave; la place de <tt>/bin/bidule</tt>.

Sous Linux, vous devez vous souvenir que la recherche dans le PATH est
fa&icirc;te dans tous les m&eacute;canismes d'appels du syst&egrave;me
d'exploitation. N'importe o&ugrave;, o&ugrave; le chemin d'un
ex&eacute;cutable est donn&eacute;, vous pouvez utiliser le nom de
la commande seul qui sera alors cherch&eacute;e au moins dans <tt>/bin</tt>
et <tt>/usr/bin</tt>, et vraisemblablement dans beaucoup d'autres endroits.

<sect>Comment r&eacute;soudre les probl&egrave;mes&nbsp;?<p>

La commande la plus simple pour avoir acc&egrave;s &agrave; l'environnement
est <tt>/usr/bin/env</tt>.

Il est egalement possible d'utiliser le r&eacute;pertoire <tt>/proc</tt>
pour trouver le PATH de n'importe quel programme. Vous devez d'abord
conna&icirc;tre le num&eacute;ro de processus
du programme. Utilisez la commande <tt>ps</tt> pour l'obtenir. Par exemple,
si <tt>xterm</tt> est le processus num&eacute;ro 1088, vous pouvez voir
son environnement avec&nbsp;:
<verb>
    # more /proc/1088/environ
</verb>
Cela ne marche pas avec des processus comme <tt>xdm</tt>. Pour
acc&eacute;der &agrave; l'environnement d'un processus du syst&egrave;me
ou d'un autre utilisateur, vous devez &ecirc;tre root.

Pour deboguer Netscape, vous pouvez cr&eacute;er le script suivant&nbsp;:
<verb>
    $ cat > /tmp/test
    #!/bin/sh
    /usr/bin/env > /tmp/env
    ^d
    $ chmod +x /tmp/test
</verb>
Ensuite, arrangez vous pour que votre programme soit appel&eacute;
&agrave; la place d'une application auxiliaire, par exemple RealAudio
(<tt>audio/x-pn-realaudio</tt>). Lorsque vous essayerez d'acc&eacute;der
&agrave; un lien RealAudio (quelque chose comme
<tt>http://www.realaudio.com/showcase</tt>), Netscape lancera
votre programme factice et sauvera l'environnement dans <tt>/tmp/env</tt>.

<sect>M&eacute;thodes pour que tous les utilisateurs aient le m&ecirc;me PATH<p>

Le r&eacute;glage le plus important est &agrave; faire dans les fichiers
commun d'initialisation des logins shells&nbsp;: <tt>/etc/csh.login</tt>
pour <tt>tcsh</tt> et <tt>/etc/profile</tt> pour <tt>bash</tt>.

Ceux qui n'obtiennent pas le bon PATH &agrave; partir de ces fichiers
sont&nbsp;: <tt>rsh</tt>, <tt>ssh</tt>, les &eacute;l&eacute;ments des
menus du gestionnaire de fen&ecirc;tres sous X ne lan&ccedil;ant pas
explicitement de login shell, les commandes lanc&eacute;s &agrave; partir
de <tt>inittab</tt>, les travaux de <tt>cron</tt>, les travaux des
d&eacute;mons comme <tt>magicfilter</tt> (lanc&eacute; par <tt>lprd</tt>),
les scripts CGI (WWW), etc...

Si le PATH est fix&eacute; dans <tt>/etc/csh.cshrc</tt>, il sera
utilis&eacute; si <tt>rsh</tt> ou <tt>ssh</tt> lance des commandes sur
une machine distante o&ugrave; l'utilisateur utilise
<tt>tcsh/csh</tt>. Par contre, il n'est pas possible de r&eacute;gler
le PATH si l'utilisateur utilise <tt>bash/sh</tt>. Voici une m&eacute;thode
pour ne garder le PATH que dans un seul fichier, par exemple
<tt>/etc/environnement-commun</tt>, dans lequel on &eacute;crit&nbsp;:
<verb>
  ${EXPORT}PATH${EQ}/bin:/usr/bin:/sbin:/usr/sbin:/usr/bin/X11:/usr/local/bin:/usr/games:.
</verb>
On peut ensuite l'utiliser &agrave; partir de <tt>/etc/csh.login</tt>
(pour <tt>tcsh</tt>
et <tt>csh</tt>)
<verb>
  set EQ=" " set EXPORT="setenv "; source /etc/environnement-commun
</verb>
A partir de <tt>/etc/profile</tt> (pour <tt>bash</tt>, mais pas pour le
vrai <tt>sh</tt>)
<verb>
  EQ='=' EXPORT="export " . /etc/environnement-commun
</verb>
Et &agrave; partir de <tt>/etc/environment</tt> (pour XDM)
<verb>
  EQ='=' EXPORT="export " . /etc/environnement-commun
</verb>

Cette m&eacute;thode marchera la plupart du temps, sauf que <tt>ssh</tt>
se plaindra des lignes contenues dans <tt>/etc/environment</tt> (ainsi
que des variables EQ et EXPORT). De plus, <tt>rsh</tt> n'aura toujours
pas le bon PATH s'il passe par <tt>bash</tt>.

<sect>Remerciements<p>

Une des raisons pour commencer l'&eacute;criture de ce document a
&eacute;t&eacute; la grosse frustration de Ari Mujunen. Juha Takala
m'a donn&eacute; de pr&eacute;cieux commentaires.

</article>

