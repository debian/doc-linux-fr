<!doctype linuxdoc system>

<!-- LinuxDoc file was created by LyX 1.0 (C) 1995-1999 by <gaucher> Sun Oct 10 16:44:41 1999
 -->
 <article>
 <title>
News Leafsite mini-HOWTO
 </title>
 <author>
Florian Kuehnert, sutok@gmx.de
 </author>
 <date>
v0.3, 4 January 1998
 </date>
 <abstract>
Version fran�aise par Philippe Gaucher (gaucher@easynet.fr). Ce HOWTO vous
 aidera � mettre en place un petit site de news � l'aide du logiciel libre Leafnode.
 Pour toute question, suggestion ou tout commentaire, �crire � Florian Kuehnert (sutok@gmx.de).
 Vous pouvez aussi me signaler tous les probl�mes que vous trouvez dans ce document.
 (C) 1998 by Florian Kuehnert.
 </abstract>
 <sect>
Pourquoi utiliser Leafnode ?
 <p>
Si vous voulez lire les news hors connexion sur votre ordinateur local,
 vous avez g�n�ralement � installer un logiciel de serveur de news comme INN
 ou CNews. Et vous avez �galement besoin d'une connexion nntp ou uucp pour alimenter
 ce serveur. Du fait que ces logiciels contiennent bien plus de fonctionnalit�s
 que n�cessaire, les choses iront mieux en installant Leafnode.
 </p>
 <p>
Leafnode est vraiment plus simple � utiliser et est aussi tr�s petit, mais
 il a quelques inconv�nients&nbsp;: Leafnode est lent et il perd des messages d�s
 qu'un probl�me se pr�sente. C'est pourquoi vous ne devriez pas l'utiliser pour
 un gros serveur de news. Il est quand m�me appropri� pour des utilisateurs
 priv�s qui ne veulent pas passer trop de temps � configurer INN.
 </p>
 <sect>
O� trouver Leafnode ?
 <p>
Leanode est disponible � <htmlurl url="ftp://ftp.troll.no/pub/freebies/" name="ftp://ftp.troll.no/pub/freebies/"> et a �t� d�velopp� par un employ� de Troll Tech
 AS, Arnt Gulbrandsen. La version la plus r�cente est 1.4 et dans certaines
 distributions (comme par exemple la Debian), Leafnode est inclus. Quoi qu'il
 en soit, v�rifiez que vous avez au moins la version 1.4 car des bugs assez
 graves avaient �t� corrig�s.
 </p>
 <sect>
Comment l'installer ?
 <p>
Un fichier d'aide � l'installation est inclus dans le package. Mais faisons-la
 ensemble&nbsp;:-). Si une version sup�rieure ou �gale � 1.4 de Leafnode est fournie
 avec votre distribution, il serait plus sage d'utiliser les binaires pr�compil�s et
 de sauter les �tapes 1 � 4.
 </p>
 <p>
1) V�rifiez qu'il n'y a pas d'autre serveur de news qui tourne sur votre
 machine. Quand vous tapez 
 </p>
 <p>
 <verb>
&dollar; telnet localhost nntp
 </verb>
</p> <p>
vous devez voir un message d'erreur. Si vous obtenez une connexion, retournez
 au prompt de votre shell, puis d�sinstallez INN, CNews ou tout logiciel du
 m�me type et commentez la ligne nntp dans /etc/inetd.conf.
 </p>
 <p>
2) V�rifiez qu'il y a un utilisateur appel� "news"&nbsp;: � regarder dans /etc/password.
 S'il n'existe pas, cr�ez-en un ou bien tapez
 </p>
 <p>
 <verb>
&dollar; adduser news
 </verb>
</p> <p>
ou bien utilisez tout outil fourni avec votre distribution.
 </p>
 <p>
3) D�compressez la source&nbsp;: 
 </p>
 <p>
 <verb>
&dollar; tar xfz leafnode-1.4.tar.gz
 </verb>
</p> <p>
et placez-vous dans le r�pertoire source
 </p>
 <p>
 <verb>
&dollar; cd leafnode-1.4
 </verb>
</p> <p>
4) Compilez le programme et installez-le&nbsp;:
 </p>
 <p>
 <verb>
&dollar; make; make install
 </verb>
</p> <p>
5) �ditez /usr/lib/leafnode/config (il peut �tre � une autre place comme
 par exemple /etc/leafnode quand vous utilisez une version pr�-compil�e fournie
 avec votre distribution Linux). La ligne "server=" doit indiquer le serveur de
 news de votre FAI.
 </p>
 <p>
6) �ditez /etc/nntpserver. Il doit contenir le nom d'h�te local (localhost
 ou tout nom donn� � votre ordinateur&nbsp;; la commande hostname devrait vous aider
 pour cela). Si dans un des fichiers de d�marrage comme /etc/profile ou &tilde;/.bash_profile,
 la variable d'environnement correspondante est d�finie, vous devriez l'ajuster
 �galement au nom de votre ordinateur.
 </p>
 <p>
7) �ditez le fichier /etc/inetd.conf&nbsp;: soyez certain qu'il n'y a pas de
 ligne commen�ant par "nntp". S'il y avait une telle ligne, commentez-la en mettant
 un "&num;" au d�but. Puis ajoutez la ligne suivante&nbsp;:
 </p>
 <p>
 <verb>
nntp stream tcp nowait news /usr/sbin/tcpd /usr/local/sbin/leafnode
 </verb>
</p> <p>
Quand quelqu'un (par exemple vous&nbsp;:-)) se connecte � votre ordinateur par
 le port NNTP, Leafnode est lanc� comme processus serveur.
 </p>
 <p>
8) Connectez-vous � votre FAI et lancez le programme "fetch" comme utilisateur root
 ou news. La premi�re fois que fetch est lanc�, il t�l�chargera une liste des groupes
 de nouvelles disponibles chez votre FAI. Cela peut prendre un certain temps&nbsp;; �a
 d�pend de la vitesse de votre connexion et du nombre de groupes de nouvelles que
 votre FAI a dans son fichier "active" &lsqb;NdT&nbsp;: c'est le nom du fichier&rsqb;.
 </p>
 <p>
9) D�marrez votre lecteur de news pr�f�r� (slrn, (r)tin et knews ne sont pas de
 mauvais points de d�part) et inscrivez-vous � tous les groupes de nouvelles que vous
 lisez. Veillez � ne pas seulement vous inscrire � ces groupes de
 nouvelles, mais aussi � entrer dedans, m�me s'ils sont vides.
 </p>
 <p>
10) Relancez "fetch" encore une fois pour t�l�charger tous les articles de tous
 les groupes de nouvelles qui vous int�ressent.
 </p>
 <sect>
Comment maintenir leafnode ?
 <p>
Maintenant vous avez obtenu un serveur de news fonctionnel et qui tourne
 mais vous avez encore quelques petites choses � faire. Vous pouvez �diter le
 fichier /usr/lib/leafnode/config pour configurer les dates d'expiration de
 vos groupes de nouvelles. Ce nombre indique quand les vieux messages doivent �tre effac�s.
 Le temps standard de 20 jours est bien souvent trop long si vous lisez des
 groupes avec un trafic cons�quent&nbsp;; 4 jours ou une semaine sont dans la plupart
 des cas un bon r�glage pour votre syst�me. Vous pouvez changer la valeur pour
 tous les groupes ("expire=n" pour mettre tous les groupes � n jours), et vous
 pouvez dire � Leafnode de changer la date pour un groupe particulier en �crivant
 groupexpire foo.bar n pour r�gler le d�lai d'expiration du groupe foo.bar
 � n jours. 
 </p>
 <p>
Ce r�glage seul ne fera pas supprimer les messages par Leafnode. Un programme
 s�par� s'en charge&nbsp;: texpire. Il peut �tre lanc� par cron ou en ligne de commande.
 Si votre ordinateur est allum� en permanence, vous avez la possibilit� d'ajouter
 la ligne suivante au fichier crontab de news (pour l'�diter, en tant qu'utilisateur
 news tapez "crontab -e", ou bien tapez sous root "crontab -u news -e")&nbsp;: 
 </p>
 <p>
 <verb>
0 19 * * * /usr/local/sbin/texpire
 </verb>
</p> <p>
Cette ligne a pour cons�quence que le daemon cron d�clenchera texpire chaque
 jour � 19:00. Reportez-vous � la page man de crontab pour d'autres r�glages.
 Si votre ordinateur n'est pas constamment allum�, vous avez la possibilit�
 de d�marrer texpire de temps en temps manuellement, quand vous remarquez que
 fetch devient plus lent. Cela fonctionne aussi bien que le lancement par cron.
 </p>
 <sect>
Comment �a marche ?
 <p>
Leafnode est un "vrai" serveur NNTP, ce qui signifie que vous pouvez �galement
 vous y connecter � partir d'un autre ordinateur (via Internet ou un r�seau local,
 etc.). � chaque fois que vous entrez dans un groupe de nouvelles avec votre lecteur
 de news, votre lecteur envoie l'information � Leafnode et lui demande. Si le
 groupe n'existe pas, Leafnode cr�e un fichier vide nomm� comme le groupe dans 
 /var/spool/news/interesting.groups. Quand vous actionnez fetch la fois suivante, les messages
 du groupe sont t�l�charg�s. Si un groupe n'a pas �t� lu depuis un certain
 temps, Leafnode arr�tera de le t�l�charger et supprimera son nom dans /var/spool/news/interesting.groups.
 Si vous vous abonnez � un groupe � fort trafic par accident, vous avez aussi
 la possibilit� de supprimer ce fichier manuellement pour que vous n'ayez pas
 � t�l�charger tous ses articles pour toute la semaine suivante. 
 </p>
 <p>
Une semaine n'est pas assez pour vous&nbsp;? Vous voulez partir en vacances et
 continuer � recevoir les news&nbsp;? Malheureusement il n'y a pas d'options dans
 Leafnode pour changer cela. Mais vous pouvez �diter le fichier leafnode.h et
 tout recompiler. Les constantes &num;defined sont TIMEOUT_LONG et TIMEOUT_SHORT&nbsp;:
 augmentez juste le temps d'une seconde. Une autre solution, plus simple, est
 de d�finir une t�che cron chaque nuit qui fait &dquot;touch /var/spool/news/interesting.groups/*&dquot;.
 
 </p>
 <p>
Si vous voulez obtenir une liste de tous les groupes de nouvelles disponibles sur
 le serveur de news qui vous alimente (par exemple quand vous voulez lire un
 nouveau groupe), il suffit de supprimer le fichier /var/spool/news/active.read.
 La commande fetch le recr�era la fois suivante et t�l�chargera � nouveau la liste des
 groupes. Fetch relira �galement la liste des groupes de temps en temps donc
 a priori vous n'avez pas � le faire manuellement.
 </p>
 <sect>
Quel lecteur de news devrais-je utiliser ?
 <p>
Il n'y a pas "un" lecteur de news d�di� � Linux, de la m�me fa�on qu'il n'y
 a pas "un" �diteur. Mon lecteur de news pr�f�r� est emacs en mode gnus qui est
 le lecteur le plus configurable de Linux. Beaucoup de gens utilisent slrn et
 tin en terminal, beaucoup de gens utilisent knews sous X. Il y a aussi trn,
 nn et encore beaucoup d'autres, si bien que vous pouvez essayer celui que vous
 voulez. Le seul lecteur que vous ne devriez pas utiliser est Netscape&nbsp;: il
 est gros, sans fonctionnalit�, et de temps en temps, il cr�e des probl�mes
 dans les news. Cela dit, c'est � vous de d�cider.
 </p>
 <p>
N'importe comment, knews n'est pas une mauvaise id�e pour une premi�re
 exp�rience car il est tr�s agr�able � utiliser et facile � comprendre.
 </p>
 <sect>
O� trouver plus d'informations&nbsp;?
 <p>
De la documentation est fournie dans le package Leafnode (lisez les fichiers
 INSTALL et README, les sources sont �galement tr�s int�ressantes). Si vous
 voulez en savoir plus sur les "gros" serveurs de news plus "professionnels", lisez
 la FAQ de INN (elle est fournie dans le package INN). Pour avoir plus d'informations
 sur votre lecteur de nouvelles, tapez "man le_nom_de_votre_lecteur" ou regardez ce
 que vous pouvez trouver dans /usr/doc.
 </p>
 <p>
Si vous avez des questions concernant le syst�me de news, demandez dans
 un groupe de nouvelles appropri� (regardez dans la hi�rarchie news.software.ALL). &lsqb;NdT&nbsp;:
 en France, essayez le forum mod�r� fr.usenet.logiciels&rsqb;.  ## fr.* n'est en aucun cas une 
                                                                ## hi�rarchie *_fran�aise_*, ni 
                                                                ## localis�e sur le territoire
                                                                ## fran�ais.
 </p>
 <p>
Pour toute question, correction ou tout commentaire concernant ce HOWTO, �crivez-moi 
(sutok@gmx.de).
 </p>
 <sect>
Remerciements
 <p>
J'aimerais remercier Michael Schulz (michaels@home.on-luebeck.de) pour
 son aide concernant les probl�mes de langage et Cornelius Krasel (krasel@wpxx02.toxi.uni-wuerzburg.de)
 pour ses remarques finales.
 </p>


 </article>
