<!doctype linuxdoc system>

<article>

<title>Linux Bridge+Firewall Mini-HOWTO version 1.2.0</title>
<author>Peter Breuer (<htmlurl url="mailto:ptb@it.uc3m.es" name="ptb@it.uc3m.es">)&nl;
Adaptation fran�aise par Etienne BERNARD (<htmlurl url="mailto:eb@via.ecp.fr" name="eb@via.ecp.fr">)</author>
<date>19 D�cembre 1997</date>

<toc>

<sect><heading>Introduction<LABEL ID="Introduction">

<p>
Vous devriez lire l'original <url url="ftp://sunsite.unc.edu/pub/Linux/docs/HOWTO/mini/Bridge" name="Bridging mini-HOWTO"> (NdT&nbsp;: ou <url url="ftp://ftp.lip6.fr/pub/linux/french/docs/mini/Bridge" name="en version fran�aise">) par Chris Cole pour une vision diff�rente sur le sujet. L'adresse email de Chris Cole est <htmlurl url="mailto:chris@polymer.uakron.edu" name="chris@polymer.uakron.edu">. La version de cet HOWTO, � partir duquel ce document est construit est la version 1.03, dat� du 23 ao�t 1996.

<sect><heading>Quoi, et pourquoi (et comment&nbsp;?)<LABEL ID="What and Why (and How?)">

<p>

<sect1><heading>Quoi<LABEL ID="What">

<p>
Un pont est un �l�ment qui connecte intelligement des brins gr�ce � deux cartes ethernet.
Un <em/firewall/ est un �l�ment isolant intelligent.

<sect1><heading>Pourquoi<LABEL ID="Why">

<p>Si vous avez de nombreux ordinateur, vous pouvez d�sirer installer un pont&nbsp;:

<enum>
<item><LABEL ID="bridge1">pour �conomiser le prix d'un nouveau <em/hub/ lorsqu'il se trouve que vous avez une carte ethernet libre&nbsp;;

<item><LABEL ID="bridge2">pour �viter d'avoir � apprendre l'<em/IP-forwarding/ et d'autres trucs alors que vous <em/avez/ deux cartes dans votre ordinateur&nbsp;;

<item><LABEL ID="bridge3">pour �viter des travaux de maintenance pour d'�ventuels changements futurs&nbsp;!
</enum>

<p>
Le terme ``nombreux ordinateurs'' peut m�me repr�senter seulement trois ordinateurs, si ceux-ci font du routage ou du pontage ou qu'ils changent de place dans la pi�ce de temps en temps&nbsp;! Vous pouvez m�me vouloir un pont pour vous amuser � trouver � quoi cela sert. Je voulais un pont pour la raison <REF ID="bridge2" NAME="2">.

<p>
Si vous �tes int�ress� par le point <REF ID="bridge1" NAME="1">, vous �tes peu dans votre cas. Lisez le <url url="ftp://sunsite.unc.edu/pub/Linux/docs/HOWTO/NET-2-HOWTO" name="NET-2-HOWTO"> et le <url url="ftp://sunsite.unc.edu/pub/Linux/docs/HOWTO/Serial-HOWTO" name="Serial-HOWTO"> pour de meilleurs astuces.

<p>Vous d�sirez un <em/firewall/ si&nbsp;:

<enum>
<item>vous essayez de prot�ger votre r�seau des acc�s ext�rieur, ou<LABEL ID="firewall1">
<item>vous d�sirez interdire l'acc�s au monde ext�rieur aux machines de votre r�seau.<LABEL ID="firewall2">
</enum>

<p>
Bizarrement, j'avais besoin du point <REF ID="firewall2" NAME="2"> ici aussi. La politique de mon universit� pour le moment est de ne pas jouer le r�le de fournisseur d'acc�s � Internet pour les <em/undergraduates/.

<sect1><heading>Comment<LABEL ID="How?">

<p>
J'ai commenc� par du pontage entre deux cartes r�seau sur une machine jouant le r�le de <em/firewall/, et j'ai fini par lancer le <em/firewall/ sans avoir coup� le pont. Cela a l'air de fonctionner, et c'est beaucoup plus flexible que chaque configuration isol�e. Je peux arr�ter le  <em/firewall/ et continuer � faire fonctionner le pont ou arr�ter le pont lorsque je veux �tre plus prudent.

<p>Je suppose que la partie ``pont'' du noyau se trouve juste au-dessus de la couche physique et que la partie <em/firewall/ se trouve dans une couche r�seau sup�rieure, afin que les parties de pontage et de <em/firewalling/ agissent en fait comme si elles �taient connect�es en ``s�rie'' et non pas en ``parall�le'' (aie&nbsp;!), selon le sch�ma suivant&nbsp;:

<tscreen><verb>
-&gt; Pont-entrant -&gt; Firewall-entrant -&gt; Noyau -&gt; Firewall-sortant -&gt; Pont-sortant -&gt;
</verb></tscreen>

<p>Il n'y a pas d'autre fa�on d'expliquer comment une machine peut �tre en m�me temps ``conducteur'' et ``isolant''. Il existe quelques embuches, mais j'en parlerai plus tard. Sch�matiquement, vous devez router les paquets que vous  voulez filtrer. De toute fa�on, cela a l'air de fonctionner parfaitement pour moi, et voici comment...

<sect><heading>PONT<LABEL ID="BRIDGING">

<sect1><heading>Logiciel<LABEL ID="Software">
<p>
R�cup�rez l'<url url="ftp://shadow.cabi.net/pub/Linux/BRCFG.tgz" name="utilitaire de configuration du pont"> depuis la page personnelle d'Alan Cox. C'est la m�me r�f�rence que dans le document de Chris. Je n'ai pas compris que c'�tait un URL <em/ftp/ en non un URL <em/http/...

<sect1><heading>Lecture pr�liminaires<LABEL ID="Prior Reading">

<p>Lisez le <url url="ftp://sunsite.unc.edu/pub/Linux/docs/HOWTO/mini/Multiple-Ethernet" name="Multiple Ethernet HOWTO"> pour obtenir des conseils pour faire reconna�tre et pour configurer plus d'une carte r�seau.

<p>Vous pourrez trouver encore plus de d�tails sur le type de commandes magiques � passer au <em/prompt/ se trouvent dans le <url url="ftp://sunsite.unc.edu/pub/Linux/docs/HOWTO/BootPrompt-HOWTO" name="Boot Prompt HOWTO">.

<p>Pour compl�ter vos lectures, lisez le <url url="ftp://sunsite.unc.edu/pub/Linux/docs/HOWTO/NET-2-HOWTO" name="NET-2 HOWTO">. C'est un document plut�t long, et vous devrez y piocher les d�tails qui vous int�ressent.

<sect1><heading>Configuration de lancement<LABEL ID="Boot configuration">

<p>Les lectures pr�c�dentes vont vous indiquer ce dont vous avez besoin pour pr�parer le noyau � reconna�tre un deuxi�me p�riph�rique ethernet lors du d�marrage, en ajoutant la ligne suivante dans votre fichier <tt>/etc/lilo.conf</tt>, et en relan�ant <tt/lilo/&nbsp;:

<tscreen><verb>append = &quot;ether=0,0,eth1&quot; </verb></tscreen>

<p>Notez le &quot;eth1&quot;. &quot;eth0&quot; repr�sente la premi�re carte. &quot;eth1&quot; est la seconde carte. Vous pouvez �galement ajouter les param�tres de d�marrage � la ligne de commande que <tt/lilo/ vous offre. Pour trois cartes&nbsp;:

<tscreen><verb>linux ether=0,0,eth1 ether=0,0,eth2 </verb></tscreen>

<p>J'utilise <tt/loadlin/ pour lancer mon noyau Linux depuis DOS&nbsp;: 

<tscreen><verb>loadlin.exe c:\vmlinuz root=/dev/hda3 ro ether=0,0,eth1 ether=0,0,eth2 </verb></tscreen>

<p>Notez que cette astuce oblige le noyau � d�tecter les cartes au d�marrage. La d�tection ne sera pas faite si vous chargez les gestionnaires de p�riph�rique ethernet en <bf/module/ (par s�curit�, puisque l'ordre de d�tection ne peut �tre d�termin�), donc si vous utilisez des modules, vous aurez � ajouter l'IRQ appropri�e et le param�tre de port pour le gestionnaire de p�riph�rique dans votre fichier <tt>/etc/conf.modules</tt>. Dans mon cas, j'ai les lignes&nbsp;:

<tscreen> <verb>
alias eth0 3c509
alias eth1 de620
options 3c509 irq=5 io=0x210
options de620 irq=7 bnc=1
</verb> </tscreen>

<p>Vous pouvez savoir si vous utilisez les modules en utilisant ``ps -aux'' pour voir si <tt/kerneld/ est lanc�, et en v�rifiant qu'il y a des fichiers <tt/.o/ dans un sous-r�pertoire du r�pertoire <tt>/lib/modules</tt>. Utilisez le nom de r�pertoire que vous donne la commande <tt/uname -r/. Si vous avez un <tt/kerneld/ lanc� et/ou vous avez un fichier <tt/foo.o/, �ditez <tt>/etc/conf.modules</tt> et lisez avec soin la page de manuel de <tt/depmod/.

<p>Notez �galement que jusque r�cemment (noyau 2.0.25), le <em/driver/ pour la carte <bf/3c509/ ne pouvait pas �tre utilis� pour plus d'une carte s'il �tait utilis� en module. J'ai vu un <em/patch/ quelque part pour corriger cette limitation. Il devrait �tre inclus dans le noyau � l'heure o� vous lisez ces lignes.

<sect1><heading>Configuration du noyau<LABEL ID="Kernel configuration">

<p>Recompilez le noyau avec le <em/bridging/&nbsp;:

<tscreen><verb>CONFIG_BRIDGE=y </verb></tscreen>

<p>J'ai �galement compil� mon noyau avec le <em/firewalling/, l'<em/IP-forwarding/ et l'<em/IP-masquerading/. C'est seulement si vous d�sirez utiliser le <em/firewalling/ �galement...

<tscreen><verb>CONFIG_FIREWALL=y           
CONFIG_NET_ALIAS=y          
CONFIG_INET=y               
CONFIG_IP_FORWARD=y         
CONFIG_IP_MULTICAST=y       
CONFIG_IP_FIREWALL=y        
CONFIG_IP_FIREWALL_VERBOSE=y
CONFIG_IP_MASQUERADE=y</verb></tscreen>

<p>Vous aurez besoin en plus de la configuration r�seau standard&nbsp;:

<tscreen><verb>CONFIG_NET=y</verb></tscreen>

<p>et je ne pense pas que vous deviez vous pr�ocupper des autres options r�seau. Les options que je n'ai pas compil� dans le noyau sont s�lectionn�es en tant que modules afin que je puisse les ajouter �ventuellement plus tard.

<p>Installez le nouveau noyau, relancez <tt/lilo/ et red�marrez sur le nouveau noyau. Rien ne devrait avoir chang� pour l'instant&nbsp;!

<sect1><heading>Adresses r�seau<LABEL ID="Network addresses">

<p>Chris dit qu'un pont ne doit pas avoir d'adresse IP mais ce n'est pas la configuration qui est pr�sent� ici.

<p>Vous allez utiliser la machine pour vous connecter au r�seau donc vous avez besoin d'une adresse et vous devez vous assurer que le device <em/loopback/ configur� normalement afin que vos logiciels puisse communiquer avec ce � quoi ils s'attendent. Si <em/loopback/ est d�sactiv�, le <em/r�solveur de noms/ ou d'autres services ne fonctionneront pas. Voyez le NET-2-HOWTO, mais votre configuration standard devrait d�j� avoir fait cela&nbsp;:

<tscreen><verb>ifconfig lo 127.0.0.1
route add -net 127.0.0.0</verb></tscreen>

<p>Vous allez devoir donner des adresses � vos cartes r�seau. J'ai chang� le fichier <tt>/etc/rc.d/rc.inet1</tt> de ma slackware (3.x) pour configurer deux cartes et vous devrez juste regarder votre fichier de configuration du r�seau et doubler ou tripler le nombre d'instructions s'y trouvant. Supposons que vous ayez d�j� une adresse �

<tscreen><verb>192.168.2.100</verb></tscreen>

<p>(cette adresse fait partie des adresses r�serv�es pour des r�seaux priv�s, mais ne faites pas attention, cela ne cassera rien si vous utilisez cette adresse par erreur) alors vous avez probablement une ligne ressemblant �

<tscreen><verb>ifconfig eth0 192.168.2.100 netmask 255.255.255.0 metric 1</verb></tscreen>

<p>dans votre fichier de configuration. La premi�re chose que vous allez probablement vouloir faire est couper l'espace des adresses atteintes par cette carte en deux afin de pouvoir �ventuellement faire un pont ou filtrer entre les deux moiti�s. Ajoutez donc une ligne qui r�duit le masque de sous-r�seau pour adresser un plus petit nombre de machines&nbsp;:

<tscreen><verb>ifconfig eth0 netmask 255.255.255.128</verb></tscreen>

<p>Essayez cette configuration. Cela restreint la carte � l'espace des adresses entre .0 et .127.

<p>A pr�sent, vous pouvez configurer votre deuxi�me carte dans la deuxi�me moiti� de l'espace des adresses locales. Assurez vous que personne n'utilise l'adresse que vous allez prendre. Pour des raisons de sym�trie, j'utiliserai ici 228=128+100. N'importe quelle adresse conviendra, � condition qu'elle ne se trouve pas dans le masque de l'autre carte. �vitez les adresses sp�ciales comme .0, .1, .128, etc... � moins que vous sachiez ce que vous faites.

<tscreen><verb>ifconfig eth1 192.168.2.228 netmask 255.255.255.128 metric 1</verb></tscreen>

<p>Cela restreint la deuxi�me carte aux adresses entre .128 et .255. 

<sect1><heading>Routage r�seau<LABEL ID="Network routing">

<p>C'est ici que les d�fauts de l'utilisation simultann�e du pont et du firewall&nbsp;: vous ne pouvez pas filtrer des paquets qui ne sont pas rout�s. Pas de route, pas de firewall. Cette r�gle est v�rifi�e en tout cas dans les version 2.0.30 ou suivantes du noyau. Les filtres du firewall sont �troitement li�s au code source de l'IP-Forwarding.

<p>Cela ne signifie pas que vous ne pouvez pas utiliser le pont. Vous pouvez installer un pont entre deux cartes et filtrer � partir d'une troisi�me. Vous pouvez n'avoir que deux cartes et les faire filtrer une adresse IP externe, comme celle d'un routeur proche, � condition que le routeur reli� � une seule carte.

<p>En d'autres termes, puisque je veux utiliser la machine comme firewall, je dois contr�ler avec pr�cision la destination physique de certains paquets.

<p>J'ai le petit r�seau de machines sur un <em/hub/ connect� � <tt/eth0/, je configure donc un r�seau de ce c�t�&nbsp;:

<tscreen><verb>route add -net 192.168.2.128 netmask 255.255.255.128 dev eth0</verb></tscreen>

<p>Remplacez le 128 par un 0 pour un r�seau de classe C entier. Ici, je ne le fais pas puisque j'ai juste divis� en deux l'espace d'adressage. Le &quot;dev eth0&quot; n'est pas n�cessaire ici, puisque l'adresse de la carte fait partie de ce r�seau, mais il peut �tre n�cessaire de l'�crire chez vous. On pourrait d�sirer plus d'une carte prenant ce sous r�seau en charge (127 machines sur un segment, bravo&nbsp;!) mais ces cartes utiliseraient le m�me masque de sous-r�seau et seraient consid�r�es comme une seule par la partie routage du noyau.

<p>Sur l'autre carte, j'ai une ligne qui passe directement � travers un gros routeur, auquel je fais confiance.

<verb>                                             client 129
         __                                        |    __ 
client 1   \    .0                    .128         |   /   net 1
client 2 --- Hub - eth0 - Kernel - eth1 - Hub - Router --- net 2
client 3 __/       .100            .228         .2 |   \__ net 3
                                                   |
                                             client 254</verb>

<p>J'utilise une route fixe (c'est-�-dire &quot;statique&quot;) depuis la carte vers ce routeur, puisque sinon il ferait partie du masque de sous-r�seau de la premi�re carte et le noyau se tromperait sur la mani�re d'envoyer les paquets au routeur. Je veux filtrer ces paquets et c'est une raison de plus de les router explicitement.

<tscreen><verb>route add 192.168.2.2 dev eth1</verb></tscreen>

<p>Je n'en ai pas besoin, puisque je n'ai pas d'autres machines dans cette moiti� de l'espace d'adressage, mais je d�clare un r�seau sur la seconde carte. La s�paration de mes interfaces r�seau en deux groupes gr�ce au routage me permettra �ventuellement de faire du filtrage tr�s pr�cis, mais vous pouvez tr�s bien vous en sortir avec beaucoup moins de routage que cela.

<tscreen><verb>route add -net 192.168.2.128 netmask 255.255.255.128 dev eth1</verb></tscreen>

<p>J'ai �galement besoin d'envoyez tous les paquets non-locaux au monde et je dis donc au noyau de les envoyer au gros routeur&nbsp;:

<tscreen><verb>route add default gw 192.168.2.2</verb></tscreen>

<sect1><heading>configuration de la carte<LABEL ID="Card configuration">

<p>Nous avions auparavant une configuration standard pour le r�seau, mais comme nous faisons du <em/bridging/, nous devons �couter sur chaque carte les paquets qui ne nous sont pas destin�s. Ceci doit aller dans le fichier de configuration r�seau&nbsp;:

<tscreen><verb>ifconfig promisc eth0
ifconfig promisc eth1</verb></tscreen>

<p>La page de manuel indique d'utiliser <tt/allmulti=promisc/, mais cela ne fonctionnait pas pour moi.

<sect1><heading>Routage additionnel<LABEL ID="Additional routing">

<p>J'ai remarqu� une chose&nbsp;: j'ai d� passer la seconde carte dans un mode lui permettant aux questions du gros routeur � propos des machines que je cache sur mon r�seau local.

<tscreen><verb>ifconfig arp eth1</verb></tscreen>

<p>Pour faire bonne mesure, j'ai effectu� cette op�ration pour l'autre carte aussi.

<tscreen><verb>ifconfig arp eth0</verb></tscreen>

<sect1><heading>Configuration du pont<LABEL ID="Bridge Configuration">

<p>Ajoutez la mise en route du pont dans votre fichier de configuration&nbsp;:

<tscreen><verb>brcfg -enable</verb></tscreen>

<p>La configuration du pont mettra en route certains ports. Vous pouvez exp�rimenter l'allumage et l'extinction des ports un � la fois&nbsp;:

<tscreen><verb>brcfg -port 0 -disable/-enable
brcfg -port 1 -disable/-enable </verb></tscreen>

<p>Vous pouvez obtenir un rapport sur l'�tat courant avec&nbsp;:

<tscreen><verb>brcfg</verb></tscreen>

<p>sans aucun param�tres. Vous pourrez voir que le pont �coute, apprend, et effectue le <em/forwarding/. (Je ne comprends pas pourquoi le code r�p�te la m�me adresse mat�rielle pour mes deux cartes, mais peu importe... le HOWTO de Chris affirme que c'est correct).

<sect1><heading>Essais<LABEL ID="Try it out">

<p>Si le r�seau est encore en fonction, essayez votre script de configuration en vrai en arr�tant les deux cartes et en l'ex�cutant&nbsp;:

<tscreen><verb>ifconfig eth0 down
ifconfig eth1 down
/etc/rc.d/rc.inet1</verb></tscreen>

<p>Avec un peu de chance, les divers syst�mes tel <bf/nfs/, <bf/ypbind/, etc... ne s'en rendront pas compte. <it/N'essayez pas ceci si vous n'�tes pas derri�re le clavier&nbsp!/

<p>Si vous d�sirez �tre plus prudent que cela, vous devrez arr�ter le plus de d�mons possible, et d�monter les r�pertoires NFS. Le pire qu'il puisse vous arriver est d'avoir � rebooter en mode <em/single-user/ (le param�tre &quot;<bf>single</bf>&quot; de <tt/lilo/ ou <tt/loadlin/), et de restaurer les fichiers � leur valeur d'avant les modifications.

<sect1><heading>V�rifications

<p>V�rifiez qu'il existe un trafic diff�rent sur chaque interface&nbsp;:

<tscreen>
<verb>tcpdump -i eth0</verb> (dans une fen�tre)
<verb>tcpdump -i eth1</verb> (dans une autre fen�tre)
</tscreen>

<p>Vous devriez �tre habitu� � l'utilisation de <bf/tcpdump/ pour trouver des �v�nements qui ne devraient pas se passer, ou qui existent mais ne devraient pas.

<p>Par exemple, recherchez les paquets qui ont travers� le pont vers la seconde carte depuis le r�seau interne. Ici, je cherche les paquets venant de la machine avec l'adresse .22&nbsp;:

<tscreen><verb>
tcpdump -i eth1 -e host 192.168.2.22
</verb></tscreen>

<p>A pr�sent, envoyez un ping depuis l'h�te en .22 vers le routeur. Vous devriez voir le paquet affich� par tcpdump.

<p>A pr�sent, vous devriez avoir un pont, et qui poss�de �galement deux adresses r�seau. V�rifiez que vous pouvez les <tt/ping/er depuis l'ext�rieur de votre r�seau local et depuis l'int�rieur, que vous pouvez utiliser <tt/telnet/ et <tt/ftp/ depuis et vers l'int�rieur du r�seau.

<sect><heading>FIREWALLING<LABEL ID="FIREWALLING">

<sect1><heading>Logiciel et lectures<LABEL ID="Software and reading">

<p>Vous devriez lire le <url url="ftp://sunsite.unc.edu/pub/Linux/docs/HOWTO/Firewall-HOWTO" name="Firewall-HOWTO">.

<p>Il vous indiquera o� trouver <tt/ipfwadm/ si vous ne l'avez pas d�j�. Vous pouvez �galement r�cup�rer d'autres outils, mais seulement <tt/ipfwadm/ m'a �t� utile. C'est pratique et de bas niveau&nbsp;! Vous pouvez voir exactement ce qu'il fait.

<sect1><heading>V�rifications pr�liminaires<LABEL ID="Preliminary checks">

<p>Vous avez compil� l'IP-forwarding et le masquerading dans le noyau, et vous allez v�rifier que le <em/firewall/ est dans son �tat par d�faut (il accepte) gr�ce �&nbsp;:

<tscreen><verb>ipfwadm -I -l
ipfwadm -O -l
ipfwadm -F -l</verb></tscreen>

<p>Ce qui, dans l'ordre, &quot;affiche les r�gles affectant la partie ..&quot;entrante ou sortante ou qui fait suivre (<em/masquerading/) &quot;.. du <em/firewall/&quot;. L'option &quot;-l&quot; signifie &quot;lister&quot;.

<p>Si vous avez compil� l'IP accounting �galement&nbsp;:

<tscreen><verb>ipfwadm -A -l</verb></tscreen>

<p>Vous devriez constater qu'aucune r�gle n'est d�finir et que l'action par d�faut est d'accepter tous les paquets. Vous pouvez retourner � cet �tat � tout moment, avec&nbsp;:

<tscreen><verb>ipfwadm -I -f
ipfwadm -O -f
ipfwadm -F -f</verb></tscreen>

<p>L'option &quot;-f&quot; signifie &quot;flush&quot; (en fran�ais, &quot;vider&quot;). Vous pourriez en avoir besoin.


<sect1><heading>R�gle par d�faut<LABEL ID="Default rule">

<p>Je veux interdire l'acc�s au monde entier depuis mon r�seau interne, et rien d'autre, donc je vais donner comme derni�re r�gle (comme r�gle par d�faut) une r�gle indiquant d'ignorer les paquets venant du r�seau interne et dirig�s vers l'ext�rieur. Je place toutes les r�gles (dans cet ordre) dans le fichier <tt>/etc/rc.d/rc.firewall</tt> que j'execute depuis <tt>/etc/rc.d/rc.local</tt> au d�marrage.

<tscreen><verb>ipfwadm -I -a reject -S 192.168.2.0/255.255.255.128 -D 0.0.0.0/0.0.0.0</verb></tscreen>

<p>Le &quot;-S&quot; repr�sente l'adresse source/masque de sous-r�seau. L'option &quot;-D&quot; est pour l'adresse destination/masque de sous-r�seau.

<p>Ce format n'a plus le vent en poupe. <tt/ipfwadm/ est intelligent et conna�t des abr�viations courantes. V�rifier les pages de manuel.

<p>Il peut �tre plus pratique de placer certaines ou toutes les r�gles sur la partie sortante du firewall en utilise &quot;-O&quot; au lieu de &quot;-I&quot;, mais je supposerai que les r�gles sont con�ues pour �tre utilis�es sur la moiti� entrante.

<sect1><heading>Acc�s par adresse<LABEL ID="Holes per address">

<p>Avant la r�gle par d�faut, je dois placer des r�gles qui servent d'exceptions pour cette interdiction g�n�rale des services ext�rieurs aux clients int�rieurs.

<p>Je veux traiter les adresses des machines derri�re le firewall de mani�re sp�ciale. Je veux emp�cher aux gens de se loguer sur la machine qui sert de firewall � moins qu'elles aient une permission sp�ciale, mais une fois connect�es, elles devraient �tre capables de parler au monde ext�rieur.

<tscreen><verb>ipfwadm -I -i accept -S 192.168.2.100/255.255.255.255 \
 -D 0.0.0.0/0.0.0.0</verb></tscreen>

<p>Je veux �galement que les clients internes soient capables de parler � la machine faisant <em/firewall/. Peut-�tre pourront-elles la persuader de les laisser sortir&nbsp;!

<tscreen><verb>ipfwadm -I -i accept -S 192.168.2.0/255.255.255.128 \
 -D 192.168.2.100/255.255.255.255</verb></tscreen>

<p>V�rifiez que vous pouvez joindre les machines � l'int�rieur du firewall depuis l'ext�rieur par <tt/telnet/, mais que vous ne pouvez pas sortir. Vous pouvez faire le premier pas, mais les clients ne peuvent pas vous envoyer de messages. Essayez �galement <tt/rlogin/ et <tt/ping/ avec <tt/tcpdump/ lanc� sur une carte ou l'autre. Vous devriez �tre capable de comprendre ce que vous lirez.

<sect1><heading>Acc�s par protocole<LABEL ID="Holes per protocol">

<p>J'assouplis les r�gles protocole par protocole. Je veux que les <tt/ping/s depuis l'ext�rieur vers l'int�rieur obtiennent une r�ponse, par exemple, donc j'ai ajout� la r�gle&nbsp;:

<tscreen><verb>ipfwadm -I -i accept -P icmp -S 192.168.2.0/255.255.255.128 \
 -D 0.0.0.0/0.0.0.0</verb></tscreen>

<p>L'option &quot;<TT>-P icmp</TT>&quot; correspond au protocole tout entier.

<p>Avant que j'installe un <em/proxy ftp/, j'autorise �galement les appels ftp sortants en rel�chant les restrictions sur un port donn�. Ceci vise les ports 20, 21 et 115 sur les machines externes&nbsp;:

<tscreen><verb>ipfwadm -I -i accept -P tcp -S 192.168.2.0/255.255.255.128 \
 -D 0.0.0.0/0.0.0.0 20 21 115</verb></tscreen>

<p>Je n'ai pas pu faire fonctionner <tt/sendmail/ entre les clients locaux sans serveur de noms. Au lieu d'installer un serveur de nom directement sur le <em/firewall/, j'ai juste autoris� les requ�tes TCP de r�solution de nom visant le plus proche serveur de nom, et j'ai plac� son adresse dans le fichier <tt>/etc/resolv.conf</tt> des clients. (&quot;<TT>nameserver 123.456.789.31</TT>&quot; sur une ligne s�par�e).

<tscreen><verb>ipfwadm -I -i accept -P tcp -S 192.168.2.0/255.255.255.128 \
 -D 123.456.789.31/255.255.255.255 54</verb></tscreen>

<p>Vous pouvez trouver quel num�ro de port et protocole requiert un service gr�ce � <tt/tcpdump/. Utilisez ce service avec un <tt/ftp/ ou un <tt/telnet/ ou autre vers ou depuis une machine interne, et observez-le sur les ports d'entr�e et de sortie du <em/firewall/ avec <tt/tcpdump/&nbsp;:

<tscreen><verb>tcpdump -i eth1 -e host client04</verb></tscreen>

<p>par exemple. Le fichier <tt>/etc/services/</tt> est une importante source d'indices. Pour laisser ENTRER le <tt/telnet/ et le <tt/ftp/ depuis l'ext�rieur, vous devez autoriser un client local � envoyer des donn�es vers l'ext�rieur sur un port donn�. Je comprends pourquoi ceci est n�cessaire pour le <tt/ftp/ (c'est le serveur qui �tablit la connexion de donn�es), mais je me demande pourquoi <tt/telnet/ en a �galement besoin.

<tscreen><verb>ipfwadm -I -i accept -P tcp -S 192.168.2.0/255.255.255.128 ftp telnet \
 -D 0.0.0.0/0.0.0.0</verb></tscreen>

<p>Il existe un probl�me particulier avec certains d�mons qui cherchent le nom d'h�te de la machine <em/firewall/ afin de d�cider quelle est leur adresse r�seau. J'ai eu des probl�mes avec <tt/rpc.yppasswdd/. Il insiste sur des informations <em/broadcast/ qui indiquent qu'il se trouve en dehors du <em/firewall/ (sur la seconde carte). Cela signifie que les clients � l'int�rieur ne peuvent pas le contacter.

<p>Au lieu de lancer l'IP aliasing ou de changer le code source du d�mon, j'ai traduit le nom vers l'adresse de la carte du c�t� int�rieur sur les clients, dans leur fichier <tt>/etc/hosts</tt>.

<sect1><heading>V�rifications

<p>Vous voudrez tester que vous pouvez toujours utiliser <tt/telnet/, <tt/rlogin/ et <tt/ping/ depuis l'ext�rieur. Depuis l'int�rieur, vous devriez �tre capable d'utiliser <tt/ping/ vers la sortie. Vous devriez �galement �tre capables d'utiliser le <tt/telnet/ vers la machine <em/firewall/ depuis l'int�rieur, et cette derni�re manipulation devrait vous permettre d'acc�der au reste.

<p>Et voil�. A pr�sent, vous voulez probablement apprendre <bf/rpc//<bf/Y/ellow <bf/P/ages et leur interaction avec le fichier de mots de passe. Le r�seau derri�re le firewall devrait vouloir emp�cher aux utilisateurs non privil�gi�s de se logguer sur le <em/firewall/, et donc de sortir. C'est trait� dans un autre HOWTO&nbsp;!

</article>
