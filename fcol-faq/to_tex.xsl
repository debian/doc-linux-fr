<?xml version="1.0" encoding="iso-8859-1"?>

<!--
This is part of the fr.comp.os.linux.* FAQ.
Copyright (c) 1997-2002 Les utilisateurs de fr.comp.os.linux.*
See the file COPYING for copying conditions.
-->

<!-- *******************************************************************
     Convertit la FAQ XML Docbk du groupe fr.comp.os.linux.* pour LaTeX
     alexandre.gilardoni@wanadoo.fr (01-2001) 
     Modifi� par Arnaud Gomes-do-Vale <arnaud@carrosse.frmug.org> (2002)
     ******************************************************************* -->


<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns="http://www.tug.org">

 <xsl:output method="text" indent="no" encoding="iso-8859-1"/>

 <xsl:strip-space elements="*"/>

 <xsl:template match="article">
  \documentclass[a4paper,11pt]{article}
  \usepackage[frenchb]{babel}
  \usepackage[T1]{fontenc}
  \usepackage{times}
  \usepackage[body={18cm,25cm}]{geometry}
  \usepackage{relsize}
  \usepackage{url}
  \usepackage{moreverb}
  \usepackage{fancyhdr}

  \pagestyle{fancy}

  \begin{document}
  \begin{sloppypar}
  <xsl:apply-templates/>
  \end{sloppypar}
  \end{document}
 </xsl:template>

 <!-- Titre et table des mati�res-->
 <xsl:template match="articleinfo">
  \author{<xsl:for-each select="author">
   <xsl:value-of select="firstname"/>
   (<xsl:value-of select="contrib"/>)
   <xsl:if test="not(position()=last())">\\</xsl:if>
  </xsl:for-each>}
  <xsl:apply-templates/>
  \maketitle
  \tableofcontents
  \newpage

  Ce document est copyright
  <xsl:value-of select="copyright/year"/>{ }
  <xsl:value-of select="copyright/holder"/>.

 </xsl:template>

 <xsl:template match="articleinfo/title">
  \title{<xsl:apply-templates/>}
 </xsl:template>

 <xsl:template match="articleinfo/titleabbrev"></xsl:template>
 <xsl:template match="articleinfo//author"></xsl:template>

 <xsl:template match="articleinfo/date">
  \date{<xsl:apply-templates/>}
 </xsl:template>

 <xsl:template match="articleinfo/copyright">
 </xsl:template>

 <xsl:template match="articleinfo/abstract">
  \begin{abstract}<xsl:apply-templates/>
  \end{abstract}
 </xsl:template>

 <!-- Sections -->

 <xsl:template match="sect1/title">
  <xsl:text>
   \section{</xsl:text><xsl:apply-templates/>}<xsl:choose><xsl:when test="ancestor::sect1[@id]">\label{<xsl:value-of select="ancestor::sect1/@id"/>}</xsl:when></xsl:choose></xsl:template>

 <xsl:template match="sect2/title">
  <xsl:text>
   \subsection{</xsl:text><xsl:apply-templates/>}<xsl:choose><xsl:when test="ancestor::sect2[@id]">\label{<xsl:value-of select="ancestor::sect2/@id"/>}</xsl:when></xsl:choose></xsl:template>

 <xsl:template match="sect3/title">
  <xsl:text>
   \subsubsection{</xsl:text><xsl:apply-templates/>}</xsl:template>

 <!-- Caract�res -->

 <xsl:template match="emphasis">\emph{<xsl:apply-templates/>}</xsl:template>

 <xsl:template match="literal">
  <xsl:variable name="remap" select="@remap"/>
  <xsl:choose>
   <xsl:when test="$remap='newsgroup'">\url{<xsl:apply-templates/>}</xsl:when>
   <xsl:otherwise>\texttt{<xsl:apply-templates/>}</xsl:otherwise>
  </xsl:choose>
 </xsl:template>

 <!-- Paragraphes -->

 <xsl:template match="para">
  \par<xsl:text> </xsl:text><xsl:apply-templates/>
 </xsl:template>

 <xsl:template match="literallayout"><xsl:apply-templates/><xsl:text>
  </xsl:text>
 </xsl:template>

 <!-- Listes -->

 <xsl:template match="glosslist">
  \begin{description}<xsl:apply-templates/>
  \end{description}</xsl:template>
 
 <xsl:template match="glossterm">\item[<xsl:apply-templates/>]~\vspace{.1cm}
 </xsl:template>

 <xsl:template match="itemizedlist">
  \begin{itemize}<xsl:apply-templates/>
  \end{itemize}</xsl:template>

 <xsl:template match="itemizedlist/listitem">
  \item<xsl:choose><xsl:when test="ancestor::itemizedlist/listitem/itemizedlist/listitem">[-]</xsl:when><xsl:otherwise>[\textbullet]</xsl:otherwise></xsl:choose><xsl:text> </xsl:text><xsl:apply-templates/></xsl:template>

 <xsl:template match="glossdef/itemizedlist/listitem">
  \item[\textbullet]<xsl:apply-templates/>
 </xsl:template>

 <xsl:template match="sect2/itemizedlist/listitem">
  \item[\textbullet]<xsl:apply-templates/>
 </xsl:template>

 <xsl:template match="orderedlist">
  \begin{enumerate}<xsl:apply-templates/>
  \end{enumerate}</xsl:template>

 <xsl:template match="orderedlist/listitem">
  \item<xsl:apply-templates/></xsl:template>

 <!-- DocBookeries diverses -->

 <xsl:template match="ulink"><xsl:apply-templates/> \url{(<xsl:value-of select="@url"/>)} </xsl:template>

 <xsl:template match="screen">
  \begin{verbatimtab}<xsl:apply-templates/>
  \end{verbatimtab}
 </xsl:template>

 <xsl:template match="quote">
  \guillemotleft\ <xsl:apply-templates/>\ \guillemotright
 </xsl:template>

 <xsl:template match="warning">
  \fbox{
  \begin{minipage}{16.8cm}
  \begin{center}
  \small\textbf{Attention :}
  \end{center}
  \small<xsl:text> </xsl:text><xsl:apply-templates/>
  \end{minipage}}
 </xsl:template>

 <xsl:template match="xref">\ref{<xsl:value-of select="@linkend"/>}</xsl:template>

 <xsl:template match="keycombo">\textbf{<xsl:for-each select="./*"><xsl:if test="position()>1"><xsl:text>-</xsl:text></xsl:if><xsl:apply-templates/></xsl:for-each>}</xsl:template>

 <xsl:template match="keycap">\textbf{<xsl:apply-templates/>}</xsl:template>

 <xsl:template match="option">\texttt{<xsl:apply-templates/>}</xsl:template>

 <xsl:template match="parameter">\texttt{<xsl:apply-templates/>}</xsl:template>

 <xsl:template match="command">\textbf{<xsl:apply-templates/>}</xsl:template>

 <xsl:template match="citation">[<xsl:apply-templates/>]</xsl:template>

 <xsl:template match="citetitle">\textit{<xsl:apply-templates/>}</xsl:template>

 <xsl:template match="computeroutput">\guillemotleft\ \texttt{<xsl:apply-templates/>}\ \guillemotright </xsl:template>

 <xsl:template match="productname">\guillemotleft\ <xsl:apply-templates/>\ \guillemotright </xsl:template>

 <xsl:template match="application">\guillemotleft\ <xsl:apply-templates/>\ \guillemotright </xsl:template>

 <xsl:template match="systemitem">
  <xsl:variable name="class" select="@class"/>
  <xsl:choose>
   <xsl:when test="$class='filesystem'"><xsl:apply-templates/></xsl:when>
   <xsl:otherwise>\texttt{<xsl:apply-templates/>}</xsl:otherwise>
  </xsl:choose>
 </xsl:template>

 <xsl:template match="filename">\texttt{<xsl:apply-templates/>}</xsl:template>

 <xsl:template match="varname">\texttt{<xsl:apply-templates/>}</xsl:template>

 <xsl:template match="constant">\texttt{<xsl:apply-templates/>}</xsl:template>

 <xsl:template match="acronym">\textit{<xsl:apply-templates/>}</xsl:template>

</xsl:stylesheet>
