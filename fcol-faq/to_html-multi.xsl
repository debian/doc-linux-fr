<?xml version='1.0'?>

<!--
This is part of the fr.comp.os.linux.* FAQ.
Copyright (c) 1997-2001 Les utilisateurs de fr.comp.os.linux.*
See the file COPYING for copying conditions.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version='1.0'>

<xsl:output method="html" encoding="ISO-8859-1"/>

<xsl:include
     href="/usr/local/share/sgml/stylesheets/xsl/docbook/html/xtchunk.xsl"/>

</xsl:stylesheet>
