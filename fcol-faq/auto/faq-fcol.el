(TeX-add-style-hook "faq-fcol"
 (function
  (lambda ()
    (LaTeX-add-labels
     "x86mini"
     "kernparam"
     "bootsec"
     "mountfstab"
     "connex")
    (TeX-run-style-hooks
     "fancyhdr"
     "moreverb"
     "url"
     "relsize"
     "geometry"
     "body={18cm"
     "25cm}"
     "times"
     "fontenc"
     "T1"
     "babel"
     "frenchb"
     "latex2e"
     "art11"
     "article"
     "a4paper"
     "11pt"))))

