<?xml version="1.0" encoding="iso-8859-1"?>

<!--
This is part of the fr.comp.os.linux.* FAQ.
Copyright (c) 1997-2001 Les utilisateurs de fr.comp.os.linux.*
See the file COPYING for copying conditions.
-->

<!-- *******************************************************************
     G�n�re la table des mati�res en html de la FAQ du groupe
     fr.comp.os.linux.* (alexandre.gilardoni@wanadoo.fr) 01.2001          
     ******************************************************************* -->

<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:html="http://www.w3.org/TR/REC-html40">

<xsl:output method="html"/>

<xsl:template match="article">
  <html>
    <head>
      <title>Questions trait�es dans la <xsl:value-of
      select="articleinfo/title"/> <xsl:value-of
      select="articleinfo/pubdate"/></title>
    </head>
    <body bgcolor="#ffffff">
      <h2>Questions trait�es dans la <xsl:value-of
      select="articleinfo/title"/> (<xsl:value-of
      select="articleinfo/pubdate"/>)</h2>
      <table width="50%" border="1"><tr><td align="center">
      <b>Table des mati�res</b></td></tr>
      </table>
        <xsl:for-each select="sect1|sect1/sect2">
          <br>
          <xsl:if test="name() = 'sect2'">&#160;&#160;&#160;&#160;</xsl:if>
          <xsl:if test="name() = 'sect1'"><br>
          </br></xsl:if>
          <xsl:number level="multiple" count="sect1|sect2" format="1.1. "/>   
          <xsl:value-of select="title"/>
          </br>  
        </xsl:for-each>
      <br></br>
      <br><b>La FAQ est disponible � l'url :</b></br>
      <br>&#160;&#160;&#160;&#160;
      <a href="http://www.linux-france.org/article/fcol-faq/">
      http://www.linux-france.org/article/fcol-faq/</a></br>
    </body>
  </html>
</xsl:template>

</xsl:stylesheet>
