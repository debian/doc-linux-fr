#!/bin/bash

[ -z "$1" ] && exit 1

GUIDE_PRATIQUE="$1"

XSLTPROC='/usr/bin/xsltproc --nonet'

DOC="non"
[ -f $GUIDE_PRATIQUE.sgml ] && DOC="$GUIDE_PRATIQUE.sgml"
[ -f $GUIDE_PRATIQUE.xml ] && DOC="$GUIDE_PRATIQUE.xml"
[ "$DOC" == "non" ] && { echo "Pas de document !"; exit 1 ; }

# Détermination du type de documents
grep -i '<!doctype' $DOC | grep -i "linuxdoc" > /dev/null && TYPE_DOC='linuxdoc'
grep -i '<!doctype' $DOC | grep -i "DocBook"  > /dev/null && TYPE_DOC='docbook-sgml'
grep -i '<!doctype' $DOC | grep -i "DocBook.*XML"  > /dev/null && TYPE_DOC='docbook-xml'

SERVEUR="traduc@traduc.org"
ARCHIVES="/ftp/pub/traduc.org/doc-vf/HOWTO/telechargement"
ARCHIVE_CVS="/www/traduc.org/sources"
DESTINATION="$SERVEUR:$ARCHIVES"
AFFICHAGE="$SERVEUR:/www/traduc.org/html/docs/HOWTO/"
SOURCES="$SERVEUR:$ARCHIVE_CVS"
NOUVEAUTES="/ftp/pub/traduc.org/doc-vf/HOWTO/nouveaux"
RELECTURE="/ftp/pub/traduc.org/projets/howto/relecture/"

OPTIONS_SCP="-B"

LOCAL_PATH="$(dirname $0)"

unset MINI

if [ $TYPE_DOC = 'docbook-xml' ] ; then

  # Récupération des informations dans le document lui-même

  TITRE=`$XSLTPROC "$LOCAL_PATH/titre.xsl" $DOC 2> /dev/null | tr -s [[:space:]] " " | sed -e "s/^[[:space:]]*//" -e "s/[[:space:]]*$//"`
  TITRE_VO=`$XSLTPROC "$LOCAL_PATH/titre-vo.xsl" $DOC 2> /dev/null | tr -s [[:space:]] " "  | sed -e "s/^[[:space:]]*//" -e "s/[[:space:]]*$//"`
  VERSION=`$XSLTPROC "$LOCAL_PATH/version.xsl" $DOC 2> /dev/null | tr -s [[:space:]] " " | sed -e "s/Version.*: //" | sed -e "s/^[[:space:]]*//" -e "s/[[:space:]]*$//"`
  VERSION_VO=`echo $VERSION | sed -e "s/\.fr\..*//" | sed -e "s/^[[:space:]]*//" -e "s/[[:space:]]*$//"`
  DATE=`$XSLTPROC "$LOCAL_PATH/date.xsl" $DOC 2> /dev/null | tr -s [[:space:]] " " | sed -e "s/^[[:space:]]*//" -e "s/[[:space:]]*$//"`
  DATE_VO=`$XSLTPROC -stringparam "version.vo" "$VERSION_VO" "$LOCAL_PATH/date-vo.xsl" $DOC 2> /dev/null | tr -s [[:space:]] " " | sed -e "s/^[[:space:]]*//" -e "s/[[:space:]]*$//"`
  RESUME=`$XSLTPROC "$LOCAL_PATH/resume.xsl" $DOC 2> /dev/null | tr -s [[:space:]] " " | sed -e "s/^[[:space:]]*//" -e "s/[[:space:]]*$//"`
  AUTEUR=`$XSLTPROC "$LOCAL_PATH/auteur.xsl" $DOC 2> /dev/null | tr -s [[:space:]] " " | sed -e "s/^[[:space:]]*//" -e "s/[[:space:]]*$//"`
  AUTEUR_ADR=`$XSLTPROC "$LOCAL_PATH/auteur-adr-el.xsl" $DOC 2> /dev/null | tr -s [[:space:]] " " | sed -e "s/ CHEZ /@/g" -e "s/ POINT /./g" -e "s/ TIRET /-/g" | sed -e "s/^[[:space:]]*//" -e "s/[[:space:]]*$//"`
  TRADUCTEUR=`$XSLTPROC "$LOCAL_PATH/traducteur.xsl" $DOC 2> /dev/null | tr -s [[:space:]] " " | sed -e "s/^[[:space:]]*//" -e "s/[[:space:]]*$//"`
  TRADUCTEUR_ADR=`$XSLTPROC "$LOCAL_PATH/traducteur-adr-el.xsl" $DOC 2> /dev/null | tr -s [[:space:]] " " | sed -e "s/ CHEZ /@/g" -e "s/ POINT /./g" -e "s/ TIRET /-/g" | sed -e "s/^[[:space:]]*//" -e "s/[[:space:]]*$//"`
  RELECTEUR=`$XSLTPROC "$LOCAL_PATH/relecteur.xsl" $DOC 2> /dev/null | tr -s [[:space:]] " " | sed -e "s/^[[:space:]]*//" -e "s/[[:space:]]*$//"`
  RELECTEUR_ADR=`$XSLTPROC "$LOCAL_PATH/relecteur-adr-el.xsl" $DOC 2> /dev/null | tr -s [[:space:]] " " | sed -e "s/ CHEZ /@/g" -e "s/ POINT /./g" -e "s/ TIRET /-/g" | sed -e "s/^[[:space:]]*//" -e "s/[[:space:]]*$//"`
  [ "$VERSION" = "$VERSION_VO" ] && ORIGINAL_EN_VF="oui" || ORIGINAL_EN_VF="non"
  
  # Mise entre guillemets des valeurs
  
  if [ -z "$TITRE" ] ; then TITRE=NULL ; else TITRE="'$TITRE'" ; fi
  if [ -z "$TITRE_VO" ] ; then TITRE_VO=NULL ; else TITRE_VO="'$TITRE_VO'" ; fi
  if [ -z "$VERSION" ] ; then VERSION=NULL ; else VERSION="'$VERSION'" ; fi
  if [ -z "$VERSION_VO" ] ; then VERSION_VO=NULL ; else VERSION_VO="'$VERSION_VO'" ; fi
  if [ -z "$DATE" ] ; then DATE=NULL ; else DATE="'$DATE'" ; fi
  if [ -z "$DATE_VO" ] ; then DATE_VO=NULL ; else DATE_VO="'$DATE_VO'" ; fi
  if [ -z "$RESUME" ] ; then RESUME=NULL ; else RESUME="\\\"$RESUME\\\"" ; fi
  if [ -z "$AUTEUR" -o "$AUTEUR" = " " ] ; then AUTEUR=NULL ; else AUTEUR="'$AUTEUR'" ; fi
  if [ -z "$AUTEUR_ADR" ] ; then AUTEUR_ADR=NULL ; else AUTEUR_ADR="'$AUTEUR_ADR'" ; fi
  if [ -z "$TRADUCTEUR" -o "$TRADUCTEUR" = " " ] ; then TRADUCTEUR=NULL ; else TRADUCTEUR="'$TRADUCTEUR'" ; fi
  if [ -z "$TRADUCTEUR_ADR" ] ; then TRADUCTEUR_ADR=NULL ; else TRADUCTEUR_ADR="'$TRADUCTEUR_ADR'" ; fi
  if [ -z "$RELECTEUR" -o "$RELECTEUR" = " " ] ; then RELECTEUR=NULL ; else RELECTEUR="'$RELECTEUR'" ; fi
  if [ -z "$RELECTEUR_ADR" ] ; then RELECTEUR_ADR=NULL ; else RELECTEUR_ADR="'$RELECTEUR_ADR'" ; fi
  ORIGINAL_EN_VF="'$ORIGINAL_EN_VF'"

  # Validation des informations
  
  echo "Titre :			$TITRE"
  echo "Titre original :	$TITRE_VO"
  echo "Version :		$VERSION"
  echo "Version v.o. :		$VERSION_VO"
  echo "Date :			$DATE"
  echo "Date v.o. :		$DATE_VO"
  echo "Auteur :		$AUTEUR"
  echo "Adr. él. auteur :	$AUTEUR_ADR"
  echo "Traducteur :		$TRADUCTEUR"
  echo "Adr. él. traducteur :	$TRADUCTEUR_ADR"
  echo "Relecteur :		$RELECTEUR"
  echo "Adr. él. relecteur :	$RELECTEUR_ADR"
  echo "Original en v.f. :	$ORIGINAL_EN_VF"
  echo "Résumé :		$RESUME"

  echo

fi
