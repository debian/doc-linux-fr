#!/bin/bash

[ -z "$1" ] && exit 1

SSH="/usr/bin/ssh"

GUIDE_PRATIQUE="$1"

# Définition de l'emplacement des catalogues SGML

#XSLTPROC='xsltproc --catalogs --nonet --novalid'
XSLTPROC='xsltproc --nonet'

DOC="non"
[ -f "$GUIDE_PRATIQUE.sgml" ] && DOC="$GUIDE_PRATIQUE.sgml"
[ -f "$GUIDE_PRATIQUE.xml" ] && DOC="$GUIDE_PRATIQUE.xml"
[ "$DOC" == "non" ] && { echo "Pas de document !"; exit 1 ; }

# Détermination du type de documents
grep -i '<!doctype' $DOC | grep -i "linuxdoc" > /dev/null && TYPE_DOC='linuxdoc'
grep -i '<!doctype' $DOC | grep -i "DocBook"  > /dev/null && TYPE_DOC='docbook-sgml'
grep -i '<!doctype' $DOC | grep -i "DocBook.*XML"  > /dev/null && TYPE_DOC='docbook-xml'

COMPTE="mon_compte"
SERVEUR="${COMPTE}@traduc.org"
ARCHIVES="/home/ftp/pub/traduc.org/doc-vf/HOWTO/telechargement"
DESTINATION="$SERVEUR:$ARCHIVES"
AFFICHAGE="$SERVEUR:/home/traduc.org/www/guidespratiques.traduc.org/www/docs/HOWTO"
NOUVEAUTES="/home/ftp/pub/traduc.org/doc-vf/HOWTO/nouveaux"
RELECTURE="/home/ftp/pub/traduc.org/projets/howto/relecture"

OPTIONS_SCP="-B -p"

# Vérification des droits

chmod a+r "$GUIDE_PRATIQUE"*

# Nettoyage du serveur ftp

"${SSH}" "$SERVEUR" "set -e ; cd \"$ARCHIVES\" && rm -fv \"ps/$1.ps\" \"ps/$1-ps.tar.gz\" \"pdf/$1.pdf\" \"pdf/$1-pdf.tar.gz\" \"text/$1\" \"text/$1-text.tar.gz\" \"text/$1.txt\" \"html/$1-html.tar.gz\" \"html-1page/$1-html1page.tar.gz\" \"html-1page/$1.html\" \"source/$1.sgml\" \"source/$1.xml\" \"source/$1-sgml.tar.gz\" \"source/$1-xml.tar.gz\""
"${SSH}" "$SERVEUR" "set -e ; cd \"$RELECTURE\" && rm -fv \"$1.html\" \"$1.xml\" \"$1.sgml\" \"$1.txt\""

# Expédition vers le serveur ftp

[ -f "$1.ps" ] && scp $OPTIONS_SCP "$1.ps" "$DESTINATION/ps/"
[ -f "$1-ps.tar.gz" ] && scp $OPTIONS_SCP "$1-ps.tar.gz" "$DESTINATION/ps/"

[ -f "$1.pdf" ] && scp $OPTIONS_SCP "$1.pdf" "$DESTINATION/pdf/"
[ -f "$1-pdf.tar.gz" ] && scp $OPTIONS_SCP "$1-pdf.tar.gz" "$DESTINATION/pdf/"

[ -f "$1" ] && scp $OPTIONS_SCP "$1" "$DESTINATION/text/"
[ -f "$1.txt" ] && scp $OPTIONS_SCP "$1.txt" "$DESTINATION/text/"
[ -f "$1-text.tar.gz" ] && scp $OPTIONS_SCP "$1-text.tar.gz" "$DESTINATION/text/"

[ -f "$1-html.tar.gz" ] && scp $OPTIONS_SCP "$1-html.tar.gz" "$DESTINATION/html/"

# Expédition du fichier source

[ -f "$1.sgml" ] && scp $OPTIONS_SCP "$1.sgml" "$DESTINATION/source/"
[ -f "$1-sgml.tar.gz" ] && scp $OPTIONS_SCP "$1-sgml.tar.gz" "$DESTINATION/source/"

if [ -f "$1-xml.tar.gz" ] ; then

  scp $OPTIONS_SCP "$1-xml.tar.gz" "$DESTINATION/source/"

elif [ -f "$1.xml" ] ; then

  scp $OPTIONS_SCP "$1.xml" "$DESTINATION/source/"
  
fi

# Envoi vers le serveur http

[ -f "$1.html" ]   && scp $OPTIONS_SCP "$1.html"      "$DESTINATION/html-1page/"
[ -d "outils/$1" ] && scp $OPTIONS_SCP -r "outils/$1" "$DESTINATION/html-1page/outils/"
[ -d "images/$1" ] && scp $OPTIONS_SCP -r "images/$1" "$DESTINATION/html-1page/images/"
[ -f "$1-html1page.tar.gz" ] && scp $OPTIONS_SCP "$1-html1page.tar.gz" "$DESTINATION/html-1page/"

[ -f "$1.html" ]   && scp $OPTIONS_SCP    "$1.html"   "$AFFICHAGE/vf/"
[ -d "outils/$1" ] && scp $OPTIONS_SCP -r "outils/$1" "$AFFICHAGE/vf/outils/"
[ -d "images/$1" ] && scp $OPTIONS_SCP -r "images/$1" "$AFFICHAGE/vf/images/"

# Création du dossier dans Nouveaux

"${SSH}" "$SERVEUR" /home/traduc.org/www/guidespratiques.traduc.org/travail/scripts/mise-a-jour.sh

