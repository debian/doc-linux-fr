#!/bin/sh

set -e

HTML=$1
TEXTE=$2
TEMPORAIRE=`mktemp`
TEMPORAIRE2=`mktemp`

cat "$HTML" | tr '\n' '\b' | perl -pe '
use locale ;
use utf8 ;
use encoding "utf8" ;
s/<a\s[^>]*?href="(http[^">]*)"[^>]*>(.*?)<\/a>/$2 \[$1\]/gi ;
s/<a\s[^>]*?href="mailto[^">]*"[^>]*>(.*?)<\/a>/$1/gi ; 
s/<a\s[^>]*?href="#[^">]*"[^>]*>(.*?)<\/a>/$1/gi ;
tr/\x{02423}/\_/' | \
tr '\b' '\n' > "$TEMPORAIRE"

set +e

tidy -m -utf8 "$TEMPORAIRE"

set -e

sed -e 's/<\/body>/<p>JvaBa62BQHa0D2alAFJVPymVFOeX4EeA<\/p><\/body>/' "$TEMPORAIRE" > "$TEMPORAIRE2"

elinks -dump-charset UTF-8 -dump "$TEMPORAIRE2" > "$TEMPORAIRE"

awk -- '/JvaBa62BQHa0D2alAFJVPymVFOeX4EeA/ {exit}
{ print }' "$TEMPORAIRE" > "$TEXTE"

rm "$TEMPORAIRE" "$TEMPORAIRE2"

