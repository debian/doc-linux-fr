#!/bin/bash

# Détermination du type de documents
grep -i '<!doctype' $1 | grep -i "linuxdoc" > /dev/null && TYPE_DOC='linuxdoc'
grep -i '<!doctype' $1 | grep -i "DocBook"  > /dev/null && TYPE_DOC='docbook-sgml'
grep -i '<!doctype' $1 | grep -i "DocBook.*XML"  > /dev/null && TYPE_DOC='docbook-xml'

# Détermination du chemin du DSSSL LDP

[ -f ldp.dsl ] && DSSSL_LDP="ldp.dsl" || DSSSL_LDP="/usr/share/sgml/docbook/stylesheet/dsssl/ldp/ldp.dsl"

# Détermination du chemin du XSL LDP

XSL_LDP_MONOHTML="/usr/share/xml/docbook/stylesheet/ldp/ldp-html.xsl"
FEUILLE_DE_STYLE_XSL_HTML="/usr/share/xml/docbook/stylesheet/nwalsh/xhtml/chunk.xsl"
FEUILLE_DE_STYLE_XSL_FO="/usr/share/xml/docbook/stylesheet/nwalsh/fo/docbook.xsl"

# Arguments LinuxDoc standards

LINUXDOC="-l fr -p a4"

# Arguments htmldoc

HTMLDOC_PDF="--charset 8859-15 --color --continuous --duplex -t pdf14 \
             --linkstyle plain --no-title --no-toc --pagemode document \
             --size a4"

HTMLDOC_PS="--charset 8859-15 --color --continuous --duplex \
            --no-title --no-toc --size a4"

# Utilisation du style LDP

STYLE="ldp"

# Utilisation de tidy
CLEAN="tidy -m"

DB_JW_SGML_HTML=" -d \"$DSSSL_LDP#html\"" ;
DB_JW_SGML_PRINT="-d \"$DSSSL_LDP#print\"" ;
DB_SGMLTOOLS_SGML_HTML=" -s \"$DSSSL_LDP#html\"" ;
DB_SGMLTOOLS_SGML_PRINT="-s \"$DSSSL_LDP#print\"" ;
DB_XML_MONOHTML="-x \"$XSL_LDP_MONOHTML\"" ;
DB_XML_HTML="    -x \"$FEUILLE_DE_STYLE_XSL_HTML\"" ;
DB_XML_PRINT="   -x \"$FEUILLE_DE_STYLE_XSL_FO\"" ;

OPTIONS_FOP="-l fr"

LOCAL_PATH="$(dirname $0)"

# Traitement

case $TYPE_DOC in

	linuxdoc)	DOCUMENT="${1%.sgml}"
			SOURCE="$1"

			[ "$DOCUMENT" != "$SOURCE" -a -d "$DOCUMENT" ] && rm -rf "$DOCUMENT"
			rm -f "$DOCUMENT-html.tar.gz"
			rm -f "$DOCUMENT.dvi"  "$DOCUMENT.dvi.gz"  "$DOCUMENT-dvi.tar.gz"
			rm -f "$DOCUMENT.ps"   "$DOCUMENT.ps.gz"   "$DOCUMENT-ps.tar.gz"
			rm -f "$DOCUMENT.pdf"  "$DOCUMENT.pdf.gz"  "$DOCUMENT-pdf.tar.gz"
			rm -f "$DOCUMENT.html" "$DOCUMENT.html.gz" "$DOCUMENT-html1page.tar.gz"

			echo "Production de la version html monobloc"
			"$LOCAL_PATH/hypertoile-unepage-traduc.org.sh" "$SOURCE"

			echo "Production de la version html multibloc"
			mkdir "$DOCUMENT"
			cd "$DOCUMENT"
			linuxdoc $LINUXDOC -B html -T 2 "../$SOURCE"
			for i in *.html ; do
				$CLEAN "$i" ;
			done
			cd ..

			echo "Production de la version texte"
			elinks -dump-charset UTF-8 -dump "$DOCUMENT.html" > "$DOCUMENT.txt"

			echo "Production de la version PostScript"
			html2ps -e ISO-8859-1 -H -D -R -U "$DOCUMENT.html" > "$DOCUMENT.ps"

			echo "Production de la version PDF"
                        ps2pdf "$DOCUMENT.ps" "$DOCUMENT.pdf"
			;;

	docbook-sgml)	DOCUMENT="${1%.sgml}"
			SOURCE="$1"

			[ "$DOCUMENT" != "$SOURCE" -a -d "$DOCUMENT" ] && rm -rf "$DOCUMENT"
			rm -f "$DOCUMENT-html.tar.gz"
			rm -f "$DOCUMENT.dvi"  "$DOCUMENT.dvi.gz"  "$DOCUMENT-dvi.tar.gz"
			rm -f "$DOCUMENT.ps"   "$DOCUMENT.ps.gz"   "$DOCUMENT-ps.tar.gz"
			rm -f "$DOCUMENT.pdf"  "$DOCUMENT.pdf.gz"  "$DOCUMENT-pdf.tar.gz"
			rm -f "$DOCUMENT.html" "$DOCUMENT.html.gz" "$DOCUMENT-html1page.tar.gz"

			echo "Production de la version html monobloc"
			"$LOCAL_PATH/hypertoile-unepage-traduc.org.sh" "$SOURCE"

			echo "Production de la version html multibloc"
			mkdir "$DOCUMENT"
			docbook2html -o "$DOCUMENT" "$SOURCE"

			echo "Production de la version texte"
			elinks -dump-charset UTF-8 -no-references -dump "$DOCUMENT.html" > "$DOCUMENT.txt"

			echo "Production de la version PostScript"
			htmldoc $HTMLDOC_PS  -f "$DOCUMENT.ps"  "$DOCUMENT.html"

			echo "Production de la version PDF"
			htmldoc $HTMLDOC_PDF -f "$DOCUMENT.pdf" "$DOCUMENT.html"
			;;

	docbook-xml)	DOCUMENT="${1%.xml}"
			SOURCE="$1"

			[ "$DOCUMENT" != "$SOURCE" -a -d "$DOCUMENT" ] && rm -rf "$DOCUMENT"
			rm -f "$DOCUMENT-html.tar.gz"
			rm -f "$DOCUMENT.dvi"  "$DOCUMENT.dvi.gz"  "$DOCUMENT-dvi.tar.gz"
			rm -f "$DOCUMENT.ps"   "$DOCUMENT.ps.gz"   "$DOCUMENT-ps.tar.gz"
			rm -f "$DOCUMENT.pdf"  "$DOCUMENT.pdf.gz"  "$DOCUMENT-pdf.tar.gz"
			rm -f "$DOCUMENT.html" "$DOCUMENT.html.gz" "$DOCUMENT-html1page.tar.gz"
			
			echo "Production de la version html monobloc"
			"$LOCAL_PATH/hypertoile-unepage-traduc.org.sh" "$SOURCE"

			echo "Production de la version html multibloc"
			mkdir "$DOCUMENT"
                        java -classpath /usr/share/java/saxon.jar:/usr/share/java/docbook-xsl-saxon.jar:/usr/share/java/xercesImpl.jar com.icl.saxon.StyleSheet \
                          "${SOURCE}" \
                          "${FEUILLE_DE_STYLE_XSL_HTML}" \
                          "base.dir"="$DOCUMENT/" \
                          "chunker.output.encoding"="UTF-8" \
                          "admon.graphics"="1" \
                          "use.extensions"="1" \
                          "callouts.extensions"="1" \
                          "html.stylesheet"="style.css" \
                          "css.decoration"="0" \
                          "blurb.on.titlepage.enabled"="0" \
                          "contrib.inline.enabled"="1" \
                          "othercredit.like.author.enabled"="1" \
                          "make.valid.html"="1" \
                          "make.year.ranges"="1" \
                          "html.cleanup"="1" \
                          "section.autolabel"="1"


			echo "Production de la version texte"
			"$LOCAL_PATH/html-en-texte.sh" "$DOCUMENT.html" "$DOCUMENT.txt"

                        echo "Production de la version FO"
                        java -classpath /usr/share/java/saxon.jar:/usr/share/java/docbook-xsl-saxon.jar:/usr/share/java/xercesImpl.jar com.icl.saxon.StyleSheet \
                          -o "${DOCUMENT}.fo" \
                          "${SOURCE}" \
                          "${FEUILLE_DE_STYLE_XSL_FO}" \
                          "admon.graphics"="0" \
                          "use.extensions"="1" \
                          "callouts.extensions"="1" \
                          "blurb.on.titlepage.enabled"="0" \
                          "contrib.inline.enabled"="1" \
                          "othercredit.like.author.enabled"="1" \
                          "make.year.ranges"="1" \
                          "section.autolabel"="1" \
                          "fop1.extensions"="1"

                        echo "Production de la version PDF"

                        fop -fo "${DOCUMENT}.fo" -pdf "${DOCUMENT}.pdf"

                        echo "Production de la version PostScript"

                        fop -fo "${DOCUMENT}.fo" -ps "${DOCUMENT}.ps"

			;;

	*)		echo "Erreur : type de document inconnu"
			exit 1
			;;

esac

exit 0
