#!/bin/bash

# Définition de l'emplacement des catalogues SGML

export SGML_CATALOG_FILES=/etc/sgml/catalog

# Détermination du type de documents
grep -i '<!doctype' $1 | grep -i "linuxdoc" > /dev/null && TYPE_DOC='linuxdoc'
grep -i '<!doctype' $1 | grep -i "DocBook"  > /dev/null && TYPE_DOC='docbook-sgml'
grep -i '<!doctype' $1 | grep -i "DocBook.*XML"  > /dev/null && TYPE_DOC='docbook-xml'

echo $TYPE_DOC

# Détermination du chemin du DSSSL LDP

[ -f ldp.dsl ] && DSSSL_LDP=ldp.dsl || DSSSL_LDP="/usr/share/sgml/docbook/stylesheet/dsssl/ldp/ldp.dsl"

# Détermination du chemin du XSL LDP

FEUILLE_DE_STYLE_XSL="/usr/share/xml/docbook/stylesheet/nwalsh/xhtml/docbook.xsl"

# Chemin vers la feuille de style modulaire

DSSSL_MODULAR_HTML="/usr/share/sgml/docbook/stylesheet/dsssl/modular/html/docbook.dsl"

# Arguments LinuxDoc standards

LINUXDOC="-c latin -l fr -p a4"

DESC_STYLE="<style type=\"text/css\">
    div.ABSTRACT { margin: 5mm ;
                   background: #DDDDDD ;
                   padding: 3mm }
    :link IMG { border: none }
    :visited IMG {  border: none }
    :active IMG { border: none }
</style>"

STYLE=`echo "$DESC_STYLE"         | \
       sed -e "s#/#\\\\\/#g"      | \
       sed -e "s#\"#\\\\\\\\\"#g" | \
       tr "\n" "§"                | \
       sed -e "s#§#\\\\\n#g"`
SIGNATURE="validator.w3.org"
ANCRE='<\/head'

CLEAN="tidy -m"

DB_JW_SGML_HTML="-d $DSSSL_MODULAR_HTML"
DB_SGMLTOOLS_SGML_HTML="-s $DSSSL_MODULAR_HTML"
unset DB_XML_MONOHTML

OPTIONS_XSLTPROC_HTML='--stringparam "admon.graphics" "1" "chunker.output.indent" "yes" "admon.graphics.extension" ".png"'

LOCAL_PATH="$(dirname $0)"

# Traitement

DOCUMENT=`echo "$1" | sed -e "s/\.\([xX]\|[sS][gG]\)[mM][lL]$//"`
SOURCE="$1"

case $TYPE_DOC in

	linuxdoc)	echo "Production de la version html monobloc"
			linuxdoc $LINUXDOC -B html -s 0 "${SOURCE%.sgml}"
			$CLEAN "$DOCUMENT.html"

			;;

	docbook-sgml)	TEMPORAIRE=`mktemp`

			echo "Production de la version html monobloc"
			docbook2html -u $DB_JW_SGML_HTML "$SOURCE"
			sed -e "s/$ANCRE/$STYLE$ANCRE/1M" "$DOCUMENT.html" > "$TEMPORAIRE"
			mv "$TEMPORAIRE" "$DOCUMENT.html"
			$CLEAN "$DOCUMENT.html"
			;;

	docbook-xml)	echo "Validation"
			FICHIER_TEMPORAIRE=`mktemp`
			xmllint --valid --noout --nonet "$SOURCE" || exit 1
			
			echo "Vérification des métainformations"
			"$LOCAL_PATH/validation-metainfo.sh" "$DOCUMENT"

			echo "Production de la version html monobloc"
                        java -classpath /usr/share/java/saxon.jar:/usr/share/java/docbook-xsl-saxon.jar:/usr/share/java/xercesImpl.jar com.icl.saxon.StyleSheet \
                          -o "${DOCUMENT}.html" \
                          "${SOURCE}" \
                          "${FEUILLE_DE_STYLE_XSL}" \
                          "admon.graphics"="1" \
                          "use.extensions"="1" \
                          "callouts.extensions"="1" \
                          "html.stylesheet"="style.css" \
                          "css.decoration"="0" \
                          "blurb.on.titlepage.enabled"="0" \
                          "contrib.inline.enabled"="1" \
                          "othercredit.like.author.enabled"="1" \
                          "make.valid.html"="1" \
                          "make.year.ranges"="1" \
                          "html.cleanup"="1" \
                          "section.autolabel"="1"
			;;

	*)		echo "Erreur : type de document inconnu"
			exit 1
			;;

esac

exit 0
