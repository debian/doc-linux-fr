<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="text" encoding="UTF-8"/>

<xsl:strip-space elements="*"/>

<xsl:template match="*">
<xsl:for-each select="//othercredit[@role='traduction']">

<xsl:value-of select="email"/>

<xsl:if test="boolean(following-sibling::othercredit[@role='traduction'][1]//email)">, </xsl:if>

</xsl:for-each>
</xsl:template>

</xsl:stylesheet>
