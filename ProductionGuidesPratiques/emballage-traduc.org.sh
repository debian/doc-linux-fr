#!/bin/bash -x

# Détermination du type de documents
grep -i '<!doctype' $1 | grep -i "linuxdoc" > /dev/null && TYPE_DOC='linuxdoc'
grep -i '<!doctype' $1 | grep -i "DocBook"  > /dev/null && TYPE_DOC='docbook-sgml'
grep -i '<!doctype' $1 | grep -i "DocBook.*XML"  > /dev/null && TYPE_DOC='docbook-xml'

# Paramétrage

case $TYPE_DOC in
	linuxdoc)	DOCUMENT="${1%.sgml}"
			SOURCE="$1"
			;;
	docbook-sgml)	DOCUMENT="${1%.sgml}"
			SOURCE="$1"
			;;
	docbook-xml)	DOCUMENT="${1%.xml}"
			SOURCE="$1"
			;;
	*)		echo "Erreur : type de document inconnu"
			exit 1
			;;
esac                                	

rm -f *.bak 

# Archivage de la version html multipages

[ -d "$DOCUMENT" ] && rm -f "$DOCUMENT"/*.bak && tar cvf "$DOCUMENT-html.tar" "$DOCUMENT"

# Supression de la version html multipage

rm -rf "$DOCUMENT"

gzip -v "$DOCUMENT"*.tar
