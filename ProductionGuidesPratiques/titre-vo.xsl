<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="text" encoding="UTF-8"/>

<xsl:strip-space elements="*"/>

<xsl:template match="*">
<xsl:value-of select="/article/articleinfo/subtitle/foreignphrase"/>
<xsl:value-of select="/book/bookinfo/subtitle/foreignphrase"/>
</xsl:template>

</xsl:stylesheet>
